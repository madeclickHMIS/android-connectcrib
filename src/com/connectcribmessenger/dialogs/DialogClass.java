package com.connectcribmessenger.dialogs;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.AbsListView.LayoutParams;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.entities.Item;
import com.connectcribmessenger.main.AccountActivationScreen;
import com.connectcribmessenger.main.R;
import com.connectcribmessenger.main.SplashScreen;
import com.connectcribmessenger.main.VerificationScreen;

public class DialogClass extends DialogFragment 
{

	private int mDialogCase;

	private AlertDialog.Builder mBuilder;




	/**
	 * Create a new instance of MyDialogFragment, providing "num"
	 * as an argument.
	 */
	public static DialogClass newInstance(int dialog_case) 
	{
		DialogClass dialogClass = new DialogClass();

		// Supply num input as an argument.
		Bundle args = new Bundle();

		args.putInt("dialog_case", dialog_case);

		dialogClass.setArguments(args);

		return dialogClass;
	}



	@Override

	public Dialog onCreateDialog(Bundle savedInstanceState) 
	{
		mDialogCase = getArguments().getInt("dialog_case");
		mBuilder = new AlertDialog.Builder(getActivity());
		switch (mDialogCase) 
		{
		case Constants.DIALOG_GCM_REGISTRATION_NO_INTERNET:
			// Use the Builder class for convenient dialog construction


			mBuilder.setMessage(R.string.dialog_internet_status_message)
			.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) 
				{
					SplashScreen.gcmRegistration();
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dismiss();
					getActivity().finish();
				}
			});
			// Create the AlertDialog object and return it
			return mBuilder.create();

		case Constants.DIALOG_GCM_REGISTRATION_FAILED:


			mBuilder.setMessage(R.string.server_issue)
			.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) 
				{
					SplashScreen.gcmRegistration();
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dismiss();
					getActivity().finish();
				}
			});
			// Create the AlertDialog object and return it
			return mBuilder.create();

		case Constants.DIALOG_GET_COUNTRY_CODE_NO_INTERNET:
			// Use the Builder class for convenient dialog construction


			mBuilder.setMessage(R.string.dialog_internet_status_message)
			.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) 
				{
					AccountActivationScreen.getCurrentCountryCode();
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dismiss();
					getActivity().finish();
				}
			});
			// Create the AlertDialog object and return it
			return mBuilder.create();

		case Constants.DIALOG_GET_COUNTRY_CODE_FAILED:


			mBuilder.setMessage(R.string.server_issue)
			.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) 
				{
					AccountActivationScreen.getCurrentCountryCode();
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dismiss();
					getActivity().finish();
				}
			});
			// Create the AlertDialog object and return it
			return mBuilder.create();

		case Constants.DIALOG_SEND_SMS__NO_INTERNET:
			// Use the Builder class for convenient dialog construction


			mBuilder.setMessage(R.string.dialog_internet_status_message)
			.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) 
				{
					VerificationScreen.sendSmsForOtp();
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dismiss();
					getActivity().finish();
				}
			});
			// Create the AlertDialog object and return it
			return mBuilder.create();

		case Constants.DIALOG_SEND_SMS__FAILED:


			mBuilder.setMessage(R.string.server_issue)
			.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) 
				{
					VerificationScreen.sendSmsForOtp();
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dismiss();
					getActivity().finish();
				}
			});
			// Create the AlertDialog object and return it
			return mBuilder.create();

		case Constants.DIALOG_CONFIRM_OTP__NO_INTERNET:
			// Use the Builder class for convenient dialog construction


			mBuilder.setMessage(R.string.dialog_internet_status_message)
			.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) 
				{
					VerificationScreen.confirmOtp();
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dismiss();
					getActivity().finish();
				}
			});
			// Create the AlertDialog object and return it
			return mBuilder.create();

		case Constants.DIALOG_TERMS_AND_CONDITION:

			return termsAndConditionsDialog();

		case Constants.DIALOG_VIDEO_OPTION:

			return videoOptionDialog();
		
		default:
			
			break;
		}
		return super.onCreateDialog(savedInstanceState);
	}
	public Dialog videoOptionDialog()
	{
		DisplayMetrics displayMetrics = new DisplayMetrics();

		getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

		final int height = displayMetrics.heightPixels;

		final int width = displayMetrics.widthPixels;

		final Dialog myDialog = new Dialog(getActivity());

		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		myDialog.setContentView(R.layout.video_option_dialog_layout);

		myDialog.getWindow().setGravity(Gravity.CENTER);

		myDialog.getWindow().setLayout((width*75)/100, LayoutParams.WRAP_CONTENT);

		myDialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);

		Button selectVideo=(Button)myDialog.findViewById(R.id.selectvideo);

		Button recordvideo=(Button)myDialog.findViewById(R.id.capturevideo);

		Button cancel=(Button)myDialog.findViewById(R.id.button_close);
		//TextView message_text=(TextView)myDialog.findViewById(R.id.textView1);
		//message_text.setText(message);

		selectVideo.setOnClickListener(new OnClickListener() {									
			public void onClick(View v) {

				myDialog.dismiss();

			}
		});

		recordvideo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				/*Intent intent = new Intent(Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				startActivityForResult(intent, PICK_FROM_GALLERY);*/
				myDialog.dismiss();

			}
		});
		cancel.setOnClickListener(new OnClickListener() {									
			public void onClick(View v) {

				myDialog.dismiss();
			}
		});				

		return myDialog;
		//myDialog.show();
	}
	public Dialog termsAndConditionsDialog()
	{
		final Dialog myDialog = new Dialog(getActivity());

		myDialog.setCancelable(false);

		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		myDialog.setContentView(R.layout.dialog_terms_conditions_webview);

		myDialog.getWindow().setGravity(Gravity.CENTER);

		DisplayMetrics displayMetrics = new DisplayMetrics();

		getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

		final int height = displayMetrics.heightPixels;

		final int width = displayMetrics.widthPixels;

		myDialog.getWindow().setLayout(width, height);

		myDialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);


		Button cancel=(Button)myDialog.findViewById(R.id.button_close);

		final Button back=(Button)myDialog.findViewById(R.id.back);

		back.setVisibility(View.GONE);

		final WebView webview=(WebView)myDialog.findViewById(R.id.webView1);

		//webview.getSettings().setJavaScriptEnabled(true);

		webview.setWebViewClient(new WebViewClient()
		{
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) 
			{
				// TODO Auto-generated method stub
				if(url.equals("Term&Conditions.html"))
				{

					GlobalClass.printDebugLog("URL", "terms and condition url-->"+url);

					view.loadUrl("file:///android_asset/"+url);

					back.setVisibility(View.VISIBLE);

					back.setOnClickListener(new OnClickListener() 
					{

						@Override
						public void onClick(View arg0) 
						{
							webview.goBack();

							back.setVisibility(View.GONE);

						}
					});
				}
				return true;
			}
		});

		webview.getSettings().setBuiltInZoomControls(true);

		try {

			InputStream fin = getActivity().getAssets().open("who1.html");

			byte[] buffer = new byte[fin.available()];

			fin.read(buffer);

			fin.close();

			webview.loadData(new String(buffer), "text/html", "UTF-8");


		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}

		//message_txt.setText(message);

		cancel.setOnClickListener(new OnClickListener() 
		{									
			public void onClick(View v) 
			{

				myDialog.dismiss();

			}
		});				

		return myDialog;
		//myDialog.show();
	}
}
