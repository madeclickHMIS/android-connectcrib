
/**
 * Copyright statement here.
 */

package com.connectcribmessenger.auxilliaryclasses;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;

import com.connectcribmessenger.comparators.ComparatorCountry;
import com.connectcribmessenger.entities.Country;
import com.connectcribmessenger.main.R;
import com.connectcribmessenger.main.R.drawable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

//Contains methods that return values from local storage for app.
public class GetValues 
{
	private Context mContext;

	private SharedPreferences mSharedPreference;

	private ConnectivityManager mConnectivityManager ;
	//Constructor
	public GetValues(Context context)
	{
		mContext=context;
		mSharedPreference=mContext.getSharedPreferences("LocalStorageFile", 0);
	}

	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 *
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	public String getRegistrationId() {
		final SharedPreferences prefs = getGCMPreferences();
		String registrationId = prefs.getString(Constants.SHAREDPREFERENCE_KEY_REGISTRATION_ID, "");
		if (registrationId==null) {
			Log.i("tag", "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt("appVersion", Integer.MIN_VALUE);
		int currentVersion = getAppVersion();
		
		GlobalClass.printDebugLog("GetValue", "registeredVersion-->"+ registeredVersion);
		
		GlobalClass.printDebugLog("GetValue", "currentVersion-->"+ currentVersion);
		
		if (registeredVersion != currentVersion) {
			Log.i("tag", "App version changed.");
			return "";
		}
		return registrationId;
	}

	public String getMessageDeliveryStatus(String msgId) 
	{
		SharedPreferences prefs = mContext.getSharedPreferences(Constants.SHAREDPREFERENCE_FILE, 0);
		
		String status = prefs.getString(msgId, "");

		return status;
	}
	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	public SharedPreferences getGCMPreferences() {
		// This sample app persists the registration ID in shared preferences, but
		// how you store the regID in your app is up to you.
		/*return mContext.getSharedPreferences(mContext.getClass().getSimpleName(),
				Context.MODE_PRIVATE);*/
		return mContext.getSharedPreferences(Constants.SHAREDPREFERENCE_FILE,
				Context.MODE_PRIVATE);
	}



	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	public int getAppVersion() {
		try {
			PackageInfo packageInfo = mContext.getPackageManager()
					.getPackageInfo(mContext.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * Get all countries with code and name from res/raw/countries.json
	 * 
	 * @return
	 */
	public List<Country> getAllCountries() {
		List<Country> allCountriesList = null;
		if (allCountriesList == null) 
		{
			try {
				allCountriesList = new ArrayList<Country>();

				// Read from local file
				String allCountriesString = readFileAsString();

				//GlobalClass.printDebugLog("GetValue", "method-->getAllCountries-->returns Json string-->"+ allCountriesString);

				JSONObject jsonObject = new JSONObject(allCountriesString);

				Iterator<?> keys = jsonObject.keys();

				// Add the data to all countries list
				while (keys.hasNext()) 
				{
					String key = (String) keys.next();

					Country country = new Country();



					country.setCode(key);

					country.setName(jsonObject.getString(key));

					String drawableName = "flag_"
							+ country.getCode().toLowerCase(Locale.ENGLISH);

					country.setCountryFlagIcon(getResId(drawableName));


					allCountriesList.add(country);
				}

				ComparatorCountry compCountry=new ComparatorCountry();

				// Sort the all countries list based on country name
				Collections.sort(allCountriesList, compCountry);

				GlobalClass.printDebugLog("GetValue", "method-->getAllCountries-->returns sorted list-->"+ allCountriesList.toString());


				// Return
				return allCountriesList;

			} 
			catch (Exception e) 
			{

				e.printStackTrace();
				//return null;
			}
		}
		return allCountriesList;

	}

	/**
	 * Get the country with code,name and flagicon from res/raw/countries.json
	 * 
	 * @return
	 */
	public Country getCountry() {
		Country country=null;



		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		int mobileNumberCountryCode = phoneUtil.getCountryCodeForRegion(GlobalClass.COUNTRY_CODE);



		if (country == null) 
		{
			try 
			{
				country=new Country();

				// Read from local file
				String allCountriesString = readFileAsString();

				JSONObject jsonObject = new JSONObject(allCountriesString);

				Iterator<?> keys = jsonObject.keys();

				// Add the data to all countries list
				while (keys.hasNext()) 
				{
					String key = (String) keys.next();

					//GlobalClass.printDebugLog("GetValue", "method-->getCountry-->country code by getConfiguration="+GlobalClass.COUNTRY_CODE);

					//GlobalClass.printDebugLog("GetValue", "method-->getCountry-->country code by file parsing="+key);

					if(key.equals(GlobalClass.COUNTRY_CODE))
					{
						//GlobalClass.printDebugLog("GetValue", "method-->getCountry-->country code by file parsing=country code by getConfiguration");

						country.setCode(key);

						country.setName(jsonObject.getString(key));

						String drawableName = "flag_"
								+ country.getCode().toLowerCase(Locale.ENGLISH);


						country.setCountryFlagIcon(getResId(drawableName));

						//country.setMNC(mnc);

						country.setMobileNumberCountryCode(mobileNumberCountryCode);

						break;
					}





				}

				//GlobalClass.printDebugLog("GetValue", "method-->getCountry-->returns-->Country Name="+ country.getName()+",Country Code="+country.getCode()+",Country FlagIcon="+country.getCountryFlagIcon());


				// Return
				return country;

			} 
			catch (Exception e) 
			{

				e.printStackTrace();
				//return null;
			}
		}
		return country;

	}

	/**
	 * R.string.countries is a json string which is Base64 encoded to avoid
	 * special characters in XML. It's Base64 decoded here to get original json.
	 * 
	 * @param context
	 * @return
	 * @throws java.io.IOException
	 */
	private String readFileAsString()
			throws java.io.IOException {
		String base64 = mContext.getResources().getString(R.string.countries);
		byte[] data = Base64.decode(base64, Base64.DEFAULT);
		return new String(data, "UTF-8");
	}

	/**
	 * The drawable image name has the format "flag_$countryCode". We need to
	 * load the drawable dynamically from country code. Code from
	 * http://stackoverflow.com/
	 * questions/3042961/how-can-i-get-the-resource-id-of
	 * -an-image-if-i-know-its-name
	 * 
	 * @param drawableName
	 * @return
	 */
	public int getResId(String drawableName) {

		try {
			Class<drawable> res = R.drawable.class;
			Field field = res.getField(drawableName);
			int drawableId = field.getInt(null);
			return drawableId;
		} catch (Exception e) {
			Log.e("COUNTRYPICKER", "Failure to get drawable id.", e);
		}
		return -1;
	}

	/**
	 * Method takes the sms and parses it to retrieve the four digit number text.
	 *  
	 * @param sms
	 * @return
	 */

	public String getOTPFromSms(String sms)
	{
		final StringBuilder sb = new StringBuilder(
				sms.length());
		for(int i = 0; i < sms.length(); i++){
			final char c = sms.charAt(i);
			if(c > 47 && c < 58){
				sb.append(c);
			}
		}

		GlobalClass.printDebugLog("OTP",sb.toString());

		return sb.toString();

	}

	/**
	 * This method gives the mobile number
	 */

	public String getMobileNumber()
	{
		TelephonyManager tm = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE); 

		return tm.getLine1Number();

	}

	/*
	 * Return TRUE if registration id received from GCM server is stored in device.
	 * Return FALSE if no registration id is stored in device.
	 */
	public boolean isRegistrationIdStored()
	{
		String regId=getRegistrationId();

		if(regId.equals(""))
		{
			return false;
		}
		else
		{
			return true;
		}

	}

	/*
	 * Returns TRUE if internet is connected else returns false
	 */

	/*public boolean isInternetAvailable()
	{
		mConnectivityManager =(ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo networkInfo = mConnectivityManager.getActiveNetworkInfo();

		if(networkInfo!=null)
		{
			return networkInfo.isConnected();	
		}
		else
		{
			return false;
		}


	}*/

	public boolean checkPlayServices() 
	{
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mContext);

		if (resultCode != ConnectionResult.SUCCESS) 
		{
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) 
			{
				GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) mContext,
						Constants.PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} 
			else 
			{
				Log.i("tag", "This device is not supported.");
				//act.finish();
			}
			return false;
		}
		return true;
	}

	/**
	 * Returns "true" if App is already registered itself and number has been confirmed
	 * else
	 * Returns "false"
	 */

	public boolean getAppFirstTimeOpenedStatus()
	{
		boolean status=mSharedPreference.getBoolean(Constants.SHAREDPREFERENCE_KEY_APP_FIRSTTIME_OPENED_STATUS, true);

		return status;
	}

	//unused
	public String getDate(String timeinmillis)
	{
		Calendar c=Calendar.getInstance();

		c.setTimeInMillis(Long.parseLong(timeinmillis));

		Date date=c.getTime();

		String dateString=""+c.get(Calendar.DATE)+"/"+c.get(Calendar.MONTH)+"/"+c.get(Calendar.YEAR);

		return dateString;
	}

}
