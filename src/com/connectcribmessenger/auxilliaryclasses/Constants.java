package com.connectcribmessenger.auxilliaryclasses;

import java.math.BigInteger;


public class Constants 
{

	/***************************************A**********************************************/

	public static final String ACTIVITY_RECENT_CHAT_LIST_SCREEN="RecentChatListScreen";

	public static final String ACTIVITY_ARCHIVED_RECENT_CHAT_LIST_SCREEN="ArchivedRecentChatListScreen";

	public static final int ACTIVITYFORRESULT_SHOW_VIDEO_LIST=1;

	//Registration API
	//http://115.112.129.194:8083/~iphonedev/connectcrib/home.php?key=save&userId=1&deviceName=324325

	//update user table
	//http://115.112.129.194:8083/~iphonedev/connectcrib/home.php?key=update&userId=1&deviceName=test&number=919717132393&name=test&lm=test&status=testbdajsgdsaj
	public static final String API_UPDATE_PROFILE_PARAMETER_1="key";

	public static final String API_UPDATE_PROFILE_PARAMETER_1_VALUE="updateprofile";

	public static final String API_UPDATE_PROFILE_PARAMETER_2="userId";

	public static final String API_UPDATE_PROFILE_PARAMETER_3="deviceName";

	public static final String API_UPDATE_PROFILE_PARAMETER_4="number";

	public static final String API_UPDATE_PROFILE_PARAMETER_5="name";

	public static final String API_UPDATE_PROFILE_PARAMETER_6="lm";

	public static final String API_UPDATE_PROFILE_PARAMETER_7="status";

	public static final String API_UPDATE_PROFILE_PARAMETER_8=" profile_pic";

	//Confirm number via OTP
	// http://115.112.129.194:8083/~iphonedev/connectcrib/home.php?key=vo&number=919717132393&otp=4429&userId=1

	public static final String APP_SERVER_ROOT_PATH="http://115.112.129.194:8083/~iphonedev/connectcrib/home.php?";

	//Registration API
	//http://115.112.129.194:8083/~iphonedev/connectcrib/home.php?key=save&userId=1&deviceName=324325

	public static final String API_REGISTRATION_PARAMETER_1="key";

	public static final String API_REGISTRATION_PARAMETER_1_VALUE="save";

	public static final String API_REGISTRATION_PARAMETER_2="userId";

	public static final String API_REGISTRATION_PARAMETER_3="deviceName";

	//Send Sms Api
	//http://115.112.129.194:8083/~iphonedev/connectcrib/home.php?key=update&userId=1&number=919717132393
	//Response Format-->{"success":"0","data":[{"msg":"failure"}]}

	public static final String API_SEND_SMS_PARAMETER_1="key";

	public static final String API_SEND_SMS_PARAMETER_1_VALUE="update";

	public static final String API_SEND_SMS_PARAMETER_2="userId";

	public static final String API_SEND_SMS_PARAMETER_3="number";

	//Confirm OTP Api
	//http://115.112.129.194:8083/~iphonedev/connectcrib/home.php?key=vo&number=919717132393&otp=4429&userId=1
	//Response Format-->{"success":"2","data":[{"msg":"OTP verification Failure"}]}
	//success=0(when can not update)
	//success=1(when OTP is confirmed successfully)

	public static final String API_CONFIRM_OTP_PARAMETER_1="key";

	public static final String API_CONFIRM_OTP_PARAMETER_1_VALUE="vo";

	public static final String API_CONFIRM_OTP_PARAMETER_2="number";

	public static final String API_CONFIRM_OTP_PARAMETER_3="otp";

	public static final String API_CONFIRM_OTP_PARAMETER_4="userId";

	//Get all the connectcrib contacts stored in device that are already registered on the server
	//http://115.112.129.194:8083/~iphonedev/connectcrib/home.php?key=userlist
	public static final String API_GET_CONNECTCRIB_CONTACTS_PARAMETER_1="key";

	public static final String API_GET_CONNECTCRIB_CONTACTS_PARAMETER_1_VALUE="userlist";

	public static final String API_GET_CONNECTCRIB_CONTACTS_PARAMETER_2="contactlist";

	//To determine whether user is online or offline.
	//http://115.112.129.194:8083/~iphonedev/connectcrib/home.php?key=networkstatus&number=+918882799508&onlinestatus=online&offlinestatus=offline

	public static final String API_UPDATE_ONLINEOFFLINE_STATUS_PARAMETER_1="key";

	public static final String API_UPDATE_ONLINEOFFLINE_STATUS_PARAMETER_1_VALUE="networkstatus";

	public static final String API_UPDATE_ONLINEOFFLINE_STATUS_PARAMETER_2="number";

	public static final String API_UPDATE_ONLINEOFFLINE_STATUS_PARAMETER_3="onlinestatus";

	public static final String API_UPDATE_ONLINEOFFLINE_STATUS_PARAMETER_4="onlinestatus";

	//http://115.112.129.194:8083/~iphonedev/connectcrib/home.php?key=getnetworkstatus&number=+918882799508
	public static final String API_GET_ONLINEOFFLINE_STATUS_PARAMETER_1="key";

	public static final String API_GET_ONLINEOFFLINE_STATUS_PARAMETER_1_VALUE="getnetworkstatus";

	public static final String API_GET_ONLINEOFFLINE_STATUS_PARAMETER_2="number";

	//http://115.112.129.194:8083/~iphonedev/imageupload/fileUpload.php
	/*public static final String API_UPLOAD_VIDEO="http://115.112.129.194:8083/~iphonedev/imageupload/fileUpload.php";*/
	// http://115.112.129.194:8083/~iphonedev/connectcrib/home.php?key=getvideo&sender_no=+918882799508&file_type=video&receipt_no=+918882799508,+919331234738

	public static final String API_UPLOAD_VIDEO="http://115.112.129.194:8083/~iphonedev/connectcrib/home.php?";

	/***************************************B**********************************************/

	public static final int BACKOFF_MILLI_SECONDS = 2000;

	public static final long BACKOFF_TIME = 2000;
	
	public static final String BACKUP_SETTING_MESSAGE_INFO_CSV_FILENAME="MessageInfo.csv";
	public static final String BACKUP_SETTING_LOCATION_INFO_CSV_FILENAME="LocationInfo.csv";
	public static final String BACKUP_SETTING_CALLLOG_INFO_CSV_FILENAME="CallLogInfo.csv";
	
	public static final String BACKUP_SETTING_INFO_TYPE_MESSAGE="Message_Info";
	public static final String BACKUP_SETTING_INFO_TYPE_LOCATION="Location_Info";
	public static final String BACKUP_SETTING_INFO_TYPE_CALLLOG="CallLog_Info";
	
	public static final String BACKUP_SETTING_APP_FOLDER="/ConnectCrib";
	public static final String BACKUP_SETTING_ZIPPED_INFO_FILE="ConnectCribData.zip";

	/***************************************C**********************************************/

	public static final String COUNTRY_CODE_API="http://ip-api.com/json";

	public static final String CHAT_TYPE_PEER_TO_PEER="peer2peer";

	public static final String CHAT_TYPE_GROUP="group";

	public static final BigInteger CRYPTOGRAPHY_CLIENTPRIVATEKEY=new BigInteger("21719");
	
	public static final BigInteger CRYPTOGRAPHY_CLIENTPUBLICKEY=new BigInteger("59");
	
	public static final BigInteger CRYPTOGRAPHY_CLIENTMODULUSKEY=new BigInteger("37001");
	



	/***************************************D**********************************************/

	public static final String DEVICE_NAME="Android";



	public static final int DIALOG_GCM_REGISTRATION_NO_INTERNET = 0;

	public static final int DIALOG_GCM_REGISTRATION_FAILED = 1;

	public static final int DIALOG_GET_COUNTRY_CODE_FAILED = 2;

	public static final int DIALOG_GET_COUNTRY_CODE_NO_INTERNET = 3;

	public static final int DIALOG_SEND_SMS__NO_INTERNET = 4;

	public static final int DIALOG_SEND_SMS__FAILED = 5;

	public static final int DIALOG_CONFIRM_OTP__NO_INTERNET = 6;

	public static final int DIALOG_CONFIRM_OTP__FAILED = 7;

	public static final int DIALOG_TERMS_AND_CONDITION = 8;

	public static final int DIALOG_SHOW_ACCOUNTS = 9;

	public static final int DIALOG_PICK_FROM_GALLERY=10;

	public static final int DIALOG_VIDEO_OPTION=11;

	public static final String DIRECTORY_NAME_UPLOADED_AUDIO="Connectcrib/Media/Audio/Audio_Uploaded";

	public static final String DIRECTORY_NAME_DOWNLOADED_AUDIO="Connectcrib/Media/Audio/Audio_Downloaded";

	public static final String DIRECTORY_NAME_UPLOADED_VIDEO="Connectcrib/Media/Video/Video_Uploaded";

	public static final String DIRECTORY_NAME_DOWNLOADED_VIDEO="Connectcrib/Media/Video/Video_Downloaded";

	public static final String DIRECTORY_NAME_UPLOADED_IMAGE="Connectcrib/Media/Image/Image_Uploaded";

	public static final String DIRECTORY_NAME_DOWNLOADED_IMAGE="Connectcrib/Media/Image/Image_Downloaded";


	/***************************************G**********************************************/

	public static final String GCM_CCS_PROJECT_ID="decisive-fabric-763";

	public static final String GCM_CCS_PROJECT_NUMBER="1081729626960";

	public static final String GCM_CCS_SENDER_ID="1081729626960";

	public static final String GCM_CCS_SERVER_API_KEY="AIzaSyAnum59WSpEUBRz1PF8eOb3YTFYz4qBTVI";

	public static final long GCM_TIME_TO_LIVE = 60L * 60L * 24L * 7L * 4L; // 4 Weeks

	public static final String GCM_DATA_KEY_MESSAGE="gcm_message";

	public static final String GCM_DATA_KEY_MESSAGE_ID="gcm_message_id";

	public static final String GCM_DATA_KEY_MESSAGE_TYPE="gcm_message_type";

	public static final String GCM_DATA_KEY_MESSAGE_TIME="gcm_message_time";

	public static final String GCM_DATA_KEY_MESSAGE_DELIVERY_STATUS="gcm_message_delivery_status";

	public static final String GCM_DATA_KEY_SENDER_NAME="gcm_message_sender_name";

	public static final String GCM_DATA_KEY_SENDER_NUMBER="gcm_message_sender_number";

	public static final String GCM_DATA_KEY_CHAT_TYPE="gcm_chat_type";

	public static final String GCM_DATA_KEY_PROFILE_PIC="gcm_profile_pic";

	public static final String GCM_DATA_KEY_GROUP_INFO="gcm_group_info";

	/***************************************H**********************************************/
	public static final String HASHMAP_KEY_PARAM="param";

	public static final String HASHMAP_KEY_SENDER_NUMBER="sender_number";

	public static final String HASHMAP_KEY_GROUP_ID="group_id";

	public static final String HASHMAP_KEY_CHAT_TYPE="chat_type";

	public static final String HASHMAP_VALUE_CHAT_SCREEN="chat_screen";

	public static final String HASHMAP_VALUE_CHAT_TYPE_GROUP="group";

	public static final String HASHMAP_VALUE_CHAT_TYPE_PEER_TO_PEER="peer2peer";

	/***************************************I**********************************************/

	public static final String IMAGE_CACHE_DIR = "thumbs";

	public static final String INTENT_SHORTCUT_NAME = "name";

	public static final String INTENT_SHORTCUT_CHAT_TYPE = "chat_type";

	public static final String INTENT_SHORTCUT_NUMBER = "number";

	public static final String INTENT_SHORTCUT_GROUP_ID = "group_id";



	/***************************************M**********************************************/

	public static final int MAX_ATTEMPTS = 5;

	public static final String MESSAGE_ARCHIVED_STATUS_YES="yes";

	public static final String MESSAGE_ARCHIVED_STATUS_NO="no";

	public static final String MESSAGE_SEEN="seen";

	public static final String MESSAGE_UNSEEN="unseen";

	public static final String MESSAGE_COUNT_PEER_TO_PEER="message_count_peer2peer";

	public static final String MESSAGE_COUNT_GROUP="message_count_group";

	public static final String MESSAGE_INCOMING_DELIVERY_STATUS="received";

	public static final String MESSAGE_INOUT_INCOMING_STATUS="incoming";

	public static final String MESSAGE_INOUT_OUTGOING_STATUS="outgoing";

	public static final String MESSAGE_DELIVERY_STATUS_NO="delivery_no";

	public static final String MESSAGE_DELIVERY_STATUS_HALF="delivery_half";

	public static final String MESSAGE_DELIVERY_STATUS_FULL="delivery_full";

	public static final String MESSAGE_DELIVERY_STATUS_READ="delivery_read";

	public static final String MESSAGE_TYPE_TEXT="text";

	public static final String MESSAGE_TYPE_AUDIO="audio";

	public static final String MESSAGE_TYPE_VIDEO="video";

	public static final String MESSAGE_TYPE_PICTURE="picture";

	public static final String MEDIA_TYPE_IMAGE="image";

	public static final String MEDIA_TYPE_VIDEO="video";

	public static final String MEDIA_TYPE_AUDIO="audio";

	/***************************************O**********************************************/

	public final static String ONCREATE = "onCreate";

	public final static String ONRESUME = "onResume";

	public final static String ONPAUSE = "onPause";

	public final static String ONRESTART = "onRestart";

	public final static String ONDESTROY = "onDestroy";

	public final static String ONSCROLLSTATECHANGED="onScrollStateChanged";

	public final static String ONSCROLLSTATECHANGED_STATE_TRUE="onScrollStateChanged_true";

	public final static String ONSCROLLSTATECHANGED_STATE_FALSE="onScrollStateChanged_false";

	/***************************************P**********************************************/

	public final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	public static final String PEER_TO_PEER_GROUP_ID="none";

	public static final String PEER_TO_PEER_GROUP_NAME="none";

	public static final String PEER_TO_PEER_GROUP_INFO="none";

	/***************************************R**********************************************/



	/***************************************S**********************************************/

	public final static String SHAREDPREFERENCE_KEY_APP_VERSION="appVersion"; 

	public static final String SHAREDPREFERENCE_KEY_REGISTRATION_ID="regId";

	public static final String SHAREDPREFERENCE_KEY_APP_FIRSTTIME_OPENED_STATUS="isOpenedForFirstTime";

	public static final String SHAREDPREFERENCE_KEY_DELIVERY_STATUS_KEY="delivery_status";

	public static final String SHAREDPREFERENCE_KEY_CHAT_TYPE="chat_type"; 

	public static final String SHAREDPREFERENCE_KEY_IS_ICON_CREATED="is_icon_created";

	public static final String SHAREDPREFERENCE_KEY_SENDER_NUMBER="sender_number";

	public static final String SHAREDPREFERENCE_KEY_GROUP_ID="group_id";

	public static final String SHAREDPREFERENCE_FILE="LocalStorageFile";

	public static final boolean SHOWLOG=true;

	public static final int SPLASH_TIME_OUT=6000;


	public static final String SYNCADAPTER_AUTHORITY="com.android.contacts";

	/*****************************************T************************************************************/

	public static final String TAG = "ImageGridFragment";

	public static final int VIDEO_MAX_DURATION_IN_MIN=10;

	public static final int VIDEO_MAX_SiZE_IN_MB=100;












}
