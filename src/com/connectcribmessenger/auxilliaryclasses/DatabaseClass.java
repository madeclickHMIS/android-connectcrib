package com.connectcribmessenger.auxilliaryclasses;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseClass extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "UserData.db";

	private static final int DATABASE_VERSION = 1;

	public static final String Table1_Name = "Chats";

	public static final String Table2_Name = "GroupChats";

	public static final String Table3_Name = "Profile";
	
	public static final String Table4_Name = "BlockedUsers";
	
	public static final String BlockedUsers_Column0_Id = "s_no";
	
	public static final String BlockedUsers_Column1_Name = "name";
	
	public static final String BlockedUsers_Column2_Number = "number";
	
	public static final String BlockedUsers_Column3_Date = "date";

	public static final String Profile_Column0_Id = "s_no";

	public static final String Profile_Column1_NAME = "name";

	public static final String Profile_Column2_NUMBER = "number";

	public static final String Profile_Column3_MOOD = "mood";

	public static final String Profile_Column4_PIC = "pic";
	
	public static final String Profile_Column5_AVAILABILITY_STATUS = "availability_status";



	public static final String GroupChats_Column0_Id = "s_no";

	public static final String GroupChats_Column1_SENDER_NUMBER = "sender_number";

	public static final String GroupChats_Column2_SENDER_NAME = "sender_name";

	public static final String GroupChats_Column3_MESSAGE = "message";

	public static final String GroupChats_Column4_MESSAGE_TYPE = "message_type";

	public static final String GroupChats_Column5_MESSAGE_TIME = "message_time";

	public static final String GroupChats_Column6_MESSAGE_DELIVERY_STATUS = "message_delivery_status";

	public static final String GroupChats_Column7_MESSAGE_INOUT_STATUS = "message_inout_status";

	public static final String GroupChats_Column8_GROUPINFO = "group_info";

	public static final String GroupChats_Column9_MESSAGE_ID = "message_id";

	public static final String GroupChats_Column10_SEEN_STATUS = "seen_status";

	public static final String GroupChats_Column11_ARCHIVE_STATUS="archive_status";


	public static final String Chats_Column0_Id = "s_no";

	public static final String Chats_Column1_NUMBER = "number";

	public static final String Chats_Column2_MESSAGE = "message";

	public static final String Chats_Column3_MESSAGE_TYPE = "message_type";

	public static final String Chats_Column4_MESSAGE_TIME = "message_time";

	public static final String Chats_Column5_MESSAGE_DELIVERY_STATUS = "message_delivery_status";

	public static final String Chats_Column6_MESSAGE_INOUT_STATUS = "message_inout_status";

	public static final String Chats_Column7_PROFILE_ICON_LINK = "profile_icon_link";

	public static final String Chats_Column8_NAME = "name";

	public static final String Chats_Column9_MESSAGE_ID = "message_id";

	public static final String Chats_Column10_SEEN_STATUS = "seen_status";

	public static final String Chats_Column11__ARCHIVE_STATUS="archive_status";
	
	public static final String DATABASE_CREATE_TABLE4 = "create table "
			+ Table4_Name + " (" 
			+ BlockedUsers_Column0_Id
			+ " integer primary key autoincrement, " 
			+ BlockedUsers_Column1_Name
			+ " text not null, " 
			+ BlockedUsers_Column2_Number 
			+ " text not null, "
			+ BlockedUsers_Column3_Date
			+ " text not null);"; 

	public static final String DATABASE_CREATE_TABLE3 = "create table "
			+ Table3_Name + " (" 
			+ Profile_Column0_Id
			+ " integer primary key autoincrement, " 
			+ Profile_Column1_NAME
			+ " text not null, " 
			+ Profile_Column2_NUMBER 
			+ " text not null, "
			+ Profile_Column3_MOOD
			+ " text not null, " 
			+ Profile_Column4_PIC
			+ " text not null, "
			+ Profile_Column5_AVAILABILITY_STATUS
			+ " text not null);"; 

	public static final String DATABASE_CREATE_TABLE2 = "create table "
			+ Table2_Name + " (" 
			+ GroupChats_Column0_Id
			+ " integer primary key autoincrement, " 
			+ GroupChats_Column1_SENDER_NUMBER
			+ " text not null, " 
			+ GroupChats_Column2_SENDER_NAME + " text not null, "
			+ GroupChats_Column3_MESSAGE 
			+ " text not null, " 
			+ GroupChats_Column4_MESSAGE_TYPE
			+ " text not null, " 
			+ GroupChats_Column5_MESSAGE_TIME
			+ " text not null, " 
			+ GroupChats_Column6_MESSAGE_DELIVERY_STATUS
			+ " text not null, " 
			+ GroupChats_Column7_MESSAGE_INOUT_STATUS
			+ " text not null, "
			+ GroupChats_Column8_GROUPINFO
			+ " text not null, " 
			+ GroupChats_Column9_MESSAGE_ID
			+ " text not null, " 
			+ GroupChats_Column10_SEEN_STATUS
			+ " text not null, " 
			+ GroupChats_Column11_ARCHIVE_STATUS
			+ " text not null);";

	public static final String DATABASE_CREATE_TABLE1 = "create table "
			+ Table1_Name + " (" 
			+ Chats_Column0_Id
			+ " integer primary key autoincrement, " 
			+ Chats_Column1_NUMBER
			+ " text not null, " 
			+ Chats_Column2_MESSAGE 
			+ " text not null, "
			+ Chats_Column3_MESSAGE_TYPE 
			+ " text not null, " 
			+ Chats_Column4_MESSAGE_TIME
			+ " text not null, " 
			+ Chats_Column5_MESSAGE_DELIVERY_STATUS
			+ " text not null, " 
			+ Chats_Column6_MESSAGE_INOUT_STATUS
			+ " text not null, " 
			+ Chats_Column7_PROFILE_ICON_LINK
			+ " text not null, "
			+ Chats_Column8_NAME
			+ " text not null, "
			+ Chats_Column9_MESSAGE_ID
			+ " text not null, "
			+ Chats_Column10_SEEN_STATUS
			+ " text not null, "
			+ Chats_Column11__ARCHIVE_STATUS
			+ " text not null);";

	public DatabaseClass(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.i("DATABASE_CREATE_TABLE11", DATABASE_CREATE_TABLE1);

		db.execSQL(DATABASE_CREATE_TABLE1);
		db.execSQL(DATABASE_CREATE_TABLE2);
		db.execSQL(DATABASE_CREATE_TABLE3);
		db.execSQL(DATABASE_CREATE_TABLE4);

		

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {

		db.execSQL("DROP TABLE IF EXISTS " + Table1_Name);
		db.execSQL("DROP TABLE IF EXISTS " + Table2_Name);
		db.execSQL("DROP TABLE IF EXISTS " + Table3_Name);
		db.execSQL("DROP TABLE IF EXISTS " + Table4_Name);

		db.execSQL(DATABASE_CREATE_TABLE1);
		db.execSQL(DATABASE_CREATE_TABLE2);
		db.execSQL(DATABASE_CREATE_TABLE3);
		db.execSQL(DATABASE_CREATE_TABLE4);

	}

	public void addRecords(SQLiteDatabase db) {
		// db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(DatabaseClass.Chats_Column1_NUMBER, "980929");

		values.put(DatabaseClass.Chats_Column2_MESSAGE,
				"How are you guys. Are you going out for..");

		values.put(DatabaseClass.Chats_Column3_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.Chats_Column4_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INCOMING_DELIVERY_STATUS);

		values.put(DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				"http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

		values.put(DatabaseClass.Chats_Column8_NAME, "John");

		values.put(DatabaseClass.Chats_Column9_MESSAGE_ID, "1");

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, Constants.MESSAGE_SEEN);

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_YES);

		db.insert(DatabaseClass.Table1_Name, null, values);

		values.put(DatabaseClass.Chats_Column1_NUMBER, "9650608967");

		values.put(DatabaseClass.Chats_Column2_MESSAGE,
				"How are you guys. Are you going out for..");

		values.put(DatabaseClass.Chats_Column3_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.Chats_Column4_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INCOMING_DELIVERY_STATUS);

		values.put(DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				"http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

		values.put(DatabaseClass.Chats_Column8_NAME, "Vishal Kalia");

		values.put(DatabaseClass.Chats_Column9_MESSAGE_ID, "2617");

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, Constants.MESSAGE_SEEN);

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_YES);

		db.insert(DatabaseClass.Table1_Name, null, values);

		values.put(DatabaseClass.Chats_Column1_NUMBER, "9492929");

		values.put(DatabaseClass.Chats_Column2_MESSAGE,
				"How are you guys. Are you going out for..");

		values.put(DatabaseClass.Chats_Column3_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.Chats_Column4_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_DELIVERY_STATUS_HALF);

		values.put(DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS, Constants.MESSAGE_INOUT_OUTGOING_STATUS);

		values.put(
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				"http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

		values.put(DatabaseClass.Chats_Column8_NAME, "Johny");

		values.put(DatabaseClass.Chats_Column9_MESSAGE_ID, "2");

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, Constants.MESSAGE_SEEN);

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_YES);

		db.insert(DatabaseClass.Table1_Name, null, values);

		values.put(DatabaseClass.Chats_Column1_NUMBER, "919930");

		values.put(DatabaseClass.Chats_Column2_MESSAGE,
				"How are you guys. Are you going out for..");

		values.put(DatabaseClass.Chats_Column3_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.Chats_Column4_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INCOMING_DELIVERY_STATUS);

		values.put(DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				"http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

		values.put(DatabaseClass.Chats_Column8_NAME, "Janardan");

		values.put(DatabaseClass.Chats_Column9_MESSAGE_ID, "3");

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, Constants.MESSAGE_SEEN);

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table1_Name, null, values);

		values.put(DatabaseClass.Chats_Column1_NUMBER, "978929");

		values.put(DatabaseClass.Chats_Column2_MESSAGE,
				"How are you guys. Are you going out for..");

		values.put(DatabaseClass.Chats_Column3_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.Chats_Column4_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INCOMING_DELIVERY_STATUS);

		values.put(DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				"http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

		values.put(DatabaseClass.Chats_Column8_NAME, "Swastik");

		values.put(DatabaseClass.Chats_Column9_MESSAGE_ID, "4");

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, Constants.MESSAGE_SEEN);

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table1_Name, null, values);

		values.put(DatabaseClass.Chats_Column1_NUMBER, "9245629");
		values.put(DatabaseClass.Chats_Column2_MESSAGE,
				"How are you guys. Are you going out for..");

		values.put(DatabaseClass.Chats_Column3_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.Chats_Column4_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INCOMING_DELIVERY_STATUS);

		values.put(DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				"http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

		values.put(DatabaseClass.Chats_Column8_NAME, "Sankirna");

		values.put(DatabaseClass.Chats_Column9_MESSAGE_ID, "5");

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, Constants.MESSAGE_UNSEEN);

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table1_Name, null, values);

		values.put(DatabaseClass.Chats_Column1_NUMBER, "923929");

		values.put(DatabaseClass.Chats_Column2_MESSAGE,
				"How are you guys. Are you going out for..");

		values.put(DatabaseClass.Chats_Column3_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.Chats_Column4_MESSAGE_TIME,String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_DELIVERY_STATUS_FULL);

		values.put(DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS, Constants.MESSAGE_INOUT_OUTGOING_STATUS);

		values.put(
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				"http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

		values.put(DatabaseClass.Chats_Column8_NAME, "Langer");

		values.put(DatabaseClass.Chats_Column9_MESSAGE_ID, "6");

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, Constants.MESSAGE_SEEN);

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table1_Name, null, values);

		values.put(DatabaseClass.Chats_Column1_NUMBER, "923529");

		values.put(DatabaseClass.Chats_Column2_MESSAGE,
				"How are you guys. Are you going out for..");

		values.put(DatabaseClass.Chats_Column3_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.Chats_Column4_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_DELIVERY_STATUS_HALF);

		values.put(DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS, Constants.MESSAGE_INOUT_OUTGOING_STATUS);

		values.put(
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				"http://www.donorperfect.com/images/small-size-icon.png");		
		values.put(DatabaseClass.Chats_Column8_NAME, "Smith");

		values.put(DatabaseClass.Chats_Column9_MESSAGE_ID, "7");

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, Constants.MESSAGE_UNSEEN);

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table1_Name, null, values);

		values.put(DatabaseClass.Chats_Column1_NUMBER, "926729");

		values.put(DatabaseClass.Chats_Column2_MESSAGE,
				"How are you guys. Are you going out for..");

		values.put(DatabaseClass.Chats_Column3_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.Chats_Column4_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INCOMING_DELIVERY_STATUS);

		values.put(DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				"http://www.dedoimedo.com/images/life/icons-twitter-logo-small.jpg");

		values.put(DatabaseClass.Chats_Column8_NAME, "Federer");

		values.put(DatabaseClass.Chats_Column9_MESSAGE_ID, "8");

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, Constants.MESSAGE_SEEN);

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table1_Name, null, values);

		values.put(DatabaseClass.Chats_Column1_NUMBER, "92929");

		values.put(DatabaseClass.Chats_Column2_MESSAGE,
				"How are you guys. Are you going out for..");

		values.put(DatabaseClass.Chats_Column3_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.Chats_Column4_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INCOMING_DELIVERY_STATUS);

		values.put(DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				"http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

		values.put(DatabaseClass.Chats_Column8_NAME, "Nadal");

		values.put(DatabaseClass.Chats_Column9_MESSAGE_ID, "9");

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, Constants.MESSAGE_SEEN);

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table1_Name, null, values);

		values.put(DatabaseClass.Chats_Column1_NUMBER, "92829");

		values.put(DatabaseClass.Chats_Column2_MESSAGE,
				"How are you guys. Are you going out for..");

		values.put(DatabaseClass.Chats_Column3_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.Chats_Column4_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INCOMING_DELIVERY_STATUS);

		values.put(DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				"http://www.iconshock.com/img_jpg/SOPHISTIQUE/graphics/jpg/256/star_icon.jpg");

		values.put(DatabaseClass.Chats_Column8_NAME, "Woods");

		values.put(DatabaseClass.Chats_Column9_MESSAGE_ID, "10");

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, Constants.MESSAGE_UNSEEN);

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table1_Name, null, values);

		values.put(DatabaseClass.Chats_Column1_NUMBER, "92929");

		values.put(DatabaseClass.Chats_Column2_MESSAGE,
				"How are you guys. Are you going out for..");

		values.put(DatabaseClass.Chats_Column3_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.Chats_Column4_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INCOMING_DELIVERY_STATUS);

		values.put(DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				"http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

		values.put(DatabaseClass.Chats_Column8_NAME, "Garcia");

		values.put(DatabaseClass.Chats_Column9_MESSAGE_ID, "11");

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, Constants.MESSAGE_SEEN);

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table1_Name, null, values);

		values.put(DatabaseClass.Chats_Column1_NUMBER, "92129");

		values.put(DatabaseClass.Chats_Column2_MESSAGE,
				"How are you guys. Are you going out for..");

		values.put(DatabaseClass.Chats_Column3_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.Chats_Column4_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INCOMING_DELIVERY_STATUS);

		values.put(DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				"http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

		values.put(DatabaseClass.Chats_Column8_NAME, "Shaun");

		values.put(DatabaseClass.Chats_Column9_MESSAGE_ID, "12");

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, Constants.MESSAGE_SEEN);

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table1_Name, null, values);

		values.put(DatabaseClass.Chats_Column1_NUMBER, "92929");

		values.put(DatabaseClass.Chats_Column2_MESSAGE,
				"How are you guys. Are you going out for..");

		values.put(DatabaseClass.Chats_Column3_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.Chats_Column4_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INCOMING_DELIVERY_STATUS);

		values.put(DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				"http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

		values.put(DatabaseClass.Chats_Column8_NAME, "Emma");

		values.put(DatabaseClass.Chats_Column9_MESSAGE_ID, "13");

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, Constants.MESSAGE_SEEN);

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table1_Name, null, values);

		values.put(DatabaseClass.Chats_Column1_NUMBER, "92929");

		values.put(DatabaseClass.Chats_Column2_MESSAGE,
				"How are you guys. Are you going out for..");

		values.put(DatabaseClass.Chats_Column3_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.Chats_Column4_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INCOMING_DELIVERY_STATUS);

		values.put(DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				"http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

		values.put(DatabaseClass.Chats_Column8_NAME, "Carry");

		values.put(DatabaseClass.Chats_Column9_MESSAGE_ID, "14");

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, Constants.MESSAGE_SEEN);

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table1_Name, null, values);

		values.put(DatabaseClass.Chats_Column1_NUMBER, "92949");

		values.put(DatabaseClass.Chats_Column2_MESSAGE,
				"How are you guys. Are you going out for..");

		values.put(DatabaseClass.Chats_Column3_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.Chats_Column4_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INCOMING_DELIVERY_STATUS);

		values.put(DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				"http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

		values.put(DatabaseClass.Chats_Column8_NAME, "Corey");

		values.put(DatabaseClass.Chats_Column9_MESSAGE_ID, "15");

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, Constants.MESSAGE_SEEN);

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table1_Name, null, values);
		// db.close();
	}

	public void addRecordsToGroup(SQLiteDatabase db) 
	{
		ContentValues values = new ContentValues();

		values.put(DatabaseClass.GroupChats_Column1_SENDER_NUMBER, "9650608967");

		values.put(DatabaseClass.GroupChats_Column2_SENDER_NAME,
				"Vishal Kalia");

		values.put(DatabaseClass.GroupChats_Column3_MESSAGE, "How are you guys. Are you going out for..");

		values.put(DatabaseClass.GroupChats_Column4_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.GroupChats_Column5_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.GroupChats_Column6_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_DELIVERY_STATUS_HALF);

		values.put(
				DatabaseClass.GroupChats_Column7_MESSAGE_INOUT_STATUS,
				Constants.MESSAGE_INOUT_OUTGOING_STATUS);

		values.put(DatabaseClass.GroupChats_Column8_GROUPINFO, group1InfoInJson());

		values.put(DatabaseClass.GroupChats_Column9_MESSAGE_ID, "1");

		values.put(DatabaseClass.GroupChats_Column10_SEEN_STATUS, Constants.MESSAGE_UNSEEN);

		values.put(DatabaseClass.GroupChats_Column11_ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_YES);


		db.insert(DatabaseClass.Table2_Name, null, values);

		values.put(DatabaseClass.GroupChats_Column1_SENDER_NUMBER, "99372987821");

		values.put(DatabaseClass.GroupChats_Column2_SENDER_NAME,
				"Laxmi");

		values.put(DatabaseClass.GroupChats_Column3_MESSAGE, "How are you guys. Are you going out for..");

		values.put(DatabaseClass.GroupChats_Column4_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.GroupChats_Column5_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.GroupChats_Column6_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS);


		values.put(
				DatabaseClass.GroupChats_Column7_MESSAGE_INOUT_STATUS,
				Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(DatabaseClass.GroupChats_Column8_GROUPINFO, group1InfoInJson());

		values.put(DatabaseClass.GroupChats_Column9_MESSAGE_ID, "2");

		values.put(DatabaseClass.GroupChats_Column10_SEEN_STATUS, Constants.MESSAGE_UNSEEN);

		values.put(DatabaseClass.GroupChats_Column11_ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_YES);

		db.insert(DatabaseClass.Table2_Name, null, values);

		values.put(DatabaseClass.GroupChats_Column1_SENDER_NUMBER, "99372987821");

		values.put(DatabaseClass.GroupChats_Column2_SENDER_NAME,
				"Laxmi");

		values.put(DatabaseClass.GroupChats_Column3_MESSAGE, "How are you guys. Are you going out for..");

		values.put(DatabaseClass.GroupChats_Column4_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.GroupChats_Column5_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.GroupChats_Column6_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INCOMING_DELIVERY_STATUS);

		values.put(
				DatabaseClass.GroupChats_Column7_MESSAGE_INOUT_STATUS,
				Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(DatabaseClass.GroupChats_Column8_GROUPINFO, group1InfoInJson());

		values.put(DatabaseClass.GroupChats_Column9_MESSAGE_ID, "3");

		values.put(DatabaseClass.GroupChats_Column10_SEEN_STATUS, Constants.MESSAGE_UNSEEN);

		values.put(DatabaseClass.GroupChats_Column11_ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_YES);

		db.insert(DatabaseClass.Table2_Name, null, values);

		values.put(DatabaseClass.GroupChats_Column1_SENDER_NUMBER, "998347821");

		values.put(DatabaseClass.GroupChats_Column2_SENDER_NAME,
				"Sangharsh");

		values.put(DatabaseClass.GroupChats_Column3_MESSAGE, "How are you guys. Are you going out for..");

		values.put(DatabaseClass.GroupChats_Column4_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.GroupChats_Column5_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.GroupChats_Column6_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(
				DatabaseClass.GroupChats_Column7_MESSAGE_INOUT_STATUS,
				Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(DatabaseClass.GroupChats_Column8_GROUPINFO, group2InfoInJson());

		values.put(DatabaseClass.GroupChats_Column9_MESSAGE_ID, "1");

		values.put(DatabaseClass.GroupChats_Column10_SEEN_STATUS, Constants.MESSAGE_SEEN);

		values.put(DatabaseClass.GroupChats_Column11_ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table2_Name, null, values);

		/***************************************************/

		values.put(DatabaseClass.GroupChats_Column1_SENDER_NUMBER, "9650608967");

		values.put(DatabaseClass.GroupChats_Column2_SENDER_NAME,
				"Vishal Kalia");

		values.put(DatabaseClass.GroupChats_Column3_MESSAGE, "How are you guys. Are you going out for..");

		values.put(DatabaseClass.GroupChats_Column4_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.GroupChats_Column5_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.GroupChats_Column6_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_DELIVERY_STATUS_HALF);

		values.put(
				DatabaseClass.GroupChats_Column7_MESSAGE_INOUT_STATUS,
				Constants.MESSAGE_INOUT_OUTGOING_STATUS);

		values.put(DatabaseClass.GroupChats_Column8_GROUPINFO, group2InfoInJson());

		values.put(DatabaseClass.GroupChats_Column9_MESSAGE_ID, "2");

		values.put(DatabaseClass.GroupChats_Column10_SEEN_STATUS, Constants.MESSAGE_UNSEEN);

		values.put(DatabaseClass.GroupChats_Column11_ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table2_Name, null, values);

		/***************************************************/

		values.put(DatabaseClass.GroupChats_Column1_SENDER_NUMBER, "9650608967");

		values.put(DatabaseClass.GroupChats_Column2_SENDER_NAME,
				"Vishal Kalia");

		values.put(DatabaseClass.GroupChats_Column3_MESSAGE, "How are you guys. Are you going out for..");

		values.put(DatabaseClass.GroupChats_Column4_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.GroupChats_Column5_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.GroupChats_Column6_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_DELIVERY_STATUS_FULL);

		values.put(
				DatabaseClass.GroupChats_Column7_MESSAGE_INOUT_STATUS,
				Constants.MESSAGE_INOUT_OUTGOING_STATUS);

		values.put(DatabaseClass.GroupChats_Column8_GROUPINFO, group2InfoInJson());

		values.put(DatabaseClass.GroupChats_Column9_MESSAGE_ID, "3");

		values.put(DatabaseClass.GroupChats_Column10_SEEN_STATUS, Constants.MESSAGE_SEEN);

		values.put(DatabaseClass.GroupChats_Column11_ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table2_Name, null, values);

		/***************************************************/

		values.put(DatabaseClass.GroupChats_Column1_SENDER_NUMBER, "9007247821");

		values.put(DatabaseClass.GroupChats_Column2_SENDER_NAME,
				"Amit");

		values.put(DatabaseClass.GroupChats_Column3_MESSAGE, "How are you guys. Are you going out for..");

		values.put(DatabaseClass.GroupChats_Column4_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.GroupChats_Column5_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.GroupChats_Column6_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(
				DatabaseClass.GroupChats_Column7_MESSAGE_INOUT_STATUS,
				Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(DatabaseClass.GroupChats_Column8_GROUPINFO, group2InfoInJson());

		values.put(DatabaseClass.GroupChats_Column9_MESSAGE_ID, "4");

		values.put(DatabaseClass.GroupChats_Column10_SEEN_STATUS, Constants.MESSAGE_UNSEEN);

		values.put(DatabaseClass.GroupChats_Column11_ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table2_Name, null, values);

		/***************************************************/

		values.put(DatabaseClass.GroupChats_Column1_SENDER_NUMBER, "99325187821");

		values.put(DatabaseClass.GroupChats_Column2_SENDER_NAME,
				"Sunil");

		values.put(DatabaseClass.GroupChats_Column3_MESSAGE, "How are you guys. Are you going out for..");

		values.put(DatabaseClass.GroupChats_Column4_MESSAGE_TYPE, "text");

		values.put(DatabaseClass.GroupChats_Column5_MESSAGE_TIME, String.valueOf(System.currentTimeMillis()));

		values.put(DatabaseClass.GroupChats_Column6_MESSAGE_DELIVERY_STATUS, Constants.MESSAGE_INCOMING_DELIVERY_STATUS);

		values.put(
				DatabaseClass.GroupChats_Column7_MESSAGE_INOUT_STATUS,
				Constants.MESSAGE_INOUT_INCOMING_STATUS);

		values.put(DatabaseClass.GroupChats_Column8_GROUPINFO, group2InfoInJson());

		values.put(DatabaseClass.GroupChats_Column9_MESSAGE_ID, "5");

		values.put(DatabaseClass.GroupChats_Column10_SEEN_STATUS, Constants.MESSAGE_UNSEEN);

		values.put(DatabaseClass.GroupChats_Column11_ARCHIVE_STATUS, Constants.MESSAGE_ARCHIVED_STATUS_NO);

		db.insert(DatabaseClass.Table2_Name, null, values);
	}

	public String group1InfoInJson()
	{
		JSONObject jobj_main=new JSONObject();
		JSONObject jobj_member=new JSONObject();
		JSONObject jobj_admin_info=new JSONObject();
		JSONArray membersArray=new JSONArray();

		try {

			jobj_main.put("GroupName", "College");

			jobj_main.put("GroupId", "1");

			jobj_member.put("Name", "Jyoti Raj Singh");
			jobj_member.put("Number", "9911904562");
			jobj_member.put("ProfilePic", "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");
			membersArray.put(jobj_member);

			jobj_member=new JSONObject();

			jobj_member.put("Name", "Vishal Kalia");
			jobj_member.put("Number", "9650608967");
			jobj_member.put("ProfilePic", "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");
			membersArray.put(jobj_member);

			jobj_member=new JSONObject();

			jobj_member.put("Name", "Laxmi");
			jobj_member.put("Number", "99372987821");
			jobj_member.put("ProfilePic", "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");
			membersArray.put(jobj_member);

			jobj_main.put("Members", membersArray);

			jobj_admin_info.put("Name", "Laxmi");
			jobj_admin_info.put("Number", "99372987821");
			jobj_admin_info.put("ProfilePic", "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

			jobj_main.put("Admin", jobj_admin_info);

			jobj_main.put("ProfilePic", "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

			jobj_main.put("status", "Be happy");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return jobj_main.toString();
	}

	public String group2InfoInJson()
	{
		JSONObject jobj_main=new JSONObject();
		JSONObject jobj_member=new JSONObject();
		JSONObject jobj_admin_info=new JSONObject();
		JSONArray membersArray=new JSONArray();

		try {

			jobj_main.put("GroupName", "Smart World");

			jobj_main.put("GroupId", "2");

			jobj_member.put("Name", "Sangharsh");
			jobj_member.put("Number", "998347821");
			jobj_member.put("ProfilePic", "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");
			membersArray.put(jobj_member);

			jobj_member=new JSONObject();

			jobj_member.put("Name", "Amit");
			jobj_member.put("Number", "9007247821");
			jobj_member.put("ProfilePic", "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");
			membersArray.put(jobj_member);

			jobj_member=new JSONObject();

			jobj_member.put("Name", "Jyoti Raj Singh");
			jobj_member.put("Number", "9911904562");
			jobj_member.put("ProfilePic", "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");
			membersArray.put(jobj_member);

			jobj_member=new JSONObject();

			jobj_member.put("Name", "Sunil");
			jobj_member.put("Number", "99325187821");
			jobj_member.put("ProfilePic", "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");
			membersArray.put(jobj_member);

			jobj_main.put("Members", membersArray);

			jobj_admin_info.put("Name", "Sunil");
			jobj_admin_info.put("Number", "99325187821");
			jobj_admin_info.put("ProfilePic", "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

			jobj_main.put("Admin", jobj_admin_info);

			jobj_main.put("ProfilePic", "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");



			jobj_main.put("Status", "Be happy");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return jobj_main.toString();
	}

	/*public void addProfile(SQLiteDatabase db)
	{
		ContentValues values = new ContentValues();

		values.put(DatabaseClass.Profile_Column1_NAME, "Jyoti Raj Singh");
		values.put(DatabaseClass.Profile_Column2_NUMBER,
				"9911904562");
		values.put(DatabaseClass.Profile_Column3_STATUS, "Working on ConnectCrib Messenger");
		values.put(DatabaseClass.Profile_Column4_PIC, "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

		db.insert(DatabaseClass.Table3_Name, null, values);
	}*/

}
