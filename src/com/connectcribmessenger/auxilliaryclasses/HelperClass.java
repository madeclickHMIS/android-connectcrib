package com.connectcribmessenger.auxilliaryclasses;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import android.util.Log;

import com.csvreader.CsvWriter;

public class HelperClass extends javax.mail.Authenticator 

{
	Context context;
	DatabaseAccessor dbAccessor;
	String username;
	String password;
	public HelperClass(Context context)
	{
		this.context=context;

		dbAccessor=new DatabaseAccessor(context);


	}
	protected PasswordAuthentication getPasswordAuthentication() {   
		return new PasswordAuthentication(username, password);   
	}   
	public void sendInfo(String attachmentFilePath,String subject,String senderUserName,String senderPassword,String recipientUserName)
	{
		username=senderUserName;
		password=senderPassword;
		String mailhost = "smtp.gmail.com";   

		Session session;   

		try
		{
			
			Properties props = new Properties();   
			props.setProperty("mail.transport.protocol", "smtp");   
			props.setProperty("mail.host", mailhost);   
			props.put("mail.smtp.auth", "true");   
			props.put("mail.smtp.port", "465");   
			props.put("mail.smtp.socketFactory.port", "465");   
			props.put("mail.smtp.socketFactory.class",   
					"javax.net.ssl.SSLSocketFactory");   
			props.put("mail.smtp.socketFactory.fallback", "false");   
			props.setProperty("mail.smtp.quitwait", "false");   

			session = Session.getDefaultInstance(props, this);  
			MimeMessage message = new MimeMessage(session);   
			message.setSender(new InternetAddress(senderUserName));   
			message.setSubject(subject);  
			if (recipientUserName.indexOf(',') > 0)   
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientUserName));   
			else  
				message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipientUserName));   
			// Create the message part 
			BodyPart messageBodyPart = new MimeBodyPart();

			// Fill the message
			messageBodyPart.setText("Hi,"+"\n\n"+"Please find the attachment!");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			// Part two is attachment
			messageBodyPart = new MimeBodyPart();

			DataSource source = new FileDataSource(attachmentFilePath);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName("MobileInformation.zip");
			multipart.addBodyPart(messageBodyPart);

			// Put parts in message
			message.setContent(multipart);

			Log.v("Mail", "Sending");
			Transport.send(message);  
		}
		catch(Exception e)
		{
			e.printStackTrace();
			e.getMessage();
		}



	}

	public String getZippedFilePath(String messageCSVFilePath,String locationCSVFilePath,String callLogCSVFilePath)
	{
		final int BUFFER = 2048; 
		String state = Environment.getExternalStorageState();
		File filedir = null;
		File file = null;
		String zippedFilePath;
		String _files[]={messageCSVFilePath,locationCSVFilePath,callLogCSVFilePath};
		try  { 
			BufferedInputStream origin = null;
			if (Environment.MEDIA_MOUNTED.equals(state)) 
			{

				filedir=new File(context.getExternalFilesDir(null).toString()+Constants.BACKUP_SETTING_APP_FOLDER);

				if (filedir.exists ())
				{


				}
				else
				{

					filedir.mkdirs();
				} 
				file=new File(filedir,Constants.BACKUP_SETTING_ZIPPED_INFO_FILE);

			}
			FileOutputStream dest = new FileOutputStream(file); 

			ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest)); 

			byte data[] = new byte[BUFFER]; 

			for(int i=0; i < _files.length; i++) 
			{ 
				Log.v("Compress", "Adding: " + _files[i]); 
				FileInputStream fi = new FileInputStream(_files[i]); 
				origin = new BufferedInputStream(fi, BUFFER); 
				ZipEntry entry = new ZipEntry(_files[i].substring(_files[i].lastIndexOf("/") + 1)); 
				out.putNextEntry(entry); 
				int count; 
				while ((count = origin.read(data, 0, BUFFER)) != -1) { 
					out.write(data, 0, count); 
				} 
				origin.close(); 
			} 

			out.close(); 
			zippedFilePath=context.getExternalFilesDir(null).toString()+Constants.BACKUP_SETTING_APP_FOLDER+"/"+Constants.BACKUP_SETTING_ZIPPED_INFO_FILE;
		}
		catch(Exception e)
		{ 
			e.printStackTrace();
			return " ";
		} 
		

		return zippedFilePath;
	}

	public String getInfoPath(String filename,String info_type)
	{
		String csvPath;
		try
		{
			String state = Environment.getExternalStorageState();
			Cursor cursor = null;
			File file;
			CsvWriter csv_writer = null;
			File filedir = null;
			
			if (Environment.MEDIA_MOUNTED.equals(state)) 
			{

				filedir=new File(context.getExternalFilesDir(null).toString()+Constants.BACKUP_SETTING_APP_FOLDER);

				if (filedir.exists ())
				{


				}
				else
				{

					filedir.mkdirs();
				} 

			}
			if(info_type.equals(Constants.BACKUP_SETTING_INFO_TYPE_MESSAGE))
			{
				try {

					file=new File(filedir,filename);
					dbAccessor.open();
					//cursor=dbAccessor.getAllMessageInfo();
					csv_writer=new CsvWriter(new FileWriter(file), ',');
					String columns[]=cursor.getColumnNames();

					csv_writer.writeRecord(columns);
					csv_writer.endRecord();

					while(cursor.moveToNext())
					{

						csv_writer.write(cursor.getString(0));
						csv_writer.write(cursor.getString(1));
						csv_writer.write(cursor.getString(2));

						csv_writer.endRecord();


					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				dbAccessor.close();
			}
			else if(info_type.equals(Constants.BACKUP_SETTING_INFO_TYPE_LOCATION))
			{

				try {

					file=new File(filedir,filename);
					dbAccessor.open();
					//cursor=dbAccessor.getAllLocationInfo();
					csv_writer=new CsvWriter(new FileWriter(file), ',');
					String columns[]=cursor.getColumnNames();

					csv_writer.writeRecord(columns);
					csv_writer.endRecord();

					while(cursor.moveToNext())
					{

						csv_writer.write(cursor.getString(0));
						csv_writer.write(cursor.getString(1));
						csv_writer.write(cursor.getString(2));
						csv_writer.write(cursor.getString(3));

						csv_writer.endRecord();


					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				dbAccessor.close();
			}
			else
			{

				try {

					file=new File(filedir,filename);
					dbAccessor.open();
					//cursor=dbAccessor.getAllCallLogInfo();
					csv_writer=new CsvWriter(new FileWriter(file), ',');
					String columns[]=cursor.getColumnNames();

					csv_writer.writeRecord(columns);
					csv_writer.endRecord();

					while(cursor.moveToNext())
					{

						csv_writer.write(cursor.getString(0));
						csv_writer.write(cursor.getString(1));
						csv_writer.write(cursor.getString(2));
						csv_writer.write(cursor.getString(3));
						csv_writer.write(cursor.getString(4));

						csv_writer.endRecord();


					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				dbAccessor.close();
			}
			csv_writer.close();
			cursor.close();
			csvPath=context.getExternalFilesDir(null).toString()+Constants.BACKUP_SETTING_APP_FOLDER+"/"+filename;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return "";
		}
		
		return csvPath;
	}

}
