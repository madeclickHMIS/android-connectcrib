package com.connectcribmessenger.auxilliaryclasses;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.datatype.Duration;

import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore.Video.Thumbnails;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;

import com.connectcribmessenger.adapters.ChatAdapter;
import com.connectcribmessenger.entities.BackupSetting;
import com.connectcribmessenger.entities.Contact;
import com.connectcribmessenger.entities.ConversationInfo;
import com.connectcribmessenger.entities.Message;
import com.connectcribmessenger.entities.Profile;
import com.connectcribmessenger.entities.RecentChat;
import com.connectcribmessenger.entities.SelectedContactForMediaMessage;
import com.connectcribmessenger.main.InvitationDialogScreen;
import com.example.android.displayingbitmaps.util.ImageFetcher;

@SuppressLint("NewApi")
public class GlobalClass 
{
	//Global variables

	public static ChatAdapter chatAdapter;

	public static ListView listview;

	public static ArrayList<Contact> contactList;

	public static ArrayList<HashMap<String, String>> messageList;

	//Used in SqliteDataProviderThread.java to set the value.
	public static ArrayList<RecentChat> recentMessageList;

	//Used in AppFunctionalityProvider.java to set the value.
	public static ArrayList<RecentChat> recentArchivedMessageList;

	//Used in SqliteDataProviderThread.java to set the value.
	public static ArrayList<Message> MessageList;

	//Used in SaveContacts.java
	public static String name;

	//Used in AppFunctionalityProvider.java to set the value.
	//Used in GcmIntentService.java to get value
	public static boolean isAppRunning;


	//Used in GcmIntentService.java to get value
	//Value set in ChatScreen.java
	public static boolean IS_CHATSCREEN_OPENED;

	//Used in GcmIntentService.java to get value
	//Value set in RecentChatListScreen.java
	public static boolean IS_RECENT_CHATS_LIST_CREEN_OPENED;


	//Value set in ArchivedRecentChatListScreen.java
	public static boolean IS_ARCHIVED_RECENT_CHATS_LIST_CREEN_OPENED;

	//Used in GcmIntentService.java to get value
	//Value set in ChatScreen.java
	public static Handler HANDLER_CHAT_SCREEN;

	//Used in GcmIntentService.java to get value
	//Value set in RecentChatListScreen.java
	public static Handler HANDLER_RECENT_CHAT_LIST_SCREEN;

	//Used in AppFunctionalityProvider.java to set the value.
	//Used in GcmRegistration.java to set value.
	//Used in SendSmsForOtp.java to get value.
	//Used in ConfirmOtp.java to get value.
	public static String REGISTRATION_ID;

	//Used in AppFunctionalityProvider.java to set the value.
	//Used in GetValues.java to get the value
	public static String COUNTRY_CODE;

	//Used in AppFunctionalityProvider.java to set and get the value.
	public static String INTERNATIONAL_MOBILE_COUNTRY_CODE;

	//Used in AppFunctionalityProvider.java to set the value.
	//Used in VerificationScreen to get the value.
	public static String OTP;

	//The value is set either from ChatScreen>Appfunctionality or from the server
	public static String MESSAGE_DELIVERY_STATUS;

	//The value is set from ChatScreen>Appfunctionality
	public static String MESSAGE_TYPE;

	//The value is set from ChatScreen>Appfunctionality
	public static String MESSAGE;

	//Used in SqliteDataProviderThread.java to set the value.
	//Used in CreateProfile.java
	public static Profile profileData;

	public static String profilePic;

	//Used in ChatListAdapter to set data
	//User in AppFunctionalityProvider.java to get the value.
	public static ConversationInfo conversationInfo;


	//User in AppFunctionalityProvider.java to set and get the value.
	public static ImageFetcher mImageFetcher;

	//User in SplashScreen.java to set  the value.
	public static ArrayList<String> accountContacts;


	//User in DatabaseAccessor.java to set and get the value.
	public static ArrayList<HashMap<String, String>> parametersForUpdatingUnseenMessages;

	//Used in SendVieoScreen
	public static ArrayList<SelectedContactForMediaMessage> selectedItems;

	//Used in EditProfile
	public static boolean isEditScreenOpened;

	public static boolean isAudioFilePathSaved;

	private static final AbstractHttpClient httpClient;

	private static final HttpRequestRetryHandler retryHandler;

	static {
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));
		schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));

		HttpParams connManagerParams = new BasicHttpParams();
		ConnManagerParams.setMaxTotalConnections(connManagerParams, 500);
		ConnManagerParams.setMaxConnectionsPerRoute(connManagerParams,
				new ConnPerRouteBean(500));
		ConnManagerParams.setTimeout(connManagerParams, 15 * 1000);

		ThreadSafeClientConnManager cm =
				new ThreadSafeClientConnManager(connManagerParams,
						schemeRegistry);

		HttpParams clientParams = new BasicHttpParams();
		HttpProtocolParams.setUserAgent(clientParams, "ConnectCrib/1.0");
		HttpConnectionParams.setConnectionTimeout(clientParams, 15 * 1000);
		HttpConnectionParams.setSoTimeout(clientParams, 15 * 1000);
		httpClient = new DefaultHttpClient(cm, clientParams);

		retryHandler = new DefaultHttpRequestRetryHandler(5, false) {

			public boolean retryRequest(IOException exception, int executionCount,
					HttpContext context) {
				if (!super.retryRequest(exception, executionCount, context)) {
					Log.d("HTTP retry-handler", "Won't retry");
					return false;
				}
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
				}
				Log.d("HTTP retry-handler", "Retrying request...");
				return true;
			}
		};

		httpClient.setHttpRequestRetryHandler(retryHandler);
	}

	public static HttpClient getHttpClient() {
		return httpClient;
	}

	private static void post(String endpoint, Map<String, String> params)
			throws IOException {
		URL url;
		try {
			url = new URL(endpoint);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("invalid url: " + endpoint);
		}
		StringBuilder bodyBuilder = new StringBuilder();
		Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
		// constructs the POST body using the parameters
		while (iterator.hasNext()) {
			Entry<String, String> param = iterator.next();
			bodyBuilder.append(param.getKey()).append('=')
			.append(param.getValue());
			if (iterator.hasNext()) {
				bodyBuilder.append('&');
			}
		}
		String body = bodyBuilder.toString();
		// Log.v(TAG, "Posting '" + body + "' to " + url);
		byte[] bytes = body.getBytes();
		HttpURLConnection conn = null;
		BufferedReader br=null;
		try {
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setFixedLengthStreamingMode(bytes.length);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded;charset=UTF-8");
			// post the request
			OutputStream out = conn.getOutputStream();
			out.write(bytes);
			out.close();
			// handle the response
			int status = conn.getResponseCode();
			if (status != 200) {
				throw new IOException("Post failed with error code " + status);
			}
			//            conn.connect();
			//            InputStream i=conn.getInputStream();
			//            InputStreamReader iReader=new InputStreamReader(i);
			//            br=new BufferedReader(iReader);
			//            StringBuilder sb=new StringBuilder();
			//            String line=null;
			//            line=br.readLine();
			//            while(line!=null)
			//            	
			//            {
			//            	sb.append(line);
			//            	line=br.readLine();
			//            }
			//         try {
			//			JSONArray jarray=new JSONArray(sb.toString());
			//			JSONObject jobject=jarray.getJSONObject(0);
			//			String success_string=jobject.getString("success");
			//			if(success_string.equals("Registration ID successfully inserted"))
			//			{
			//				Singleton.registration_success_status="registered";
			//			}
			//			else if(success_string.equals("Registration ID already exists"))
			//			{
			//				Singleton.registration_success_status="already registered";
			//			}
			//			
			//		} catch (JSONException e) {
			//			// TODO Auto-generated catch block
			//			e.printStackTrace();
			//		}   
		} finally {
			if (conn != null) {
				conn.disconnect();
				//br.close();
			}
		}
	}

	/*public static void safeClose(HttpClient client)
	{
	    if(client != null && client.getConnectionManager() != null)
	    {
	        client.getConnectionManager().shutdown();
	    }
	}*/
	/*
	 * These are the global methods. These methods can be used any where in the
	 * app Values modified by any of the methods will reflect to all app.
	 */
	public static Bitmap getBackgroundBitmapImage(Resources res, int resId,
			int reqWidth, int reqHeight) {

		Bitmap scaledBitmap = Bitmap
				.createScaledBitmap(
						decodeSampledBitmapFromResource(res, resId, reqWidth,
								reqHeight), reqWidth, reqHeight, true);

		Log.v("Bitmap dimensions-->", "width-->" + scaledBitmap.getWidth()
				+ " height-->" + scaledBitmap.getHeight());
		return scaledBitmap;

	}

	public static Drawable getBackgroundDrawableImage(Resources res, int resId,
			int reqWidth, int reqHeight) {
		Bitmap scaledBitmap = Bitmap
				.createScaledBitmap(
						decodeSampledBitmapFromResource(res, resId, reqWidth,
								reqHeight), reqWidth, reqHeight, true);

		Drawable drawable = new BitmapDrawable(res, scaledBitmap);
		Log.v("Bitmap dimensions-->", "width-->" + scaledBitmap.getWidth()
				+ " height-->" + scaledBitmap.getHeight());
		return drawable;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	public static Bitmap decodeSampledBitmapFromResource(Resources res,
			int resId, int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);
		Log.v("samplesize-->",
				"samplingsize-->"
						+ calculateInSampleSize(options, reqWidth, reqHeight));
		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}

	public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
		int width = bm.getWidth();
		int height = bm.getHeight();
		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;
		// CREATE A MATRIX FOR THE MANIPULATION
		Matrix matrix = new Matrix();
		// RESIZE THE BIT MAP
		matrix.postScale(scaleWidth, scaleHeight);
		// RECREATE THE NEW BITMAP
		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
				matrix, false);
		return resizedBitmap;

	}

	//Methode to get the list of the recent chats

	public static ArrayList<HashMap<String, String>> getRecentChatList(ArrayList<HashMap<String, String>> totalMessageList)
	{
		ArrayList<HashMap<String, String>> recentChatList=new ArrayList<>();

		int arrayLength=totalMessageList.size();

		Log.i("GlobalClass", "Size of the recent chatlist-->"+arrayLength);

		for(int i=0;i<arrayLength;i++)
		{
			boolean flag = false;
			String number=null;


			number=totalMessageList.get(i).get("number");

			Log.i("GlobalClass", "number for loop "+i+"-->"+number);

			if(recentChatList.isEmpty())
			{

				Log.i("GlobalClass", "recentchat added when list empty ");
				recentChatList.add(totalMessageList.get(i));

			}
			else
			{
				Log.i("GlobalClass", "List not empty");
				for(int j=0;j<recentChatList.size();j++)
				{
					if(number.equals(recentChatList.get(j).get("number")))
					{
						flag=true;
					}

				}

				if(flag==false)
				{
					Log.i("GlobalClass", "recentchat added for loop "+i);
					recentChatList.add(totalMessageList.get(i));
				}
			}
		}

		return recentChatList;
	}

	/*
	 * This method shows debug log for the app.
	 * If showLog boolean variable should be set to false, if don't want to show logs in the app
	 */
	public static void printDebugLog(String title,String message)
	{
		if(Constants.SHOWLOG)
		{
			Log.d(title, message);
		}
	}

	/*
	 * Returns TRUE if internet is connected else returns false
	 */

	public static boolean isInternetAvailable(Context mContext)
	{
		ConnectivityManager mConnectivityManager =(ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo networkInfo = mConnectivityManager.getActiveNetworkInfo();

		if(networkInfo!=null)
		{
			return networkInfo.isConnected();	
		}
		else
		{
			return false;
		}


	}

	//Used in UploadVideo.java
	public static String getVideoDuration(String videoPath)
	{
		MediaMetadataRetriever retriever = new MediaMetadataRetriever();
		retriever.setDataSource(videoPath);
		String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
		long timeInmillisec = Long.parseLong( time );

		float durationInSeconds=timeInmillisec/1000;

		BigDecimal durationInMinutes=round((durationInSeconds/60),2);

		return String.valueOf(durationInMinutes.floatValue());

	}

	//Used in self
	public static  BigDecimal round(float d, int decimalPlace) {
		BigDecimal bd = new BigDecimal(Float.toString(d));
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);       
		return bd;
	}

	//Used in UploadVideo.java
	public static String getVideoSize(String videoPath)
	{
		File file = new File(videoPath);
		long sizeInBytes = file.length();

		float sizeInKiloBytes=sizeInBytes/1024;

		//float durationInMegaByte=durationInKiloBytes/1024;
		BigDecimal sizeInMegaByte=round(sizeInKiloBytes/1024, 2);

		return String.valueOf(sizeInMegaByte.floatValue());

	}

	public static void setVideoThumbnail(String videoUrl,ImageView imageview)
	{
		Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail(videoUrl, 
				Thumbnails.MICRO_KIND);

		imageview.setImageBitmap(bmThumbnail);
	}

	public static Bitmap getVideoThumbnail(String videoUrl)
	{
		Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail(videoUrl, 
				Thumbnails.MICRO_KIND);

		return bmThumbnail;

	}

	//Used in ChatAdapter.java
	public static String getTimeToDisplay(String timeinmillis)
	{
		String timeToDisplay=null;

		Calendar c=Calendar.getInstance();

		int currentDay=c.get(Calendar.DATE);

		int currentMonth=c.get(Calendar.MONTH)+1;

		int currentYear=c.get(Calendar.YEAR);

		String currentDate=""+currentDay+"/"+currentMonth+"/"+currentYear;

		c.setTimeInMillis(Long.parseLong(timeinmillis));

		int previousDay=c.get(Calendar.DATE);

		int previousMonth=c.get(Calendar.MONTH)+1;

		int previousYear=c.get(Calendar.YEAR);

		String previousDate=""+previousDay+"/"+previousMonth+"/"+previousYear;

		if(currentDate.equals(previousDate))
		{
			String hour=String.valueOf(c.get(Calendar.HOUR));
			String min=String.valueOf(c.get(Calendar.MINUTE));
			/*if(String.valueOf(c.get(Calendar.MINUTE)).length()==2)
				{
					timeToDisplay=""+c.get(Calendar.HOUR)+":"+c.get(Calendar.MINUTE);
				}
				else
				{
					timeToDisplay=""+c.get(Calendar.HOUR)+":0"+c.get(Calendar.MINUTE);
				}*/

			if(String.valueOf(c.get(Calendar.MINUTE)).length()<2)
			{
				min="0"+c.get(Calendar.MINUTE);
				//timeToDisplay=""+c.get(Calendar.HOUR)+":"+c.get(Calendar.MINUTE);
			}

			if(String.valueOf(c.get(Calendar.HOUR)).length()<2)
			{
				hour="0"+c.get(Calendar.HOUR);	
			}

			/*else
				{
					timeToDisplay=""+c.get(Calendar.HOUR)+":0"+c.get(Calendar.MINUTE);
				}*/
			timeToDisplay=hour+":"+min;

		}
		else if((currentDay-previousDay)==1&&(currentMonth==previousMonth)&&(currentYear==previousYear))
		{
			timeToDisplay="Yesterday";
		}
		else
		{
			timeToDisplay=previousDate;
		}
		return timeToDisplay;
	}

	/**
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(String type) 
	{
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/**
	 * returning image / video
	 */
	public static File getOutputMediaFile(String type) 
	{
		File mediaStorageDir ;

		File mediaFile = null;

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());

		if(type.equals(Constants.MEDIA_TYPE_VIDEO))
		{
			mediaStorageDir = new File(
					Environment
					.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES),
					Constants.DIRECTORY_NAME_DOWNLOADED_VIDEO);

			// Create the storage directory if it does not exist

			if (!mediaStorageDir.exists()) 
			{
				if (!mediaStorageDir.mkdirs()) 
				{
					Log.d("GlobalClass getOutputMediaFile", "Oops! Failed create "
							+ Constants.DIRECTORY_NAME_DOWNLOADED_VIDEO + " directory");
					return null;
				}
			}

			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VIDEO_" + timeStamp + ".mp4");
		}

		if(type.equals(Constants.MEDIA_TYPE_AUDIO))
		{
			mediaStorageDir = new File(
					Environment
					.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC),
					Constants.DIRECTORY_NAME_DOWNLOADED_AUDIO); 

			// Create the storage directory if it does not exist

			if (!mediaStorageDir.exists()) 
			{
				if (!mediaStorageDir.mkdirs()) 
				{
					Log.d("GlobalClass getOutputMediaFile", "Oops! Failed create "
							+ Constants.DIRECTORY_NAME_DOWNLOADED_AUDIO + " directory");
					return null;
				}
			}
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "AUD_" + timeStamp + ".mp3");
		}

		if(type.equals(Constants.MEDIA_TYPE_IMAGE))
		{
			mediaStorageDir = new File(
					Environment
					.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
					Constants.DIRECTORY_NAME_DOWNLOADED_IMAGE); 

			// Create the storage directory if it does not exist

			if (!mediaStorageDir.exists()) 
			{
				if (!mediaStorageDir.mkdirs()) 
				{
					Log.d("GlobalClass getOutputMediaFile", "Oops! Failed create "
							+ Constants.DIRECTORY_NAME_DOWNLOADED_IMAGE + " directory");
					return null;
				}
			}

			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
		}


		return mediaFile;
	}

	public static void setChatScreenMessages(String chatType,String senderNumber,String groupId,Activity act)
	{
		DatabaseAccessor dbAccessor=new DatabaseAccessor(act);

		dbAccessor.open();
		if(chatType.equals(Constants.CHAT_TYPE_PEER_TO_PEER))
		{
			Cursor cursor=dbAccessor.getPeer2PeerMessage(senderNumber);
			cursor.moveToFirst();

			Log.i("GlobalClass setChatScreenMessages", "Number of Records in table-->"+cursor.getCount());

			ArrayList<com.connectcribmessenger.entities.Message> messagesList=new ArrayList<>();

			for(int i=0;i<cursor.getCount();i++)
			{


				com.connectcribmessenger.entities.Message message=new com.connectcribmessenger.entities.Message();

				message.setMessage(cursor.getString(2));

				message.setMessageDeliveryStatus(cursor.getString(5));

				message.setMessageSendername(cursor.getString(8));

				message.setMessageTime(cursor.getString(4));

				message.setMessageType(cursor.getString(3));
				if(message.getMessageType().equals(Constants.MEDIA_TYPE_VIDEO))
				{
					try 
					{
						JSONObject jobj=new JSONObject(message.getMessage());

						if(jobj.has("status"))
						{
							message.setVideoStatus(jobj.getString("status"));
						}
						else
						{
							message.setVideoStatus("notdownloaded");
						}
						if(jobj.has("thumnail_link"))
						{
							message.setVideoThumbNailLink(jobj.getString("thumnail_link"));

							message.setThumbNail(GlobalClass.getBitmapFromUrl(jobj.getString("thumnail_link")));
						}
						else if(jobj.has("thumbnail_link"))
						{
							message.setVideoThumbNailLink(jobj.getString("thumbnail_link"));

							message.setThumbNail(GlobalClass.getVideoThumbnail(jobj.getString("thumbnail_link")));
						}
						if(jobj.has("link"))
						{

							message.setVideoLink(jobj.getString("link"));

						}
						if(jobj.has("size"))
						{
							message.setVideoSize(jobj.getString("size"));
						}
						else
						{
							message.setVideoSize("0.0 mb");

						}

						if(jobj.has("duration"))
						{
							message.setVideoDuration(jobj.getString("duration"));

						}
						else
						{
							message.setVideoDuration("0.0 min");
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
				}


				message.setMessageSenderNumber(cursor.getString(1));

				message.setMessageInOutStatus(cursor.getString(6));

				message.setMessageId(cursor.getString(9));

				message.setMessageSeenStatus(cursor.getString(10));


				messagesList.add(message);

				cursor.moveToNext();
			}

			GlobalClass.MessageList=messagesList;



			if(GlobalClass.MessageList!=null)
			{
				GlobalClass.printDebugLog("GlobalClass setChatScreenMessages", "Peer2peerChatList Content-->"+GlobalClass.MessageList.toString());


			}
			else
			{
				Log.i("GlobalClass setChatScreenMessages", "GlobalClass.MessageList has no data (null)");

			}
		}
		else
		{

			Cursor cursor=dbAccessor.getAllMessagesGroupChat();
			cursor.moveToFirst();

			Log.i("group specificMessagesList_Count", ""+cursor.getCount());

			ArrayList<com.connectcribmessenger.entities.Message> messagesList=new ArrayList<>();

			for(int i=0;i<cursor.getCount();i++)
			{
				if(groupId.equals(getgroupId(cursor.getString(8))))
				{
					com.connectcribmessenger.entities.Message message=new com.connectcribmessenger.entities.Message();

					message.setMessage(cursor.getString(3));

					message.setMessageDeliveryStatus(cursor.getString(6));

					message.setMessageSendername(cursor.getString(2));

					message.setMessageTime(cursor.getString(5));

					message.setMessageType(cursor.getString(4));

					if(message.getMessageType().equals(Constants.MEDIA_TYPE_VIDEO))
					{
						try 
						{
							JSONObject jobj=new JSONObject(message.getMessage());

							if(jobj.has("status"))
							{
								message.setVideoStatus(jobj.getString("status"));
							}
							else
							{
								message.setVideoStatus("notdownloaded");
							}
							if(jobj.has("thumnail_link"))
							{
								message.setVideoThumbNailLink(jobj.getString("thumnail_link"));

								message.setThumbNail(GlobalClass.getBitmapFromUrl(jobj.getString("thumnail_link")));
							}
							else if(jobj.has("thumbnail_link"))
							{
								message.setVideoThumbNailLink(jobj.getString("thumbnail_link"));

								message.setThumbNail(GlobalClass.getVideoThumbnail(jobj.getString("thumbnail_link")));
							}
							if(jobj.has("link"))
							{

								message.setVideoLink(jobj.getString("link"));

							}
							if(jobj.has("size"))
							{
								message.setVideoSize(jobj.getString("size"));
							}
							else
							{
								message.setVideoSize("0.0 mb");

							}

							if(jobj.has("duration"))
							{
								message.setVideoDuration(jobj.getString("duration"));

							}
							else
							{
								message.setVideoDuration("0.0 min");
							}

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}	
					}

					message.setMessageSenderNumber(cursor.getString(1));

					message.setMessageInOutStatus(cursor.getString(7));

					message.setMessageId(cursor.getString(9));

					message.setMessageSeenStatus(cursor.getString(10));

					messagesList.add(message);	
				}


				cursor.moveToNext();
			}

			GlobalClass.MessageList=messagesList;

			if(GlobalClass.MessageList!=null)
			{
				GlobalClass.printDebugLog("GlobalClass setChatScreenMessages", "GroupChatList Content-->"+GlobalClass.MessageList.toString());


			}
			else
			{
				Log.i("GlobalClass setChatScreenMessages", "GlobalClass.MessageList has no data (null)");

			}


		}
		dbAccessor.close();

	}

	public static String getgroupId(String json)
	{
		String groupId=null;
		try {
			JSONObject jobj=new JSONObject(json);
			groupId=jobj.getString("GroupId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return groupId;
	}

	public static Bitmap getBitmapFromUrl(String httpUrl)
	{
		try
		{
			URL url = new URL(httpUrl);

			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();

			connection.setDoInput(true);

			connection.connect();

			InputStream input = connection.getInputStream();

			Bitmap myBitmap = BitmapFactory.decodeStream(input);



			return myBitmap;

		} 
		catch (IOException e) 
		{
			e.printStackTrace();

			return null;
		}
	}
	public static String EncryptData(String MyMessage,BigInteger privatekey,BigInteger modulus)
	{

		String EncryptedMessage = "";

		BigInteger mg;
		BigInteger encrypt;
		for(int i=0;i< MyMessage.length();i++)
		{

			if(i>0)
				EncryptedMessage+=" ";
			mg = intToBigint((int) MyMessage.charAt(i));
			encrypt = encrypt(mg,privatekey,modulus);
			EncryptedMessage+= encrypt.toString();
		}

		return EncryptedMessage;
	}
	public static String  DecryptData(String MyMessage,BigInteger publicKey,BigInteger modulus)
	{

		String DecryptedMessage = "";
		BigInteger mg;
		BigInteger decrypt;
		String[] tokens = MyMessage.split(" ");
		int ascii;
		char ch;
		for(int i=0;i< tokens.length; i++)
		{
			mg = intToBigint(Integer.parseInt(tokens[i]));
			decrypt = decrypt(mg,publicKey,modulus);
			ascii = decrypt.intValue();
			ch = (char) ascii;
			if ((int) ch > 255) {
				//				DecryptedMessage += "\\u" + Integer.toHexString((int) ch) + "--";
				DecryptedMessage += "\\u" + Integer.toHexString((int) ch);
			} else {
				DecryptedMessage += ch;
			}
			//ch = (char) ascii;
			//			DecryptedMessage+=  ch;

		}

		try {
			String newMessage=new String(DecryptedMessage.getBytes(), "UTF-8");

			return newMessage;


		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//String newMessage=new String(DecryptedMessage,"UTF-8");
		//return newMessage;
		return DecryptedMessage;
	}
	static BigInteger encrypt(BigInteger message,BigInteger privatekey,BigInteger modulus) {
		return message.modPow(privatekey, modulus);
	}

	static BigInteger decrypt(BigInteger encrypted,BigInteger publickey,BigInteger modulus) {
		return encrypted.modPow(publickey, modulus);
	}

	static BigInteger intToBigint(int x) {  
		return BigInteger.valueOf(x);  
	}

	/*public static String messageWithEmoji(String message)
	{
		StringBuilder sb = new StringBuilder();
		for (char curr : message.toCharArray()) {
		    sb.append((SUPPORTED_EMOJI_SET.contains(curr)) ? convertCharToImgTag(curr) : curr);
		}

		return sb.toString();
	}*/


	public static Bitmap blurImage (Bitmap input,Context context)
	{
		//Bitmap overlay = FastBlur.doBlur(overlay, 15, true);
		RenderScript rsScript = RenderScript.create (context);
		Allocation alloc = Allocation.createCubemapFromBitmap (rsScript, input);

		ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(rsScript, alloc.getElement());
		blur.setRadius (12);
		blur.setInput (alloc);

		Bitmap result = Bitmap.createBitmap(input.getWidth(), input.getHeight(), input.getConfig ());
		Allocation outAlloc = Allocation.createCubemapFromBitmap (rsScript, result);
		blur.forEach (outAlloc);
		outAlloc.copyFrom (result);

		rsScript.destroy ();
		return result;
	}

	public static void cancelNotification(Context context,int notificationId)
	{
		NotificationManager	mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancel(notificationId);
	}

	public static BackupSetting getBackupSettings(Context context)
	{
		
		SharedPreferences sp=context.getSharedPreferences("backupsettings", 0);
		
		BackupSetting backupSetting=new BackupSetting();
		
		backupSetting.setSenderEmail(sp.getString("senderemail",""));
		
		backupSetting.setSenderPassword(sp.getString("senderemailpass", ""));
		
		backupSetting.setRecipientEmail(sp.getString("recipientemail",""));
		
		backupSetting.setDuration(sp.getString("duration",""));
		
		return backupSetting;

	}

	public static void setBackupSettings(Context context,HashMap<String, String> values)
	{
		SharedPreferences sp=context.getSharedPreferences("backupsettings", 0);

		SharedPreferences.Editor editor=sp.edit();

		editor.putString("senderemail", values.get("senderemail"));

		editor.putString("senderemailpass", values.get("senderemailpass"));

		editor.putString("recipientemail", values.get("recipientemail"));

		editor.putString("hrs", values.get("hrs"));
		
		editor.putString("min", values.get("min"));
		
		editor.putString("sec", values.get("sec"));
		
		editor.putString("duration", values.get("duration"));

		editor.commit();
	}
	
	public static long getBackupDurationInMillis(String hrs,String mins,String secs)
	{
		long hoursInMillis=Integer.valueOf(hrs)*60*60*60;
		
		long minutesInMillis=Integer.valueOf(mins)*60*60;
		
		long secondsInMillis=Integer.valueOf(secs)*60;
		
		long duration=hoursInMillis+minutesInMillis+secondsInMillis;
		
		return duration;
	}
}

