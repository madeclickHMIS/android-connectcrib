
/*
 * Copyright statement here.
 */

package com.connectcribmessenger.auxilliaryclasses;

import android.content.Context;
import android.content.SharedPreferences;

/*
 * It contains methods to store values in XML file using SharedPreference.
 */

public class StoreValues 
{
	private Context mContext;
	private SharedPreferences mSharedPreference;
	
	// Constructor
	public StoreValues(Context context)
	{
		this.mContext=context;
		
		mSharedPreference=mContext.getSharedPreferences(Constants.SHAREDPREFERENCE_FILE, 0);
	}
	
	public void storeRegistrationId(String regId)
	{
		SharedPreferences.Editor editor=mSharedPreference.edit();
		
		editor.putString(Constants.SHAREDPREFERENCE_KEY_REGISTRATION_ID, regId);
		
		editor.commit();
		
	}
	
	public void storeMessageDeliveryStatus(String msgId,String status)
	{
		SharedPreferences.Editor editor=mSharedPreference.edit();
		
		editor.putString(msgId, status);
		
		editor.commit();
		
	}
	
	public void storeAppFirstTimeOpenedStatus(boolean val)
	{
		SharedPreferences.Editor editor=mSharedPreference.edit();
		
		editor.putBoolean(Constants.SHAREDPREFERENCE_KEY_APP_FIRSTTIME_OPENED_STATUS, val);
		
		editor.commit();
		
	}

}
