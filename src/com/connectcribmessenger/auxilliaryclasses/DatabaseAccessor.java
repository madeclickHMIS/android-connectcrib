package com.connectcribmessenger.auxilliaryclasses;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseAccessor {
	DatabaseClass dbCreator;
	SQLiteDatabase db;

	public DatabaseAccessor(Context context) {
		dbCreator = new DatabaseClass(context);
	}

	public void open() throws SQLException {

		db = dbCreator.getWritableDatabase();

	}

	public void close() {

		db.close();

	}

	// To insert profile;
	public int insertProfile(String name, String number,
			String mood, String profilepic,String availability_status) {

		open();
		ContentValues values = new ContentValues();

		values.put(DatabaseClass.Profile_Column1_NAME, name);

		values.put(DatabaseClass.Profile_Column2_NUMBER,
				number);

		values.put(DatabaseClass.Profile_Column3_MOOD, mood);

		values.put(DatabaseClass.Profile_Column4_PIC, profilepic);

		values.put(DatabaseClass.Profile_Column5_AVAILABILITY_STATUS, availability_status);

		int i=(int) db.insert(DatabaseClass.Table3_Name, null, values);

		close();

		return i;

	}
	
	//Used in GcmIntentService
		public void updateProfile(String name, String number,
				String mood, String profilepic,String availability_status) {
			// TODO Auto-generated method stub
			open();
			ContentValues values = new ContentValues();

			values.put(DatabaseClass.Profile_Column1_NAME, name);

			values.put(DatabaseClass.Profile_Column2_NUMBER,
					number);

			values.put(DatabaseClass.Profile_Column3_MOOD, mood);

			values.put(DatabaseClass.Profile_Column4_PIC, profilepic);

			values.put(DatabaseClass.Profile_Column5_AVAILABILITY_STATUS, availability_status);

			int i=db.update(DatabaseClass.Table3_Name, values, DatabaseClass.Profile_Column2_NUMBER+"='"+number+"'",null);

			System.out.print("number of rows affected after update="+i);

			close();
		}

	// To insert profile;
	public int insertBlockedUser(String name, String number,
			String date)
	{

		open();
		ContentValues values = new ContentValues();

		values.put(DatabaseClass.BlockedUsers_Column1_Name, name);

		values.put(DatabaseClass.BlockedUsers_Column2_Number,
				number);

		values.put(DatabaseClass.BlockedUsers_Column3_Date, date);



		int i=(int) db.insert(DatabaseClass.Table4_Name, null, values);

		close();

		return i;

	}



	// To insert record of the new peer2peer message that will be received via cloud or
	// message sent from the device;
	public int insertPeer2PeerMessage(String number, String message,
			String message_type, String message_time,
			String message_delivery_status, String message_inout_status,
			String profile_icon_link,String name,String message_id,String seenStatus,String archivedStatus) {

		open();
		ContentValues values = new ContentValues();

		values.put(DatabaseClass.Chats_Column1_NUMBER, number);
		values.put(DatabaseClass.Chats_Column2_MESSAGE, message);
		values.put(DatabaseClass.Chats_Column3_MESSAGE_TYPE, message_type);
		values.put(DatabaseClass.Chats_Column4_MESSAGE_TIME, message_time);
		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS,
				message_delivery_status);
		values.put(DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS,
				message_inout_status);

		values.put(DatabaseClass.Chats_Column7_PROFILE_ICON_LINK, profile_icon_link);

		values.put(DatabaseClass.Chats_Column8_NAME, name);

		values.put(DatabaseClass.Chats_Column9_MESSAGE_ID, message_id);

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, seenStatus);

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, archivedStatus);

		int i = (int) db.insert(DatabaseClass.Table1_Name, null, values);

		close();
		return i;

	}

	// To insert record of the new group message that will be received via cloud or
	// message sent from the device;
	public int insertGroupMessage(String senderNumber, String senderName,String message,
			String message_type, String message_time,
			String message_delivery_status, String message_inout_status,
			String groupInfo,String message_id,String seenStatus,String archivedStatus) {

		open();
		ContentValues values = new ContentValues();

		values.put(DatabaseClass.GroupChats_Column1_SENDER_NUMBER, senderNumber);
		values.put(DatabaseClass.GroupChats_Column2_SENDER_NAME,
				senderName);
		values.put(DatabaseClass.GroupChats_Column3_MESSAGE, message);
		values.put(DatabaseClass.GroupChats_Column4_MESSAGE_TYPE, message_type);
		values.put(DatabaseClass.GroupChats_Column5_MESSAGE_TIME, message_time);
		values.put(DatabaseClass.GroupChats_Column6_MESSAGE_DELIVERY_STATUS, message_delivery_status);

		values.put(
				DatabaseClass.GroupChats_Column7_MESSAGE_INOUT_STATUS,
				message_inout_status);

		values.put(DatabaseClass.GroupChats_Column8_GROUPINFO, groupInfo);

		values.put(DatabaseClass.GroupChats_Column9_MESSAGE_ID, message_id);

		values.put(DatabaseClass.GroupChats_Column10_SEEN_STATUS, seenStatus);

		values.put(DatabaseClass.GroupChats_Column11_ARCHIVE_STATUS, archivedStatus);

		int i = (int) db.insert(DatabaseClass.Table2_Name, null, values);

		close();

		return i;

	}

	// To get the messages of the particular number selected by the user
	public Cursor getPeer2PeerMessage(String number) {
		//open();
		Cursor cursor = db.query(DatabaseClass.Table1_Name, new String[] {
				DatabaseClass.Chats_Column0_Id,
				DatabaseClass.Chats_Column1_NUMBER,
				DatabaseClass.Chats_Column2_MESSAGE,
				DatabaseClass.Chats_Column3_MESSAGE_TYPE,
				DatabaseClass.Chats_Column4_MESSAGE_TIME,
				DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS,
				DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS,
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				DatabaseClass.Chats_Column8_NAME, 
				DatabaseClass.Chats_Column9_MESSAGE_ID,
				DatabaseClass.Chats_Column10_SEEN_STATUS,
				DatabaseClass.Chats_Column11__ARCHIVE_STATUS
		},
		DatabaseClass.Chats_Column1_NUMBER + "='" + number + "'", null, null,
		null, null, null);
		//close();
		return cursor;
	}

	// To get the messages of the particular number selected by the user
	public Cursor getBlockedUsers(String number) {
		//open();
		Cursor cursor = db.query(DatabaseClass.Table4_Name, new String[] {
				DatabaseClass.BlockedUsers_Column0_Id,
				DatabaseClass.BlockedUsers_Column1_Name,
				DatabaseClass.BlockedUsers_Column2_Number,
				DatabaseClass.BlockedUsers_Column3_Date,

		},
		DatabaseClass.BlockedUsers_Column2_Number + "='" + number + "'", null, null,
		null, null, null);
		//close();
		return cursor;
	}

	// To get the messages of the particular number selected by the user
	public Cursor getPeer2PeerMessage(String number,String messageId) {
		//open();
		Cursor cursor = db.query(DatabaseClass.Table1_Name, new String[] {
				DatabaseClass.Chats_Column0_Id,
				DatabaseClass.Chats_Column1_NUMBER,
				DatabaseClass.Chats_Column2_MESSAGE,
				DatabaseClass.Chats_Column3_MESSAGE_TYPE,
				DatabaseClass.Chats_Column4_MESSAGE_TIME,
				DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS,
				DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS,
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				DatabaseClass.Chats_Column8_NAME, 
				DatabaseClass.Chats_Column9_MESSAGE_ID,
				DatabaseClass.Chats_Column10_SEEN_STATUS,
				DatabaseClass.Chats_Column11__ARCHIVE_STATUS
		},
		DatabaseClass.Chats_Column1_NUMBER + "='" + number + "' and "+DatabaseClass.Chats_Column9_MESSAGE_ID+"='"+messageId+"'", null, null,
		null, null, null);
		//close();
		return cursor;
	}


	// To get the profile of the app user stored locally in sqlitedatabase
	public Cursor getProfile() {

		Cursor cursor = db.query(DatabaseClass.Table3_Name, new String[] {
				DatabaseClass.Profile_Column0_Id, 
				DatabaseClass.Profile_Column1_NAME,
				DatabaseClass.Profile_Column2_NUMBER,
				DatabaseClass.Profile_Column3_MOOD,
				DatabaseClass.Profile_Column4_PIC,
				DatabaseClass.Profile_Column5_AVAILABILITY_STATUS},
				null, null,null, null, null);

		return cursor;
	}

	//Used in GcmIntentService
	public void updateGroupChatMessageSeenStatus(String groupInfo,String messageid,String seenStatus) {
		// TODO Auto-generated method stub
		open();

		ContentValues values= new ContentValues();

		values.put(DatabaseClass.GroupChats_Column10_SEEN_STATUS, seenStatus);

		int i=db.update(DatabaseClass.Table2_Name, values, DatabaseClass.GroupChats_Column9_MESSAGE_ID+"='"+messageid+"' and "+DatabaseClass.GroupChats_Column8_GROUPINFO+"='"+groupInfo+"'",null);

		System.out.print("number of rows affected after update="+i);

		close();
	}

	//Used in sqlitedataprovider
	public void updateGroupChatMessageSeenStatus(String groupInfo,String number) {
		// TODO Auto-generated method stub
		open();

		ContentValues values= new ContentValues();

		values.put(DatabaseClass.GroupChats_Column10_SEEN_STATUS, Constants.MESSAGE_SEEN);

		int i=db.update(DatabaseClass.Table2_Name, values, DatabaseClass.GroupChats_Column1_SENDER_NUMBER+"='"+number+"' and "+DatabaseClass.GroupChats_Column8_GROUPINFO+"='"+groupInfo+"'",null);

		System.out.print("number of rows affected after update="+i);

		close();
	}
	//Used in GcmIntentService
	public void updateGroupChatDeliveryStatus(String groupInfo,String messageid,String deliveryStatus) {
		// TODO Auto-generated method stub
		open();

		ContentValues values= new ContentValues();

		values.put(DatabaseClass.GroupChats_Column6_MESSAGE_DELIVERY_STATUS, deliveryStatus);

		int i=db.update(DatabaseClass.Table2_Name, values, DatabaseClass.GroupChats_Column9_MESSAGE_ID+"='"+messageid+"' and "+DatabaseClass.GroupChats_Column8_GROUPINFO+"='"+groupInfo+"'",null);

		System.out.print("number of rows affected after update="+i);

		close();
	}

	//Used in GcmIntentService
	public void updateGroupArchivedStatus(String groupInfo,String archiveStatus) {
		// TODO Auto-generated method stub
		open();

		ContentValues values= new ContentValues();

		values.put(DatabaseClass.GroupChats_Column11_ARCHIVE_STATUS, archiveStatus);

		int i=db.update(DatabaseClass.Table2_Name, values,DatabaseClass.GroupChats_Column8_GROUPINFO+"='"+groupInfo+"'",null);

		System.out.print("number of rows affected after update="+i);

		close();
	}

	//Used in sqlitedataprovider
	public void updatePeerToPeerChatMessageSeenStatus(String messageid,String seenStatus) {
		// TODO Auto-generated method stub
		open();

		ContentValues values= new ContentValues();

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, seenStatus);

		int i=db.update(DatabaseClass.Table1_Name, values, DatabaseClass.Chats_Column9_MESSAGE_ID+"='"+messageid+"'",null);

		System.out.print("number of rows affected after update="+i);

		close();
	}

	//Used in sqlitedataprovider
	public void updatePeerToPeerProfilePic(String number,String profile_pic) {
		// TODO Auto-generated method stub
		open();

		ContentValues values= new ContentValues();

		values.put(DatabaseClass.Chats_Column7_PROFILE_ICON_LINK, profile_pic);

		int i=db.update(DatabaseClass.Table1_Name, values, DatabaseClass.Chats_Column1_NUMBER+"='"+number+"'",null);

		System.out.print("number of rows affected after update="+i);

		close();
	}

	//Used in GcmIntentService
	public void updatePeerToPeerChatMessageSeenStatus(String number) {
		// TODO Auto-generated method stub
		open();

		ContentValues values= new ContentValues();

		values.put(DatabaseClass.Chats_Column10_SEEN_STATUS, Constants.MESSAGE_SEEN);

		int i=db.update(DatabaseClass.Table1_Name, values, DatabaseClass.Chats_Column1_NUMBER+"='"+number+"'",null);

		System.out.print("number of rows affected after update="+i);

		close();
	}
	//Used in GcmIntentService
	public void updatePeerToPeerChatDeliveryStatus(String messageid,String deliveryStatus) {
		// TODO Auto-generated method stub
		open();

		ContentValues values= new ContentValues();

		values.put(DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS, deliveryStatus);

		int i=db.update(DatabaseClass.Table1_Name, values, DatabaseClass.Chats_Column9_MESSAGE_ID+"='"+messageid+"'",null);

		System.out.print("number of rows affected after update="+i);

		close();
	}

	//Used in GcmIntentService
	public void updatePeerToPeerMessage(String messageid,String number,String message) {
		// TODO Auto-generated method stub
		open();

		ContentValues values= new ContentValues();

		values.put(DatabaseClass.Chats_Column2_MESSAGE, message);

		int i=db.update(DatabaseClass.Table1_Name, values, DatabaseClass.Chats_Column9_MESSAGE_ID+"='"+messageid+"'  and "+ DatabaseClass.Chats_Column1_NUMBER+"='"+number+"'",null);

		System.out.print("number of rows affected after update="+i);

		close();
	}



	//Used in GcmIntentService
	public void updatePeerToPeerArchivedStatus(String senderNumber,String archivedStatus) {
		// TODO Auto-generated method stub
		open();

		ContentValues values= new ContentValues();

		values.put(DatabaseClass.Chats_Column11__ARCHIVE_STATUS, archivedStatus);

		int i=db.update(DatabaseClass.Table1_Name, values, DatabaseClass.Chats_Column1_NUMBER+"='"+senderNumber+"'",null);

		System.out.print("number of rows affected after update="+i);

		close();
	}
	// To get all the messages on the device
	public Cursor getAllMessagesPeerChat() {

		Cursor cursor = db.query(DatabaseClass.Table1_Name, new String[] {
				DatabaseClass.Chats_Column0_Id, DatabaseClass.Chats_Column1_NUMBER,
				DatabaseClass.Chats_Column2_MESSAGE,
				DatabaseClass.Chats_Column3_MESSAGE_TYPE,
				DatabaseClass.Chats_Column4_MESSAGE_TIME,
				DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS,
				DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS,
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,
				DatabaseClass.Chats_Column8_NAME,
				DatabaseClass.Chats_Column9_MESSAGE_ID,
				DatabaseClass.Chats_Column10_SEEN_STATUS,
				DatabaseClass.Chats_Column11__ARCHIVE_STATUS}, null, null, null,
				null, null);

		return cursor;
	}

	// To get all the messages on the device
	public Cursor getAllMessagesGroupChat() {

		Cursor cursor = db.query(DatabaseClass.Table2_Name, new String[] {
				DatabaseClass.GroupChats_Column0_Id, 
				DatabaseClass.GroupChats_Column1_SENDER_NUMBER,
				DatabaseClass.GroupChats_Column2_SENDER_NAME,
				DatabaseClass.GroupChats_Column3_MESSAGE,
				DatabaseClass.GroupChats_Column4_MESSAGE_TYPE,
				DatabaseClass.GroupChats_Column5_MESSAGE_TIME,
				DatabaseClass.GroupChats_Column6_MESSAGE_DELIVERY_STATUS,
				DatabaseClass.GroupChats_Column7_MESSAGE_INOUT_STATUS,
				DatabaseClass.GroupChats_Column8_GROUPINFO,
				DatabaseClass.GroupChats_Column9_MESSAGE_ID,
				DatabaseClass.GroupChats_Column10_SEEN_STATUS,
				DatabaseClass.GroupChats_Column11_ARCHIVE_STATUS}, 
				null, null, null,
				null, null);

		return cursor;
	}

	// To get the number of unseen messages of peer2peer table for a selected number by the user.
	public int getUnseenMessageCountPeer2Peer(String number) {
		int count = 0;
		open();
		Cursor cursor = db.query(DatabaseClass.Table1_Name, new String[] {
				DatabaseClass.Chats_Column0_Id, 
				DatabaseClass.Chats_Column1_NUMBER,				
				DatabaseClass.Chats_Column10_SEEN_STATUS },
				DatabaseClass.Chats_Column1_NUMBER + "='" + number + "' and "+DatabaseClass.Chats_Column10_SEEN_STATUS + "='" + Constants.MESSAGE_UNSEEN + "'", null, null,
				null, null, null);
		ArrayList<HashMap<String, String>> parametersForUpdatingUnseenMessages=new ArrayList<>();

		count = cursor.getCount();

		cursor.moveToFirst();

		for(int i=0;i<cursor.getCount();i++)
		{
			HashMap<String, String> parameters=new HashMap<>();

			parameters.put("number", cursor.getString(1));

			parametersForUpdatingUnseenMessages.add(parameters);

			cursor.moveToNext();
		}
		GlobalClass.parametersForUpdatingUnseenMessages=parametersForUpdatingUnseenMessages;

		close();
		return count;
	}

	// To get the number of unseen messages of peer2peer table for a selected number by the user.
	public int getUnseenMessageCountGroup(String groupInfo) {
		int count = 0;
		open();
		Cursor cursor = db.query(DatabaseClass.Table2_Name, new String[] {
				DatabaseClass.GroupChats_Column0_Id, 
				DatabaseClass.GroupChats_Column1_SENDER_NUMBER,
				DatabaseClass.GroupChats_Column8_GROUPINFO,
				DatabaseClass.GroupChats_Column10_SEEN_STATUS},
				DatabaseClass.GroupChats_Column8_GROUPINFO + "='" + groupInfo + "' and "+DatabaseClass.GroupChats_Column10_SEEN_STATUS + "='" + Constants.MESSAGE_UNSEEN + "'", null, null,
				null, null, null);

		ArrayList<HashMap<String, String>> parametersForUpdatingUnseenMessages=new ArrayList<>();

		count = cursor.getCount();

		cursor.moveToFirst();

		for(int i=0;i<cursor.getCount();i++)
		{
			HashMap<String, String> parameters=new HashMap<>();

			parameters.put("number", cursor.getString(1));

			parameters.put("groupInfo", cursor.getString(2));

			parametersForUpdatingUnseenMessages.add(parameters);

			cursor.moveToNext();
		}
		GlobalClass.parametersForUpdatingUnseenMessages=parametersForUpdatingUnseenMessages;
		close();
		return count;
	}

	// To get the number of records for a selected number by the user.
	public int getNoOfMessages(String number) {
		int count = 0;
		open();
		Cursor cursor = db.query(DatabaseClass.Table1_Name, new String[] {
				DatabaseClass.Chats_Column0_Id, DatabaseClass.Chats_Column1_NUMBER,
				DatabaseClass.Chats_Column2_MESSAGE,
				DatabaseClass.Chats_Column3_MESSAGE_TYPE,
				DatabaseClass.Chats_Column4_MESSAGE_TIME,
				DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS,
				DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS,
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,DatabaseClass.Chats_Column8_NAME },
				DatabaseClass.Chats_Column1_NUMBER + "='" + number + "'", null, null,
				null, null, null);

		count = cursor.getCount();
		close();
		return count;
	}

	// To get all the records on the device
	public int getNoOfMessages() {
		int count = 0;
		open();
		Cursor cursor = db.query(DatabaseClass.Table1_Name, new String[] {
				DatabaseClass.Chats_Column0_Id, DatabaseClass.Chats_Column1_NUMBER,
				DatabaseClass.Chats_Column2_MESSAGE,
				DatabaseClass.Chats_Column3_MESSAGE_TYPE,
				DatabaseClass.Chats_Column4_MESSAGE_TIME,
				DatabaseClass.Chats_Column5_MESSAGE_DELIVERY_STATUS,
				DatabaseClass.Chats_Column6_MESSAGE_INOUT_STATUS,
				DatabaseClass.Chats_Column7_PROFILE_ICON_LINK,DatabaseClass.Chats_Column8_NAME }, null, null, null,
				null, null);

		count = cursor.getCount();
		close();
		return count;
	}

	// To delete all the messages of a particular number.
	public void deletePeer2peerMessages(String number) {
		// TODO Auto-generated method stub
		open();

		db.delete(DatabaseClass.Table1_Name, DatabaseClass.Chats_Column1_NUMBER + "='"
				+ number + "'", null);
		close();
	}
	// To delete all the messages of a particular number.
	public void deletePeer2peerMessages(String number,String messageId) {
		// TODO Auto-generated method stub
		open();

		db.delete(DatabaseClass.Table1_Name, DatabaseClass.Chats_Column1_NUMBER + "='"
				+ number + "' and "+DatabaseClass.Chats_Column9_MESSAGE_ID+"='"+messageId+"'", null);
		close();
	}
	// To delete all the messages of a particular number.
	public void deleteGroupMessages(String groupInfo) {
		// TODO Auto-generated method stub
		open();

		db.delete(DatabaseClass.Table2_Name, DatabaseClass.GroupChats_Column8_GROUPINFO + "='"
				+ groupInfo + "'", null);
		close();
	}

	// To delete all the messages of a particular number.
	public void deleteGroupMessages(String senderNumber,String messageId) {
		// TODO Auto-generated method stub
		open();

		db.delete(DatabaseClass.Table2_Name, DatabaseClass.GroupChats_Column1_SENDER_NUMBER + "='"
				+ senderNumber + "' and "+DatabaseClass.GroupChats_Column9_MESSAGE_ID+"='"+messageId+"'", null);
		close();
	}

	// To delete all the messages selected by the user for a particular number.
	public void deleteMessages(String number, String time) {
		// TODO Auto-generated method stub
		open();

		db.delete(DatabaseClass.Table1_Name, DatabaseClass.Chats_Column1_NUMBER + "='"
				+ number + "' and " + DatabaseClass.Chats_Column4_MESSAGE_TIME + "='"
				+ time + "'", null);
		close();
	}

	// To delete all the message on the device
	public void deleteMessages() {
		// TODO Auto-generated method stub
		open();

		db.delete(DatabaseClass.Table1_Name, null, null);

		close();
	}

	public String group2InfoInJson(String groupName,String groupId,String sendername,String senderNumber,String senderProfilePic)
	{
		JSONObject jobj_main=new JSONObject();
		JSONObject jobj_member=new JSONObject();
		JSONObject jobj_admin_info=new JSONObject();
		JSONArray membersArray=new JSONArray();

		try {

			jobj_main.put("GroupName", "Smart World");

			jobj_main.put("GroupId", "2");

			jobj_member.put("Name", "Sangharsh");
			jobj_member.put("Number", "998347821");
			jobj_member.put("ProfilePic", "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");
			membersArray.put(jobj_member);

			jobj_member=new JSONObject();

			jobj_member.put("Name", "Amit");
			jobj_member.put("Number", "9007247821");
			jobj_member.put("ProfilePic", "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");
			membersArray.put(jobj_member);

			jobj_member=new JSONObject();

			jobj_member.put("Name", "Jyoti Raj");
			jobj_member.put("Number", "9911904562");
			jobj_member.put("ProfilePic", "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");
			membersArray.put(jobj_member);

			jobj_member=new JSONObject();

			jobj_member.put("Name", "Sunil");
			jobj_member.put("Number", "99325187821");
			jobj_member.put("ProfilePic", "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");
			membersArray.put(jobj_member);

			jobj_main.put("Members", membersArray);

			jobj_admin_info.put("Name", "Sunil");
			jobj_admin_info.put("Number", "99325187821");
			jobj_admin_info.put("ProfilePic", "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");

			jobj_main.put("Admin", jobj_admin_info);

			jobj_main.put("ProfilePic", "http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");



			jobj_main.put("Status", "Be happy");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return jobj_main.toString();
	}


}
