package com.connectcribmessenger.comparators;

import java.util.Comparator;

import com.connectcribmessenger.entities.Country;

public class ComparatorCountry implements Comparator<Country> {

	
	@Override
	public int compare(Country arg0,
			Country arg1) {
		return arg0.getName().compareToIgnoreCase(arg1.getName());
		
	}

}
