package com.connectcribmessenger.comparators;

import java.util.Comparator;
import java.util.HashMap;

import com.connectcribmessenger.entities.RecentChat;

public class MyComparatorTime implements Comparator<RecentChat> {
	

	@Override
	public int compare(RecentChat arg0,
			RecentChat arg1) {
		
		/*//for ascending order 
		 if (Long.parseLong(arg0.getTime()) >Long.parseLong(arg1.getTime())) {
		        return 1;
		    } else if (Long.parseLong(arg0.getTime()) < Long.parseLong(arg1.getTime())) {
		        return -1;
		    }*/
		 
		//for descending order 
		 if (Long.parseLong(arg0.getTime()) >Long.parseLong(arg1.getTime())) {
		        return -1;
		    } else if (Long.parseLong(arg0.getTime()) < Long.parseLong(arg1.getTime())) {
		        return 1;
		    }
		return 0;
		
	}
}
