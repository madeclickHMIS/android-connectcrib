package com.connectcribmessenger.interfaces;

public interface CommonMethods 
{
	public void instantiateNonViewClasses();

	public void instantiateViews();

	public void setproperties();
	
	public void setViewListeners();

}
