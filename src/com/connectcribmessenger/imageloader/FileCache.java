package com.connectcribmessenger.imageloader;
 
import java.io.File;

import android.content.Context;
import android.util.Log;
 
public class FileCache {
     
    private File cacheDir;
     
    public FileCache(Context context){
         
        //Find the dir at SDCARD to save cached images
         
        if (android.os.Environment.getExternalStorageState().equals(
                                     android.os.Environment.MEDIA_MOUNTED))
        {
            //if SDCARD is mounted (SDCARD is present on device and mounted)
            cacheDir = new File(
                       android.os.Environment.getExternalStorageDirectory(),"LazyList");
            Log.i("cacheDir", "MEDIA_MOUNTED");
        }
        else
        {
            // if checking on simulator the create cache dir in your application context
            cacheDir=context.getCacheDir();
            Log.i("cacheDir", "MEDIA_NotMOUNTED");
        }
         
        if(!cacheDir.exists()){
            // create cache dir in your application context
            cacheDir.mkdirs();
            Log.i("cacheDir", "!cacheDir.exists()");
        }
    }
     
    public File getFile(String url){
        //Identify images by hashcode or encode by URLEncoder.encode.
        String filename=String.valueOf(url.hashCode());
         
        File f = new File(cacheDir, filename);
        Log.i("File f Name", ""+f.getName());
        Log.i("File f AbsolutePath", ""+f.getAbsolutePath());
        return f;
         
    }
     
    public void clear(){
        // list all files inside cache directory
        File[] files=cacheDir.listFiles();
        if(files==null)
            return;
        //delete all cache directory files
        for(File f:files)
            f.delete();
    }
 
}