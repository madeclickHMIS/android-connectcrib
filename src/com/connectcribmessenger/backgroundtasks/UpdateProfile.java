package com.connectcribmessenger.backgroundtasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;

public class UpdateProfile extends AsyncTask<String,Void,String> 
{
	private ProgressDialog pd;

	private Activity mActivity;

	private Handler mHandler;

	public UpdateProfile(Activity mActivity,Handler mHandler)
	{
		this.mActivity=mActivity;

		this.mHandler=mHandler;
	}
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();

		pd=new ProgressDialog(mActivity);

		pd.setMessage("Saving Profile..");

		pd.setCancelable(false);

		pd.show();
	}
	@Override
	protected String doInBackground(String... params)
	{


		HttpParams myParams = new BasicHttpParams();

		//HttpConnectionParams.setConnectionTimeout(myParams, 10000);

		//HttpConnectionParams.setSoTimeout(myParams, 10000);

		//HttpClient httpclient = new DefaultHttpClient(myParams );

		HttpClient httpclient=GlobalClass.getHttpClient();


		try {

			HttpPost httpPost = new HttpPost(Constants.APP_SERVER_ROOT_PATH);

			List<NameValuePair> nameValuePair=new ArrayList<NameValuePair>();	

			GlobalClass.printDebugLog("SendSmsForOtp", "Api-->"+Constants.APP_SERVER_ROOT_PATH);

			GlobalClass.printDebugLog("SendSmsForOtp", "key-->"+Constants.API_UPDATE_PROFILE_PARAMETER_1_VALUE);

			GlobalClass.printDebugLog("SendSmsForOtp", "userId-->"+params[5]);

			GlobalClass.printDebugLog("SendSmsForOtp", "number-->"+params[0]);

			GlobalClass.printDebugLog("SendSmsForOtp", "name-->"+params[1]);

			GlobalClass.printDebugLog("SendSmsForOtp", "mood-->"+params[2]);

			GlobalClass.printDebugLog("SendSmsForOtp", "status-->"+params[3]);

			//GlobalClass.printDebugLog("SendSmsForOtp", "profile_pic-->"+params[4]);

			nameValuePair.add(new BasicNameValuePair(Constants.API_UPDATE_PROFILE_PARAMETER_1,Constants.API_UPDATE_PROFILE_PARAMETER_1_VALUE));

			nameValuePair.add(new BasicNameValuePair(Constants.API_UPDATE_PROFILE_PARAMETER_2,params[5]));

			nameValuePair.add(new BasicNameValuePair(Constants.API_UPDATE_PROFILE_PARAMETER_4,params[0]));

			nameValuePair.add(new BasicNameValuePair(Constants.API_UPDATE_PROFILE_PARAMETER_5,params[1]));

			nameValuePair.add(new BasicNameValuePair(Constants.API_UPDATE_PROFILE_PARAMETER_6,params[2]));

			nameValuePair.add(new BasicNameValuePair(Constants.API_UPDATE_PROFILE_PARAMETER_7,params[3]));

			if(params[4]!=null)
			{
				nameValuePair.add(new BasicNameValuePair(Constants.API_UPDATE_PROFILE_PARAMETER_8,params[4]));	
			}




			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));  


			HttpResponse response = httpclient.execute(httpPost);

			String jsonData = EntityUtils.toString(response.getEntity());
			
			
			
			

			GlobalClass.printDebugLog("ConnectCrib Serever Data", "update profile on connectcrib-->\n"+jsonData);


			JSONObject jobj=new JSONObject(jsonData);

			//GlobalClass.safeClose(httpclient);

			return jobj.toString();


		} 

		catch (ClientProtocolException e)
		{
			GlobalClass.printDebugLog("UpdateProfile Exception", "ClientProtocolException while getting connectcrib contacts");

			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			GlobalClass.printDebugLog("UpdateProfile Exception", "IOException while getting connectcrib contacts");
			e.printStackTrace();

		} 
		catch (JSONException e) 
		{
			GlobalClass.printDebugLog("UpdateProfile Exception", "JSONException while getting connectcrib contacts");

			e.printStackTrace();
		}

		catch (Exception e) {
			e.printStackTrace();
		}

		//GlobalClass.safeClose(httpclient);
		
		return null;  

	}

	@Override
	protected void onPostExecute(String result)
	{
		if(pd!=null)
		{
			pd.dismiss();
			pd=null;
		}
		Message m=new Message();

		Bundle b=new Bundle();

		if(result!=null)
		{
			b.putString("UpdateProfileAsyncResult", result);	
		}
		else
		{
			b.putString("UpdateProfileAsyncResult", "nodata");
		}

		m.setData(b);

		mHandler.sendMessage(m);




	}
}
