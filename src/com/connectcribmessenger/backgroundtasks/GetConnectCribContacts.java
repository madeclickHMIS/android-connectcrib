package com.connectcribmessenger.backgroundtasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.entities.Contact;

public class GetConnectCribContacts extends AsyncTask<String,Void,ArrayList<Contact>> {

	private ProgressDialog pd;

	private Activity mActivity;

	private Handler mHandler;

	public GetConnectCribContacts(Activity mActivity,Handler mHandler)
	{
		this.mActivity=mActivity;

		this.mHandler=mHandler;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		pd=new ProgressDialog(mActivity);

		pd.setMessage("Getting Contacts..");

		pd.setCancelable(false);

		pd.show();	

	}
	@Override
	protected ArrayList<Contact> doInBackground(String... params)
	{
		ArrayList<Contact> connectCribContactList=new ArrayList<>();

		//HttpParams myParams = new BasicHttpParams();

		//HttpConnectionParams.setConnectionTimeout(myParams, 30000);

		//HttpConnectionParams.setSoTimeout(myParams, 30000);

		//		HttpClient httpclient = new DefaultHttpClient(myParams );

		HttpClient httpclient = GlobalClass.getHttpClient();


		try {

			HttpPost httpPost = new HttpPost(Constants.APP_SERVER_ROOT_PATH);

			List<NameValuePair> nameValuePair=new ArrayList<NameValuePair>();	

			/*GlobalClass.printDebugLog("SendSmsForOtp", "Api-->"+Constants.APP_SERVER_ROOT_PATH);

			GlobalClass.printDebugLog("SendSmsForOtp", "key-->"+Constants.API_SEND_SMS_PARAMETER_1_VALUE);

			GlobalClass.printDebugLog("SendSmsForOtp", "userId-->"+GlobalClass.REGISTRATION_ID);

			GlobalClass.printDebugLog("SendSmsForOtp", "number-->"+number);
			 */
			nameValuePair.add(new BasicNameValuePair(Constants.API_GET_CONNECTCRIB_CONTACTS_PARAMETER_1,Constants.API_GET_CONNECTCRIB_CONTACTS_PARAMETER_1_VALUE));

			nameValuePair.add(new BasicNameValuePair(Constants.API_GET_CONNECTCRIB_CONTACTS_PARAMETER_2,params[0]));	



			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));  



			//httppost.setHeader("Content-type", "application/json");

			//StringEntity se = new StringEntity(params[0]); 

			//se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

			//httppost.setEntity(se); 

			HttpResponse response = httpclient.execute(httpPost);

			String jsonData = EntityUtils.toString(response.getEntity());

			GlobalClass.printDebugLog("ContactListAll Serever Data", "Contacts on connectcrib-->\n"+jsonData);



			JSONObject jobj=new JSONObject(jsonData);

			if(jobj.getString("success").equals("0"))
			{
				Toast.makeText(mActivity, "No Contacts Available", Toast.LENGTH_LONG).show();
			}
			else
			{
				JSONArray jarray=jobj.getJSONArray("data");

				for(int i=0;i<jarray.length();i++)
				{
					Contact contact=new Contact();

					jobj=jarray.getJSONObject(i);

					contact.setName(jobj.getString("name"));

					contact.setNumber(jobj.getString("number"));

					contact.setMood(jobj.getString("latest_mood"));

					contact.setAvailabilityStatus(jobj.getString("status"));

					contact.setProfilePicPath(jobj.getString("profile_image"));

					connectCribContactList.add(contact);

				}
			}

			//GlobalClass.safeClose(httpclient);

			return connectCribContactList;


		} 

		catch (ClientProtocolException e)
		{
			GlobalClass.printDebugLog("ConnectCrib Serever Data", "ClientProtocolException while getting connectcrib contacts");

			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			GlobalClass.printDebugLog("ConnectCrib Serever Data", "IOException while getting connectcrib contacts");
			e.printStackTrace();

			//Toast.makeText(mActivity, "Connection timeout. Please try again.", Toast.LENGTH_LONG).show();

		} 
		catch (JSONException e) 
		{
			GlobalClass.printDebugLog("ConnectCrib Serever Data", "JSONException while getting connectcrib contacts");

			e.printStackTrace();
		}

		catch (Exception e) {
			e.printStackTrace();
		}

		//	GlobalClass.safeClose(httpclient);
		return null;  
	}

	@Override
	protected void onPostExecute(ArrayList<Contact> connectCribContactList)
	{
		pd.dismiss();

		Message m=new Message();

		Bundle b=new Bundle();

		if(connectCribContactList!=null)
		{
			b.putParcelableArrayList("GetConnectCribContactsAsyncResult", connectCribContactList);	
		}
		else
		{
			ArrayList<Contact> connectCribContactList2=new ArrayList<>();

			b.putParcelableArrayList("GetConnectCribContactsAsyncResult", connectCribContactList2);
		}


		m.setData(b);

		mHandler.sendMessage(m);


	}

}
