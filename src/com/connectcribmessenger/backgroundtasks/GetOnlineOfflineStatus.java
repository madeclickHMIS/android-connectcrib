package com.connectcribmessenger.backgroundtasks;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.DatabaseAccessor;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.entities.Profile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class GetOnlineOfflineStatus extends AsyncTask<String,Void,String> 
{

	private Activity mActivity;

	private Handler mHandler;

	public GetOnlineOfflineStatus(Activity mActivity,Handler mHandler)
	{
		this.mActivity=mActivity;

		this.mHandler=mHandler;
	}
	@Override
	protected String doInBackground(String... params) 
	{
		boolean run=true;

		String status=null;

		DatabaseAccessor dbaccessor=new DatabaseAccessor(mActivity);

		dbaccessor.open();

		Cursor c=dbaccessor.getProfile();

		c.moveToFirst();

		Profile profile=new Profile();

		profile.setProfileId(c.getInt(0));

		profile.setProfileName(c.getString(1));

		profile.setProfileNumber(c.getString(2));

		profile.setProfilePic(c.getString(3));

		profile.setProfileStatus(c.getString(4));

		GlobalClass.profileData=profile;


		c.close();

		dbaccessor.close();

		HttpParams myParams = new BasicHttpParams();

		HttpConnectionParams.setConnectionTimeout(myParams, 10000);

		HttpConnectionParams.setSoTimeout(myParams, 10000);

		HttpClient httpclient = new DefaultHttpClient(myParams );

		while (run)
		{
			 if (isCancelled())  
		         break;

			try {

				HttpPost httpPost = new HttpPost(Constants.APP_SERVER_ROOT_PATH);

				List<NameValuePair> nameValuePair=new ArrayList<NameValuePair>();	


				GlobalClass.printDebugLog("get online status", "conversation number-->"+GlobalClass.conversationInfo.getConversationNumber());

				nameValuePair.add(new BasicNameValuePair(Constants.API_GET_ONLINEOFFLINE_STATUS_PARAMETER_1,Constants.API_GET_ONLINEOFFLINE_STATUS_PARAMETER_1_VALUE));

				nameValuePair.add(new BasicNameValuePair(Constants.API_GET_ONLINEOFFLINE_STATUS_PARAMETER_2,GlobalClass.conversationInfo.getConversationNumber()));	



				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));  




				HttpResponse response = httpclient.execute(httpPost);

				String jsonData = EntityUtils.toString(response.getEntity());

				GlobalClass.printDebugLog("GetOnlineOfflineAsyncResult", "getOnlineOfflineStatus-->\n"+jsonData);



				JSONObject jobj=new JSONObject(jsonData);

				if(jobj.getString("success").equals("0"))
				{
					run=true;

				}
				else
				{
					status= jobj.getString("data");


					break;
				}

			} 
			catch(NullPointerException nullexception)
			{
				nullexception.printStackTrace();
				break;
			}
			catch (Exception e) 
			{
				run=true;

				e.printStackTrace();

			}
		}

		return status;
	}
	@Override
	protected void onPostExecute(String result) 
	{

		if(!isCancelled())
		{
			Message m=new Message();

			Bundle b=new Bundle();

			if(result!=null)
			{
				GlobalClass.printDebugLog("GetOnlineOfflineAsyncResult", "getOnlineOfflineStatus-->\n"+result);
				
				b.putString("GetOnlineOfflineAsyncResult", result);	
			}
			else
			{
				b.putString("GetOnlineOfflineAsyncResult", "nodata");
			}

			m.setData(b);

			mHandler.sendMessage(m);
		}
		

	}
}
