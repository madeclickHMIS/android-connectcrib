package com.connectcribmessenger.backgroundtasks;

import java.util.ArrayList;

import org.json.JSONArray;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;

import com.connectcribmessenger.auxilliaryclasses.GlobalClass;


public class SaveContacts extends AsyncTask<Void, Void, String> {
	Activity mActivity;
	ArrayList<JSONArray> jsonArrays;
	int index = 0;
	String number;
	Handler handler;
	public SaveContacts(Activity activity,Handler handler, String number) {
		jsonArrays = new ArrayList<JSONArray>();
		mActivity = activity;
		this.number = number;
		this.handler=handler;
	}

	@Override
	protected String doInBackground(Void... params) {
		// TODO Auto-generated method stub
		GlobalClass.name=readContacts(number);
		return null;
	}
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		Message m=new Message();

		Bundle b=new Bundle();

		b.putString("name", result);


		handler.sendMessage(m);

		//pd.dismiss();
	}
	public String readContacts(String number) {

		try {

			ContentResolver cr = mActivity.getContentResolver();
			Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
					null, null, null);
			String phone = null;
			// System.out.println("Cur get Count:: "+cur.getCount());
			if (cur.getCount() > 0) {
				while (cur.moveToNext()) {
					String id = cur.getString(cur
							.getColumnIndex(ContactsContract.Contacts._ID));
					String name = cur
							.getString(cur
									.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

					if (Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
						Cursor pCur = cr
								.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
										null,
										ContactsContract.CommonDataKinds.Phone.CONTACT_ID
										+ " = ?", new String[] { id },
										null);
						while (pCur.moveToNext()) {
							phone = pCur
									.getString(pCur
											.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

						}
						if (phone.equals(number)) {
							pCur.close();
							return name;
						}

					}

				}
				return number;
			}
			cur.close();
		} catch (Exception e) {
			System.out.println("Exception in getting  contacts: " + e);
		}
		return number;
	}

}
