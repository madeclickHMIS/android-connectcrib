package com.connectcribmessenger.backgroundtasks;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.ContactsContract;

import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.entities.Contact;

public class GetContactsFromPhone extends AsyncTask<String,Void,ArrayList<Contact>> {
	private ProgressDialog pd;

	private Activity mActivity;
	
	private Handler mHandler;

	public GetContactsFromPhone(Activity mActivity,Handler mHandler)
	{
		this.mActivity=mActivity;
		
		this.mHandler=mHandler;
	}
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		pd=new ProgressDialog(mActivity);

		pd.setMessage("Getting Contacts..");

		pd.setCancelable(false);

		pd.show();	

	}
	@Override
	protected ArrayList<Contact> doInBackground(String... params)
	{
		return getContacts();  
	}

	@Override
	protected void onPostExecute(ArrayList<Contact> contactList)
	{
		pd.dismiss();

		/*converting the contactlist into json format*/


		JSONArray jsonArrayContacts=new JSONArray();
		JSONObject jsonObjectContact=new JSONObject();
		GlobalClass.printDebugLog("ConnectCrib", "device contacts size: "+contactList.size());
		for(int i=0;i<contactList.size();i++)
		{


			try 
			{
				JSONObject jobj=new JSONObject();
				jobj.put("number", contactList.get(i).getNumber());

				jsonArrayContacts.put(jobj);

			} 
			catch (JSONException e) 
			{
				// TODO Auto-generated catch block

				GlobalClass.printDebugLog("ConnectCrib Exception", "JSONException while converting device contact into json format");

				e.printStackTrace();

				return;
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		try {

			GlobalClass.printDebugLog("ConnectCrib", "json contacts size: "+jsonArrayContacts.length());

			jsonObjectContact.put("contacts", jsonArrayContacts);

			GlobalClass.printDebugLog("ConnectCrib", "JsonFormat of device contacts-->"+jsonObjectContact.toString());

		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}




		/*This background task to get all the connectcrib contacts available on
		 *the device*/

		if(GlobalClass.isInternetAvailable(mActivity))
		{
			new GetConnectCribContacts(mActivity, mHandler).execute(jsonObjectContact.toString());
		}
		else
		{
			//If no internet connection is available then dialog is displayed
			//showDialog(Constants.DIALOG_GCM_REGISTRATION_NO_INTERNET,"Internet");

			new AlertDialog.Builder(mActivity)
			.setTitle("ConnectCrib")
			.setMessage("No Internet Connection Available.")

			.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) { 
					dialog.dismiss();
				}
			})
			.setIcon(android.R.drawable.ic_dialog_alert)
			.show();
		}


	}
	
	private ArrayList<Contact> getContacts() 
	{
		ArrayList<Contact> contactList=new ArrayList<>();
		ContentResolver cr=mActivity.getContentResolver();
		Cursor cur=cr.query(ContactsContract.Contacts.CONTENT_URI,null,null,null,null);
		if (cur.getCount() > 0) 
		{
			GlobalClass.printDebugLog("number Size", "Device Contact List Length-->"+cur.getCount());
			while (cur.moveToNext()) 
			{
				Contact contact=new Contact();
				String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
				String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				if(Integer.parseInt(cur.getString 
						(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER )))  > 0) 
				{
					//Query phone here.  Covered next
					Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
							null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?", new 
							String[]{id}, null);
					while (pCur.moveToNext()) 
					{
						// Do something with phones
						String pnumber 
						=pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

						GlobalClass.printDebugLog("profile number", GlobalClass.profileData.getProfileNumber());
						GlobalClass.printDebugLog("contact", pnumber);
						if(pnumber!=null&&pnumber.length()>0&&!pnumber.equals(GlobalClass.profileData.getProfileNumber()))
						{
							contact.setNumber(pnumber);

							contact.setName(name.trim().toLowerCase());

							contact.setStatus("Hey there! I am using ConnectCribMessenger");

							contact.setProfilePicPath("http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");
						}



					} 
					pCur.close();
				}


				contactList.add(contact);


			}

			GlobalClass.printDebugLog("number Size", "Contact List Length-->"+contactList.size());
		}
		cur.close();
		return contactList;
	}

}
