package com.connectcribmessenger.backgroundtasks;

import java.math.BigDecimal;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.widget.TextView;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.entities.Video;

public class GetVideoDetail extends AsyncTask<Void,Void,Void> 
{
	private Activity mActivity;

	private TextView videoDuration;

	private TextView videoTime;
	
	private String videoUrl;

	public GetVideoDetail(Activity mActivity,TextView videoDuration,TextView videoTime,String videoUrl)
	{
		this.mActivity=mActivity;

		this.videoDuration=videoDuration;

		this.videoTime=videoTime;
	}
	protected void onPreExecute() 
	{

	};
	@Override
	protected Void doInBackground(Void... params) 
	{/*
		String[] proj = { MediaStore.Video.Media._ID,
				MediaStore.Video.Media.DATA,
				MediaStore.Video.Media.DISPLAY_NAME,
				MediaStore.Video.Media.SIZE,
				MediaStore.Video.Media.DURATION};
		Cursor videocursor = managedQuery(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
	                proj, null, null, null);
		Cursor videocursor = mActivity.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
				proj, null, null, MediaStore.Video.Media.DATE_TAKEN + " DESC");

		int count = videocursor.getCount();

		GlobalClass.printDebugLog("VideoListScreen", "Total Number of videos-->"+count);

		while(videocursor.moveToNext())
		{
			long durationInMillis=Long.parseLong(videocursor.getString(4));

			float durationInSeconds=durationInMillis/1000;

			BigDecimal durationInMinutes=round((durationInSeconds/60),2);

			long sizeInBytes=Long.parseLong(videocursor.getString(3));

			float sizeInKiloBytes=sizeInBytes/1024;

			//float durationInMegaByte=durationInKiloBytes/1024;
			BigDecimal sizeInMegaByte=round(sizeInKiloBytes/1024, 2);

			if(sizeInMegaByte.intValue()<Constants.VIDEO_MAX_SiZE_IN_MB&&durationInMinutes.intValue()<Constants.VIDEO_MAX_DURATION_IN_MIN)
			{
				Video video=new Video();

				video.setTitle(videocursor.getString(2));

				video.setPath(videocursor.getString(1));



				//int remainingSizeInSeconds=(int)(sizeInSeconds%60);

				video.setSize(sizeInMegaByte.floatValue()+" mb");



				video.setDuration(durationInMinutes.floatValue()+" min");

				//videoList.add(video);	
			}


		}
		return null;
	*/
		return null;}
	@Override
	protected void onPostExecute(Void result) 
	{
		// TODO Auto-generated method stub
		super.onPostExecute(result);
	}
}
