package com.connectcribmessenger.backgroundtasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class ConfirmOtp extends AsyncTask<Void,Void,String> 
{
	private Handler mHandler;

	private String mOtp;

	private String mNumber;

	public ConfirmOtp(Handler handler,String Otp,String number)
	{
		mHandler=handler;

		mOtp=Otp;

		mNumber=number;
	}
	@Override
	protected String doInBackground(Void... params) {
		String JSON_Response=null;

		HttpParams httpParameters = new BasicHttpParams();

		HttpConnectionParams.setConnectionTimeout(httpParameters, 50000);

		HttpConnectionParams.setSoTimeout(httpParameters, 50000);

		HttpClient httpClient=new DefaultHttpClient(httpParameters);

		HttpPost httpPost=new HttpPost(Constants.APP_SERVER_ROOT_PATH);


		HttpResponse httpResponse;
		try 
		{
			List<NameValuePair> nameValuePair=new ArrayList<NameValuePair>();	

			GlobalClass.printDebugLog("ConfirmOtp", "Api-->"+Constants.APP_SERVER_ROOT_PATH);

			GlobalClass.printDebugLog("ConfirmOtp", "key-->"+Constants.API_CONFIRM_OTP_PARAMETER_1_VALUE);

			GlobalClass.printDebugLog("ConfirmOtp", "userId-->"+GlobalClass.REGISTRATION_ID);

			GlobalClass.printDebugLog("ConfirmOtp", "number-->"+mNumber);

			GlobalClass.printDebugLog("ConfirmOtp", "otp-->"+mOtp);


			nameValuePair.add(new BasicNameValuePair(Constants.API_CONFIRM_OTP_PARAMETER_1,Constants.API_CONFIRM_OTP_PARAMETER_1_VALUE));

			nameValuePair.add(new BasicNameValuePair(Constants.API_CONFIRM_OTP_PARAMETER_2,mNumber));	

			nameValuePair.add(new BasicNameValuePair(Constants.API_CONFIRM_OTP_PARAMETER_3,mOtp));

			nameValuePair.add(new BasicNameValuePair(Constants.API_CONFIRM_OTP_PARAMETER_4,GlobalClass.REGISTRATION_ID));

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));  
			
			GlobalClass.printDebugLog("ConfirmOtp", "URL-->"+httpPost.getURI().toString());

			httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity=httpResponse.getEntity();

			JSON_Response=EntityUtils.toString(httpEntity);
			
			GlobalClass.printDebugLog("ConfirmOtp", "ConfirmOtp Response-->"+JSON_Response);

			return JSON_Response;

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		

		return JSON_Response;
	}

	@Override
	protected void onPostExecute(String result) {
		Message msg=new Message();

		Bundle b=new Bundle();

		if(result!=null)
		{
			try {
				JSONObject jobj=new JSONObject(result);

				b.putString("ConfirmOtp", jobj.getString("success"));

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				b.putString("ConfirmOtp", "2");
			}	
		}
		else
		{
			b.putString("ConfirmOtp", "2");
		}



		msg.setData(b);

		mHandler.sendMessage(msg);
	}

}
