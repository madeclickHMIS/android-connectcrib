package com.connectcribmessenger.backgroundtasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;

public class SendSmsForOtp extends AsyncTask<Void,Void,String> 
{

	private Handler mHandler;
	
	private String number;
	
	public SendSmsForOtp(Handler handler,String number)
	{
		mHandler=handler;
		
		this.number=number;
	}
	@Override
	protected String doInBackground(Void... params) {
		String JSON_Response=null;

		HttpParams httpParameters = new BasicHttpParams();

		HttpConnectionParams.setConnectionTimeout(httpParameters, 50000);

		HttpConnectionParams.setSoTimeout(httpParameters, 50000);

		HttpClient httpClient=new DefaultHttpClient(httpParameters);

		HttpPost httpPost=new HttpPost(Constants.APP_SERVER_ROOT_PATH);


		HttpResponse httpResponse;
		try 
		{
			List<NameValuePair> nameValuePair=new ArrayList<NameValuePair>();	
			
			GlobalClass.printDebugLog("SendSmsForOtp", "Api-->"+Constants.APP_SERVER_ROOT_PATH);
			
			GlobalClass.printDebugLog("SendSmsForOtp", "key-->"+Constants.API_SEND_SMS_PARAMETER_1_VALUE);
			
			GlobalClass.printDebugLog("SendSmsForOtp", "userId-->"+GlobalClass.REGISTRATION_ID);
			
			GlobalClass.printDebugLog("SendSmsForOtp", "number-->"+number);

			nameValuePair.add(new BasicNameValuePair(Constants.API_SEND_SMS_PARAMETER_1,Constants.API_SEND_SMS_PARAMETER_1_VALUE));

			nameValuePair.add(new BasicNameValuePair(Constants.API_SEND_SMS_PARAMETER_2,GlobalClass.REGISTRATION_ID));	

			nameValuePair.add(new BasicNameValuePair(Constants.API_SEND_SMS_PARAMETER_3,number));

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));  
			
			httpResponse = httpClient.execute(httpPost);
			
			HttpEntity httpEntity=httpResponse.getEntity();

			JSON_Response=EntityUtils.toString(httpEntity);

			return JSON_Response;

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		GlobalClass.printDebugLog("SendSmsForOtp", "SendSmsForOtp Response-->"+JSON_Response);

		return JSON_Response;
	}

	@Override
	protected void onPostExecute(String result) 
	{
		Message msg=new Message();
		
		Bundle b=new Bundle();
		
		if(result!=null)
		{
			GlobalClass.printDebugLog("SendSmsForOtp", "SendSmsForOtp Response-->"+result);
			try {
				JSONObject jobj=new JSONObject(result);
				
				b.putString("SendSmsForOtp", jobj.getString("success"));
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				b.putString("SendSmsForOtp", "0");
			}	
		}
		else
		{
			b.putString("SendSmsForOtp", "0");
		}
		
		
		
		msg.setData(b);
		
		mHandler.sendMessage(msg);
		
		
	}

}
