package com.connectcribmessenger.backgroundtasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;

public class GetProfile extends AsyncTask<String,Void,JSONObject> 
{
	private ProgressDialog pd;

	private Activity mActivity;

	private Handler mHandler;

	public GetProfile(Activity mActivity,Handler mHandler)
	{
		this.mActivity=mActivity;

		this.mHandler=mHandler;
	}
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();

		pd=new ProgressDialog(mActivity);

		pd.setMessage("Getting profile..");

		pd.setCancelable(false);

		pd.show();

	}
	@Override
	protected JSONObject doInBackground(String... params)
	{
		//ArrayList<Contact> connectCribContactList=new ArrayList<>();

		/*HttpParams myParams = new BasicHttpParams();

		HttpConnectionParams.setConnectionTimeout(myParams, 30000);

		HttpConnectionParams.setSoTimeout(myParams, 30000);*/

		//		HttpClient httpclient = new DefaultHttpClient(myParams );
		HttpClient httpclient = GlobalClass.getHttpClient();


		try {

			HttpPost httpPost = new HttpPost(Constants.APP_SERVER_ROOT_PATH);

			List<NameValuePair> nameValuePair=new ArrayList<NameValuePair>();	

			/*GlobalClass.printDebugLog("SendSmsForOtp", "Api-->"+Constants.APP_SERVER_ROOT_PATH);

			GlobalClass.printDebugLog("SendSmsForOtp", "key-->"+Constants.API_SEND_SMS_PARAMETER_1_VALUE);

			GlobalClass.printDebugLog("SendSmsForOtp", "userId-->"+GlobalClass.REGISTRATION_ID);

			GlobalClass.printDebugLog("SendSmsForOtp", "number-->"+number);
			 */
			nameValuePair.add(new BasicNameValuePair(Constants.API_GET_CONNECTCRIB_CONTACTS_PARAMETER_1,Constants.API_GET_CONNECTCRIB_CONTACTS_PARAMETER_1_VALUE));

			nameValuePair.add(new BasicNameValuePair(Constants.API_GET_CONNECTCRIB_CONTACTS_PARAMETER_2,params[0]));	



			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));  


			HttpResponse response = httpclient.execute(httpPost);

			String jsonData = EntityUtils.toString(response.getEntity());

			GlobalClass.printDebugLog("GetProfile", "GetProfileResponse-->\n"+jsonData);



			JSONObject jobj=new JSONObject(jsonData);

			if(jobj.getString("success").equals("0"))
			{
				Toast.makeText(mActivity, "Could not get profile", Toast.LENGTH_LONG).show();
				//GlobalClass.safeClose(httpclient);
				return null;

			}
			else
			{
				JSONArray jarray=jobj.getJSONArray("data");

				GlobalClass.printDebugLog("GetProfile", "GetProfile Returned data-->\n"+jarray.getJSONObject(0).toString());

				//GlobalClass.safeClose(httpclient);
				return jarray.getJSONObject(0);



			}

		} 

		catch (ClientProtocolException e)
		{
			GlobalClass.printDebugLog("Edit Profile Exception", "ClientProtocolException while getting connectcrib contacts");

			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			GlobalClass.printDebugLog("Edit Profile Exception", "IOException while getting connectcrib contacts");
			e.printStackTrace();

			//Toast.makeText(mActivity, "Connection timeout. Please try again.", Toast.LENGTH_LONG).show();

		} 
		catch (JSONException e) 
		{
			GlobalClass.printDebugLog("Edit Profile Exception", "JSONException while getting connectcrib contacts");

			e.printStackTrace();
		}

		catch (Exception e) {
			e.printStackTrace();
		}

		//GlobalClass.safeClose(httpclient);
		return null;  
	}

	@Override
	protected void onPostExecute(JSONObject jobj)
	{
		if(pd!=null)
		{
			pd.dismiss();
			pd=null;
		}

		Message m=new Message();

		Bundle b=new Bundle();

		if(jobj!=null)
		{
			b.putString("GetProfileAsyncResult", jobj.toString());	
		}
		else
		{
			b.putString("GetProfileAsyncResult", "nodata");
		}
		m.setData(b);

		mHandler.sendMessage(m);




	}
}
