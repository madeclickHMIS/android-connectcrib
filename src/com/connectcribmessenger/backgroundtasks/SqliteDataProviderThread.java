package com.connectcribmessenger.backgroundtasks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.DatabaseAccessor;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.comparators.MyComparatorTime;
import com.connectcribmessenger.entities.Profile;
import com.connectcribmessenger.entities.RecentChat;

public class SqliteDataProviderThread extends AsyncTask<String,Void,String> {

	//Activity act;
	Handler handler;
	HashMap<String,String> insertValues;
	ProgressDialog pd;
	DatabaseAccessor dbAccessor;

	//AppBuilderClass mAppbuilder;

	IntentService intentServiceContext;

	TextView textView;

	int message_count;

	Context mContext;



	public SqliteDataProviderThread(Context mContext, Handler handler, HashMap<String,String> insertValues)
	{
		this.mContext=mContext;
		this.handler=handler;
		this.insertValues=insertValues;
		//mAppbuilder=new AppBuilderClass(act);
		dbAccessor=new DatabaseAccessor(mContext);
		dbAccessor.open();
	}

	/*public SqliteDataProviderThread(IntentService intentServiceContext, Handler handler, HashMap<String,String> insertValues)
	{
		this.act=act;
		this.handler=handler;
		this.insertValues=insertValues;
		mAppbuilder=new AppBuilderClass(act);
		dbAccessor=new DatabaseAccessor(act);
		dbAccessor.open();
	}*/

	public SqliteDataProviderThread(Context mContext, HashMap<String,String> insertValues,TextView textView)
	{
		this.mContext=mContext;
		this.textView=textView;
		this.insertValues=insertValues;
		//mAppbuilder=new AppBuilderClass(act);
		dbAccessor=new DatabaseAccessor(mContext);
		dbAccessor.open();
	}

	public SqliteDataProviderThread(Context mContext, HashMap<String,String> insertValues)
	{
		this.mContext=mContext;

		this.insertValues=insertValues;
		//mAppbuilder=new AppBuilderClass(act);
		dbAccessor=new DatabaseAccessor(mContext);
		dbAccessor.open();
	}

	@Override
	protected void onPreExecute() 
	{
		if(insertValues.get(Constants.HASHMAP_KEY_PARAM).equals(Constants.HASHMAP_VALUE_CHAT_SCREEN))
		{
			pd=new ProgressDialog(mContext);

			pd.setMessage("Please wait..");

			pd.show();
		}

	}



	@Override
	protected String doInBackground(String... params) 
	{

		if(insertValues.get("param").equals("recent_chats")||insertValues.get("param").equals("get_latest_recent_chat_list"))
		{
			Cursor cursor=dbAccessor.getAllMessagesPeerChat();
			cursor.moveToFirst();

			Log.i("allMessagesList_Count", ""+cursor.getCount());

			ArrayList<RecentChat> recentChatList=new ArrayList<>();

			for(int i=0;i<cursor.getCount();i++)
			{
				RecentChat recentChat=new RecentChat();

				recentChat.setChatType(Constants.CHAT_TYPE_PEER_TO_PEER);
				recentChat.setSenderName(cursor.getString(8));
				recentChat.setSenderNumber(cursor.getString(1));
				recentChat.setMessageType(cursor.getString(3));
				recentChat.setMessage(cursor.getString(2));
				recentChat.setTime(cursor.getString(4));
				recentChat.setPic(cursor.getString(7));
				recentChat.setGroupId(Constants.PEER_TO_PEER_GROUP_ID);
				recentChat.setGroupName(Constants.PEER_TO_PEER_GROUP_NAME);
				recentChat.setDeliverStatus(cursor.getString(5));
				recentChat.setMessageInOutStatus(cursor.getString(6));
				recentChat.setGroupInfo(Constants.PEER_TO_PEER_GROUP_NAME);
				recentChat.setMessage_id(cursor.getString(9));
				recentChat.setSeenStatus(cursor.getString(10));
				recentChat.setArchivedStatus(cursor.getString(11));

				recentChatList.add(recentChat);


				cursor.moveToNext();
			}

			cursor.close();

			cursor=dbAccessor.getAllMessagesGroupChat();
			cursor.moveToFirst();

			for(int i=0;i<cursor.getCount();i++)
			{
				RecentChat recentChat=new RecentChat();

				recentChat.setChatType(Constants.CHAT_TYPE_GROUP);
				recentChat.setSenderName(cursor.getString(2));
				recentChat.setSenderNumber(cursor.getString(1));
				recentChat.setMessageType(cursor.getString(4));
				recentChat.setMessage(cursor.getString(3));
				recentChat.setTime(cursor.getString(5));
				recentChat.setPic(getgroupProfilePicLink(cursor.getString(8)));
				recentChat.setGroupId(getgroupId(cursor.getString(8)));
				recentChat.setDeliverStatus(cursor.getString(6));
				recentChat.setGroupName(getgroupName(cursor.getString(8)));
				recentChat.setMessageInOutStatus(cursor.getString(7));
				recentChat.setGroupInfo(cursor.getString(8));
				recentChat.setMessage_id(cursor.getString(9));
				recentChat.setSeenStatus(cursor.getString(10));
				recentChat.setArchivedStatus(cursor.getString(11));

				recentChatList.add(recentChat);


				cursor.moveToNext();
			}

			//GlobalClass.printDebugLog("SqliteDataProviderThread", "recentChatList Content-->"+recentChatList.toString());


			Collections.sort(recentChatList, new MyComparatorTime());

			//GlobalClass.printDebugLog("SqliteDataProviderThread", "sorted recentChatList Content-->"+recentChatList.toString());

			GlobalClass.recentMessageList=getRecentChatList(recentChatList);





			if(GlobalClass.recentMessageList!=null)
			{
				//GlobalClass.printDebugLog("SqliteDataProviderThread", "filtered recentChatList Content-->"+GlobalClass.recentMessageList.toString());
				if(insertValues.get("param").equals("get_latest_recent_chat_list"))
				{
					return "get_latest_recent_chat_list";
				}
				else
				{
					return "recent_chats";
				}

			}
			else
			{
				Log.i("recentMessageList", "no data (null)");
				return "0";
			}


		}
		else if(insertValues.get("param").equals("profiledata"))
		{
			Cursor cursor=dbAccessor.getProfile();
			cursor.moveToFirst();

			Profile profileData=new Profile();

			profileData.setProfileId(cursor.getInt(0));
			profileData.setProfileName(cursor.getString(1));
			profileData.setProfileNumber(cursor.getString(2));
			profileData.setProfileStatus(cursor.getString(3));
			profileData.setProfilePic(cursor.getString(4));

			GlobalClass.profileData=profileData;

			if(GlobalClass.profileData!=null)
			{
				GlobalClass.printDebugLog("profileData", GlobalClass.profileData.toString());

				return "profileData";
			}
			else
			{
				//GlobalClass.printDebugLog("profileData", "no data (null)");

				return "0";
			}


		}

		/*Called from ChatScreen Line: 111*/
		else if(insertValues.get(Constants.HASHMAP_KEY_PARAM).equals(Constants.HASHMAP_VALUE_CHAT_SCREEN))
		{
			if(insertValues.get(Constants.HASHMAP_KEY_CHAT_TYPE).equals(Constants.HASHMAP_VALUE_CHAT_TYPE_PEER_TO_PEER))
			{
				Cursor cursor=dbAccessor.getPeer2PeerMessage(insertValues.get(Constants.HASHMAP_KEY_SENDER_NUMBER));
				cursor.moveToFirst();

				Log.i("peer2peer specificMessagesList_Count", ""+cursor.getCount());

				ArrayList<com.connectcribmessenger.entities.Message> messagesList=new ArrayList<>();

				for(int i=0;i<cursor.getCount();i++)
				{


					com.connectcribmessenger.entities.Message message=new com.connectcribmessenger.entities.Message();

					message.setMessage(cursor.getString(2));

					message.setMessageDeliveryStatus(cursor.getString(5));

					message.setMessageSendername(cursor.getString(8));

					message.setMessageTime(cursor.getString(4));

					message.setMessageType(cursor.getString(3));

					if(message.getMessageType().equals(Constants.MEDIA_TYPE_VIDEO))
					{
						try 
						{
							JSONObject jobj=new JSONObject(message.getMessage());

							if(jobj.has("status"))
							{
								message.setVideoStatus(jobj.getString("status"));
							}
							else
							{
								message.setVideoStatus("notdownloaded");
							}
							if(jobj.has("thumnail_link"))
							{
								message.setVideoThumbNailLink(jobj.getString("thumnail_link"));

								//message.setThumbNail(GlobalClass.blurImage(GlobalClass.getBitmapFromUrl(jobj.getString("thumnail_link")),mContext ));
								
								message.setThumbNail(GlobalClass.getBitmapFromUrl(jobj.getString("thumnail_link")));
							}
							else if(jobj.has("thumbnail_link"))
							{
								message.setVideoThumbNailLink(jobj.getString("thumbnail_link"));

								message.setThumbNail(GlobalClass.getVideoThumbnail(jobj.getString("thumbnail_link")));
							}
							if(jobj.has("link"))
							{

								message.setVideoLink(jobj.getString("link"));

							}
							if(jobj.has("size"))
							{
								message.setVideoSize(jobj.getString("size"));
							}
							else
							{
								message.setVideoSize("0.0 mb");

							}

							if(jobj.has("duration"))
							{
								message.setVideoDuration(jobj.getString("duration"));

							}
							else
							{
								message.setVideoDuration("0.0 min");
							}

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					message.setMessageSenderNumber(cursor.getString(1));

					message.setMessageInOutStatus(cursor.getString(6));

					message.setMessageId(cursor.getString(9));

					message.setMessageSeenStatus(cursor.getString(10));


					messagesList.add(message);

					cursor.moveToNext();
				}

				GlobalClass.MessageList=messagesList;



				if(GlobalClass.MessageList!=null)
				{
					//GlobalClass.printDebugLog("peer2peer specificNumberMessageList", "Peer2peerChatList Content-->"+GlobalClass.MessageList.toString());

					return Constants.CHAT_TYPE_PEER_TO_PEER;
				}
				else
				{
					Log.i("peer2peer specificNumberMessageList", "no data (null)");
					return "0";
				}
			}
			else
			{

				Cursor cursor=dbAccessor.getAllMessagesGroupChat();
				cursor.moveToFirst();

				Log.i("group specificMessagesList_Count", ""+cursor.getCount());

				ArrayList<com.connectcribmessenger.entities.Message> messagesList=new ArrayList<>();

				for(int i=0;i<cursor.getCount();i++)
				{
					if(insertValues.get(Constants.HASHMAP_KEY_GROUP_ID).equals(getgroupId(cursor.getString(8))))
					{
						com.connectcribmessenger.entities.Message message=new com.connectcribmessenger.entities.Message();

						message.setMessage(cursor.getString(3));

						message.setMessageDeliveryStatus(cursor.getString(6));

						message.setMessageSendername(cursor.getString(2));

						message.setMessageTime(cursor.getString(5));

						message.setMessageType(cursor.getString(4));

						if(message.getMessageType().equals(Constants.MEDIA_TYPE_VIDEO))
						{
							try 
							{
								JSONObject jobj=new JSONObject(message.getMessage());

								if(jobj.has("status"))
								{
									message.setVideoStatus(jobj.getString("status"));
								}
								else
								{
									message.setVideoStatus("notdownloaded");
								}
								if(jobj.has("thumnail_link"))
								{

									message.setThumbNail(GlobalClass.getBitmapFromUrl(jobj.getString("thumnail_link")));
								}
								else if(jobj.has("thumbnail_link"))
								{
									message.setVideoThumbNailLink(jobj.getString("thumbnail_link"));
								}
								if(jobj.has("link"))
								{

									message.setVideoLink(jobj.getString("link"));

								}
								if(jobj.has("size"))
								{
									message.setVideoSize(jobj.getString("size"));
								}
								else
								{
									message.setVideoSize("0.0 mb");

								}

								if(jobj.has("duration"))
								{
									message.setVideoDuration(jobj.getString("duration"));

								}
								else
								{
									message.setVideoDuration("0.0 min");
								}

							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						message.setMessageSenderNumber(cursor.getString(1));

						message.setMessageInOutStatus(cursor.getString(7));

						message.setMessageId(cursor.getString(9));

						message.setMessageSeenStatus(cursor.getString(10));

						messagesList.add(message);	
					}


					cursor.moveToNext();
				}

				GlobalClass.MessageList=messagesList;

				if(GlobalClass.MessageList!=null)
				{
					//GlobalClass.printDebugLog("Group SqliteDataProviderThread", "GroupChatList Content-->"+GlobalClass.MessageList.toString());

					return Constants.CHAT_TYPE_GROUP;
				}
				else
				{
					Log.i("peer2peer specificNumberMessagrecentChatListeList", "no data (null)");
					return "0";
				}


			}

		}

		/*Called from ChatListAdapter Line No: 238*/
		else if(insertValues.get("param").equals(Constants.MESSAGE_COUNT_PEER_TO_PEER))
		{
			message_count=dbAccessor.getUnseenMessageCountPeer2Peer(insertValues.get("number"));

			return "message_count";
		}

		/*Called from ChatListAdapter Line No: 186*/
		else if(insertValues.get("param").equals(Constants.MESSAGE_COUNT_GROUP))
		{
			message_count=dbAccessor.getUnseenMessageCountGroup(insertValues.get("group_info"));

			return "message_count";
		}

		/*Called from ChatScreen line no: 178*/
		else if(insertValues.get("param").equals("updateGroupMessageSeen"))
		{
			for(int i=0;i<GlobalClass.MessageList.size();i++)
			{
				if(GlobalClass.MessageList.get(i).getMessageSeenStatus().equals(Constants.MESSAGE_UNSEEN))
				{
					dbAccessor.updateGroupChatMessageSeenStatus(GlobalClass.conversationInfo.getConversationGroupInfo(), GlobalClass.MessageList.get(i).getMessageSenderNumber());	
				}

			}


			return "GroupMessageSeenUpdated";
		}

		/*Called from ChatScreen line no: 178*/
		else if(insertValues.get("param").equals("updatePeer2PeerMessageSeen"))
		{
			for(int i=0;i<GlobalClass.MessageList.size();i++)
			{
				if(GlobalClass.MessageList.get(i).getMessageSeenStatus().equals(Constants.MESSAGE_UNSEEN))
				{
					dbAccessor.updatePeerToPeerChatMessageSeenStatus(GlobalClass.MessageList.get(i).getMessageSenderNumber());
				}

			}


			return "Peer2PeerMessageSeenUpdated";
		}


		return null;

	}

	@Override
	protected void onPostExecute(String result) 
	{
		if(insertValues.get(Constants.HASHMAP_KEY_PARAM).equals(Constants.HASHMAP_VALUE_CHAT_SCREEN))
		{
			pd.cancel();
		}

		dbAccessor.close();

		//pd.dismiss();

		if(result.equals("message_count"))
		{
			if(message_count!=0)
			{
				textView.setVisibility(View.VISIBLE);

				textView.setText(String.valueOf(message_count));
			}

		}
		else if(handler!=null)
		{
			//GlobalClass.printDebugLog("SqlitedataProviderThread", result);
			Message m=new Message();

			Bundle b=new Bundle();

			b.putString("SqlitedataProviderThread", result);

			m.setData(b);
			handler.sendMessage(m);	
		}


	}

	public String getgroupProfilePicLink(String json)
	{
		String groupProfilePic=null;
		try {
			JSONObject jobj=new JSONObject(json);
			groupProfilePic=jobj.getString("ProfilePic");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return groupProfilePic;
	}

	public String getgroupId(String json)
	{
		String groupId=null;
		try {
			JSONObject jobj=new JSONObject(json);
			groupId=jobj.getString("GroupId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return groupId;
	}

	public String getgroupName(String json)
	{
		String groupName=null;
		try {
			JSONObject jobj=new JSONObject(json);
			groupName=jobj.getString("GroupName");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return groupName;
	}
	public  ArrayList<RecentChat> getRecentChatList(ArrayList<RecentChat> totalMessageList)
	{
		ArrayList<RecentChat> recentChatList=new ArrayList<>();

		int arrayLength=totalMessageList.size();



		Log.i("GlobalClass", "Size of the recent chatlist-->"+arrayLength);

		for(int i=0;i<arrayLength;i++)
		{
			boolean flag = false;
			String number=null;
			String chatType=null;
			String groupId=null;


			number=totalMessageList.get(i).getSenderNumber();
			chatType=totalMessageList.get(i).getChatType();
			groupId=totalMessageList.get(i).getGroupId();

			Log.i("GlobalClass", "number for loop "+i+"-->"+number);

			if(recentChatList.isEmpty())
			{

				Log.i("GlobalClass", "recentchat added when list empty ");
				recentChatList.add(totalMessageList.get(i));

			}
			else
			{
				Log.i("GlobalClass", "List not empty");
				for(int j=0;j<recentChatList.size();j++)
				{
					if(chatType.equals(Constants.CHAT_TYPE_PEER_TO_PEER))
					{
						if(number.equals(recentChatList.get(j).getSenderNumber()))
						{
							flag=true;
						}
					}
					else
					{
						if(recentChatList.get(j).getGroupId().equals(groupId))
						{
							flag=true;
						}
					}


				}

				if(flag==false)
				{
					Log.i("GlobalClass", "recentchat added for loop "+i);
					recentChatList.add(totalMessageList.get(i));
				}
			}
		}

		return getUnArchivedRecentChatList(recentChatList);
	}

	public ArrayList<RecentChat> getUnArchivedRecentChatList(ArrayList<RecentChat> recentChat)
	{
		ArrayList<RecentChat> unArchivedRecentChats=new ArrayList<>();
		
		ArrayList<RecentChat> archivedRecentChats=new ArrayList<>();

		for(int i=0;i<recentChat.size();i++)
		{
			if(recentChat.get(i).getArchivedStatus().equals(Constants.MESSAGE_ARCHIVED_STATUS_NO))
			{
				unArchivedRecentChats.add(recentChat.get(i));
			}
			else
			{
				archivedRecentChats.add(recentChat.get(i));
			}
		}
		GlobalClass.recentArchivedMessageList=archivedRecentChats;
		return unArchivedRecentChats;
	}

}
