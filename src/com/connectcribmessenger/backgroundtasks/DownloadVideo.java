package com.connectcribmessenger.backgroundtasks;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.connectcribmessenger.adapters.ChatAdapter;
import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.DatabaseAccessor;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.customclasses.ProgressWheel;
import com.connectcribmessenger.entities.Message;

public class DownloadVideo extends AsyncTask<String,Integer,String> 
{
	private Activity mActivity;

	private ProgressWheel progressBar;

	private String videoFilePath;

	private ImageView thumbnailImage;

	private TableRow videoDetail;

	private Button downloadBtn;

	private String thumbnailUrl;

	private Message message;

	private TextView videoSize;

	private TextView videoTime;

	public DownloadVideo(Activity mActivity,ProgressWheel progressBar,String videoFilePath,ImageView thumbnailImage,TableRow videoDetail,Button downloadBtn,String thumbnailUrl,Message message,TextView videoSize,TextView videoTime)
	{
		this.mActivity=mActivity;

		this.progressBar=progressBar;

		this.videoFilePath=videoFilePath;

		this.thumbnailImage=thumbnailImage;

		this.videoDetail=videoDetail;

		this.downloadBtn=downloadBtn;

		this.thumbnailUrl=thumbnailUrl;

		this.message=message;

		this.videoSize=videoSize;

		this.videoTime=videoTime;
	}
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();

		progressBar.resetCount();
	}

	@Override
	protected void onProgressUpdate(Integer... progress) {
		// Making progress bar visible
		progressBar.setVisibility(View.VISIBLE);

		// updating progress bar value
		//progressBar.setProgress(progress[0]);
		if(isCancelled())
		{
			Toast.makeText(mActivity, "Image uploading cancelled.",Toast.LENGTH_LONG).show();
			return;
		}
		progressBar.incrementProgress(progress[0]);

		// updating percentage value
		//txtPercentage.setText(String.valueOf(progress[0]) + "%");
	}
	@Override
	protected String doInBackground(String... params) 
	{
		int count;
		try 
		{
			GlobalClass.printDebugLog("videoUrl", params[0]);
			System.setProperty("http.keepAlive", "false");
			URL url = new URL(params[0]);

			URLConnection conection = url.openConnection();
			conection.connect();
			// getting file length
			int lenghtOfFile = conection.getContentLength();

			// input stream to read file - with 8k buffer
			InputStream input = new BufferedInputStream(url.openStream(), 8192);

			// Output stream to write file
			//		OutputStream output = new FileOutputStream("/sdcard/downloadedfile.jpg");

			/*OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory()
					.getPath()+"/"+"ConnectCrib"+"/"+"Media"+"/"+"DownloadedVideo"+"/"+"video.mp4");*/
			GlobalClass.printDebugLog("DownloadVideo", "Loaction where video will be stored-->"+videoFilePath);

			OutputStream output = new FileOutputStream(videoFilePath);

			byte data[] = new byte[1024];

			long total = 0;

			while ((count = input.read(data)) != -1) {
				total += count;
				// publishing the progress....
				// After this onProgressUpdate will be called
				publishProgress((int)((total*100)/lenghtOfFile));

				// writing data to file
				output.write(data, 0, count);
			}

			// flushing output
			output.flush();

			// closing streams
			output.close();
			input.close();
			JSONObject jobj=new JSONObject();

			try {
				jobj.put("status", "downloaded");

				jobj.put("thumbnail_link", videoFilePath);

				jobj.put("link", videoFilePath);

				jobj.put("duration", ""+GlobalClass.getVideoDuration(videoFilePath));

				jobj.put("size", ""+GlobalClass.getVideoSize(videoFilePath));

				DatabaseAccessor dbAccessor=new DatabaseAccessor(mActivity);

				dbAccessor.updatePeerToPeerMessage(message.getMessageId(), GlobalClass.conversationInfo.getConversationNumber(), jobj.toString());
				//GlobalClass.chatAdapter.updateList(jobj.toString(), Constants.MESSAGE_DELIVERY_STATUS_NO, GlobalClass.profileData.getProfileName(), GlobalClass.profileData.getProfileNumber(), message.getMessageTime(), Constants.MESSAGE_TYPE_VIDEO,Constants.MESSAGE_INOUT_OUTGOING_STATUS,GlobalClass.listview,Constants.MESSAGE_SEEN);

				GlobalClass.setChatScreenMessages(GlobalClass.conversationInfo.getConversationType(), GlobalClass.conversationInfo.getConversationNumber(), GlobalClass.conversationInfo.getConversationGroupId(), mActivity);

			} catch (JSONException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			return "downloaded";

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		//ChatAdapter.videoUrl=videoFilePath;

		//ChatAdapter.videostatus="downloaded";




		return null;
	}

	@Override
	protected void onPostExecute(String result) 
	{
		// TODO Auto-generated method stub
		super.onPostExecute(result);



		if(result!=null&&result.equals("downloaded"))
		{
			GlobalClass.printDebugLog("DownloadVideo", result);

			videoDetail.setVisibility(View.VISIBLE);

			downloadBtn.setVisibility(View.INVISIBLE);

			progressBar.setVisibility(View.INVISIBLE);

			GlobalClass.setVideoThumbnail(videoFilePath, thumbnailImage);

			videoSize.setText(GlobalClass.getVideoSize(videoFilePath));

			videoTime.setText(GlobalClass.getTimeToDisplay(message.getMessageTime()));

			thumbnailImage.setOnClickListener(new OnClickListener() 
			{

				@Override
				public void onClick(View v) 
				{
					Intent intent = new Intent(Intent.ACTION_VIEW);

					intent.setDataAndType(Uri.fromFile(new File(videoFilePath)), "video/*");

					mActivity.startActivity(intent);

				}
			});


		}
		else
		{
			videoDetail.setVisibility(View.INVISIBLE);

			downloadBtn.setVisibility(View.VISIBLE);

			progressBar.setVisibility(View.INVISIBLE);
		}
	}


}
