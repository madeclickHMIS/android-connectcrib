package com.connectcribmessenger.backgroundtasks;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.connectcribmessenger.adapters.ChatAdapter;
import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.DatabaseAccessor;
import com.connectcribmessenger.auxilliaryclasses.GetValues;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.customclasses.ProgressWheel;
import com.connectcribmessenger.entities.SelectedContactForMediaMessage;
import com.connectcribmessenger.main.AndroidMultiPartEntity;
import com.connectcribmessenger.main.AndroidMultiPartEntity.ProgressListener;
import com.google.android.gms.gcm.GoogleCloudMessaging;
/*import android.os.RecoverySystem.ProgressListener;*/

public class UploadVideo extends AsyncTask<Void, Integer, String> 
{
	private Activity mActivity;

	private ProgressWheel progressBar;

	private String filePath;

	private long totalSize;

	private AtomicInteger msgId;

	private GetValues mGetValues;

	private ArrayList<SelectedContactForMediaMessage> selectedItems;

	public UploadVideo(Activity mActivity,ProgressWheel progressBar,String filePath,ArrayList<SelectedContactForMediaMessage> selectedItems)
	{
		this.mActivity=mActivity;

		this.progressBar=progressBar;

		this.filePath=filePath;

		this.selectedItems=selectedItems;

		msgId=new AtomicInteger();

		mGetValues=new GetValues(mActivity);
	}
	@Override
	protected void onPreExecute() {
		// setting progress bar to zero
		progressBar.resetCount();
		//progressBar.setProgress(0);

		super.onPreExecute();
	}

	@Override
	protected void onProgressUpdate(Integer... progress) {
		// Making progress bar visible
		progressBar.setVisibility(View.VISIBLE);

		// updating progress bar value
		//progressBar.setProgress(progress[0]);
		if(isCancelled())
		{
			Toast.makeText(mActivity, "Image uploading cancelled.",Toast.LENGTH_LONG).show();
			return;
		}
		progressBar.incrementProgress(progress[0]);

		// updating percentage value
		//txtPercentage.setText(String.valueOf(progress[0]) + "%");
	}

	@Override
	protected String doInBackground(Void... params) {
		return uploadFile();
	}

	@SuppressWarnings("deprecation")
	private String uploadFile() {
		String responseString = null;

		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(Constants.API_UPLOAD_VIDEO);

		GlobalClass.printDebugLog("upload url", Constants.API_UPLOAD_VIDEO);

		try {
			AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
					new ProgressListener() {

						@Override
						public void transferred(long num) 
						{

							publishProgress((int) ((num / (float) totalSize) * 100));
						}
					});

			File sourceFile = new File(filePath);
			//key=getvideo&sender_no=+918882799508&file_type=video&receipt_no=+918882799508,+919331234738
			// Adding file data to http body
			entity.addPart("image", new FileBody(sourceFile));
			
			

			// Extra parameters if you want to pass to server
			/*entity.addPart("website",
					new StringBody("www.androidhive.info"));*/
			entity.addPart("key",
					new StringBody("getvideo"));
			/*entity.addPart("email", new StringBody("abc@gmail.com"));*/
			entity.addPart("sender_no", new StringBody(GlobalClass.profileData.getProfileNumber()));

			StringBuilder recipientContacts=new StringBuilder();

			for(int i=0;i<selectedItems.size();i++)
			{
				if(i==selectedItems.size()-1)
				{
					recipientContacts.append(selectedItems.get(i).getNumber());	
				}
				else
				{
					recipientContacts.append(selectedItems.get(i).getNumber()+",");	
				}

			}

			entity.addPart("file_type", new StringBody("video"));

			entity.addPart("receipt_no", new StringBody(recipientContacts.toString()));

			totalSize = entity.getContentLength();

			httppost.setEntity(entity);


			// Making server call
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity r_entity = response.getEntity();

			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				// Server response
				responseString = EntityUtils.toString(r_entity);
			} else {
				responseString = "Error occurred! Http Status Code: "
						+ statusCode;
			}

		} catch (ClientProtocolException e) {
			responseString = e.toString();
		} catch (IOException e) {
			responseString = e.toString();
		}

		return responseString;

	}

	@Override
	protected void onPostExecute(String result) {
		Log.e("UploadVideo", "Response from server: " + result);
		if(result!=null)
		{
			try
			{
				JSONObject jobj=new JSONObject(result);

				if(jobj.getString("success").equals("1"))
				{
					String id = Integer.toString(msgId.incrementAndGet());
					JSONObject jobjMessage=new JSONObject();

					try {



						jobjMessage.put("status", "uploaded");

						jobjMessage.put("thumbnail_link", filePath);

						jobjMessage.put("link", filePath);

						jobjMessage.put("duration", ""+GlobalClass.getVideoDuration(filePath));

						jobjMessage.put("size", ""+GlobalClass.getVideoSize(filePath));


						//sendCCSMessage(result,Constants.MESSAGE_TYPE_VIDEO,Constants.CHAT_TYPE_PEER_TO_PEER,"","",id);

						sendCCSMessage(jobjMessage.toString(),result,Constants.MESSAGE_TYPE_VIDEO,Constants.CHAT_TYPE_PEER_TO_PEER,"","",id);

						Toast.makeText(mActivity, "Video sent successfully.",Toast.LENGTH_LONG).show();	


					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}


				}
				else
				{
					Toast.makeText(mActivity, "Video not sent.",Toast.LENGTH_LONG).show();
				}
			} 
			catch (JSONException e) 
			{

				Toast.makeText(mActivity, "Video not sent.",Toast.LENGTH_LONG).show();

				e.printStackTrace();
			}

		}
		else
		{
			Toast.makeText(mActivity, "Image not sent.",Toast.LENGTH_LONG).show();	
		}


		//showAlert(result);

		super.onPostExecute(result);
	}
	/**
	 * Method to show alert dialog
	 * *//*
	private void showAlert(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		builder.setMessage(message).setTitle("Response from Servers")
		.setCancelable(false)
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				mActivity.finish();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}*/

	public void sendCCSMessage(String messageToInsert,String messageToSend,String messageType,String chatType,String pic,String groupInfo,String msgId)
	{
		final GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(mActivity);





		String time=String.valueOf(Calendar.getInstance().getTimeInMillis());

		if(mGetValues.getRegistrationId() == null || mGetValues.getRegistrationId().equals(""))
		{
			Toast.makeText(mActivity, "You must register first", Toast.LENGTH_LONG).show();
			return;
		}

		new AsyncTask<String, Void, String>()
		{
			@Override
			protected String doInBackground(String... params)
			{
				String msg = "";
				try
				{
					for(int i=0;i<selectedItems.size();i++)
					{
						Bundle data = new Bundle();
						data.putString("message", params[0]);
						data.putString("chat_type", "peer2peer");

						data.putString("sender_name", selectedItems.get(i).getName());

						data.putString("sender_number", GlobalClass.profileData.getProfileNumber());

						data.putString("recipient_number", selectedItems.get(i).getNumber());

						data.putString("message", params[0]);

						data.putString("message_type", params[2]);

						data.putString("message_time", params[3]);

						data.putString("sender_pic", selectedItems.get(i).getProfilePic());

						

						GlobalClass.printDebugLog("addrees", Constants.GCM_CCS_SENDER_ID+"@gcm.googleapis.com");

						GlobalClass.printDebugLog("ttl", ""+Constants.GCM_TIME_TO_LIVE);

						GlobalClass.printDebugLog("msgId", ""+params[6]);

						GlobalClass.printDebugLog("data", ""+data);

						gcm.send(Constants.GCM_CCS_SENDER_ID+"@gcm.googleapis.com", params[6], 1000, data);

					}
					saveMessage(params[7],params[2],params[6],params[3],Constants.MESSAGE_SEEN);


					msg=params[0];
				}
				catch (IOException ex)
				{
					msg = "Error :" + ex.getMessage();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg)
			{

			}
		}.execute(messageToSend, chatType,messageType,time,pic,groupInfo,msgId,messageToInsert);
	}

	public void saveMessage(String message,String messageType,String msgId,String time,String seenStatus)
	{
		DatabaseAccessor dbAccessor=new DatabaseAccessor(mActivity);

		//	String time=String.valueOf(Calendar.getInstance().getTimeInMillis());

		for(int i=0;i<selectedItems.size();i++)
		{
			int rowaffected=dbAccessor.insertPeer2PeerMessage(selectedItems.get(i).getNumber(), message, messageType, time, Constants.MESSAGE_DELIVERY_STATUS_NO, Constants.MESSAGE_INOUT_OUTGOING_STATUS, selectedItems.get(i).getProfilePic(), selectedItems.get(i).getName(),msgId,seenStatus,Constants.MESSAGE_ARCHIVED_STATUS_NO);
			
			GlobalClass.printDebugLog("UploadVideo", "rows affected-->"+rowaffected);

		}

		if(GlobalClass.IS_CHATSCREEN_OPENED)
		{
			GlobalClass.chatAdapter.updateList(message, Constants.MESSAGE_DELIVERY_STATUS_NO, GlobalClass.conversationInfo.getConversationName(), GlobalClass.conversationInfo.getConversationNumber(), time, messageType, Constants.MESSAGE_INOUT_OUTGOING_STATUS, GlobalClass.listview, Constants.MESSAGE_SEEN);
		}

	}


}
