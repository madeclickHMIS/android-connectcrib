package com.connectcribmessenger.backgroundtasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.GetValues;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmRegistration extends AsyncTask<Void,Void,String> 
{
	private Context mContext;

	private Handler mHandler;

	private String regId;

	private Random mRandom;

	private long randomBackOffTime;

	private Activity mActivity;

	private GetValues mGetValues;

	private GoogleCloudMessaging mGoogleCloudMesaging;
	public GcmRegistration(Context context,Handler handler)
	{
		this.mHandler=handler;

		mContext=context;

		mActivity=(Activity) mContext;

		mGoogleCloudMesaging = GoogleCloudMessaging.getInstance(mContext);

		mRandom = new Random();

		mGetValues=new GetValues(mContext);


	}
	@Override
	protected String doInBackground(Void... params) 
	{
		regId=getRegistrationIdFromGcmServer();

		if(regId!=null)
		{
			if(GlobalClass.isInternetAvailable(mContext))
			{
				String status=sendRegistrationIdToBackend(regId);

				if(status.equals("success"))
				{
					storeRegistrationId(regId);

					return "success";
				}
				else
				{
					return "failed";
				}
			}
			else
			{
				return "nonetwork";
			}

		}
		else
		{
			return "failed";
		}




	}

	@Override
	protected void onPostExecute(String result) 
	{
		GlobalClass.printDebugLog("GcmRegistration onPost()", "Result got is-->"+result);

		Message message=new Message();

		Bundle b=new Bundle();	

		b.putString("status", result);

		message.setData(b);

		mHandler.sendMessage(message);
	}

	private String getRegistrationIdFromGcmServer()
	{
		String regId=null;
		try 
		{

			randomBackOffTime=Constants.BACKOFF_TIME + mRandom.nextInt(1000);
			for (int i = 1; i <= Constants.MAX_ATTEMPTS; i++) 
			{
				GlobalClass.printDebugLog("Registration Attempt", "Attempt #" + i + " to register");
				try {

					regId = mGoogleCloudMesaging.register(Constants.GCM_CCS_SENDER_ID);

					break;

				} 
				catch (Exception e)
				{

					GlobalClass.printDebugLog("Registration Error", "Failed to get registration key from gcm on attempt #"+i);
					e.printStackTrace();
					if (i == Constants.MAX_ATTEMPTS) 
					{
						break;
					}
					try 
					{
						GlobalClass.printDebugLog("Sleep Time", "Sleeping for " + randomBackOffTime + " ms before retrying to get Registration Id from GCM Serevr");
						Thread.sleep(randomBackOffTime);
					} 
					catch (Exception e1) 
					{
						// Activity finished before we complete - exit.
						GlobalClass.printDebugLog("Retry Aborted", "Thread interrupted: abort remaining retries for get the Registration Id from GCM server!");
						Thread.currentThread().interrupt();
						break;

					}
					// increase backoff exponentially
					randomBackOffTime *= 2;
				}
			}




		} 

		catch (Exception ex)
		{
			GlobalClass.printDebugLog("Error Getting RegistrationId", "There is some error in getting Registration Id from GCM server. Please the check the stackTrace");
			ex.printStackTrace();

		}
		return regId;

	}



	/**
	 * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
	 * or CCS to send messages to your app. Not needed for this demo since the
	 * device sends upstream messages to a server that echoes back the message
	 * using the 'from' address in the message.
	 */
	public String sendRegistrationIdToBackend(String regId) 
	{
		String status=null;
		try 
		{
			randomBackOffTime=Constants.BACKOFF_TIME + mRandom.nextInt(1000);
			for (int i = 1; i <= Constants.MAX_ATTEMPTS; i++) 
			{
				GlobalClass.printDebugLog("Attempt", "Attempt #" + i + " to send RegistrationId on App Server");

				try {

					GlobalClass.printDebugLog("Registration", "Sending the Registration Id on App server-->"+Constants.APP_SERVER_ROOT_PATH);

					postforregister(Constants.APP_SERVER_ROOT_PATH,regId);
					status="success";
					break;

				} catch (IOException e) {

					GlobalClass.printDebugLog("Registration Error", "Failed to send registration key to App Server on attempt #"+i);

					if (i == Constants.MAX_ATTEMPTS) 
					{
						status="failed";
						break;
					}
					try {
						GlobalClass.printDebugLog("Sleep Time", "Sleeping for " + randomBackOffTime + " ms before retrying to send on App Server");
						Thread.sleep(randomBackOffTime);
					} catch (Exception e1) {
						// Activity finished before we complete - exit.
						GlobalClass.printDebugLog("Retry Aborted", "Thread interrupted: abort remaining retries to send the Registration Id to App Server!");
						Thread.currentThread().interrupt();
						status="failed";
						break;
					}
					// increase backoff exponentially
					randomBackOffTime *= 2;
				}
			}
		} 
		catch (Exception e) 
		{
			GlobalClass.printDebugLog("Error Getting RegistrationId", "There is some error in sending Registration Id to App server. Please the check the stackTrace");
			e.printStackTrace();
		}

		return status;

	}

	public void postforregister(String endpoint,String regId)
			throws IOException {

		String JSON_Response=null;

		HttpParams httpParameters = new BasicHttpParams();

		HttpConnectionParams.setConnectionTimeout(httpParameters, 50000);

		HttpConnectionParams.setSoTimeout(httpParameters, 50000);

		HttpClient httpClient=new DefaultHttpClient(httpParameters);

		HttpPost httpPost=new HttpPost(Constants.APP_SERVER_ROOT_PATH);



		List<NameValuePair> nameValuePair=new ArrayList<NameValuePair>();	

		nameValuePair.add(new BasicNameValuePair(Constants.API_REGISTRATION_PARAMETER_1,Constants.API_REGISTRATION_PARAMETER_1_VALUE));

		nameValuePair.add(new BasicNameValuePair(Constants.API_REGISTRATION_PARAMETER_2,regId));	

		nameValuePair.add(new BasicNameValuePair(Constants.API_REGISTRATION_PARAMETER_3,Constants.DEVICE_NAME));

		httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));  

		HttpResponse httpResponse=httpClient.execute(httpPost);

		HttpEntity httpEntity=httpResponse.getEntity();

		JSON_Response=EntityUtils.toString(httpEntity);	

		GlobalClass.printDebugLog("Registration Post Response", "Post response of sending registration Id on App Server-->"+JSON_Response);



	}

	public void storeRegistrationId(String regId) {

		final SharedPreferences prefs = mGetValues.getGCMPreferences();

		int appVersion = mGetValues.getAppVersion();

		GlobalClass.printDebugLog("Registration Id", "-->"+regId);
		GlobalClass.printDebugLog("Storing RegId", "Saving regId on app version " + appVersion);

		SharedPreferences.Editor editor = prefs.edit();

		editor.putString(Constants.SHAREDPREFERENCE_KEY_REGISTRATION_ID, regId);

		editor.putInt("appVersion", appVersion);

		editor.commit();
		
		GlobalClass.REGISTRATION_ID=regId;
	}
}
