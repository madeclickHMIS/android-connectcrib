package com.connectcribmessenger.backgroundtasks;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;

public class GetCountryCode extends AsyncTask<Void,Void,String> 
{
	private Handler mHandler;

	public GetCountryCode(Handler handler)
	{
		mHandler=handler;
	}

	@Override
	protected String doInBackground(Void... params) 
	{
		String JSON_Response=null;

		HttpParams httpParameters = new BasicHttpParams();

		HttpConnectionParams.setConnectionTimeout(httpParameters, 50000);

		HttpConnectionParams.setSoTimeout(httpParameters, 50000);

		HttpClient httpClient=new DefaultHttpClient(httpParameters);

		HttpGet httpGet=new HttpGet(Constants.COUNTRY_CODE_API);


		HttpResponse httpResponse;
		try 
		{
			httpResponse = httpClient.execute(httpGet);
			HttpEntity httpEntity=httpResponse.getEntity();

			JSON_Response=EntityUtils.toString(httpEntity);

			return JSON_Response;

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		GlobalClass.printDebugLog("GetCountryCode", "Get Country Code Response-->"+JSON_Response);

		return JSON_Response;
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);

		if(result!=null)
		{
			try {
				JSONObject jobj=new JSONObject(result);

				String countryCode=jobj.getString("countryCode");

				Message message=new Message();

				Bundle b=new Bundle();

				b.putString("countryCode", countryCode);

				message.setData(b);

				mHandler.sendMessage(message);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				Message message=new Message();

				Bundle b=new Bundle();

				b.putString("countryCode", "noval");

				message.setData(b);

				mHandler.sendMessage(message);
			}
		}
		else
		{
			Message message=new Message();

			Bundle b=new Bundle();

			b.putString("countryCode", "noval");

			message.setData(b);

			mHandler.sendMessage(message);
		}


	}

}
