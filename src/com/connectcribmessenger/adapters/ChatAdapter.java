package com.connectcribmessenger.adapters;

import java.io.File;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.TextCrypter;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.backgroundtasks.DownloadVideo;
import com.connectcribmessenger.customclasses.ProgressWheel;
import com.connectcribmessenger.entities.Message;
import com.connectcribmessenger.main.AppBuilderClass;
import com.connectcribmessenger.main.R;
import com.example.android.displayingbitmaps.util.ImageFetcher;
import com.rockerhieu.emojicon.EmojiconTextView;

public class ChatAdapter extends BaseAdapter 
{

	private Activity activity;

	private LayoutInflater inflater;

	//private String mConversationType;

	ViewHolder viewHolder;

	LinearLayout root;

	AppBuilderClass mAppbuilder;

	ImageFetcher mImageFetcher;

	//public String  videostatus;

	//String thumbnailUrl=null;

	//public String videoUrl;

	public static String videoFilePath;

	//public String videoSize;

	//public String videoDuration;

	View row;
	public TextCrypter crypter;
	
	private Map<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();
	
	public ChatAdapter(Activity activity,ImageFetcher mImageFetcher)
	{
		this.activity=activity;

		inflater = activity.getLayoutInflater();

		this.mImageFetcher=mImageFetcher;

		//mConversationType=GlobalClass.conversationInfo.getConversationType();

		mAppbuilder=new AppBuilderClass(activity);
		
		crypter=new TextCrypter();
	}
	@Override
	public int getCount() 
	{

		return GlobalClass.MessageList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return GlobalClass.MessageList.get(position).getMessageSenderNumber();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	static class ViewHolder 
	{

		public EmojiconTextView chat_text;

		public EmojiconTextView datetime_text;

		public LinearLayout singleMessageContainer;

		public LinearLayout root;

		public RelativeLayout videoLayout;

		public Button downloadBtn;

		public ProgressWheel progressBar;

		public TextView videoIcon;

		public TextView videoSize;

		public TextView videoTime;

		public TableRow videoDetail;

		public ImageView thumbnailImage;
		

	}

	public void setNewSelection(int position, boolean value) {
		//Toast.makeText(activity, "setNewSelection position-->"+position+" value-->"+value, Toast.LENGTH_SHORT).show();


		mSelection.put(position, value);
		
		notifyDataSetChanged();
	}

	public boolean isPositionChecked(int position) {
		Boolean result = mSelection.get(position);
		return result == null ? false : result;
	}

	public Set<Integer> getCurrentCheckedPosition() 
	{
		return mSelection.keySet();
	}

	public void removeSelection(int position) 
	{

		mSelection.remove(position);

		notifyDataSetChanged();
	}

	public Map<Integer, Boolean> getAllSelection() 
	{
		//Toast.makeText(activity, "getAllSelection Total selected items-->"+mSelection.size(), Toast.LENGTH_SHORT).show();
		return mSelection;

	}

	public void clearSelection() {
		mSelection = new HashMap<Integer, Boolean>();
		notifyDataSetChanged();
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		View row = convertView;

		ViewHolder holder;
		// reuse views
		if (row == null) 
		{
			//LayoutInflater inflater = context.getLayoutInflater();
			row=inflater.inflate(R.layout.chat_row, null);
			// configure view holder
			holder = new ViewHolder();

			holder.chat_text=(EmojiconTextView)row.findViewById(R.id.chatTextView);

			holder.datetime_text=(EmojiconTextView)row.findViewById(R.id.textView1);

			holder.videoLayout=(RelativeLayout)row.findViewById(R.id.videorow);

			holder.downloadBtn=(Button)row.findViewById(R.id.download);

			holder.videoSize=(TextView)row.findViewById(R.id.videosize);

			holder.videoIcon=(TextView)row.findViewById(R.id.videoicon);

			holder.videoTime=(TextView)row.findViewById(R.id.videotime);

			holder.videoDetail=(TableRow)row.findViewById(R.id.videoDetail);

			holder.progressBar=(ProgressWheel)row.findViewById(R.id.progressBarTwo);

			holder.thumbnailImage=(ImageView)row.findViewById(R.id.thumnailImage);

			holder.singleMessageContainer = (LinearLayout) row.findViewById(R.id.singleMessageContainer);
			//holder.singleMessageContainer = (LinearLayout) row.findViewById(R.id.root);

			holder.root = (LinearLayout) row.findViewById(R.id.root);



			row.setTag(holder);
		}

		// fill data
		final ViewHolder holder2 = (ViewHolder) row.getTag();

		if(holder2.videoLayout!=null)
		{
			//Toast.makeText(activity, "not null", Toast.LENGTH_LONG).show();
		}
		else
		{
			//Toast.makeText(activity, "null", Toast.LENGTH_LONG).show();
		}

		final Message message=GlobalClass.MessageList.get(position);


		final int deliverIcon;

		if(message.getMessageDeliveryStatus().equals(Constants.MESSAGE_DELIVERY_STATUS_NO))
		{
			deliverIcon=R.drawable.waiting;
		}
		else if(message.getMessageDeliveryStatus().equals(Constants.MESSAGE_DELIVERY_STATUS_HALF))
		{
			deliverIcon=R.drawable.chat_arrow_gray;
		}
		else if(message.getMessageDeliveryStatus().equals(Constants.MESSAGE_DELIVERY_STATUS_FULL))
		{
			deliverIcon=R.drawable.message_sent;
		}
		else
		{
			deliverIcon=R.drawable.message_read;
		}

		//GlobalClass.printDebugLog("ChatAdapter", "Message Type-->"+message.getMessageType());

		if(message.getMessageType().equals(Constants.MESSAGE_TYPE_VIDEO))
		{
			/*holder2.videoLayout.setVisibility(View.VISIBLE);

			holder2.singleMessageContainer.setVisibility(View.GONE);*/

			//GlobalClass.printDebugLog("ChatAdapter", "Message in/out status-->"+message.getMessageInOutStatus());

			//GlobalClass.printDebugLog("ChatAdapter", "Message-->"+message.getMessage());

			holder2.videoLayout.setVisibility(View.VISIBLE);

			holder2.singleMessageContainer.setVisibility(View.GONE);

			if(message.getMessageInOutStatus().equals(Constants.MESSAGE_INOUT_INCOMING_STATUS))
			{
				holder2.videoLayout.setBackgroundResource(R.drawable.incoming);

				holder2.root.setGravity(Gravity.LEFT);

				//GlobalClass.printDebugLog("ChatAdapter", "videostatus-->"+message.getVideoStatus());

				if(message.getVideoStatus()!=null&&message.getVideoStatus().equals("downloaded"))
				{
					holder2.videoDetail.setVisibility(View.VISIBLE);

					holder2.downloadBtn.setVisibility(View.INVISIBLE);

					holder2.progressBar.setVisibility(View.INVISIBLE);

					//GlobalClass.setVideoThumbnail(videoUrl, holder2.thumbnailImage);

					holder2.thumbnailImage.setImageBitmap(message.getThumbNail());

					holder2.videoSize.setText(message.getVideoSize()+" mb");

					holder2.videoTime.setText(GlobalClass.getTimeToDisplay(message.getMessageTime())+"     ");

					mAppbuilder.setDrawableInText(holder2.videoTime, holder2.videoTime.getText().toString(), deliverIcon, holder2.videoTime.getText().toString().length()-4, holder2.videoTime.getText().toString().length()-2, 30);

					//GlobalClass.printDebugLog("ChatAdapter","downloaded video path-->"+message.getVideoLink());

					holder2.thumbnailImage.setOnClickListener(new OnClickListener() 
					{

						@Override
						public void onClick(View v) 
						{
							Intent intent = new Intent(Intent.ACTION_VIEW);

							intent.setDataAndType(Uri.fromFile(new File(message.getVideoLink())), "video/*");
							//intent.setDataAndType(Uri.parse(message.getVideoLink()), "video/*");

							activity.startActivity(intent);

						}
					});
				}
				else
				{
					holder2.videoDetail.setVisibility(View.INVISIBLE);

					holder2.downloadBtn.setVisibility(View.VISIBLE);

					holder2.progressBar.setVisibility(View.INVISIBLE);

					mImageFetcher.loadImage(message.getVideoThumbNailLink(), holder2.thumbnailImage);



					holder2.downloadBtn.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) 
						{
							holder2.downloadBtn.setVisibility(View.INVISIBLE);

							holder2.progressBar.setVisibility(View.VISIBLE);

							GlobalClass.printDebugLog("ChatAdapter","Not downloaded video path-->"+message.getVideoLink());

							//							videoFilePath=getAudioFilePath();
							//							videoFilePath=createImageFile().getAbsolutePath();
							videoFilePath=GlobalClass.getOutputMediaFile(Constants.MEDIA_TYPE_VIDEO).getAbsolutePath();

							//GlobalClass.printDebugLog("ChatAdapter","Location where video will be downloaded-->"+ videoFilePath);

							new DownloadVideo(activity, holder2.progressBar, videoFilePath,holder2.thumbnailImage,holder2.videoDetail,holder2.downloadBtn,message.getVideoThumbNailLink(),message,holder2.videoSize,holder2.videoTime).execute(message.getVideoLink());

						}
					});
				}
			}
			else if(message.getMessageInOutStatus().equals(Constants.MESSAGE_INOUT_OUTGOING_STATUS))
			{
				//GlobalClass.printDebugLog("ChatAdapter", message.getMessageInOutStatus());
				holder2.videoLayout.setBackgroundResource(R.drawable.outgoing);

				holder2.root.setGravity(Gravity.RIGHT);

				//GlobalClass.printDebugLog("ChatAdapter", "videostatus-->"+message.getVideoStatus());

				if(message.getVideoStatus()!=null&&message.getVideoStatus().equals("uploaded"))
				{
					holder2.videoDetail.setVisibility(View.VISIBLE);

					holder2.downloadBtn.setVisibility(View.INVISIBLE);

					holder2.progressBar.setVisibility(View.INVISIBLE);



					//GlobalClass.setVideoThumbnail(videoUrl, holder2.thumbnailImage);

					holder2.thumbnailImage.setImageBitmap(message.getThumbNail());

					holder2.videoSize.setText(message.getVideoSize()+" mb");

					holder2.videoTime.setText(GlobalClass.getTimeToDisplay(message.getMessageTime())+"     ");



					//mAppbuilder.setDrawableInText(holder2.videoTime, holder2.videoTime.getText().toString(), deliverIcon, holder2.datetime_text.getText().toString().length()-4, holder2.datetime_text.getText().toString().length()-2, 30);

					//GlobalClass.printDebugLog("ChatAdapter", "uploaded video path-->"+message.getVideoLink());

					holder2.thumbnailImage.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) 
						{
							Intent intent = new Intent(Intent.ACTION_VIEW);

							//intent.setDataAndType(Uri.parse(videoUrl), "video/*");

							intent.setDataAndType(Uri.fromFile(new File(message.getVideoLink())), "video/*");

							activity.startActivity(intent);

						}
					});
				}
				else
				{
					holder2.videoDetail.setVisibility(View.VISIBLE);

					holder2.downloadBtn.setVisibility(View.INVISIBLE);

					holder2.progressBar.setVisibility(View.INVISIBLE);

					//GlobalClass.printDebugLog("ChatAdapter","Not uploaded Video Path-->"+message.getVideoLink());



					holder2.thumbnailImage.setImageBitmap(message.getThumbNail());

					holder2.videoSize.setText(message.getVideoSize()+" mb");

					holder2.videoTime.setText("Time:"+GlobalClass.getTimeToDisplay(message.getMessageTime())+"     ");

					mAppbuilder.setDrawableInText(holder2.datetime_text, holder2.datetime_text.getText().toString(), deliverIcon, holder2.datetime_text.getText().toString().length()-4, holder2.datetime_text.getText().toString().length()-2, 30);


					holder2.thumbnailImage.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) 
						{
							Intent intent = new Intent(Intent.ACTION_VIEW);

							//intent.setDataAndType(Uri.parse(videoUrl), "video/*");

							intent.setDataAndType(Uri.fromFile(new File(message.getVideoLink())), "video/*");

							activity.startActivity(intent);

						}
					});
				}
			}




		}
		else if(message.getMessageType().equals(Constants.MESSAGE_TYPE_TEXT))
		{
			holder2.videoLayout.setVisibility(View.GONE);

			holder2.singleMessageContainer.setVisibility(View.VISIBLE);

			/*GlobalClass.printDebugLog("Message", "messagetext-->"+message.getMessage());

			GlobalClass.printDebugLog("Message", "messageinout-->"+message.getMessageInOutStatus());

			GlobalClass.printDebugLog("Message", "messageinout-->"+message.getMessageSeenStatus());

			GlobalClass.printDebugLog("Message", "messageinout-->"+message.getMessageDeliveryStatus());*/

			holder2.chat_text.setTextColor(Color.WHITE);

			//holder2.chat_text.setText(message.getMessage());

			//String message=GlobalClass.EncryptData(msg, Constants.CRYPTOGRAPHY_CLIENTPRIVATEKEY, Constants.CRYPTOGRAPHY_CLIENTMODULUSKEY);
			//GlobalClass.printDebugLog("ChatAdapter EncryptedMessage", "Encrypted Message-->"+message.getMessage());

			//GlobalClass.printDebugLog("ChatAdapter EncryptedMessage", "Decrypted Message-->"+GlobalClass.DecryptData(message.getMessage(), Constants.CRYPTOGRAPHY_CLIENTPUBLICKEY, Constants.CRYPTOGRAPHY_CLIENTMODULUSKEY));
			//holder2.chat_text.setText("Ghcghchgc\ud83d\ude03\ud83d\ude03fyju\ud83d\ude0c6\u20e3");
			//holder2.chat_text.setText(GlobalClass.DecryptData(message.getMessage(), Constants.CRYPTOGRAPHY_CLIENTPUBLICKEY, Constants.CRYPTOGRAPHY_CLIENTMODULUSKEY));
			try 
			{
								
				String res = new String( crypter.decrypt( message.getMessage() ), "UTF-8" );
				
				res = URLDecoder.decode(res,"UTF-8");

				//GlobalClass.printDebugLog("ChatAdapter DecryptedMessage", "Decrypted Message-->"+res);

				
				holder2.chat_text.setText(res);
			
			} catch (Exception e1) 
			{
				// TODO Auto-generated catch block
				
				holder2.chat_text.setText(message.getMessage());
				
				e1.printStackTrace();
			}
			
			
			

			if(message.getMessageInOutStatus().equals(Constants.MESSAGE_INOUT_INCOMING_STATUS))
			{
				//holder.datetime_text.setVisibility(View.VISIBLE);
				holder2.datetime_text.setText(GlobalClass.getTimeToDisplay(message.getMessageTime())+"     ");
				mAppbuilder.setDrawableInText(holder2.datetime_text, holder2.datetime_text.getText().toString(), deliverIcon, holder2.datetime_text.getText().toString().length()-4, holder2.datetime_text.getText().toString().length()-2, 30);
				holder2.singleMessageContainer.setBackgroundResource(R.drawable.incoming);
				holder2.root.setGravity(Gravity.LEFT);
			}
			else if(message.getMessageInOutStatus().equals(Constants.MESSAGE_INOUT_OUTGOING_STATUS))
			{
				//holder.datetime_text.setVisibility(View.GONE);
				holder2.datetime_text.setText(GlobalClass.getTimeToDisplay(message.getMessageTime()));

				holder2.singleMessageContainer.setBackgroundResource(R.drawable.outgoing);
				holder2.root.setGravity(Gravity.RIGHT);
			}

			if(message.getMessageSeenStatus().equals(Constants.MESSAGE_UNSEEN))
			{
				/*DatabaseAccessor dbAccessor=new DatabaseAccessor(activity);

				if(GlobalClass.conversationInfo.getConversationType().equals(Constants.CHAT_TYPE_PEER_TO_PEER))
				{
					dbAccessor.updatePeerToPeerChatMessageSeenStatus(message.getMessageId(), Constants.MESSAGE_SEEN);
				}
				else
				{
					dbAccessor.updateGroupChatMessageSeenStatus(GlobalClass.conversationInfo.getConversationGroupInfo(), message.getMessageId(), Constants.MESSAGE_SEEN);
				}*/

			}
		}
		//View v = super.getView(position, convertView, parent);//let the adapter handle setting up the row views
		if(row!=null)
		{
			row.setBackgroundColor(activity.getResources().getColor(android.R.color.background_light)); //default color

			if (mSelection.get(position) != null) {
				row.setBackgroundColor(activity.getResources().getColor(android.R.color.holo_blue_light));// this is a selected position so make it red
			}	
		}


		return row;
	}
	public File getAlbumStorageDir(String albumName) {
		// Get the directory for the user's public pictures directory.
		File file = new File(Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_PICTURES), albumName);
		if (!file.mkdirs()) {
			Log.e("ChatAdapter getAlbumStorageDir", "Directory not created");
		}
		return file;
	}
	private String getAudioFilePath(){
		// String filepath = Environment.getExternalStorageDirectory().getPath();
		File file=null;
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
		{
			String filepath =activity.getExternalFilesDir(null).getPath();
			file = new File(filepath,"ConnectCrib");

			if(!file.exists()){
				file.mkdirs();
			}

		}
		else
		{
			Toast.makeText(activity, "SD card not available to write", Toast.LENGTH_LONG).show();
		}

		if(file==null)
		{
			return "null";
		}
		else
		{
			return (file.getAbsolutePath() + "/Media/DownloadedVideo/" + System.currentTimeMillis() + "_connectCribVideo.3gp");
		}


	}
	public void updateList(String message,String deliverStatus,String senderName,String senderNumber,String time,String msgType,String messageInOutStatus,ListView listview,String seenStatus)
	{
		Message msg=new Message();

		msg.setMessage(message);
		if(msgType.equals(Constants.MESSAGE_TYPE_VIDEO))
		{
			JSONObject jobj;
			try 
			{
				jobj = new JSONObject(msg.getMessage());



				if(jobj.has("status"))
				{


					msg.setVideoStatus(jobj.getString("status"));
				}
				else
				{

					msg.setVideoStatus("notdownloaded");
				}
				if(jobj.has("thumnail_link"))
				{
					msg.setVideoThumbNailLink(jobj.getString("thumnail_link"));
					msg.setThumbNail(GlobalClass.getBitmapFromUrl(jobj.getString("thumnail_link")));
				}
				else if(jobj.has("thumbnail_link"))
				{
					msg.setVideoThumbNailLink(jobj.getString("thumbnail_link"));

					msg.setThumbNail(GlobalClass.getVideoThumbnail(jobj.getString("thumbnail_link")));
				}
				if(jobj.has("link"))
				{

					msg.setVideoLink(jobj.getString("link"));
				}
				if(jobj.has("size"))
				{

					msg.setVideoSize(jobj.getString("size"));
				}
				else
				{

					msg.setVideoSize("0.0 mb");
				}

				if(jobj.has("duration"))
				{

					msg.setVideoDuration(jobj.getString("duration"));

				}
				else
				{
					msg.setVideoDuration("0.0 min");
				}







			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

		else
		{


			msg.setVideoStatus("");

			msg.setVideoThumbNailLink("");

			msg.setVideoLink("");

			msg.setVideoSize("");

			msg.setVideoDuration("");

			msg.setVideoDuration("");


		}

		msg.setMessageDeliveryStatus(deliverStatus);
		msg.setMessageSendername(senderName);
		msg.setMessageTime(time);
		msg.setMessageType(msgType);
		msg.setMessageSenderNumber(senderNumber);
		msg.setMessageInOutStatus(messageInOutStatus);
		msg.setMessageSeenStatus(seenStatus);

		GlobalClass.MessageList.add(msg);

		/*listview.setAdapter(this);


		listview.setSelection(getCount() - 1);*/


	}

	/** Create a file Uri for saving an image or video */
	private  Uri getOutputMediaFileUri(){
		//		return Uri.fromFile(getOutputMediaFile());

		return Uri.fromFile(createImageFile());
	}
	/*private File createImageFile(){

		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

		String imageFileName = "ConnectCribVideo_" + timeStamp + "_";

		File filedir = null;
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
		{
			//filedir = new File(Environment.getExternalStorageDirectory()+File.separator +"CrazyZooImages"+File.separator +String.valueOf(System.currentTimeMillis())+ "_image.jpg");

			filedir=new File(activity.getExternalFilesDir(null).toString()+"/ConnectCrib/Media/DownloadedVideo");

			if (filedir.exists ())
			{
				Log.i("filedir.exists ()",""+filedir.exists ());
				try 
				{
					filedir=new File(filedir,imageFileName+".mp4");

				} catch (Exception e)
				{
					e.printStackTrace();
				}

			}
			else
			{
				Log.i("filedir.exists ()",""+filedir.exists ());

				filedir.mkdirs();

				filedir=new File(filedir,imageFileName+".mp4");
			}


		}
		else
		{
			Toast.makeText(activity, "SD card not available to write", Toast.LENGTH_LONG).show();
		}



		//image_path=filedir.getAbsolutePath();
		//image_path=getExternalFilesDir(null).toString()+"/CrazyZoo/"+imageFileName+".jpg";
		//noteimagepath=getExternalFilesDir(null).toString()+"/MakeNotesImages/"+imageFileName+".jpeg";
		//Log.i("absolute_path2",image_path);
		return  filedir;
	}*/

	private File createImageFile(){

		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

		String imageFileName = "ConnectCribVideo_" + timeStamp + "_";

		File filedir = null;
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
		{
			//filedir = new File(Environment.getExternalStorageDirectory()+File.separator +"CrazyZooImages"+File.separator +String.valueOf(System.currentTimeMillis())+ "_image.jpg");

			//			filedir=new File(activity.getExternalFilesDir(null).getPath()+"/ConnectCrib/Media/DownloadedVideo/"+imageFileName+".mp4");

			filedir=new File(activity.getExternalFilesDir(Environment.DIRECTORY_MOVIES)+"/ConnectCrib/Media/DownloadedVideo/"+imageFileName+".mp4");
			if (filedir.exists ())
			{
				GlobalClass.printDebugLog("ChatAdpater createImageFile()", "File name that exists for saving video-->"+filedir.getPath());
				//Log.i("ChatAdpater createImageFile()","File name that exists for saving video-->"+filedir.getPath());
				/*try 
				{
					filedir=new File(filedir,imageFileName+".mp4");

				} catch (Exception e)
				{
					e.printStackTrace();
				}*/

			}
			else
			{
				GlobalClass.printDebugLog("ChatAdpater createImageFile()", "File doesn't exists for saving video-->"+filedir.getPath());

				GlobalClass.printDebugLog("ChatAdpater createImageFile()", "Creating Directory-->"+filedir.getPath());

				filedir.mkdirs();

			}


		}
		else
		{
			Toast.makeText(activity, "SD card not available to write", Toast.LENGTH_LONG).show();
		}



		//image_path=filedir.getAbsolutePath();
		//image_path=getExternalFilesDir(null).toString()+"/CrazyZoo/"+imageFileName+".jpg";
		//noteimagepath=getExternalFilesDir(null).toString()+"/MakeNotesImages/"+imageFileName+".jpeg";
		//Log.i("absolute_path2",image_path);
		return  filedir;
	}


}
