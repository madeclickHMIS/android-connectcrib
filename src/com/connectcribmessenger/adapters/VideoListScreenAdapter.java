package com.connectcribmessenger.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.connectcribmessenger.entities.Video;
import com.connectcribmessenger.main.R;
import com.connectcribmessenger.main.SendVideoScreen;

public class VideoListScreenAdapter extends BaseAdapter 
{
	private Activity mActivity;

	private ArrayList<Video> videolist;

	private LayoutInflater mLayoutInflater;

	public VideoListScreenAdapter(Activity mActivity,ArrayList<Video> videolist)
	{
		this.mActivity=mActivity;

		this.videolist=videolist;

		mLayoutInflater = this.mActivity.getLayoutInflater();
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return videolist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return videolist.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	static class ViewHolder 
	{
		public RelativeLayout videoRelativelayout;

		public VideoView videoView;

		public ImageView thumbnailImageView;

		public ImageView playBtn;

		public TextView title;

		public TextView size;

		public TextView duration;

		public Button selectVideo;

	}
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		ViewHolder viewHolder;
		// reuse views
		if (convertView == null) 
		{
			//LayoutInflater inflater = context.getLayoutInflater();
			//			convertView=mLayoutInflater.inflate(R.layout.videolist_item_row, null);

			convertView=mLayoutInflater.inflate(R.layout.videolistrow, null);

			// configure view holder
			viewHolder = new ViewHolder();

			viewHolder.playBtn=(ImageView)convertView.findViewById(R.id.play_button);

			viewHolder.videoView=(VideoView)convertView.findViewById(R.id.videoView);

			viewHolder.thumbnailImageView=(ImageView)convertView.findViewById(R.id.imageView1);

			viewHolder.videoRelativelayout=(RelativeLayout)convertView.findViewById(R.id.videoFrameLayout);

			viewHolder.title=(TextView)convertView.findViewById(R.id.title);

			viewHolder.size=(TextView)convertView.findViewById(R.id.size);

			viewHolder.duration=(TextView)convertView.findViewById(R.id.duration);

			viewHolder.selectVideo=(Button)convertView.findViewById(R.id.select);


			convertView.setTag(viewHolder);
		}

		// fill data
		final ViewHolder holder = (ViewHolder) convertView.getTag();

		//mVideoPath=getIntent().getStringExtra("videoPath");

		holder.title.setText(videolist.get(position).getTitle());

		holder.size.setText(videolist.get(position).getSize());

		holder.duration.setText(videolist.get(position).getDuration());

		holder.thumbnailImageView.setImageBitmap(videolist.get(position).getThumbnail());

		//holder.videoView.setVideoPath(videolist.get(position).getPath());

		/*holder.videoView.requestFocus();

		holder.playBtn.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v) 
			{
				holder.videoView.start();

				holder.playBtn.setVisibility(View.GONE);


			}
		});

		holder.videoView.setOnCompletionListener(new OnCompletionListener() 
		{

			@Override
			public void onCompletion(MediaPlayer mp) 
			{

				holder.playBtn.setVisibility(View.VISIBLE);
				//mThumbNailImageView.setVisibility(View.VISIBLE);
			}
		});

		holder.videoView.setOnPreparedListener(new OnPreparedListener() 
		{

			@Override
			public void onPrepared(MediaPlayer mp) 
			{

				holder.videoView.start();

				Handler handler=new Handler();

				handler.postDelayed(new Runnable() {

					@Override
					public void run() 
					{
						holder.videoView.pause();

					}
				}, 100);


			}
		});

		holder.videoRelativelayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v) 
			{

				if(holder.videoView.isPlaying())
				{
					holder.videoView.pause();

					holder.playBtn.setVisibility(View.VISIBLE);

				}


			}
		});
		 */
		holder.selectVideo.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				Intent intent=new Intent(mActivity,SendVideoScreen.class);

				intent.putExtra("videoPath", videolist.get(position).getPath());

				mActivity.startActivity(intent);

				mActivity.finish();

			}
		});

		return convertView;
	}

}
