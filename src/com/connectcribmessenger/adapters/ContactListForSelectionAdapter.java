package com.connectcribmessenger.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.entities.Contact;
import com.connectcribmessenger.entities.Model;
import com.connectcribmessenger.main.R;
import com.example.android.displayingbitmaps.util.ImageFetcher;

public class ContactListForSelectionAdapter extends BaseAdapter 
{

	private Activity act;

	private ArrayList<Contact> contactList;

	private ImageFetcher mImageFetcher;

	private LayoutInflater mLayoutInflater;
	
	private View convertView;

	public ContactListForSelectionAdapter(Activity act,ArrayList<Contact> contactList,ImageFetcher mImageFetcher)
	{
		this.act=act;

		this.contactList=contactList;
		
		GlobalClass.contactList=contactList;

		this.mImageFetcher=mImageFetcher;

		mLayoutInflater = act.getLayoutInflater();

	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return GlobalClass.contactList.size();
	}

	@Override
	public Contact getItem(int position) {
		// TODO Auto-generated method stub
		return GlobalClass.contactList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	static class ViewHolder 
	{
		public TextView name_text;

		public TextView status_text;

		public ImageView image;

		public CheckBox checkbox;

	}

	@Override
	public View getView(int position, View convertVieww, ViewGroup parent) {

		convertView=convertVieww;
		final ViewHolder viewHolder;
		// reuse views
		if (convertView == null) 
		{
			//LayoutInflater inflater = context.getLayoutInflater();
			convertView=mLayoutInflater.inflate(R.layout.contact_list_for_selection, null);
			// configure view holder
			viewHolder = new ViewHolder();

			viewHolder.image=(ImageView)convertView.findViewById(R.id.thumbnail_imageview);

			viewHolder.name_text=(TextView)convertView.findViewById(R.id.name_textview);

			viewHolder.status_text=(TextView)convertView.findViewById(R.id.status_textview);

			viewHolder.checkbox=(CheckBox)convertView.findViewById(R.id.check);

			viewHolder.checkbox
			.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() 
			{

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					Contact element = (Contact) viewHolder.checkbox
							.getTag();
					element.setSelected(buttonView.isChecked());
					
					
						//convertView.setSelected(buttonView.isChecked());
					
					

				}
			});

			convertView.setTag(viewHolder);

			viewHolder.checkbox.setTag(GlobalClass.contactList.get(position));
		}
		else
		{


			((ViewHolder) convertView.getTag()).checkbox.setTag(GlobalClass.contactList.get(position));

		}

		// fill data
		ViewHolder holder = (ViewHolder) convertView.getTag();

		mImageFetcher.loadImage(GlobalClass.contactList.get(position).getProfilePicPath(), holder.image);

		holder.name_text.setText(GlobalClass.contactList.get(position).getName());

		holder.status_text.setText(GlobalClass.contactList.get(position).getStatus());

		holder.checkbox.setChecked(GlobalClass.contactList.get(position).isSelected());

		return convertView;
	}

}
