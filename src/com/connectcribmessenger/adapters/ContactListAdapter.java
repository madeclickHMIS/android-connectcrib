package com.connectcribmessenger.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.entities.Contact;
import com.connectcribmessenger.entities.ConversationInfo;
import com.connectcribmessenger.main.ChatScreen;
import com.connectcribmessenger.main.ContactListAllScreen;
import com.connectcribmessenger.main.R;
import com.example.android.displayingbitmaps.util.ImageFetcher;

public class ContactListAdapter extends BaseAdapter 
{

	private Activity act;

	private ArrayList<Contact> contactList;

	private ImageFetcher mImageFetcher;

	private LayoutInflater mLayoutInflater;

	private HashMap<Integer, ImageView> imagemap;

	public ContactListAdapter(Activity act,ArrayList<Contact> contactList,ImageFetcher mImageFetcher)
	{
		this.act=act;

		this.contactList=contactList;

		this.mImageFetcher=mImageFetcher;

		mLayoutInflater = act.getLayoutInflater();

		imagemap=new HashMap<Integer, ImageView>();

	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return contactList.size();
	}

	@Override
	public Contact getItem(int position) {
		// TODO Auto-generated method stub
		return contactList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	static class ViewHolder 
	{
		public TextView name_text;

		public TextView status_text;

		public ImageView image;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;
		// reuse views
		if (convertView == null) 
		{
			//LayoutInflater inflater = context.getLayoutInflater();
			convertView=mLayoutInflater.inflate(R.layout.contact_list_row, null);
			// configure view holder
			viewHolder = new ViewHolder();

			viewHolder.image=(ImageView)convertView.findViewById(R.id.thumbnail_imageview);

			viewHolder.name_text=(TextView)convertView.findViewById(R.id.name_textview);

			viewHolder.status_text=(TextView)convertView.findViewById(R.id.status_textview);

			convertView.setTag(viewHolder);
		}

		// fill data
		ViewHolder holder = (ViewHolder) convertView.getTag();

		mImageFetcher.loadImage(contactList.get(position).getProfilePicPath(), holder.image);

		holder.name_text.setText(contactList.get(position).getName());

		holder.status_text.setText(contactList.get(position).getMood());

		imagemap.put(position, holder.image);

		return convertView;
	}

	public ImageView getImageView(int position)
	{
		return imagemap.get(position);
	}
	public void onItemClick(int mPosition,ImageView mImageview)
	{
		ConversationInfo conversationInfo=new ConversationInfo();

		Contact contact=(Contact)getItem(mPosition);


		conversationInfo.setConversationName(contact.getName());

		conversationInfo.setConversationNumber(contact.getNumber());

		conversationInfo.setConversationImage(mImageview.getDrawable());

		conversationInfo.setConversationImagePath(contact.getProfilePicPath());

		conversationInfo.setConversationType(Constants.CHAT_TYPE_PEER_TO_PEER);

		conversationInfo.setConversationGroupId("");

		conversationInfo.setConversationGroupInfo("");

		GlobalClass.conversationInfo=conversationInfo;

		Intent i=new Intent(act,ChatScreen.class);

		act.startActivity(i);

		act.finish();
	}

}
