package com.connectcribmessenger.adapters;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.main.CreateProfile;

public class UploadVideo extends AsyncTask<Void,Void,String> 
{
	private ProgressDialog pd;

	private Activity mActivity;

	private Handler mHandler;

	private HashMap<String, String> mParameters;

	private String imageFileName;

	public UploadVideo(Activity mActivity,Handler handler, HashMap<String, String> mParameters) {

		this.mActivity = mActivity;

		this.mParameters = mParameters;

		this.mHandler=mHandler;

		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

		imageFileName = "Video_" + timeStamp + "_";
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();

		pd=new ProgressDialog(mActivity);

		pd.setMessage("Saving Profile..");

		pd.setCancelable(false);

		pd.show();
	}

	@Override
	protected String doInBackground(Void... params) 
	{
		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		DataInputStream inStream = null;
		String lineEnd = "rn";
		String twoHyphens = "--";
		String boundary =  "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1*1024*1024;
		//String responseFromServer = "";
		StringBuilder responseFromServer=null;
		String urlString = Constants.APP_SERVER_ROOT_PATH;
		try
		{
			//------------------ CLIENT REQUEST
			FileInputStream fileInputStream = new FileInputStream(new File(mParameters.get("path")) );
			// open a URL connection to the Servlet
			URL url = new URL(urlString);
			// Open a HTTP connection to the URL
			conn = (HttpURLConnection) url.openConnection();
			// Allow Inputs
			conn.setDoInput(true);
			// Allow Outputs
			conn.setDoOutput(true);
			// Don't use a cached copy.
			conn.setUseCaches(false);
			// Use a post method.
			conn.setRequestMethod("POST");

			conn.setRequestProperty("Connection", "Keep-Alive");

			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);

			conn.setRequestProperty("uploaded_file", imageFileName);

			dos = new DataOutputStream( conn.getOutputStream() );

			dos.writeBytes(twoHyphens + boundary + lineEnd);

			dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
					+ imageFileName + "\"" + lineEnd);

			dos.writeBytes(lineEnd);
			// create a buffer of maximum size
			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];
			// read file and write it into form...
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			while (bytesRead > 0)
			{
				dos.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}
			// send multipart form data necesssary after file data...
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
			// close streams
			Log.e("Debug","File is written");
			fileInputStream.close();
			dos.flush();
			dos.close();
		}
		catch (MalformedURLException ex)
		{
			Log.e("Debug", "error: " + ex.getMessage(), ex);
		}
		catch (IOException ioe)
		{
			Log.e("Debug", "error: " + ioe.getMessage(), ioe);
		}
		//------------------ read the SERVER RESPONSE
		try 
		{
			inStream = new DataInputStream ( conn.getInputStream() );
			String line = null;
			BufferedReader reader = null;
			//StringBuilder sb=null;
			try {
				reader=new BufferedReader(new InputStreamReader(inStream));
				responseFromServer=new StringBuilder();
				line = reader.readLine();
				while(line!=null)
				{
					responseFromServer.append(line);
					line=reader.readLine();

				}
			} 
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally 
			{
				if (reader != null) 
				{
					try {
						reader.close();
						inStream.close();
					} catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
			}





		}
		catch (IOException ioex)
		{
			Log.e("Debug", "error: " + ioex.getMessage(), ioex);
		}
		return responseFromServer.toString();

	}

	@Override
	protected void onPostExecute(String result) 
	{
		// TODO Auto-generated method stub
		super.onPostExecute(result);

	}

	public int uploadFile(String profileimage, String number,String name,
			String mood, String availabilitystatus) 
	{
		int serverResponseCode = 0;
		JSONObject jsonObject = null ;
		try {
			//contact = new JSONArray();

			jsonObject = new JSONObject();

			jsonObject.put("key", "update");

			jsonObject.put("number", number);

			jsonObject.put("name", name);

			jsonObject.put("lm", mood);

			jsonObject.put("status", availabilitystatus);

			//contact.put(jsonObject);

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		String fileName = profileimage;

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(profileimage);

		if (!sourceFile.isFile()) {
			// dialog.dismiss();

			Log.e("uploadFile", "Source File not exist :" + profileimage);
			return 0;

		} else {
			try {

				// open a URL connection to the Servlet
				FileInputStream fileInputStream = new FileInputStream(
						sourceFile);
				URL url = new URL(Constants.APP_SERVER_ROOT_PATH
						+ URLEncoder.encode(jsonObject.toString(), "UTF-8"));

				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				conn.setRequestProperty("uploaded_file", fileName);

				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
						+ fileName + "\"" + lineEnd);

				dos.writeBytes(lineEnd);

				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0) {

					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();

				String serverResponseMessage = conn.getResponseMessage();

				Log.i("uploadFile", "HTTP Response is : "
						+ serverResponseMessage + ": " + serverResponseCode);

				if (serverResponseCode == 200) 
				{
					/*
					runOnUiThread(new Runnable() {
						public void run() 
						{
							Toast.makeText(CreateProfile.this, "File Upload Complete.", 
									Toast.LENGTH_SHORT).show();
						}
					});*/
				}

				// close the streams //
				fileInputStream.close();
				dos.flush();
				dos.close();

			} 

			catch (Exception ex) {

				// dialog.dismiss();
				ex.printStackTrace();
				/*
				runOnUiThread(new Runnable() {
					public void run() 
					{
						Toast.makeText(CreateProfile.this, "Failed to save profile", 
								Toast.LENGTH_SHORT).show();
					}
				});*/

				Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
			}
			// dialog.dismiss();
			return serverResponseCode;

		} // End else block
	}
}
