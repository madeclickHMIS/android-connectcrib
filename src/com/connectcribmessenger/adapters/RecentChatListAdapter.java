package com.connectcribmessenger.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.backgroundtasks.SqliteDataProviderThread;
import com.connectcribmessenger.entities.ConversationInfo;
import com.connectcribmessenger.entities.RecentChat;
import com.connectcribmessenger.imageloader.ImageLoader;
import com.connectcribmessenger.main.AppBuilderClass;
import com.connectcribmessenger.main.ChatScreen;
import com.connectcribmessenger.main.R;
import com.example.android.displayingbitmaps.util.ImageFetcher;

public class RecentChatListAdapter extends BaseAdapter {
	Activity act;
	ArrayList<Integer> images;
	ArrayList<String> names;
	ArrayList<String> chats;
	LayoutInflater inflater;
	public ImageLoader imageLoader;
	ViewHolder viewHolder;
	ArrayList<RecentChat> recentChatList;

	AppBuilderClass mAppbuilder;

	ImageFetcher mImageFetcher;

	private SqliteDataProviderThread sqliteDataProvider;
	
	private HashMap<Integer, ImageView> imagemap;

	public RecentChatListAdapter(Activity act,ArrayList<RecentChat> recentChatList,ImageFetcher mImageFetcher)
	{
		this.act=act;

		this.recentChatList=recentChatList;

		inflater = act.getLayoutInflater();

		this.mImageFetcher=mImageFetcher;

		imageLoader = new ImageLoader(act);

		mAppbuilder=new AppBuilderClass(act);
		
		imagemap=new HashMap<>();


	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return recentChatList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return recentChatList.get(position).getSenderName();
	}

	@Override
	public long getItemId(int position) 
	{
		// TODO Auto-generated method stub
		return position;
	}

	static class ViewHolder 
	{
		public TextView name_text;
		public TextView chat_text;
		public TextView time_text;
		public TextView new_message_count;
		public ImageView image;
		public Button user_detail_image;
	}
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		// reuse views
		if (convertView == null) 
		{
			//LayoutInflater inflater = context.getLayoutInflater();
			convertView=inflater.inflate(R.layout.row2, null);
			// configure view holder
			viewHolder = new ViewHolder();
			viewHolder.image=(ImageView)convertView.findViewById(R.id.image);


			//viewHolder.user_detail_image=(Button)convertView.findViewById(R.id.item_options_img);
			viewHolder.name_text=(TextView)convertView.findViewById(R.id.name);
			viewHolder.chat_text=(TextView)convertView.findViewById(R.id.chat);
			viewHolder.time_text=(TextView)convertView.findViewById(R.id.time);
			viewHolder.new_message_count=(TextView)convertView.findViewById(R.id.new_message_count);

			//viewHolder.new_message_count.setVisibility(View.INVISIBLE);

			convertView.setTag(viewHolder);
		}

		// fill data
		final ViewHolder holder = (ViewHolder) convertView.getTag();
		if(recentChatList.get(position).getSeenStatus().equals(Constants.MESSAGE_UNSEEN))
		{
			holder.new_message_count.setVisibility(View.VISIBLE);
		}
		else
		{
			holder.new_message_count.setVisibility(View.INVISIBLE);
		}

		//holder.image.setBackgroundResource(images.get(position));
		/*ViewTreeObserver vto = viewHolder.image.getViewTreeObserver();
		vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
			public boolean onPreDraw() {
				int height = viewHolder.image.getMeasuredHeight();
				int width = viewHolder.image.getMeasuredWidth();
				//tv.setText("Height: " + height + " Width: " + width);
				imageLoader.initImageSize(height,width);
				Log.i("imageview","Position-->"+position+" Height: " + height + " Width: " + width);
				imageLoader.DisplayImage(recentChatList.get(position).getPic(), holder.image);
				return true;
			}
		});*/
		mImageFetcher.loadImage(recentChatList.get(position).getPic(), holder.image);



		if(recentChatList.get(position).getChatType().equals(Constants.CHAT_TYPE_GROUP))
		{
			holder.name_text.setText(recentChatList.get(position).getGroupName());	
			holder.chat_text.setText("      "+recentChatList.get(position).getSenderName()+":"+recentChatList.get(position).getMessage());
			if(recentChatList.get(position).getMessageInOutStatus().equals(Constants.MESSAGE_INOUT_OUTGOING_STATUS))
			{
				int deliverIcon;
				if(recentChatList.get(position).getDeliverStatus().equals(Constants.MESSAGE_DELIVERY_STATUS_NO))
				{
					deliverIcon=R.drawable.waiting;
				}
				else if(recentChatList.get(position).getDeliverStatus().equals(Constants.MESSAGE_DELIVERY_STATUS_HALF))
				{
					deliverIcon=R.drawable.chat_arrow_gray;
				}
				else if(recentChatList.get(position).getDeliverStatus().equals(Constants.MESSAGE_DELIVERY_STATUS_FULL))
				{
					deliverIcon=R.drawable.message_sent;
				}
				else
				{
					deliverIcon=R.drawable.message_read;
				}
				mAppbuilder.setDrawableInText(holder.chat_text, holder.chat_text.getText().toString(),deliverIcon,0,2,30); 
			}
			else
			{
				if(recentChatList.get(position).getSeenStatus().equals(Constants.MESSAGE_SEEN))
				{

					mAppbuilder.setDrawableInText(holder.chat_text, holder.chat_text.getText().toString(),R.drawable.arrow_gray,0,2,30); 
				}
				else
				{
					mAppbuilder.setDrawableInText(holder.chat_text, holder.chat_text.getText().toString(),R.drawable.arrow_blue,0,2,30); 
					HashMap<String, String> insertValues=new HashMap<>();

					insertValues.put("param", Constants.MESSAGE_COUNT_GROUP);

					insertValues.put("group_info", recentChatList.get(position).getGroupInfo());
					sqliteDataProvider=new SqliteDataProviderThread(act,insertValues,holder.new_message_count);

					sqliteDataProvider.execute(null,null,null);

				}

			}


		}
		else
		{
			holder.name_text.setText(recentChatList.get(position).getSenderName());	
			holder.chat_text.setText("      "+recentChatList.get(position).getMessage());

			if(recentChatList.get(position).getMessageInOutStatus().equals(Constants.MESSAGE_INOUT_OUTGOING_STATUS))
			{
				int deliverIcon;
				if(recentChatList.get(position).getDeliverStatus().equals(Constants.MESSAGE_DELIVERY_STATUS_NO))
				{
					deliverIcon=R.drawable.waiting;
				}
				else if(recentChatList.get(position).getDeliverStatus().equals(Constants.MESSAGE_DELIVERY_STATUS_HALF))
				{
					deliverIcon=R.drawable.chat_arrow_gray;
				}
				else if(recentChatList.get(position).getDeliverStatus().equals(Constants.MESSAGE_DELIVERY_STATUS_FULL))
				{
					deliverIcon=R.drawable.message_sent;
				}
				else
				{
					deliverIcon=R.drawable.message_read;
				}
				mAppbuilder.setDrawableInText(holder.chat_text, holder.chat_text.getText().toString(),deliverIcon,0,2,30); 
			}
			else
			{

				if(recentChatList.get(position).getSeenStatus().equals(Constants.MESSAGE_SEEN))
				{
					mAppbuilder.setDrawableInText(holder.chat_text, holder.chat_text.getText().toString(),R.drawable.arrow_gray,0,2,30); 
				}
				else
				{
					mAppbuilder.setDrawableInText(holder.chat_text, holder.chat_text.getText().toString(),R.drawable.arrow_blue,0,2,30);
					HashMap<String, String> insertValues=new HashMap<>();

					insertValues.put("param", Constants.MESSAGE_COUNT_PEER_TO_PEER);

					insertValues.put("number", recentChatList.get(position).getSenderNumber());

					sqliteDataProvider=new SqliteDataProviderThread(act,insertValues,holder.new_message_count);

					sqliteDataProvider.execute(null,null,null);
				}
			}

		}



		//		holder.time_text.setText(recentChatList.get(position).getTime());
		holder.time_text.setText(GlobalClass.getTimeToDisplay(recentChatList.get(position).getTime()));
		
		imagemap.put(position, holder.image);
		//convertView.setOnClickListener(new OnItemClickListener(position,holder.image));
		return convertView;
	}
	private class OnItemClickListener  implements OnClickListener{          
		private int mPosition;

		private ImageView mImageview;

		OnItemClickListener(int position,ImageView imageview){
			mPosition = position;
			mImageview=imageview;
		}

		@Override
		public void onClick(View arg0) 
		{
			ConversationInfo conversationInfo=new ConversationInfo();

			if(recentChatList.get(mPosition).getChatType().equals("group"))
			{
				conversationInfo.setConversationName(recentChatList.get(mPosition).getGroupName());
			}
			else
			{
				conversationInfo.setConversationName(recentChatList.get(mPosition).getSenderName());
			}



			conversationInfo.setConversationNumber(recentChatList.get(mPosition).getSenderNumber());
			conversationInfo.setConversationImage(mImageview.getDrawable());
			conversationInfo.setConversationImagePath(recentChatList.get(mPosition).getPic());
			conversationInfo.setConversationType(recentChatList.get(mPosition).getChatType());
			conversationInfo.setConversationGroupId(recentChatList.get(mPosition).getGroupId());
			conversationInfo.setConversationGroupInfo(recentChatList.get(mPosition).getGroupInfo());

			GlobalClass.conversationInfo=conversationInfo;

			Intent i=new Intent(act,ChatScreen.class);



			act.startActivity(i);

		}              
	} 

	/**
	 * Sets the item height. Useful for when we know the column width so the height can be set
	 * to match.
	 *
	 * @param height
	 */
	public void setItemHeight(int size) 
	{     
		GlobalClass.mImageFetcher.setImageSize(size);
		notifyDataSetChanged();
	}

	public void onItemClick(int mPosition,ImageView mImageview)
	{
		ConversationInfo conversationInfo=new ConversationInfo();

		if(recentChatList.get(mPosition).getChatType().equals("group"))
		{
			conversationInfo.setConversationName(recentChatList.get(mPosition).getGroupName());
		}
		else
		{
			conversationInfo.setConversationName(recentChatList.get(mPosition).getSenderName());
		}



		conversationInfo.setConversationNumber(recentChatList.get(mPosition).getSenderNumber());
		conversationInfo.setConversationImage(mImageview.getDrawable());
		conversationInfo.setConversationImagePath(recentChatList.get(mPosition).getPic());
		conversationInfo.setConversationType(recentChatList.get(mPosition).getChatType());
		conversationInfo.setConversationGroupId(recentChatList.get(mPosition).getGroupId());
		conversationInfo.setConversationGroupInfo(recentChatList.get(mPosition).getGroupInfo());

		GlobalClass.conversationInfo=conversationInfo;

		Intent i=new Intent(act,ChatScreen.class);



		act.startActivity(i);
	}

public ImageView getImageView(int position)
{
	return imagemap.get(position);
}
}
