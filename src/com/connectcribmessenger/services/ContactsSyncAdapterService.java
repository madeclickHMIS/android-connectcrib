package com.connectcribmessenger.services;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.OperationCanceledException;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SyncResult;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.ContactsContract.RawContacts;
import android.provider.ContactsContract.RawContacts.Entity;
import android.util.Log;
import android.widget.Toast;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.DatabaseAccessor;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.entities.Contact;
import com.connectcribmessenger.main.ContactListAllScreen;
import com.connectcribmessenger.main.R;

public class ContactsSyncAdapterService extends Service 
{
	private static final String TAG = "ContactsSyncAdapterService";
	private static SyncAdapterImpl sSyncAdapter = null;
	private static ContentResolver mContentResolver = null;
	private static String UsernameColumn = ContactsContract.RawContacts.SYNC1;
	private static String PhotoTimestampColumn = ContactsContract.RawContacts.SYNC2;

	public ContactsSyncAdapterService() {
		super();
	}

	private static class SyncAdapterImpl extends AbstractThreadedSyncAdapter {
		private static Context mContext;

		public SyncAdapterImpl(Context context) {
			super(context, true);
			mContext = context;
		}

		@Override
		public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
			try 
			{
				/************added code***************/
				mContentResolver = mContext.getContentResolver();

				ArrayList<Contact> contactList=getConnectCribContacts(getContacts() );

				ArrayList<Integer> rawContactIds=getRawContactIds(account);

				ArrayList<String> accountContacts=getAccountContacts(rawContactIds);

				ArrayList<String> notAssocitaedContacts=getNotAssociatedContacts(accountContacts, contactList);

				if(notAssocitaedContacts.size()>0)
				{
					for(int i=0;i<notAssocitaedContacts.size();i++)
					{
						addContact(account, notAssocitaedContacts.get(i), "connectcrib");
					}	
				}
				else
				{
					for(int i=0;i<contactList.size();i++)
					{
						addContact(account, contactList.get(i).getNumber(), "connectcrib");
					}
				}


				/***************************/

				//ContactsSyncAdapterService.performSync(mContext, account, extras, authority, provider, syncResult);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}

		private static ArrayList<String> getNotAssociatedContacts(ArrayList<String> numbers,ArrayList<Contact> servercontacts)
		{
			ArrayList<String> notAssociatedContacts=new ArrayList<>();

			for(int i=0;i<numbers.size();i++)
			{
				String number=numbers.get(i);

				for(int j=0;j<servercontacts.size();j++)
				{


					if(!(servercontacts.get(j).equals(number)))
					{
						GlobalClass.printDebugLog("notAssociatedContact","notAssociatedContact-->"+ number);
						notAssociatedContacts.add(number);
					}
				}

			}

			GlobalClass.printDebugLog("notAssociatedContacts","notAssociatedContacts-->"+ notAssociatedContacts.toString());


			return notAssociatedContacts;
		}
		private static ArrayList<String> getAccountContacts(ArrayList<Integer> rawContactIds)
		{
			ArrayList<String> accountContacts=new ArrayList<>();

			for(int i=0;i<rawContactIds.size();i++)
			{
				Cursor c=mContentResolver.query(ContactsContract.Data.CONTENT_URI,
						new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER,
				},
				ContactsContract.Data.RAW_CONTACT_ID + " = ? ",
				new String[]{String.valueOf(rawContactIds.get(i))},null
						);
				if(c.getCount()>0)
				{
					while(c.moveToNext())
					{
						GlobalClass.printDebugLog("accountContacts","RawContact Id Account Contact-->"+ c.getString(0));
						accountContacts.add(c.getString(0));


					}
				}
			}


			return accountContacts;
		}
		private static ArrayList<Integer> getRawContactIds(Account account)
		{
			ArrayList<Integer> rawContactIds=new ArrayList<>();

			Cursor c=mContentResolver.query(ContactsContract.RawContacts.CONTENT_URI,
					new String[]{ContactsContract.RawContacts._ID,
			},
			ContactsContract.RawContacts.ACCOUNT_NAME + " = ? AND " +
					ContactsContract.RawContacts.ACCOUNT_TYPE + " = ? ",
					new String[]{account.name,account.type},null
					);
			if(c.getCount()>0)
			{
				while(c.moveToNext())
				{
					GlobalClass.printDebugLog("rawContactIds","RawContact Id-->"+ c.getInt(0));
					rawContactIds.add(c.getInt(0));
				}
			}

			return rawContactIds;
		}
		private static ArrayList<Contact> getConnectCribContacts(ArrayList<Contact> contactList)
		{


			JSONArray jsonArrayContacts=new JSONArray();

			JSONObject jsonObjectContact=new JSONObject();

			GlobalClass.printDebugLog("ConnectCrib", "device contacts size: "+contactList.size());

			for(int i=0;i<contactList.size();i++)
			{

				try 
				{
					JSONObject jobj=new JSONObject();

					jobj.put("number", contactList.get(i).getNumber());

					jsonArrayContacts.put(jobj);

				} 
				catch (JSONException e) 
				{
					// TODO Auto-generated catch block

					GlobalClass.printDebugLog("ConnectCrib Exception", "JSONException while converting device contact into json format");

					e.printStackTrace();


				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}

			try {
				jsonObjectContact.put("contacts", jsonArrayContacts);

				GlobalClass.printDebugLog("ConnectCrib", "JsonFormat of device contacts-->"+jsonObjectContact.toString());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}


			HttpClient httpclient = GlobalClass.getHttpClient();

			/*This background task to get all the connectcrib contacts available on
			 *the device*/

			if(GlobalClass.isInternetAvailable(mContext))
			{

				ArrayList<Contact> connectCribContactList=new ArrayList<>();

				/*HttpParams myParams = new BasicHttpParams();

				HttpConnectionParams.setConnectionTimeout(myParams, 10000);

				HttpConnectionParams.setSoTimeout(myParams, 10000);*/

				//				HttpClient httpclient = new DefaultHttpClient(myParams );

				/*HttpClient httpclient = GlobalClass.getHttpClient();*/


				try 
				{
					HttpPost httpPost = new HttpPost(Constants.APP_SERVER_ROOT_PATH);

					List<NameValuePair> nameValuePair=new ArrayList<NameValuePair>();	

					/*GlobalClass.printDebugLog("SendSmsForOtp", "Api-->"+Constants.APP_SERVER_ROOT_PATH);

					GlobalClass.printDebugLog("SendSmsForOtp", "key-->"+Constants.API_SEND_SMS_PARAMETER_1_VALUE);

					GlobalClass.printDebugLog("SendSmsForOtp", "userId-->"+GlobalClass.REGISTRATION_ID);

					GlobalClass.printDebugLog("SendSmsForOtp", "number-->"+number);
					 */
					nameValuePair.add(new BasicNameValuePair(Constants.API_GET_CONNECTCRIB_CONTACTS_PARAMETER_1,Constants.API_GET_CONNECTCRIB_CONTACTS_PARAMETER_1_VALUE));

					nameValuePair.add(new BasicNameValuePair(Constants.API_GET_CONNECTCRIB_CONTACTS_PARAMETER_2,jsonObjectContact.toString()));	

					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));  


					//httppost.setHeader("Content-type", "application/json");

					//StringEntity se = new StringEntity(params[0]); 

					//se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

					//httppost.setEntity(se); 

					HttpResponse response = httpclient.execute(httpPost);

					String jsonData = EntityUtils.toString(response.getEntity());

					GlobalClass.printDebugLog("ConnectCrib Serever Data", "Contacts on connectcrib-->\n"+jsonData);

					JSONObject jobj=new JSONObject(jsonData);

					if(jobj.getString("success").equals("0"))
					{
						//Toast.makeText(ContactListAllScreen.this, "No Contacts Available", Toast.LENGTH_LONG).show();
					}
					else
					{
						JSONArray jarray=jobj.getJSONArray("data");

						for(int i=0;i<jarray.length();i++)
						{
							Contact contact=new Contact();

							jobj=jarray.getJSONObject(i);

							contact.setName(jobj.getString("name"));

							contact.setNumber(jobj.getString("number"));

							contact.setMood(jobj.getString("latest_mood"));

							contact.setAvailabilityStatus(jobj.getString("status"));

							contact.setProfilePicPath(jobj.getString("profile_image"));

							connectCribContactList.add(contact);

						}
						
						//GlobalClass.safeClose(httpclient);
						
						return connectCribContactList;
					}



				} 

				catch (ClientProtocolException e)
				{
					GlobalClass.printDebugLog("ConnectCrib Serever Data", "ClientProtocolException while getting connectcrib contacts");

					e.printStackTrace();
				} 
				catch (IOException e) 
				{
					GlobalClass.printDebugLog("ConnectCrib Serever Data", "IOException while getting connectcrib contacts");
					e.printStackTrace();

				} 
				catch (JSONException e) 
				{
					GlobalClass.printDebugLog("ConnectCrib Serever Data", "JSONException while getting connectcrib contacts");

					e.printStackTrace();
				}

				catch (Exception e) {
					e.printStackTrace();
				}



			}
			else
			{

			}
			//GlobalClass.safeClose(httpclient);
			
			return null;



		}

		public static ArrayList<Contact> getContacts() 
		{
			ArrayList<Contact> contactList=new ArrayList<>();
			ContentResolver cr=mContext.getContentResolver();
			Cursor cur=cr.query(ContactsContract.Contacts.CONTENT_URI,null,null,null,null);
			if (cur.getCount() > 0) 
			{
				GlobalClass.printDebugLog("number Size", "Device Contact List Length-->"+cur.getCount());
				while (cur.moveToNext()) 
				{
					Contact contact=new Contact();
					String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
					String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
					if(Integer.parseInt(cur.getString 
							(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER )))  > 0) 
					{
						//Query phone here.  Covered next
						Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
								null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?", new 
								String[]{id}, null);
						while (pCur.moveToNext()) 
						{
							// Do something with phones
							String pnumber 
							=pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

							DatabaseAccessor dbaccessor=new DatabaseAccessor(mContext);

							dbaccessor.open();

							Cursor c=dbaccessor.getProfile();
							c.moveToFirst();
							String profilenum=c.getString(2);

							c.close();

							dbaccessor.close();
							GlobalClass.printDebugLog("profile number", profilenum);
							GlobalClass.printDebugLog("contact", pnumber);
							if(pnumber!=null&&pnumber.length()>0&&!pnumber.equals(profilenum))
							{
								contact.setNumber(pnumber);

								contact.setName(name.trim().toLowerCase());

								contact.setStatus("Hey there! I am using ConnectCribMessenger");

								contact.setProfilePicPath("http://fc09.deviantart.net/fs71/f/2010/330/9/e/profile_icon_by_art311-d33mwsf.png");
							}



						} 
						pCur.close();
					}


					contactList.add(contact);


				}

				GlobalClass.printDebugLog("number Size", "Contact List Length-->"+contactList.size());
			}
			cur.close();
			return contactList;
		}
	}




	@Override
	public IBinder onBind(Intent intent) {
		IBinder ret = null;
		ret = getSyncAdapter().getSyncAdapterBinder();
		return ret;
	}

	private SyncAdapterImpl getSyncAdapter() {
		if (sSyncAdapter == null)
			sSyncAdapter = new SyncAdapterImpl(this);
		return sSyncAdapter;
	}

	private static void addContact(Account account, String number, String username) {
		Log.i(TAG, "Adding contact: " + number);
		ArrayList<ContentProviderOperation> operationList = new ArrayList<ContentProviderOperation>();

		ContentProviderOperation.Builder builder = ContentProviderOperation.newInsert(RawContacts.CONTENT_URI);

		builder.withValue(RawContacts.ACCOUNT_NAME, account.name);

		builder.withValue(RawContacts.ACCOUNT_TYPE, account.type);

		builder.withValue(RawContacts.SYNC1, username);

		operationList.add(builder.build());

		builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);

		builder.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0);
		//		builder.withValueBackReference(ContactsContract.CommonDataKinds.StructuredName.RAW_CONTACT_ID, 0);

		builder.withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);

		builder.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, number);
		/*builder.withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
		builder.withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, "Jyoti Raj Singh");*/

		operationList.add(builder.build());

		builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);

		builder.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0);

		builder.withValue(ContactsContract.Data.MIMETYPE, "vnd.android.cursor.item/vnd.com.connectcribmessenger.main.profile");

		builder.withValue(ContactsContract.Data.DATA1, username);

		builder.withValue(ContactsContract.Data.DATA2, "ConnectCrib");

		builder.withValue(ContactsContract.Data.DATA3, number);

		operationList.add(builder.build());

		try 
		{
			mContentResolver.applyBatch(ContactsContract.AUTHORITY, operationList);
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void updateContactStatus(ArrayList<ContentProviderOperation> operationList, long rawContactId, String status) {

		Uri rawContactUri = ContentUris.withAppendedId(RawContacts.CONTENT_URI, rawContactId);

		Uri entityUri = Uri.withAppendedPath(rawContactUri, Entity.CONTENT_DIRECTORY);

		Cursor c = mContentResolver.query(entityUri, new String[] { RawContacts.SOURCE_ID, Entity.DATA_ID, Entity.MIMETYPE, Entity.DATA1 }, null, null, null);

		try 
		{
			while (c.moveToNext()) 
			{
				if (!c.isNull(1)) 
				{
					String mimeType = c.getString(2);

					if (mimeType.equals("vnd.android.cursor.item/vnd.com.connectcribmessenger.main.profile")) {

						ContentProviderOperation.Builder builder = ContentProviderOperation.newInsert(ContactsContract.StatusUpdates.CONTENT_URI);

						builder.withValue(ContactsContract.StatusUpdates.DATA_ID, c.getLong(1));

						builder.withValue(ContactsContract.StatusUpdates.STATUS, status);

						builder.withValue(ContactsContract.StatusUpdates.STATUS_RES_PACKAGE, "org.c99.SyncProviderDemo");

						builder.withValue(ContactsContract.StatusUpdates.STATUS_LABEL, R.string.app_name);

						builder.withValue(ContactsContract.StatusUpdates.STATUS_ICON, R.drawable.logo_home);

						builder.withValue(ContactsContract.StatusUpdates.STATUS_TIMESTAMP, System.currentTimeMillis());

						operationList.add(builder.build());

						//Only change the text of our custom entry to the status message pre-Honeycomb, as the newer contacts app shows
						//statuses elsewhere
						if(Integer.decode(Build.VERSION.SDK) < 11) 
						{
							builder = ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI);

							builder.withSelection(BaseColumns._ID + " = '" + c.getLong(1) + "'", null);

							builder.withValue(ContactsContract.Data.DATA3, status);

							operationList.add(builder.build());
						}
					}
				}
			}
		} finally {
			c.close();
		}
	}

	private static void updateContactPhoto(ArrayList<ContentProviderOperation> operationList, long rawContactId, byte[] photo) {

		ContentProviderOperation.Builder builder = ContentProviderOperation.newDelete(ContactsContract.Data.CONTENT_URI);

		builder.withSelection(ContactsContract.Data.RAW_CONTACT_ID + " = '" + rawContactId 
				+ "' AND " + ContactsContract.Data.MIMETYPE + " = '" + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE + "'", null);

		operationList.add(builder.build());

		try {
			if(photo != null) 
			{
				builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);

				builder.withValue(ContactsContract.CommonDataKinds.Photo.RAW_CONTACT_ID, rawContactId);

				builder.withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE);

				builder.withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, photo);

				operationList.add(builder.build());

				builder = ContentProviderOperation.newUpdate(ContactsContract.RawContacts.CONTENT_URI);

				builder.withSelection(ContactsContract.RawContacts.CONTACT_ID + " = '" + rawContactId + "'", null);

				builder.withValue(PhotoTimestampColumn, String.valueOf(System.currentTimeMillis()));

				operationList.add(builder.build());
			}
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static class SyncEntry {
		public Long raw_id = 0L;
		public Long photo_timestamp = null;
	}

	private static void performSync(Context context, Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult)
			throws OperationCanceledException 
			{

		HashMap<String, SyncEntry> localContacts = new HashMap<String, SyncEntry>();

		mContentResolver = context.getContentResolver();

		Log.i(TAG, "performSync: " + account.toString());

		// Load the local contacts
		Uri rawContactUri = RawContacts.CONTENT_URI.buildUpon().appendQueryParameter(RawContacts.ACCOUNT_NAME, account.name).appendQueryParameter(
				RawContacts.ACCOUNT_TYPE, account.type).build();

		Cursor c1 = mContentResolver.query(rawContactUri, new String[] { BaseColumns._ID, UsernameColumn, PhotoTimestampColumn }, null, null, null);

		while (c1.moveToNext()) 
		{
			SyncEntry entry = new SyncEntry();

			entry.raw_id = c1.getLong(c1.getColumnIndex(BaseColumns._ID));

			entry.photo_timestamp = c1.getLong(c1.getColumnIndex(PhotoTimestampColumn));

			localContacts.put(c1.getString(1), entry);
		}

		ArrayList<ContentProviderOperation> operationList = new ArrayList<ContentProviderOperation>();

		try 
		{
			// If we don't have any contacts, create one. Otherwise, set a
			// status message

			//localContacts.get("connectcrib")-->connectcrib is username

			if (localContacts.get("connectcrib") == null) 
			{
				addContact(account, "Elmer Fudd", "connectcrib");
			} 
			else 
			{
				if (localContacts.get("connectcrib").photo_timestamp == null || System.currentTimeMillis() > (localContacts.get("connectcrib").photo_timestamp + 604800000L)) 
				{
					//You would probably download an image file and just pass the bytes, but this sample doesn't use network so we'll decode and re-compress the icon resource to get the bytes
					ByteArrayOutputStream stream = new ByteArrayOutputStream();

					Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.logo_home);

					icon.compress(CompressFormat.PNG, 0, stream);

					updateContactPhoto(operationList, localContacts.get("connectcrib").raw_id, stream.toByteArray());
				}
				updateContactStatus(operationList, localContacts.get("connectcrib").raw_id, "hunting wabbits");
			}

			if (operationList.size() > 0)

				mContentResolver.applyBatch(ContactsContract.AUTHORITY, operationList);
		} 
		catch(Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
			}
}
