package com.connectcribmessenger.services;

import java.net.URLDecoder;
import java.util.HashMap;

import android.app.Activity;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.DatabaseAccessor;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.auxilliaryclasses.TextCrypter;
import com.connectcribmessenger.backgroundtasks.SqliteDataProviderThread;
import com.connectcribmessenger.broadcastreceivers.GcmBroadcastReceiver;
import com.connectcribmessenger.main.AppBuilderClass;
import com.connectcribmessenger.main.ChatScreen;
import com.connectcribmessenger.main.InvitationDialogScreen;
import com.connectcribmessenger.main.R;
import com.connectcribmessenger.main.RecentChatListScreen;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmIntentService extends IntentService 
{
	public static final int NOTIFICATION_ID = 1;

	private NotificationManager mNotificationManager;

	NotificationCompat.Builder builder;

	private AppBuilderClass mAppBuilder;

	//private GetValues mGetvalues;

	//private StoreValues mStoreValues;

	private DatabaseAccessor dbAccessor;

	public GcmIntentService() 
	{
		super("GcmIntentService");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) 
	{
		//Toast.makeText(this, "onHandleIntent called", Toast.LENGTH_LONG).show();
		initObjects();
		GlobalClass.printDebugLog("onHandleIntent called", "onHandleIntent called");
		Bundle extras = intent.getExtras();

		for(String key : extras.keySet()){
			Object obj = extras.get(key);

			GlobalClass.printDebugLog(key, obj.toString());//later parse it as per your required type
		}
		
		//GlobalClass.printDebugLog("ChatListAdapter", "profilepic-->"+GlobalClass.profileData.getProfilePic());


		if(extras.get("onlineofflinestatus")!=null&&extras.get("number")!=null)
		{
			if(GlobalClass.HANDLER_CHAT_SCREEN!=null)
			{
				if(intent.getStringExtra("number").equals(GlobalClass.conversationInfo.getConversationNumber()))
				{
					Bundle b=new Bundle();

					b.putString("GcmIntentservice_Status", intent.getStringExtra("onlineofflinestatus"));
					Message m=new Message();

					m.setData(b);

					GlobalClass.HANDLER_CHAT_SCREEN.sendMessage(m);
				}


			}

		}

		if(extras.get("profile_pic")!=null&&extras.get("number")!=null)
		{
			dbAccessor.updatePeerToPeerProfilePic(intent.getStringExtra("number"), intent.getStringExtra("profile_pic"));
			if(GlobalClass.HANDLER_CHAT_SCREEN!=null)
			{

				if(intent.getStringExtra("number").equals(GlobalClass.conversationInfo.getConversationNumber()))
				{
					Bundle b=new Bundle();

					b.putString("profile_pic", intent.getStringExtra("profile_pic"));
					Message m=new Message();

					m.setData(b);

					GlobalClass.HANDLER_CHAT_SCREEN.sendMessage(m);
				}


			}

		}
		//GlobalClass.printDebugLog("data", extras.toString());

		//GlobalClass.printDebugLog("data", intent.toString());

		//GlobalClass.printDebugLog("data", extras.getString("data"));

		//GlobalClass.printDebugLog("to", extras.getString("to"));

		//GlobalClass.printDebugLog("status", "GCM message received");
		//Toast.makeText(this, extras.toString(), Toast.LENGTH_LONG).show();
		//Toast.makeText(this, "GCM message received", Toast.LENGTH_SHORT).show();

		//Toast.makeText(this, "has data", Toast.LENGTH_LONG).show();
		//Bundle extras = intent.getExtras();
		//Toast.makeText(this, "has data", Toast.LENGTH_LONG).show();
		//Toast.makeText(this, extras.toString(), Toast.LENGTH_LONG).show();

		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) // has effect of unparcelling Bundle
		{
			GlobalClass.printDebugLog("data", "has data");

			//GlobalClass.printDebugLog("status", "GCM message received");

			/** Filter messages based on message type. Since it is likely that GCM
			 * will be extended in the future with new message types, just ignore
			 * any message types you're not interested in, or that you don't
			 * recognize.*/

			if (GoogleCloudMessaging.
					MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
				sendNotification("Send error: " + extras.toString(),"GCM");
			} else if (GoogleCloudMessaging.
					MESSAGE_TYPE_DELETED.equals(messageType)) {
				sendNotification("Deleted messages on server: " +
						extras.toString(),"GCM");
				// If it's a regular GCM message, do some work.
			} else if (GoogleCloudMessaging.
					MESSAGE_TYPE_MESSAGE.equals(messageType)) {
				/*				 // This loop represents the service doing some work.
                for (int i=0; i<5; i++) {
                    Log.i("CCS Message", "Working... " + (i+1)
                            + "/5 @ " + SystemClock.elapsedRealtime());
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                    }
                }*/
				Log.i("CCS Message", "Completed work @ " + SystemClock.elapsedRealtime());
				// Post notification of received message.
				// sendNotification("Received: " + extras.toString());
				Log.i("CCS Message", "Received: " + extras.toString());

				if(extras.containsKey(Constants.GCM_DATA_KEY_MESSAGE_DELIVERY_STATUS))
				{


					if(extras.getString(Constants.GCM_DATA_KEY_CHAT_TYPE).equals(Constants.CHAT_TYPE_PEER_TO_PEER))
					{
						dbAccessor.updatePeerToPeerChatDeliveryStatus(extras.getString(Constants.GCM_DATA_KEY_MESSAGE_ID), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_DELIVERY_STATUS));

					}
					else if(extras.getString(Constants.GCM_DATA_KEY_CHAT_TYPE).equals(Constants.CHAT_TYPE_GROUP))
					{
						dbAccessor.updateGroupChatDeliveryStatus(extras.getString(Constants.GCM_DATA_KEY_GROUP_INFO),extras.getString(Constants.GCM_DATA_KEY_MESSAGE_ID), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_DELIVERY_STATUS));
					}

					if(GlobalClass.IS_RECENT_CHATS_LIST_CREEN_OPENED)
					{
						getLatestRecentListAndUpdateList(GlobalClass.HANDLER_RECENT_CHAT_LIST_SCREEN);


					}	
					else if(GlobalClass.IS_CHATSCREEN_OPENED)
					{
						fetchConversation(GlobalClass.HANDLER_CHAT_SCREEN);

					}
				}
				else if(extras.containsKey(Constants.GCM_DATA_KEY_MESSAGE))
				{

					if(extras.getString(Constants.GCM_DATA_KEY_CHAT_TYPE).equals(Constants.CHAT_TYPE_PEER_TO_PEER))
					{
						dbAccessor.open();

						Cursor c=dbAccessor.getPeer2PeerMessage(extras.getString(Constants.GCM_DATA_KEY_SENDER_NUMBER));


						c.moveToFirst();

						Log.i("GcmIntentService", "cursor count-->" + c.getCount());

						if(c.getCount()<=0)
						{
							c=dbAccessor.getBlockedUsers(extras.getString(Constants.GCM_DATA_KEY_SENDER_NUMBER));

							c.moveToFirst();

							if(c.getCount()<=0)
							{
								Log.i("GcmIntentService", "Chat Invitation.");
								
								sendInvitationNotification("Chat Invitation.", extras);
								
								return;
							}

						}
						else
						{
							dbAccessor.insertPeer2PeerMessage(extras.getString(Constants.GCM_DATA_KEY_SENDER_NUMBER), extras.getString(Constants.GCM_DATA_KEY_MESSAGE), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_TYPE), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_TIME), Constants.MESSAGE_INCOMING_DELIVERY_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS, extras.getString(Constants.GCM_DATA_KEY_PROFILE_PIC), extras.getString(Constants.GCM_DATA_KEY_SENDER_NAME), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_ID), Constants.MESSAGE_UNSEEN,Constants.MESSAGE_ARCHIVED_STATUS_NO);

							dbAccessor.updatePeerToPeerArchivedStatus(extras.getString(Constants.GCM_DATA_KEY_SENDER_NUMBER), Constants.MESSAGE_ARCHIVED_STATUS_NO);
						}



						c.close();

						dbAccessor.close();
					 

						//dbAccessor.insertPeer2PeerMessage(extras.getString(Constants.GCM_DATA_KEY_SENDER_NUMBER), extras.getString(Constants.GCM_DATA_KEY_MESSAGE), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_TYPE), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_TIME), Constants.MESSAGE_INCOMING_DELIVERY_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS, extras.getString(Constants.GCM_DATA_KEY_PROFILE_PIC), extras.getString(Constants.GCM_DATA_KEY_SENDER_NAME), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_ID), Constants.MESSAGE_UNSEEN,Constants.MESSAGE_ARCHIVED_STATUS_NO);

						//dbAccessor.updatePeerToPeerArchivedStatus(extras.getString(Constants.GCM_DATA_KEY_SENDER_NUMBER), Constants.MESSAGE_ARCHIVED_STATUS_NO);
					}
					else if(extras.getString(Constants.GCM_DATA_KEY_CHAT_TYPE).equals(Constants.CHAT_TYPE_GROUP))
					{

						dbAccessor.insertGroupMessage(extras.getString(Constants.GCM_DATA_KEY_SENDER_NUMBER), extras.getString(Constants.GCM_DATA_KEY_SENDER_NAME), extras.getString(Constants.GCM_DATA_KEY_MESSAGE), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_TYPE), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_TIME), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_DELIVERY_STATUS), Constants.MESSAGE_INOUT_INCOMING_STATUS, extras.getString(Constants.GCM_DATA_KEY_GROUP_INFO), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_ID), Constants.MESSAGE_UNSEEN,Constants.MESSAGE_ARCHIVED_STATUS_NO);

						dbAccessor.updateGroupArchivedStatus(extras.getString(Constants.GCM_DATA_KEY_GROUP_INFO), Constants.MESSAGE_ARCHIVED_STATUS_NO);
					}

					if(GlobalClass.IS_RECENT_CHATS_LIST_CREEN_OPENED)
					{

						getLatestRecentListAndUpdateList(GlobalClass.HANDLER_RECENT_CHAT_LIST_SCREEN);
						
						clearNotification();
					
					}	
					else if(GlobalClass.IS_CHATSCREEN_OPENED)
					{
						/*if(extras.getString(Constants.GCM_DATA_KEY_CHAT_TYPE).equals(Constants.CHAT_TYPE_PEER_TO_PEER))
						{

							GlobalClass.chatAdapter.updateList(extras.getString(Constants.GCM_DATA_KEY_MESSAGE),Constants.MESSAGE_INCOMING_DELIVERY_STATUS, extras.getString(Constants.GCM_DATA_KEY_SENDER_NAME), extras.getString(Constants.GCM_DATA_KEY_SENDER_NUMBER), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_TIME), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_TYPE),Constants.MESSAGE_INOUT_INCOMING_STATUS, GlobalClass.listview, Constants.MESSAGE_SEEN);

						}
						else if(extras.getString(Constants.GCM_DATA_KEY_CHAT_TYPE).equals(Constants.CHAT_TYPE_GROUP))
						{
							GlobalClass.chatAdapter.updateList(extras.getString(Constants.GCM_DATA_KEY_MESSAGE),Constants.MESSAGE_INCOMING_DELIVERY_STATUS, extras.getString(Constants.GCM_DATA_KEY_SENDER_NAME), extras.getString(Constants.GCM_DATA_KEY_SENDER_NUMBER),  extras.getString(Constants.GCM_DATA_KEY_MESSAGE_TIME), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_TYPE),Constants.MESSAGE_INOUT_INCOMING_STATUS,GlobalClass.listview,Constants.MESSAGE_SEEN);
						}*/

						//	GlobalClass.chatAdapter.updateList(extras.getString(Constants.GCM_DATA_KEY_MESSAGE),Constants.MESSAGE_INCOMING_DELIVERY_STATUS, extras.getString(Constants.GCM_DATA_KEY_SENDER_NAME), extras.getString(Constants.GCM_DATA_KEY_SENDER_NUMBER), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_TIME), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_TYPE),Constants.MESSAGE_INOUT_INCOMING_STATUS, GlobalClass.listview, Constants.MESSAGE_SEEN);
						GlobalClass.printDebugLog(GlobalClass.conversationInfo.getConversationNumber(), GlobalClass.conversationInfo.getConversationNumber());

						GlobalClass.printDebugLog(extras.getString(Constants.GCM_DATA_KEY_SENDER_NUMBER), extras.getString(Constants.GCM_DATA_KEY_SENDER_NUMBER));
						if(GlobalClass.conversationInfo.getConversationNumber().equals(extras.getString(Constants.GCM_DATA_KEY_SENDER_NUMBER)))
						{
							//Toast.makeText(this, "updating list", Toast.LENGTH_LONG).show();
							//GlobalClass.listview.smoothScrollToPosition(GlobalClass.MessageList.size()-1);
							GlobalClass.chatAdapter.updateList(extras.getString(Constants.GCM_DATA_KEY_MESSAGE),Constants.MESSAGE_INCOMING_DELIVERY_STATUS, extras.getString(Constants.GCM_DATA_KEY_SENDER_NAME), extras.getString(Constants.GCM_DATA_KEY_SENDER_NUMBER),  extras.getString(Constants.GCM_DATA_KEY_MESSAGE_TIME), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_TYPE),Constants.MESSAGE_INOUT_INCOMING_STATUS,GlobalClass.listview,Constants.MESSAGE_SEEN);


							Message m=new Message();

							Bundle b=new Bundle();

							b.putString("ccs_message", "update_list");

							m.setData(b);
							GlobalClass.HANDLER_CHAT_SCREEN.sendMessage(m);	
							//GlobalClass.listview.setAdapter(GlobalClass.chatAdapter);
						}
						else
						{
							sendNotification(extras.getString(Constants.GCM_DATA_KEY_MESSAGE),extras.getString(Constants.GCM_DATA_KEY_SENDER_NAME));
						}

						//fetchConversation(GlobalClass.HANDLER_CHAT_SCREEN);
						clearNotification();

					}

					else
					{
						sendNotification(extras.getString(Constants.GCM_DATA_KEY_MESSAGE),extras.getString(Constants.GCM_DATA_KEY_SENDER_NAME));
					}
				}
			}



		}
		else
		{
			GlobalClass.printDebugLog("data", "has no data");
			Toast.makeText(this, "no data", Toast.LENGTH_LONG).show();	
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);


	}
	/**
	 * Put the message into a notification and post it.
	 * This is just one simple example of what you might choose to do with 
	 * a GCM message.
	 * @param msg
	 * @param extras
	 */
	private void sendNotification(String msg, Bundle extras)
	{
		mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

		Intent intent = new Intent(this, ChatScreen.class).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		// If this is a notification type message include the data from the message 
		// with the intent
		if (extras != null)
		{
			intent.putExtras(extras);
			intent.setAction("com.antoinecampbell.gcmdemo.NOTIFICATION");
		}
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);

		NotificationCompat.Builder mBuilder = 
				new NotificationCompat.Builder(this)
		.setSmallIcon(R.drawable.ic_stat_cloud)
		.setContentTitle("GCM Notification")
		.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
		.setContentText(msg)
		.setTicker(msg)
		.setAutoCancel(true)
		.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);

		mBuilder.setContentIntent(contentIntent);
		
		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
	}

	/**
	 * Remove the app's notification
	 */
	public void clearNotification()
	{
		mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancel(NOTIFICATION_ID);
	}

	// Put the message into a notification and post it.
	// This is just one simple example of what you might choose to do with
	// a GCM message.
	private void sendNotification(String msg,String senderName) {
		mNotificationManager = (NotificationManager)
				this.getSystemService(Context.NOTIFICATION_SERVICE);

		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				new Intent(this, RecentChatListScreen.class), 0);
		
		NotificationCompat.Builder mBuilder =
				new NotificationCompat.Builder(this);
		try 
		{
							
			String res = new String( new TextCrypter().decrypt(msg), "UTF-8" );
			
			res = URLDecoder.decode(res,"UTF-8");

			mBuilder
			.setSmallIcon(R.drawable.smile)
			.setContentTitle(senderName)
			.setStyle(new NotificationCompat.BigTextStyle()
			.bigText(msg))
			.setContentText(res)
			.setDefaults(Notification.DEFAULT_SOUND)
			.setAutoCancel(false);

			
		
		} catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			
			mBuilder
			.setSmallIcon(R.drawable.smile)
			.setContentTitle(senderName)
			.setStyle(new NotificationCompat.BigTextStyle()
			.bigText(msg))
			.setContentText(msg)
			.setDefaults(Notification.DEFAULT_SOUND)
			.setAutoCancel(false);

			
			e1.printStackTrace();
		}

		mBuilder.setContentIntent(contentIntent);
		
		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

		/*NotificationCompat.Builder mBuilder =
				new NotificationCompat.Builder(this)
		.setSmallIcon(R.drawable.smile)
		.setContentTitle(senderName)
		.setStyle(new NotificationCompat.BigTextStyle()
		.bigText(msg))
		.setContentText(msg)
		.setDefaults(Notification.DEFAULT_SOUND)
		.setAutoCancel(false);

		mBuilder.setContentIntent(contentIntent);
		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());*/
	}

	// Put the message into a notification and post it.
	// This is just one simple example of what you might choose to do with
	// a GCM message.
	private void sendInvitationNotification(String msg,Bundle extras) {
		mNotificationManager = (NotificationManager)
				this.getSystemService(Context.NOTIFICATION_SERVICE);

		Intent intent=new Intent(this, InvitationDialogScreen.class);

		intent.putExtra("extras", extras);

		

		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				intent, 0);



		NotificationCompat.Builder mBuilder =
				new NotificationCompat.Builder(this)
		.setSmallIcon(R.drawable.smile)
		.setContentTitle(extras.getString(Constants.GCM_DATA_KEY_SENDER_NAME))
		.setStyle(new NotificationCompat.BigTextStyle()
		.bigText(msg))
		.setContentText(msg)
		.setDefaults(Notification.DEFAULT_SOUND)
		.setAutoCancel(false);

		mBuilder.setContentIntent(contentIntent);
		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
	}


	private void initObjects()
	{
		//mAppBuilder=new AppBuilderClass(this);

		//mGetvalues=new GetValues(this);

		//mStoreValues=new StoreValues(this);

		dbAccessor=new DatabaseAccessor(this);
	}

	public void getLatestRecentListAndUpdateList(Handler handler)
	{
		HashMap<String,String> insertValues=new HashMap<>();

		insertValues.put("param", "get_latest_recent_chat_list");

		SqliteDataProviderThread sqliteThread=new SqliteDataProviderThread(this,handler, insertValues);

		sqliteThread.execute();
	}

	public void fetchConversation(Handler handler)
	{
		String chatType;

		String senderNumber;

		String groupId;

		if(GlobalClass.conversationInfo!=null)
		{
			chatType=GlobalClass.conversationInfo.getConversationType();

			senderNumber=GlobalClass.conversationInfo.getConversationNumber();

			groupId=GlobalClass.conversationInfo.getConversationGroupId();

		}
		else
		{

			chatType=getSharedPreferences(Constants.SHAREDPREFERENCE_FILE, Activity.MODE_PRIVATE).getString(Constants.SHAREDPREFERENCE_KEY_CHAT_TYPE, "noval");

			senderNumber=getSharedPreferences(Constants.SHAREDPREFERENCE_FILE, Activity.MODE_PRIVATE).getString(Constants.SHAREDPREFERENCE_KEY_SENDER_NUMBER, "noval");

			groupId=getSharedPreferences(Constants.SHAREDPREFERENCE_FILE, Activity.MODE_PRIVATE).getString(Constants.SHAREDPREFERENCE_KEY_GROUP_ID, "noval");

		}

		if(!chatType.equals("noval")&&!senderNumber.equals("noval")&&!groupId.equals("noval"))
		{
			HashMap<String,String> insertValues=new HashMap<>();

			insertValues.put(Constants.HASHMAP_KEY_PARAM, Constants.HASHMAP_VALUE_CHAT_SCREEN);

			insertValues.put(Constants.HASHMAP_KEY_CHAT_TYPE, chatType);

			insertValues.put(Constants.HASHMAP_KEY_SENDER_NUMBER, senderNumber);

			insertValues.put(Constants.HASHMAP_KEY_GROUP_ID, groupId);


			SqliteDataProviderThread sqliteThread=new SqliteDataProviderThread(this,handler, insertValues);

			sqliteThread.execute();
		}


	}


}
