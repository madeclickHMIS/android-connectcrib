package com.connectcribmessenger.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.DatabaseAccessor;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.entities.Profile;

public class UpdateOfflineStatus extends IntentService 
{

	public UpdateOfflineStatus() {
		super("UpdateOfflineStatus");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) 
	{
		boolean run=true;
		
		DatabaseAccessor dbaccessor=new DatabaseAccessor(this);

		dbaccessor.open();

		Cursor c=dbaccessor.getProfile();

		c.moveToFirst();

		Profile profile=new Profile();

		profile.setProfileId(c.getInt(0));

		profile.setProfileName(c.getString(1));

		profile.setProfileNumber(c.getString(2));

		profile.setProfilePic(c.getString(3));

		profile.setProfileStatus(c.getString(4));

		GlobalClass.profileData=profile;


		c.close();

		dbaccessor.close();

		/*HttpParams myParams = new BasicHttpParams();

		HttpConnectionParams.setConnectionTimeout(myParams, 30000);

		HttpConnectionParams.setSoTimeout(myParams, 30000);*/

//		HttpClient httpclient = new DefaultHttpClient(myParams );
		
		HttpClient httpclient = GlobalClass.getHttpClient();

		while (run) 
		{
			try {

				HttpPost httpPost = new HttpPost(Constants.APP_SERVER_ROOT_PATH);

				List<NameValuePair> nameValuePair=new ArrayList<NameValuePair>();	



				GlobalClass.printDebugLog("Profile Number", "profilenumber for offline-->"+GlobalClass.profileData.getProfileNumber());

				nameValuePair.add(new BasicNameValuePair(Constants.API_UPDATE_ONLINEOFFLINE_STATUS_PARAMETER_1,Constants.API_UPDATE_ONLINEOFFLINE_STATUS_PARAMETER_1_VALUE));

				nameValuePair.add(new BasicNameValuePair(Constants.API_UPDATE_ONLINEOFFLINE_STATUS_PARAMETER_2,GlobalClass.profileData.getProfileNumber()));	

				nameValuePair.add(new BasicNameValuePair(Constants.API_UPDATE_ONLINEOFFLINE_STATUS_PARAMETER_4,"offline"));

				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));  




				HttpResponse response = httpclient.execute(httpPost);

				String jsonData = EntityUtils.toString(response.getEntity());

				GlobalClass.printDebugLog("response", "makeOfflineStatus-->\n"+jsonData);



				JSONObject jobj=new JSONObject(jsonData);

				if(jobj.getString("success").equals("0"))
				{
					run=true;
				}
				else
				{
					//GlobalClass.safeClose(httpclient);
					
					run=false;
				}


			} 



			catch (Exception e) 
			{
				run=true;

				e.printStackTrace();

			}
		}

	}

}
