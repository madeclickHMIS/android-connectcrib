package com.connectcribmessenger.main;

import java.util.ArrayList;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.view.Window;
import android.widget.ImageView;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;

public class SplashScreen extends FragmentActivity implements Callback
{
	ImageView slideImageView;



	private long startTime;

	ImageView header,progress_dots;

	private  static Handler handler;

	private static  AppBuilderClass mAppBuilderClass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.splash);

		slideImageView = (ImageView)findViewById(R.id.header_image);

		progress_dots = (ImageView)findViewById(R.id.progress_dots);

		//Setting start time for spalsh screen timeout
		startTime=System.currentTimeMillis();

		mAppBuilderClass=new AppBuilderClass(this);

		handler=new Handler(this);

		//Starts the splash screen animations that include Slide show at top and Progressing dots
		mAppBuilderClass.startSplashScreenAnimation(slideImageView,progress_dots);

		//Performs the registration process with GCM,App Server and local
		gcmRegistration();
		try
		{
			addAccount();	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}



		//addContact(accounts[0], accounts[0].name, accounts[0].name,"+919953828107");

	}
	private void addAccount()
	{
		AccountManager am = AccountManager.get(this);
		Account[] accounts=am.getAccounts();
		boolean isavailable=false;

		Account myAccount = null;

		if(accounts.length>0)
		{
			GlobalClass.printDebugLog("ConnectCrib SplashScreen", "number of accounts-->"+accounts.length);
			for(int i=0;i<accounts.length;i++)
			{
				if(accounts[i].type.equals(getString(R.string.ACCOUNT_TYPE)))
				{
					GlobalClass.printDebugLog("ConnectCrib SplashScreen", "account available-->"+accounts[i].type);

					isavailable=true;

					myAccount=accounts[i];

					ArrayList<Integer> rawContactIds=getRawContactIds(myAccount);

					GlobalClass.accountContacts=getAccountContacts(rawContactIds);

					GlobalClass.printDebugLog("SplashScreen", "GlobalClass.accountContacts-->"+GlobalClass.accountContacts.toString());

				}

			}

			if(!isavailable)
			{
				Account account = new Account(getString(R.string.ACCOUNT_NAME), getString(R.string.ACCOUNT_TYPE));
				String password = "12345";
				AccountManager accountManager = AccountManager.get(this);
				accountManager.addAccountExplicitly(account, password, null);
			}

			if(myAccount!=null)
			{
				if(ContentResolver.isSyncActive(myAccount, Constants.SYNCADAPTER_AUTHORITY))
				{
					ContentResolver.requestSync(
							myAccount, 
							Constants.SYNCADAPTER_AUTHORITY, 
							new Bundle());
				}
			}

		}
		else
		{
			GlobalClass.printDebugLog("ConnectCrib SplashScreen", "number of accounts-->"+accounts.length);
			Account account = new Account(getString(R.string.ACCOUNT_NAME), getString(R.string.ACCOUNT_TYPE));
			String password = "12345";
			AccountManager accountManager = AccountManager.get(this);
			accountManager.addAccountExplicitly(account, password, null);
		}	
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}
	@Override
	public boolean handleMessage(Message msg)
	{
		String status=msg.getData().getString("status");

		if(status!=null)
		{
			mAppBuilderClass.navigateFromSplash(status, startTime);
		}


		return true;
	}

	public static void gcmRegistration()
	{
		mAppBuilderClass.gcmRegistration(handler);
	}
	private  ArrayList<String> getAccountContacts(ArrayList<Integer> rawContactIds)
	{
		ArrayList<String> accountContacts=new ArrayList<>();

		for(int i=0;i<rawContactIds.size();i++)
		{
			Cursor c=getContentResolver().query(ContactsContract.Data.CONTENT_URI,
					new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER,
			},
			ContactsContract.Data.RAW_CONTACT_ID + " = ? ",
			new String[]{String.valueOf(rawContactIds.get(i))},null
					);
			if(c.getCount()>0)
			{
				while(c.moveToNext())
				{
					GlobalClass.printDebugLog("accountContacts","RawContact Id Account Contact-->"+ c.getString(0));
					accountContacts.add(c.getString(0));


				}
			}
		}


		return accountContacts;
	}
	private ArrayList<Integer> getRawContactIds(Account account)
	{
		ArrayList<Integer> rawContactIds=new ArrayList<>();

		Cursor c=getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI,
				new String[]{ContactsContract.RawContacts._ID,
		},
		ContactsContract.RawContacts.ACCOUNT_NAME + " = ? AND " +
				ContactsContract.RawContacts.ACCOUNT_TYPE + " = ? ",
				new String[]{account.name,account.type},null
				);
		if(c.getCount()>0)
		{
			while(c.moveToNext())
			{
				GlobalClass.printDebugLog("rawContactIds","RawContact Id-->"+ c.getInt(0));
				rawContactIds.add(c.getInt(0));
			}
		}

		return rawContactIds;
	}
	/*@Override
	public boolean onTouchEvent(MotionEvent event) {

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		Toast.makeText(SplashScreen.this, "Density of screen-->"+metrics.densityDpi, Toast.LENGTH_LONG).show();
		switch(metrics.densityDpi){
		     case DisplayMetrics.DENSITY_LOW:
		                break;
		     case DisplayMetrics.DENSITY_MEDIUM:
		                 break;
		     case DisplayMetrics.DENSITY_HIGH:
		    	 Toast.makeText(SplashScreen.this, "hdpi", Toast.LENGTH_LONG).show();
		                 break;
		     case DisplayMetrics.DENSITY_XHIGH:
		    	 Toast.makeText(SplashScreen.this, "xhdpi", Toast.LENGTH_LONG).show();
                 break;
		     case DisplayMetrics.DENSITY_XXHIGH:
		    	 Toast.makeText(SplashScreen.this, "xxhdpi", Toast.LENGTH_LONG).show();
                 break;
		}

		return true;
	}*/

}
