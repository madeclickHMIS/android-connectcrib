package com.connectcribmessenger.main;

import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.Window;

import com.connectcribmessenger.auxilliaryclasses.GlobalClass;

public class VerificationScreen extends FragmentActivity implements Callback 
{

	private static AppBuilderClass mAppBuilder;

	private static Handler mHandler;

	private static String number;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//getSupportActionBar().setTitle("ConnectCrib");
		//getSupportActionBar().setLogo(getResources().getDrawable(R.drawable.logo_small));
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.verificationscreen);
		
		GlobalClass.isAppRunning=true;
		
		number=getIntent().getStringExtra("number");
		
		GlobalClass.printDebugLog("number", "VerificationScreen-->"+number);

		mAppBuilder=new AppBuilderClass(this);

		mHandler=new Handler(this);
				
		mAppBuilder.registerIncomingSmsReceiver(mHandler);

		sendSmsForOtp();

		
	}

	@Override
	protected void onDestroy() 
	{
		// TODO Auto-generated method stub
		super.onDestroy();

		mAppBuilder.unregisterIncomingSmsReceiver();
	}

	@Override
	public boolean handleMessage(Message msg)
	{
		String sendSmsForOtp=msg.getData().getString("SendSmsForOtp");
		
		String ConfirmOtp=msg.getData().getString("ConfirmOtp");
		
		String incomingSmsReceiver=msg.getData().getString("IncomingSmsReceiver");

		mAppBuilder.verifyContactNumber(number,sendSmsForOtp, incomingSmsReceiver, ConfirmOtp);
		
		return true;
	}

	public static void sendSmsForOtp()
	{
		mAppBuilder.sendSmsForOtp(mHandler,number);
	}
	
	public static void confirmOtp()
	{
		mAppBuilder.confirmOtp(mHandler, GlobalClass.OTP,number);
	}
}
