package com.connectcribmessenger.main;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.DatabaseAccessor;
import com.connectcribmessenger.auxilliaryclasses.GetValues;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.backgroundtasks.GetProfile;
import com.connectcribmessenger.backgroundtasks.UpdateProfile;
import com.connectcribmessenger.entities.Profile;
import com.connectcribmessenger.interfaces.CommonMethods;
import com.example.android.displayingbitmaps.util.ImageCache;
import com.example.android.displayingbitmaps.util.ImageFetcher;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;

public class EditProfile extends ActionBarActivity implements EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener,CommonMethods, Callback
{
	private ImageView profile_image;

	private Button changePic;

	private Button removePic;

	private Button next;

	private Button mSmileyBtn;

	private EditText name;

	private EditText mood;

	private TextView availabilityStatus;

	private FrameLayout mEmojiContainerLayout;

	private boolean isEmojiOpened=false;

	private String number;

	private List<String> availabilityStatusList;

	private ArrayAdapter<String> availabilityStatusAdapter;

	private String profileImagePath;

	public Bitmap profileimageBitmap;

	private ImageFetcher mImageFetcher;

	private boolean isfirst=true;

	private Context context;

	private Handler mHandler;

	private int DIALOG_CROPIMAGE=11;



	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		context=this;
		setContentView(R.layout.create_profile);

		createImageFetcherFromNetwork();

		instantiateViews();

		if(savedInstanceState!=null)
		{
			GlobalClass.profileData=savedInstanceState.getParcelable("profiledata");

			isfirst=savedInstanceState.getBoolean("isfirst");

			name.setText(savedInstanceState.getString("name"));

			mood.setText(savedInstanceState.getString("mood"));

			availabilityStatus.setText(savedInstanceState.getString("availability_status"));

			mImageFetcher.loadImage(GlobalClass.profileData.getProfilePic(), profile_image);
		}

		instantiateNonViewClasses();

		setproperties();

		setViewListeners();

	}
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		// Save the user's current game state

		savedInstanceState.putParcelable("profiledata", GlobalClass.profileData);

		savedInstanceState.putBoolean("isfirst", isfirst);

		savedInstanceState.putString("name", name.getText().toString());

		savedInstanceState.putString("mood", mood.getText().toString());

		savedInstanceState.putString("availability_status", availabilityStatus.getText().toString());

		// Always call the superclass so it can save the view hierarchy state
		super.onSaveInstanceState(savedInstanceState);
	}
	@Override
	public void instantiateNonViewClasses() 
	{
		mHandler=new Handler(this);
		number=getIntent().getStringExtra("number");

		availabilityStatusList = Arrays.asList(getResources().getStringArray(R.array.availabilty_status_options));

		availabilityStatusAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, availabilityStatusList);

	}

	@Override
	public void instantiateViews() 
	{
		profile_image=(ImageView)findViewById(R.id.profilepic_imageview);

		changePic=(Button)findViewById(R.id.changepic_btn);

		removePic=(Button)findViewById(R.id.removepic_btn);

		next=(Button)findViewById(R.id.next_btn);

		mSmileyBtn=(Button)findViewById(R.id.smiley_btn);

		name=(EditText)findViewById(R.id.name);

		mood=(EditText)findViewById(R.id.emoji_edittext);

		availabilityStatus=(TextView)findViewById(R.id.availability_status);



		mEmojiContainerLayout=(FrameLayout)findViewById(R.id.emoji_container);


	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		mImageFetcher.closeCache();

		GlobalClass.isEditScreenOpened=false;
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mImageFetcher.setPauseWork(false);

		mImageFetcher.setExitTasksEarly(true);

		mImageFetcher.flushCache();
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mImageFetcher.setExitTasksEarly(false);
	}

	public void createImageFetcherFromNetwork()
	{

		int mImageThumbWidth = getResources().getDimensionPixelSize(R.dimen.profile_icon_width);

		int mImageThumbHeight = getResources().getDimensionPixelSize(R.dimen.profile_icon_height);

		GlobalClass.printDebugLog("EditProfile", "ProfileImage Width-->"+mImageThumbWidth);

		GlobalClass.printDebugLog("EditProfile", "ProfileImage Height-->"+mImageThumbHeight);


		/*int mImageThumbWidth =profile_image.getWidth();

		int mImageThumbHeight =profile_image.getHeight();*/

		ImageCache.ImageCacheParams cacheParams =
				new ImageCache.ImageCacheParams(this, Constants.IMAGE_CACHE_DIR);

		cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

		// The ImageFetcher takes care of loading images into our ImageView children asynchronously
		mImageFetcher = new ImageFetcher(this,mImageThumbWidth,mImageThumbHeight);

		//mImageFetcher.setLoadingImage(R.drawable.empty_photo);

		mImageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);



	}

	@Override
	public void setproperties() 
	{
		getSupportActionBar().setHomeButtonEnabled(true);

		getSupportActionBar().setTitle("Edit Profile");

		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.header));

		mEmojiContainerLayout.setVisibility(View.GONE);

		mSmileyBtn.setBackgroundResource(R.drawable.smile);

		setEmojiconFragment(false);

		next.setText("Save Profile");

		GlobalClass.isEditScreenOpened=true;
		/*name.setText(GlobalClass.profileData.getProfileName());

		mood.setText(GlobalClass.profileData.getProfileStatus());

		availabilityStatus.setText("Available");*/

		/*availabilityStatus.setText(availabilityStatusList.get(0));

		profileimageBitmap=((BitmapDrawable)getResources().getDrawable(R.drawable.demo_profilepic)).getBitmap();
		//scaledpic=Bitmap.createScaledBitmap(scaledpic, pic_width, pic_height, false);

		profile_image.setImageBitmap(profileimageBitmap);*/

	}

	@Override
	public void setViewListeners() 
	{
		mood.setOnFocusChangeListener(new OnFocusChangeListener() 
		{

			@Override
			public void onFocusChange(View v, boolean hasFocus) 
			{
				if(hasFocus)
				{
					if(isEmojiOpened)
					{
						mEmojiContainerLayout.setVisibility(View.GONE);

						mSmileyBtn.setBackgroundResource(R.drawable.smile);

						isEmojiOpened=false;
					}


				}

			}
		});



		availabilityStatus.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) 
			{
				new AlertDialog.Builder(EditProfile.this)
				.setTitle("Set Availabilty Status")
				.setAdapter(availabilityStatusAdapter, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {


						availabilityStatus.setText(availabilityStatusList.get(which));
						dialog.dismiss();
					}
				}).create().show();

				availabilityStatus.clearFocus();
				return false;
			}
		});

		name.setOnFocusChangeListener(new OnFocusChangeListener() 
		{

			@Override
			public void onFocusChange(View v, boolean hasFocus) 
			{
				if(hasFocus)
				{
					if(isEmojiOpened)
					{
						mEmojiContainerLayout.setVisibility(View.GONE);

						mSmileyBtn.setBackgroundResource(R.drawable.smile);

						isEmojiOpened=false;
					}
				}

			}
		});
		mSmileyBtn.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				if(isEmojiOpened)
				{
					mEmojiContainerLayout.setVisibility(View.GONE);

					mSmileyBtn.setBackgroundResource(R.drawable.smile);

					isEmojiOpened=false;
				}
				else
				{
					hideSoftKeyboard(mood);

					mEmojiContainerLayout.setVisibility(View.VISIBLE);

					mSmileyBtn.setBackgroundResource(R.drawable.down_arrow);

					isEmojiOpened=true;
				}
				// TODO Auto-generated method stub

			}
		});

		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				hideSoftKeyboard(mood);

				if(isEmojiOpened)
				{
					mEmojiContainerLayout.setVisibility(View.GONE);

					mSmileyBtn.setBackgroundResource(R.drawable.smile);

					isEmojiOpened=false;
				}

				if(name!=null&&!(name.getText().toString().length()==0)&&mood!=null&&!(mood.getText().toString().length()==0)&&availabilityStatus!=null&&!(availabilityStatus.getText().toString().length()==0))
				{
					//profile_image.setDrawingCacheEnabled(true);

					//profile_image.buildDrawingCache(true);

					//profileimageBitmap = Bitmap.createBitmap(profile_image.getDrawingCache());


					//profile_image.setDrawingCacheEnabled(false);


					if(GlobalClass.isInternetAvailable(EditProfile.this))
					{
						String base64Image=null;
						if(profileimageBitmap!=null)
						{
							base64Image=createBase64OfImage(profileimageBitmap);
						}
						updateProfile(number,name.getText().toString(),mood.getText().toString(),availabilityStatus.getText().toString(),base64Image);	
					}
					else
					{
						new AlertDialog.Builder(EditProfile.this).
						setMessage("Internet Connection is not available. Please try again.").
						setPositiveButton("Retry", new android.content.DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) 
							{
								updateProfile(number,name.getText().toString(),mood.getText().toString(),availabilityStatus.getText().toString(),createBase64OfImage(profileimageBitmap));

							}
						}).
						setNegativeButton("Cancel", new android.content.DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {

								dialog.dismiss();
								finish();
							}
						}).create().show();
					}




					/*else
					{
						Toast.makeText(getApplicationContext(), "Please set profile pic", Toast.LENGTH_LONG).show();
					}*/

				}
				else
				{

					Toast.makeText(getApplicationContext(), "Please enter all the fields", Toast.LENGTH_LONG).show();

				}

			}
		});

		changePic.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				startActivityForResult(intent, Constants.DIALOG_PICK_FROM_GALLERY);
				//myDialog.dismiss();

			}
		});

		removePic.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v)
			{

				profileimageBitmap=((BitmapDrawable)getResources().getDrawable(R.drawable.demo_profilepic)).getBitmap();
				//scaledpic=Bitmap.createScaledBitmap(scaledpic, pic_width, pic_height, false);

				profile_image.setImageBitmap(profileimageBitmap);
			}
		});


	}

	//create helping method cropCapturedImage(Uri picUri)
	public void cropCapturedImage(Uri picUri,int height,int width){
		//call the standard crop action intent 
		Intent cropIntent = new Intent("com.android.camera.action.CROP");
		//indicate image type and Uri of image
		cropIntent.setDataAndType(picUri, "image/*");
		//set crop properties
		cropIntent.putExtra("crop", "true");
		//indicate aspect of desired crop
		cropIntent.putExtra("aspectX", 36);
		cropIntent.putExtra("aspectY", 37);
		//indicate output X and Y
		/*cropIntent.putExtra("outputX", 256);
			cropIntent.putExtra("outputY", 256);*/
		cropIntent.putExtra("outputX", width);
		cropIntent.putExtra("outputY", height);

		//cropIntent.putExtra("scale", false);
		//retrieve data on return
		cropIntent.putExtra("return-data", true);
		//start the activity - we handle returning in onActivityResult
		startActivityForResult(cropIntent, DIALOG_CROPIMAGE);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == Constants.DIALOG_PICK_FROM_GALLERY && resultCode == RESULT_OK) 
		{  
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getContentResolver().query(selectedImage,
					filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String picturePath = cursor.getString(columnIndex);
			profileImagePath=picturePath;
			cursor.close();

			int mImageThumbWidth = getResources().getDimensionPixelSize(R.dimen.profile_icon_width);

			int mImageThumbHeight = getResources().getDimensionPixelSize(R.dimen.profile_icon_height);

			GlobalClass.printDebugLog("EditProfile", "imageview width-->"+profile_image.getWidth());

			GlobalClass.printDebugLog("EditProfile", "imageview height-->"+profile_image.getHeight());

			GlobalClass.printDebugLog("EditProfile", "req width-->"+mImageThumbWidth);

			GlobalClass.printDebugLog("EditProfile", "req height-->"+mImageThumbHeight);

			cropCapturedImage(selectedImage, mImageThumbHeight, mImageThumbWidth);

			/*profileimageBitmap=decodeSampledBitmapFromResource(picturePath, getResources().getDimensionPixelSize(R.dimen.profile_icon_width), getResources().getDimensionPixelSize(R.dimen.profile_icon_height));

			if(profileimageBitmap!=null)
			{
				Log.i("profile_pic", "not null");
				profile_image.setImageDrawable(null);
				profile_image.refreshDrawableState();
				profile_image.setImageBitmap(profileimageBitmap);
			}
			else
			{
				Log.i("profile_pic", "not null");
			}*/
		}

		if(requestCode == DIALOG_CROPIMAGE && resultCode == RESULT_OK){
			//Create an instance of bundle and get the returned data
			Bundle extras = data.getExtras();
			//get the cropped bitmap from extras
			Bitmap thePic = extras.getParcelable("data");
			profileimageBitmap=thePic;
			//set image bitmap to image view
			profile_image.setImageDrawable(null);
			profile_image.refreshDrawableState();
			profile_image.setImageBitmap(thePic);
		}
	}

	public static int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {

		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;
		Log.v("height", ""+height);
		Log.v("width", ""+width);
		if (height > reqHeight || width > reqWidth) {


			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
			Log.v("inSampleSize2", ""+inSampleSize);
		}
		Log.v("inSampleSize1", ""+inSampleSize);
		return inSampleSize;
	}

	public static Bitmap decodeSampledBitmapFromResource(String pathName,
			int reqWidth, int reqHeight) {


		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(pathName, options);

		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		final int height = options.outHeight;
		final int width = options.outWidth;

		Log.v("modifiedheight", ""+height);
		Log.v("modifiedwidth", ""+width);

		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(pathName, options);
	}
	@Override
	public void onEmojiconBackspaceClicked(View v) 
	{
		EmojiconsFragment.backspace(mood);

	}

	@Override
	public void onEmojiconClicked(Emojicon emojicon) 
	{

		EmojiconsFragment.input(mood, emojicon);

	}

	private void setEmojiconFragment(boolean useSystemDefault) 
	{
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.emoji_container, EmojiconsFragment.newInstance(useSystemDefault))
		.commit();
	}

	public void hideSoftKeyboard(EditText editText)
	{
		InputMethodManager imm = (InputMethodManager)getSystemService(
				Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
	}

	@Override
	public void onBackPressed() 
	{
		// TODO Auto-generated method stub
		super.onBackPressed();

		if(isEmojiOpened)
		{
			mEmojiContainerLayout.setVisibility(View.GONE);

			mSmileyBtn.setBackgroundResource(R.drawable.smile);

			isEmojiOpened=false;
		}
	}

	@Override
	public void onAttachedToWindow() {
		// TODO Auto-generated method stub
		super.onAttachedToWindow();
		if(isfirst)
		{
			getProfile();
			isfirst=false;
		}

		/*createImageFetcherFromNetwork();
	mImageFetcher.loadImage(GlobalClass.profileData.getProfilePic(), profile_image);*/
	}

	public String createBase64OfImage(Bitmap profileimage)
	{
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		profileimage.compress(Bitmap.CompressFormat.PNG, 90, bao);

		byte [] imageinByteArray=bao.toByteArray();

		String imageinstring=Base64.encodeToString(imageinByteArray, Base64.DEFAULT);

		return imageinstring;
	}

	public void updateProfile(String number,String name,String mood,String availabilitystatus,String profileimage)
	{
		/*This background task fetches all the contacts which has number*/

		if(GlobalClass.isInternetAvailable(this))
		{
			new UpdateProfile(this, mHandler).execute(number,name,mood,availabilitystatus,profileimage,new GetValues(EditProfile.this).getRegistrationId());	
		}
		else
		{

			new AlertDialog.Builder(this)
			.setTitle("ConnectCrib")
			.setMessage("No Internet Connection Available.")

			.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) { 
					dialog.dismiss();
					finish();
				}
			})
			.setIcon(android.R.drawable.ic_dialog_alert)
			.show();
		}

	}



	private void getProfile()
	{
		/*This background task to get all the connectcrib contacts available on
		 *the device*/
		JSONArray jsonArrayContacts=new JSONArray();
		JSONObject jsonObjectContact=new JSONObject();

		JSONObject jobj=new JSONObject();
		try {
			jobj.put("number", GlobalClass.profileData.getProfileNumber());
			jsonArrayContacts.put(jobj);

			jsonObjectContact.put("contacts", jsonArrayContacts);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


		if(GlobalClass.isInternetAvailable(this))
		{
			new GetProfile(this, mHandler).execute(jsonObjectContact.toString());
		}
		else
		{

			//Toast.makeText(this, "", duration)
			new AlertDialog.Builder(this)
			.setTitle("ConnectCrib")
			.setMessage("No Internet Connection Available.")

			.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) { 

					dialog.dismiss();
					finish();
				}
			})
			.setIcon(android.R.drawable.ic_dialog_alert)
			.show();
		}


	}


	@Override
	public boolean handleMessage(Message msg) 
	{


		String GetProfileAsyncResult=msg.getData().getString("GetProfileAsyncResult");

		String UpdateProfileAsyncResult=msg.getData().getString("UpdateProfileAsyncResult");


		if(GetProfileAsyncResult!=null)
		{

			if(!GetProfileAsyncResult.equals("nodata"))
			{
				try 
				{
					JSONObject jobj=new JSONObject(GetProfileAsyncResult);

					Profile profile=new Profile();


					profile.setProfileName(jobj.getString("name"));

					profile.setProfileNumber(jobj.getString("number"));

					profile.setProfileStatus(jobj.getString("latest_mood"));

					profile.setProfilePic(jobj.getString("profile_image"));

					GlobalClass.profileData=profile;

					mImageFetcher.loadImage(GlobalClass.profileData.getProfilePic(), profile_image);

					name.setText(GlobalClass.profileData.getProfileName());

					mood.setText(GlobalClass.profileData.getProfileStatus());

					availabilityStatus.setText(jobj.getString("status"));

				} 
				catch (JSONException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else
			{
				Toast.makeText(this, "Could not get profile", Toast.LENGTH_LONG).show();

				finish();
			}

		}

		if(UpdateProfileAsyncResult!=null)
		{

			if(!UpdateProfileAsyncResult.equals("nodata"))
			{
				try {
					JSONObject jobj=new JSONObject(UpdateProfileAsyncResult);

					GlobalClass.printDebugLog("EditProfile", "ConnectCrib Success"+jobj.getString("success"));
					
					GlobalClass.printDebugLog("EditProfile","Edited profile pic"+ jobj.getString("data"));


					if((jobj.getString("success")).equals("1"))
					{
						Profile profile=new Profile();

						profile.setProfileName(EditProfile.this.name.getText().toString());

						profile.setProfileNumber(EditProfile.this.number);

						profile.setProfileStatus(EditProfile.this.mood.getText().toString());

						profile.setProfilePic(jobj.getString("data"));

						profile.setmAvailabilityStatus(EditProfile.this.availabilityStatus.getText().toString());

						GlobalClass.profileData=profile;

						DatabaseAccessor dbAccessor=new DatabaseAccessor(EditProfile.this);

						dbAccessor.updateProfile(profile.getProfileName(),profile.getProfileNumber(), profile.getProfileStatus(),profile.getProfilePic(),profile.getmAvailabilityStatus());

						finish();

						Toast.makeText(EditProfile.this.getApplicationContext(), "Profile updated successfully", Toast.LENGTH_LONG).show();

					}
					else
					{
						Toast.makeText(EditProfile.this.getApplicationContext(), "Profile could not be saved", Toast.LENGTH_LONG).show();
						finish();

					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					finish();
				}
			}
			else
			{
				Toast.makeText(this, "Could not update profile", Toast.LENGTH_LONG).show();

				finish();
			}
		}
		return true;
	}

}
