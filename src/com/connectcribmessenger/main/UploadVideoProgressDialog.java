package com.connectcribmessenger.main;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.backgroundtasks.UploadVideo;
import com.connectcribmessenger.customclasses.ProgressWheel;
import com.connectcribmessenger.entities.SelectedContactForMediaMessage;
import com.connectcribmessenger.interfaces.CommonMethods;

public class UploadVideoProgressDialog extends Activity implements CommonMethods
{
	private Button close;

	private ProgressWheel progressBar;

	private String filePath;
	
	private UploadVideo uploadvideo;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.dialog_uploadvideo);
		
		

		instantiateViews();
		
		setproperties();
		
		instantiateNonViewClasses();
		
		setViewListeners();

		uploadvideo.execute();
	}

	@Override
	public void instantiateNonViewClasses()
	{
		if(GlobalClass.chatAdapter!=null)
		{
			SelectedContactForMediaMessage selectedItems=new SelectedContactForMediaMessage();
			
			selectedItems.setConversationType(GlobalClass.conversationInfo.getConversationType());
			
			selectedItems.setGrroupInfo(GlobalClass.conversationInfo.getConversationGroupInfo());
			
			selectedItems.setMessageType(Constants.MESSAGE_TYPE_VIDEO);
			
			selectedItems.setName(GlobalClass.conversationInfo.getConversationName());
			
			selectedItems.setNumber(GlobalClass.conversationInfo.getConversationNumber());
			
			selectedItems.setProfilePic(GlobalClass.conversationInfo.getConversationImagePath());
			
			ArrayList<SelectedContactForMediaMessage> selectedList=new ArrayList<>();
			
			selectedList.add(selectedItems);
			
			uploadvideo=new UploadVideo(this, progressBar, filePath,selectedList);	
		}
		else
		{
			uploadvideo=new UploadVideo(this, progressBar, filePath,GlobalClass.selectedItems);	
		}
		

	}

	@Override
	public void instantiateViews() {

		close=(Button)findViewById(R.id.button_close);

		progressBar=(ProgressWheel)findViewById(R.id.progressBarTwo);
		
		

	}

	@Override
	public void setproperties() 
	{
		filePath=getIntent().getStringExtra("videoPath");
		

		DisplayMetrics displayMetrics = new DisplayMetrics();
		
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		
		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

		final int height = displayMetrics.heightPixels;

		final int width = displayMetrics.widthPixels;
		
		getWindow().setLayout((width*75)/100, (width*80)/100);

	}

	@Override
	public void setViewListeners() 
	{
		close.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				/*if(!uploadvideo.isCancelled())
				{
					uploadvideo.cancel(true);
				}*/
				uploadvideo.cancel(true);
				
				finish();

			}
		});

	}
}
