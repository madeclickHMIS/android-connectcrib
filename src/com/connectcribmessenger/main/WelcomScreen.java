package com.connectcribmessenger.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

public class WelcomScreen extends FragmentActivity 
{
	DemoCollectionPagerAdapter mDemoCollectionPagerAdapter;

	Button next;
	
	ViewPager mViewPager;
	
	private static AppBuilderClass mAppBuilder;
	
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.welcome_screen);
		
		mAppBuilder=new AppBuilderClass(this);
		
		mAppBuilder.storeAppFirstTimeOpenedStatus(false);
		
		mDemoCollectionPagerAdapter = new DemoCollectionPagerAdapter(getSupportFragmentManager());

		// Set up action bar.
		//final ActionBar actionBar = getActionBar();

		// Specify that the Home button should show an "Up" caret, indicating that touching the
		// button will take the user one step up in the application's hierarchy.
		//actionBar.setDisplayHomeAsUpEnabled(true);

		// Set up the ViewPager, attaching the adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mDemoCollectionPagerAdapter);
		next=(Button)findViewById(R.id.next_btn);
		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//Toast.makeText(AccountActivationScreen.this, "Height-->"+next.getHeight()+" and width-->"+next.getWidth(), Toast.LENGTH_LONG).show();
				Intent i=new Intent(WelcomScreen.this,RecentChatListScreen.class);
				startActivity(i);
				finish();

			}
		});
	}
	public static class DemoCollectionPagerAdapter extends FragmentStatePagerAdapter {

		public DemoCollectionPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int i) 
		{
			Log.i("item_index", ""+i);
			Fragment fragment = null;
			switch (i) {
			case 0:
				
				fragment = new SubscriptionSlideFragment();
				break;
			
			case 1 :
				fragment = new AppTourSlideFragment();
				break;



			}
			// Fragment fragment = new DemoObjectFragment();
			/*Fragment fragment = new ProfileFragment();
			Bundle args = new Bundle();
			 args.putInt(DemoObjectFragment.ARG_OBJECT, i + 1); // Our object is just an integer :-P
			fragment.setArguments(args);*/
			return fragment;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 2;
		}
		@Override
		public CharSequence getPageTitle(int position) 
		{
			CharSequence title = null;
			switch (position) {
			case 0:
				title="Plan Subscription";

				break;
			case 1:
				title="Short Tour Of The App";
				break;



			}
			return title;
		}
	}
}
