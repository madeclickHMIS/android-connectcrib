package com.connectcribmessenger.main;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.connectcribmessenger.adapters.ChatListAdapter;
import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.DatabaseAccessor;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.backgroundtasks.SqliteDataProviderThread;
import com.connectcribmessenger.entities.Item;
import com.connectcribmessenger.entities.Profile;
import com.connectcribmessenger.entities.RecentChat;
import com.connectcribmessenger.interfaces.CommonMethods;
import com.connectcribmessenger.quickactionbar.ActionItem;
import com.connectcribmessenger.quickactionbar.QuickAction;
import com.connectcribmessenger.services.UpdateOfflineStatus;
import com.connectcribmessenger.services.UpdateOnlineStatus;
import com.example.android.displayingbitmaps.util.ImageCache;
import com.example.android.displayingbitmaps.util.ImageFetcher;
import com.example.android.displayingbitmaps.util.Utils;

public class RecentChatListScreen extends ActionBarActivity implements Callback,CommonMethods 
{


	private Button send_video_btn;

	private Button change_status_btn;

	private Button send_audio_btn;

	private ListView list;

	private Handler handler;

	private AppBuilderClass mAppBuilder;

	private boolean isDataAvailable=false;

	private ImageFetcher mImageFetcher;

	private ChatListAdapter chatListAdapter;

	private View list_footer;

	private TextView list_footer_textview;

	private Button more;

	private QuickAction mQuickAction;

	private View view;

	private static int retry=0;

	private boolean isServiceStarted=false;

	private boolean isOfflineStared=false;

	private int RECORD_VIDEO=0;

	private Uri mUri;
	
	private MediaPlayer mPlayer;
	
	private String audiofilepath;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		/*getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		getSupportActionBar().hide();*/

		setContentView(R.layout.recent_chat_list_screen);

		//Toast.makeText(this, ""+getIntent().getStringExtra(Constants.INTENT_SHORTCUT_NUMBER), Toast.LENGTH_SHORT).show();

		instantiateViews();

		instantiateNonViewClasses();

		setproperties();

		setViewListeners();		

		if(savedInstanceState!=null)
		{
			isServiceStarted=savedInstanceState.getBoolean("isServiceStarted");
		}

		if(!isServiceStarted)
		{
			Intent intentService=new Intent(this,UpdateOnlineStatus.class);

			startService(intentService);
			
			isServiceStarted=true;
		}

		registerForContextMenu(list);

	}

	@Override
	protected void onSaveInstanceState(Bundle savedInstanceState) 
	{



		savedInstanceState.putBoolean("isServiceStarted", isServiceStarted);

		savedInstanceState.putParcelable("file_uri", mUri);

		super.onSaveInstanceState(savedInstanceState);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo)
	{
		super.onCreateContextMenu(menu, v, menuInfo);

		MenuInflater inflater = getMenuInflater();

		inflater.inflate(R.menu.context_menu_recent_chatlist, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) 
	{
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();

		int position = info.position;

		switch (item.getItemId()) 
		{
		case R.id.item1:

			//Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();

			Intent intent=new Intent(this,ViewContactScreen.class);

			startActivity(intent);

			return true;

		case R.id.item2:

			Toast.makeText(this, "Shortcut Created", Toast.LENGTH_SHORT).show();

			createShortcut(GlobalClass.recentMessageList.get(position).getSenderName(),GlobalClass.recentMessageList.get(position).getChatType(),GlobalClass.recentMessageList.get(position).getSenderNumber(),GlobalClass.recentMessageList.get(position).getGroupId());

			return true;

		case R.id.item3:

			//Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();

			removeListItem(info.targetView, position);

			return true;
		case R.id.item4:

			Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();

			deleteConversation(info.targetView,position);

			return true;

		case R.id.item5:

			Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
			showAvailableAccounts();
			return true;

		default:

			return super.onContextItemSelected(item);
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();

		inflater.inflate(R.menu.dashboard_actions, menu);
		/*mQuickAction.show(view);
		mQuickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_CENTER);*/
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		Intent intent;
		// Handle presses on the action bar items
		switch (item.getItemId()) 
		{

		case android.R.id.home:

			intent=new Intent(this,ProfileActivity.class);

			startActivity(intent);

			return true;
			//case R.id.action_notification:
			// openSearch();
			//	return true;

		case R.id.action_contacts:
			// openSettings();
			intent=new Intent(this,ContactListAllScreen.class);

			startActivity(intent);
			return true;
		case R.id.action_settings:
			// openSettings();
			intent=new Intent(this,SettingsScreen.class);

			startActivity(intent);
			return true;


		default:

			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onResume() 
	{
		// TODO Auto-generated method stub
		super.onResume();

		if(isDataAvailable)
		{
			mImageFetcher.setExitTasksEarly(false);

			chatListAdapter.notifyDataSetChanged();
			//mAppBuilder.activiyCallbacks(Constants.ACTIVITY_RECENT_CHAT_LIST_SCREEN, Constants.ONRESUME);
		}
		/**Get profile data from the device database and store in App
		 * The purpose of this method is to that the number of the user
		 * would be matched with the sender of the messages, if the sender is user itself
		 * then the messages would be considered outgoing and will be put right side in the 
		 * chatlist row
		 * This method returns result through handler in handleMessage() method in this activity
		 */


		mAppBuilder.getProfileDataFromDatabase(handler);
	}

	@Override
	protected void onPause() 
	{
		// TODO Auto-generated method stub
		super.onPause();

		if(isDataAvailable)
		{
			mImageFetcher.setPauseWork(false);

			mImageFetcher.setExitTasksEarly(true);

			mImageFetcher.flushCache();
			//mAppBuilder.activiyCallbacks(Constants.ACTIVITY_RECENT_CHAT_LIST_SCREEN, Constants.ONPAUSE);
		}

	}
	@Override
	protected void onStop() 
	{
		// TODO Auto-generated method stub
		super.onStop();

		GlobalClass.IS_RECENT_CHATS_LIST_CREEN_OPENED=false;

		GlobalClass.HANDLER_RECENT_CHAT_LIST_SCREEN=null;

		isDataAvailable=false;
	}
	@Override
	protected void onDestroy() 
	{
		// TODO Auto-generated method stub
		super.onDestroy();

		if(isDataAvailable)
		{
			mImageFetcher.closeCache();
			//mAppBuilder.activiyCallbacks(Constants.ACTIVITY_RECENT_CHAT_LIST_SCREEN, Constants.ONDESTROY);
		}

		//Toast.makeText(this, "On destroy of RecentScren", Toast.LENGTH_LONG).show();

		GlobalClass.IS_RECENT_CHATS_LIST_CREEN_OPENED=false;

		GlobalClass.HANDLER_RECENT_CHAT_LIST_SCREEN=null;

		/*Intent intentService=new Intent(this,UpdateOfflineStatus.class);

		startService(intentService);*/

		//makeOfflineStatus();

		if(isFinishing())
		{
			Intent intentService=new Intent(this,UpdateOfflineStatus.class);

			startService(intentService);	
		}

	}

	@Override
	public boolean handleMessage(Message msg) 
	{
		String status=msg.getData().getString("SqlitedataProviderThread");

		if(!isDataAvailable)
		{
			if(status.equals("recent_chats"))
			{
				isDataAvailable=true;
			}
			/**
			 * This method gets the recent chats from the database and displays it in
			 * the ListView on this Activity
			 * */
//			displayRecentChatsInList(status);
		}

		displayRecentChatsInList(status);

		return true;
	}



	//Used in RecentChatListScreen
	private void addShortcut(String name,String chatType,String number,String groupId) {

		//Adding shortcut for MainActivity 
		//on Home screen
		Intent shortcutIntent = new Intent(getApplicationContext(),
				RecentChatListScreen.class);

		shortcutIntent.putExtra(Constants.INTENT_SHORTCUT_NAME, name);

		shortcutIntent.putExtra(Constants.INTENT_SHORTCUT_NUMBER, number);

		shortcutIntent.putExtra(Constants.INTENT_SHORTCUT_CHAT_TYPE, chatType);

		shortcutIntent.putExtra(Constants.INTENT_SHORTCUT_GROUP_ID, groupId);

		shortcutIntent.setAction(Intent.ACTION_MAIN);

		Intent addIntent = new Intent();

		addIntent
		.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);

		addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, name);

		addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
				Intent.ShortcutIconResource.fromContext(getApplicationContext(),
						R.drawable.logo_small));

		addIntent
		.setAction("com.android.launcher.action.INSTALL_SHORTCUT");

		getApplicationContext().sendBroadcast(addIntent);
	}



	public void createShortcut(String name,String chatType,String number,String groupId)
	{
		addShortcut(name,chatType,number,groupId);
		/*if(!getSharedPreferences(Constants.SHAREDPREFERENCE_FILE, Activity.MODE_PRIVATE).getBoolean(Constants.SHAREDPREFERENCE_KEY_IS_ICON_CREATED+name, false)){

			addShortcut(name,chatType,number,groupId);

			getSharedPreferences(Constants.SHAREDPREFERENCE_FILE, Activity.MODE_PRIVATE).edit().putBoolean(Constants.SHAREDPREFERENCE_KEY_IS_ICON_CREATED, true).commit();

			getSharedPreferences(Constants.SHAREDPREFERENCE_FILE, Activity.MODE_PRIVATE).edit().putString(Constants.SHAREDPREFERENCE_KEY_CHAT_TYPE, chatType).commit();

			getSharedPreferences(Constants.SHAREDPREFERENCE_FILE, Activity.MODE_PRIVATE).edit().putString(Constants.SHAREDPREFERENCE_KEY_SENDER_NUMBER, number).commit();

			getSharedPreferences(Constants.SHAREDPREFERENCE_FILE, Activity.MODE_PRIVATE).edit().putString(Constants.SHAREDPREFERENCE_KEY_GROUP_ID, groupId).commit();
		}*/
	}



	public void displayRecentChatsInList(String msg)
	{
		if(msg.equals("profileData"))
		{
			HashMap<String,String> insertValues=new HashMap<>();

			insertValues.put("param", "recent_chats");

			SqliteDataProviderThread sqliteThread=new SqliteDataProviderThread(this,handler, insertValues);

			sqliteThread.execute();
		}
		else if(msg.equals("recent_chats")||msg.equals("get_latest_recent_chat_list"))
		{
			createImageFetcherFromNetwork();

			addListFooterInfo();

			chatListAdapter=new ChatListAdapter(this, GlobalClass.recentMessageList,mImageFetcher);

			list.setAdapter(chatListAdapter);
		}
	}

	public void addListFooterInfo()
	{

		if(GlobalClass.recentArchivedMessageList!=null)
		{
			if(GlobalClass.recentArchivedMessageList.size()>0)
			{

				list_footer_textview.setText(""+GlobalClass.recentArchivedMessageList.size()+" Archives");


				list_footer.setOnClickListener(new OnClickListener() 
				{

					@Override
					public void onClick(View v) 
					{
						Intent intent=new Intent(RecentChatListScreen.this,ArchivedRecentChatListScreen.class);

						startActivity(intent);

					}
				});
			}
			else
			{
				list_footer_textview.setText("Tap on chat to view more options.");

				list_footer.setOnClickListener(null);

			}

		}

	}


	public void createImageFetcherFromNetwork()
	{

		int mImageThumbWidth = getResources().getDimensionPixelSize(R.dimen.list_icon_width);

		int mImageThumbHeight = getResources().getDimensionPixelSize(R.dimen.list_icon_height);

		ImageCache.ImageCacheParams cacheParams =
				new ImageCache.ImageCacheParams(this, Constants.IMAGE_CACHE_DIR);

		cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

		// The ImageFetcher takes care of loading images into our ImageView children asynchronously
		mImageFetcher = new ImageFetcher(this,mImageThumbWidth,mImageThumbHeight);

		mImageFetcher.setLoadingImage(R.drawable.empty_photo);

		mImageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);



	}

	public void removeListItem(View rowView, final int position) {

		final Animation animation = AnimationUtils.loadAnimation(this,android.R.anim.slide_out_right); 

		animation.setDuration(800);

		rowView.startAnimation(animation);

		animation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) 
			{
				Handler handle = new Handler();

				handle.post(new Runnable() 
				{

					@Override
					public void run() 
					{
						DatabaseAccessor dbAccessor=new DatabaseAccessor(RecentChatListScreen.this);

						if(GlobalClass.recentMessageList.get(position).getChatType().equals(Constants.CHAT_TYPE_PEER_TO_PEER))
						{
							dbAccessor.updatePeerToPeerArchivedStatus(GlobalClass.recentMessageList.get(position).getSenderNumber(), Constants.MESSAGE_ARCHIVED_STATUS_YES);

						}
						else
						{
							dbAccessor.updateGroupArchivedStatus(GlobalClass.recentMessageList.get(position).getGroupInfo(), Constants.MESSAGE_ARCHIVED_STATUS_YES);
						}

						if(GlobalClass.recentArchivedMessageList!=null)
						{
							GlobalClass.recentArchivedMessageList.add(GlobalClass.recentMessageList.get(position));
						}
						else
						{
							ArrayList<RecentChat> archivedChat=new ArrayList<>();

							archivedChat.add(GlobalClass.recentMessageList.get(position));

							GlobalClass.recentArchivedMessageList=archivedChat;
						}

						addListFooterInfo();

						Toast.makeText(RecentChatListScreen.this, "Conversation has been archived", Toast.LENGTH_LONG).show();

						GlobalClass.recentMessageList.remove(position);


						chatListAdapter.notifyDataSetChanged();

					}
				});

			}
		});




	}

	public void deleteConversation(View rowView, final int position) {

		final Animation animation = AnimationUtils.loadAnimation(this,android.R.anim.slide_out_right); 

		animation.setDuration(800);

		rowView.startAnimation(animation);

		Handler handle = new Handler();

		handle.postDelayed(new Runnable() 
		{

			@Override
			public void run() {

				DatabaseAccessor dbAccessor=new DatabaseAccessor(RecentChatListScreen.this);

				if(GlobalClass.recentMessageList.get(position).getChatType().equals(Constants.CHAT_TYPE_PEER_TO_PEER))
				{
					//dbAccessor.updatePeerToPeerArchivedStatus(GlobalClass.recentMessageList.get(position).getSenderNumber(), Constants.MESSAGE_ARCHIVED_STATUS_YES);
					dbAccessor.deletePeer2peerMessages(GlobalClass.recentMessageList.get(position).getSenderNumber());
				}
				else
				{
					dbAccessor.deleteGroupMessages(GlobalClass.recentMessageList.get(position).getGroupInfo());
					//dbAccessor.updateGroupArchivedStatus(GlobalClass.recentMessageList.get(position).getGroupInfo(), Constants.MESSAGE_ARCHIVED_STATUS_YES);
				}


				Toast.makeText(RecentChatListScreen.this, "Conversation has been deleted", Toast.LENGTH_LONG).show();

				GlobalClass.recentMessageList.remove(position);


				chatListAdapter.notifyDataSetChanged();

				animation.cancel();
			}
		},1000);

	}

	private ArrayList<Item> getData() 
	{
		ArrayList<Item> accountsList = new ArrayList<Item>();

		/*//Getting all registered Google Accounts;
		try {
			Account[] accounts = AccountManager.get(this).getAccountsByType("com.google");
			for (Account account : accounts) {
				Item item = new Item( account.type, account.name);
				accountsList.add(item);
			}
		} catch (Exception e) {
			Log.i("Exception", "Exception:" + e);
		}
		 */
		//For all registered accounts;
		try 
		{
			Account[] accounts = AccountManager.get(this).getAccounts();

			for (Account account : accounts) 
			{
				Item item = new Item( account.type, account.name);

				accountsList.add(item);
			}
		} 
		catch (Exception e) 
		{
			Log.i("Exception", "Exception:" + e);
		}
		return accountsList;

	}

	private void showAvailableAccounts()
	{


		ArrayList<Item> itemList=getData();

		ArrayList<String> emailAddresses=new ArrayList<>();



		for (int i = 0; i < itemList.size(); i++) 
		{
			if(isEmailValid(itemList.get(i).getValue()))
			{
				emailAddresses.add(itemList.get(i).getValue());
				//items[i]=itemList.get(i).getValue();
			}

		}

		final String items[]=new String[emailAddresses.size()];

		for (int i = 0; i < emailAddresses.size(); i++) 
		{
			items[i]=emailAddresses.get(i);

		}

		new AlertDialog.Builder(this)

		.setSingleChoiceItems(items, 0, null)
		.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int whichButton) 
			{
				dialog.dismiss();

				int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
				// Do something useful withe the position of the selected radio button

				Toast.makeText(RecentChatListScreen.this, "Conversation will be email from app server to "+items[selectedPosition], Toast.LENGTH_SHORT).show();
			}
		})
		.show();
	}

	boolean isEmailValid(CharSequence email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	public void initiateQuickActionBar()
	{
		// Add action item
		ActionItem addAction = new ActionItem();

		addAction.setTitle("Contacts");
		addAction.setIcon(getResources().getDrawable(R.drawable.settings_contact));

		// Accept action item
		ActionItem accAction = new ActionItem();

		accAction.setTitle("New Group");
		accAction.setIcon(getResources().getDrawable(R.drawable.ic_launcher));

		// Upload action item
		ActionItem upAction = new ActionItem();

		upAction.setTitle("Settings");
		upAction.setIcon(getResources().getDrawable(R.drawable.ic_launcher));

		mQuickAction = new QuickAction(this);

		mQuickAction.addActionItem(addAction);
		mQuickAction.addActionItem(accAction);
		mQuickAction.addActionItem(upAction);

		// setup the action item click listener
		mQuickAction
		.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			public void onItemClick(int pos) {

				if (pos == 0) { // Add item selected
					Toast.makeText(RecentChatListScreen.this,
							"PHONE item selected", Toast.LENGTH_SHORT)
							.show();
				} else if (pos == 1) { // Accept item selected
					Toast.makeText(RecentChatListScreen.this,
							"GMAIL item selected", Toast.LENGTH_SHORT)
							.show();
				} else if (pos == 2) { // Upload item selected
					Toast.makeText(RecentChatListScreen.this, "TALK selected",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
	}




	@Override
	public void instantiateNonViewClasses() 
	{
		handler=new Handler(this);

		mAppBuilder=new AppBuilderClass(this);



		list_footer = getLayoutInflater().inflate(R.layout.archived_details_row, null);

		list_footer_textview=(TextView)list_footer.findViewById(R.id.textView1);

		GlobalClass.IS_RECENT_CHATS_LIST_CREEN_OPENED=true;

		GlobalClass.HANDLER_RECENT_CHAT_LIST_SCREEN=handler;	

		DatabaseAccessor dbaccessor=new DatabaseAccessor(this);

		dbaccessor.open();

		Cursor c=dbaccessor.getProfile();

		c.moveToFirst();

		GlobalClass.printDebugLog("RecentChalist", "name-->"+c.getString(1));

		GlobalClass.printDebugLog("RecentChalist","number-->"+c.getString(2));

		GlobalClass.printDebugLog("RecentChalist","status-->"+ c.getString(3));

		GlobalClass.printDebugLog("RecentChalist","pic-->"+ c.getString(4));
		Profile profile=new Profile();

		profile.setProfileId(c.getInt(0));

		profile.setProfileName(c.getString(1));

		profile.setProfileNumber(c.getString(2));

		profile.setProfilePic(c.getString(4));

		profile.setProfileStatus(c.getString(3));

		profile.setmAvailabilityStatus(c.getString(5));

		GlobalClass.profileData=profile;
		
		GlobalClass.profilePic=c.getString(4);
		
		GlobalClass.printDebugLog("RecentChalist","GlobalClass.profileData"+ GlobalClass.profileData.getProfilePic());



		c.close();

		dbaccessor.close();

	}

	@Override
	public void instantiateViews() 
	{
		list=(ListView)findViewById(R.id.listView1);

		send_audio_btn=(Button)findViewById(R.id.audio_btn);

		send_video_btn=(Button)findViewById(R.id.video_btn);

		change_status_btn=(Button)findViewById(R.id.status_btn);

	}

	@Override
	public void setproperties() 
	{
		getSupportActionBar().setHomeButtonEnabled(true);

		getSupportActionBar().setTitle("Chats");

		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.header));


		list.addFooterView(list_footer);

		GlobalClass.IS_RECENT_CHATS_LIST_CREEN_OPENED=true;
		
		GlobalClass.HANDLER_RECENT_CHAT_LIST_SCREEN=handler;
		
		NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
		
		
		mNotificationManager.cancel(1);

	}

	@Override
	public void setViewListeners() 
	{


		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) 
			{
				if(position<GlobalClass.recentMessageList.size())
				{
					GlobalClass.printDebugLog("Recent List Item clicked", "Recent List Item clicked");
					chatListAdapter.onItemClick(position, chatListAdapter.getImageView(position));
					//mAppBuilder.callOnItemClickRecentChatList(position);
				}
				else
				{
					Intent intent=new Intent(RecentChatListScreen.this,ArchivedRecentChatListScreen.class);

					startActivity(intent);
				}


			}
		});
		list.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) 
			{
				// Pause fetcher to ensure smoother scrolling when flinging
				if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING)
				{
					// Before Honeycomb pause image loading on scroll to help with performance
					if (!Utils.hasHoneycomb()) 
					{
						if(isDataAvailable)
						{
							mImageFetcher.setPauseWork(true);

						}  
					}
				} else 
				{
					mImageFetcher.setPauseWork(false);

				}

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub

			}
		});

		send_video_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) 
			{
				/*Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

				intent.setType("video/*");

				startActivityForResult(intent, Constants.ACTIVITYFORRESULT_SHOW_VIDEO_LIST);*/

				videoOptionDialog().show();

			}
		});

		change_status_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) 
			{
				Intent intent = new Intent(RecentChatListScreen.this,EditProfile.class);

				intent.putExtra("number", GlobalClass.profileData.getProfileNumber());

				startActivity(intent);

			}
		});

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if(requestCode == Constants.ACTIVITYFORRESULT_SHOW_VIDEO_LIST && resultCode == Activity.RESULT_OK)
		{
			try
			{
				String path = data.getData().toString();

				//This uncommented code gives the actual path that could be used to generate the video thumbnail
				/* Uri uri=data.getData();

                String[] projection = { MediaStore.Images.Media.DATA };
                Cursor cursor = getContentResolver().query(uri, projection, null, null,null);

                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();

                path= cursor.getString(column_index);*/

				Intent intent=new Intent(this,SendVideoScreen.class);

				intent.putExtra("videoPath", path);

				startActivity(intent);

				/* mVideo.setVideoPath(path);
                mVideo.requestFocus();
                mVideo.start();*/

			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		if (requestCode == RECORD_VIDEO) 
		{
			if (resultCode == RESULT_OK) 
			{

				// video successfully recorded
				// launching upload activity
	
				launchUploadActivity(false);

			} else if (resultCode == RESULT_CANCELED) {

				// user cancelled recording
				Toast.makeText(getApplicationContext(),
						"User cancelled video recording", Toast.LENGTH_SHORT)
						.show();

			} 
			else 
			{
				// failed to record video
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to record video", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}

	private void launchUploadActivity(boolean isImage){
		Intent i = new Intent(RecentChatListScreen.this, SendVideoScreen.class);
		i.putExtra("videoPath", mUri.getPath());
		i.putExtra("isImage", isImage);
		startActivity(i);
	}
	public Dialog videoOptionDialog()
	{
		DisplayMetrics displayMetrics = new DisplayMetrics();

		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

		final int height = displayMetrics.heightPixels;

		final int width = displayMetrics.widthPixels;

		final Dialog myDialog = new Dialog(this);
		myDialog.setCancelable(false);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		myDialog.setContentView(R.layout.video_option_dialog_layout);

		myDialog.getWindow().setGravity(Gravity.CENTER);

		//		myDialog.getWindow().setLayout((width*75)/100, LayoutParams.WRAP_CONTENT);

		myDialog.getWindow().setLayout((width*75)/100, (width*80)/100);

		myDialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);

		Button selectVideo=(Button)myDialog.findViewById(R.id.selectvideo);

		Button recordvideo=(Button)myDialog.findViewById(R.id.capturevideo);

		Button cancel=(Button)myDialog.findViewById(R.id.button_close);
		//TextView message_text=(TextView)myDialog.findViewById(R.id.textView1);
		//message_text.setText(message);

		selectVideo.setOnClickListener(new OnClickListener() 
		{									
			public void onClick(View v) 
			{
				Intent intent=new Intent(RecentChatListScreen.this,VideoListScreen.class);

				startActivity(intent);

				myDialog.dismiss();

			}
		});

		recordvideo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) 
			{
				// Checking camera availability
				if (!isDeviceSupportCamera()) {
					Toast.makeText(getApplicationContext(),
							"Sorry! Your device doesn't support camera",
							Toast.LENGTH_LONG).show();
					// will close the app if the device does't have camera
					// finish();
				}
				else
				{
					
					mUri = getOutputMediaFileUri();

					if(mUri!=null)
					{
						recordVideo(mUri);
					}
					else
					{
						Toast.makeText(RecentChatListScreen.this, "Sorry, can not record video from camera", Toast.LENGTH_LONG).show();
					}	
				}

				myDialog.dismiss();

			}
		});
		cancel.setOnClickListener(new OnClickListener() {									
			public void onClick(View v) {

				myDialog.dismiss();
			}
		});				

		return myDialog;
		//myDialog.show();
	}


	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		// get the file url
		mUri = savedInstanceState.getParcelable("file_uri");
	}
	/**
	 * Checking device has camera hardware or not
	 * */
	private boolean isDeviceSupportCamera() {
		if (getApplicationContext().getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA)) {
			// this device has a camera
			return true;
		} else {
			// no camera on this device
			return false;
		}
	}

	/**
	 * Launching camera app to record video
	 */
	private void recordVideo(Uri mUri)
	{
		Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);



		// set video quality
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, mUri); // set the image file
		// name

		// start the video capture Intent
		startActivityForResult(intent, RECORD_VIDEO);
	}
	/** Create a file Uri for saving an image or video */
	private  Uri getOutputMediaFileUri(){
		//		return Uri.fromFile(getOutputMediaFile());

		return Uri.fromFile(createImageFile());
	}

	private File createImageFile(){

		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "ConnectCribVideo_" + timeStamp + "_";

		File filedir = null;
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
		{
			//filedir = new File(Environment.getExternalStorageDirectory()+File.separator +"CrazyZooImages"+File.separator +String.valueOf(System.currentTimeMillis())+ "_image.jpg");

			filedir=new File(getExternalFilesDir(null).toString()+"/ConnectCrib/Media/RecordedVideo");

			if (filedir.exists ())
			{
				Log.i("filedir.exists ()",""+filedir.exists ());
				try 
				{
					filedir=new File(filedir,imageFileName+".mp4");

				} catch (Exception e)
				{
					e.printStackTrace();
				}

			}
			else
			{
				Log.i("filedir.exists ()",""+filedir.exists ());
				filedir.mkdirs();
			}


		}
		else
		{
			Toast.makeText(this, "SD card not available to write", Toast.LENGTH_LONG).show();
		}



		//image_path=filedir.getAbsolutePath();
		//image_path=getExternalFilesDir(null).toString()+"/CrazyZoo/"+imageFileName+".jpg";
		//noteimagepath=getExternalFilesDir(null).toString()+"/MakeNotesImages/"+imageFileName+".jpeg";
		//Log.i("absolute_path2",image_path);
		return  filedir;
	}
	@Override
	public void onBackPressed() 
	{
		super.onBackPressed();

		//makeOfflineStatus();
	}

	/*public void setSoundOptionDialog(Activity act)
	{
		DisplayMetrics displayMetrics = new DisplayMetrics();

		act.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

		final int height = displayMetrics.heightPixels;

		final int width = displayMetrics.widthPixels;
	
		final Dialog myDialog = new Dialog(act);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.dialog_audioset_options);
		myDialog.getWindow().setGravity(Gravity.CENTER);
		//myDialog.getWindow().setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		myDialog.getWindow().setLayout((width*75)/100, LayoutParams.WRAP_CONTENT);
		//myDialog.getWindow().getAttributes().windowAnimations = R.style.Animations_SmileWindow; 
		myDialog.setCancelable(true);
		myDialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);

		Button recordnew=(Button)myDialog.findViewById(R.id.recordnew);
		Button selectfromdevice=(Button)myDialog.findViewById(R.id.selectfromdevice);
		Button listen=(Button)myDialog.findViewById(R.id.listen);
		if(GlobalClass.isAudioFilePathSaved)
		{
			listen.setVisibility(View.VISIBLE);
		}
		else
		{
			listen.setVisibility(View.GONE);
		}
		myDialog.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface arg0) {
				if(mPlayer!=null)
				{
					if(mPlayer.isPlaying())
					{
						mPlayer.pause();
						mPlayer.stop();
						mPlayer.release();
						mPlayer=null;
					}
				}

				GlobalClass.isAudioFilePathSaved=true;

			}
		});
		recordnew.setOnClickListener(new OnClickListener() {									
			public void onClick(View v) 
			{

				try 
				{
					String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
					String imageFileName = "3GP_" + timeStamp + "_";

					String mFileName= getExternalFilesDir(null).toString()+"/MakeNotesAudio";
					mFileName += "/"+audiofilename.getText().toString()+".3gp";

					mFileName += "/"+imageFileName+".3gp";
					//					audiofilepath=mFileName;

					if(getAudioFilename().equals("null"))
					{
						finish();

						Toast.makeText(AddNewActivity.this, "SD card not available.", Toast.LENGTH_LONG).show();
					}
					else
					{
						audiofilepath=getAudioFilename();

						Log.i("new audiofile", "audio>"+audiofilepath);
						//showAudioRecordDialog(audiofilepath);
						Intent i=new Intent(AddNewActivity.this,RecordActivity.class);
						i.putExtra("audioname", audiofilepath);
						startActivity(i);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				myDialog.cancel(); 	
			}
		});
		//camera.setVisibility(View.GONE);
		selectfromdevice.setOnClickListener(new OnClickListener() {									
			public void onClick(View v) 
			{
				selectSoundOptionDialog();

				myDialog.cancel(); 	

			}
		});


}*/

}
