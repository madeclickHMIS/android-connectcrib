package com.connectcribmessenger.main;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.broadcastreceivers.InformationSenderReceiver;
import com.connectcribmessenger.entities.BackupSetting;
import com.connectcribmessenger.interfaces.CommonMethods;
import com.google.android.gms.internal.mp;

public class BackupSettingActivity extends ActionBarActivity implements CommonMethods
{
	ArrayAdapter<String> hoursAdapter;

	List<String> hoursList;

	ArrayAdapter<String> minAdapter;

	List<String> minList;

	ArrayAdapter<String> secAdapter;

	List<String> secList;

	/*private TextView mHoursTextView;

	private TextView mMinText;

	private TextView mSecText;*/

	private EditText mHoursEditText;

	private EditText mMinEditText;

	private EditText mSecEditText;

	private EditText mSenderEmailEditText;

	private EditText mSenderpasswordEditText;

	private EditText mRecipientEmailEditText;

	private Button save;

	private Button cancel;

	private String hours;

	private String mins;

	private String secs;

	private AlarmManager am;

	private PendingIntent informationSenderPendingIntent;
	
	BackupSetting backupSetting;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.backup_setting);

		setproperties();

		instantiateViews();

		instantiateNonViewClasses();

		setViewListeners();

	}

	@Override
	public void instantiateNonViewClasses() 
	{
		hoursList = Arrays.asList(getResources().getStringArray(R.array.hours));

		hoursAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, hoursList);

		minList = Arrays.asList(getResources().getStringArray(R.array.min));

		minAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, minList);	

		secList = Arrays.asList(getResources().getStringArray(R.array.sec));

		secAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, secList);	
	}

	@Override
	public void instantiateViews() 
	{
		/*mHoursTextView=(TextView)findViewById(R.id.hours);

		mMinText=(TextView)findViewById(R.id.min);

		mSecText=(TextView)findViewById(R.id.sec);*/

		mHoursEditText=(EditText)findViewById(R.id.hours);

		mMinEditText=(EditText)findViewById(R.id.min);

		mSecEditText=(EditText)findViewById(R.id.sec);

		mSenderEmailEditText=(EditText)findViewById(R.id.emailFromEditText);

		mSenderpasswordEditText=(EditText)findViewById(R.id.passwordEditText);

		mRecipientEmailEditText=(EditText)findViewById(R.id.emailToEditText);

		save=(Button)findViewById(R.id.save);

		cancel=(Button)findViewById(R.id.cancel);

	}

	@Override
	public void setproperties() 
	{

		backupSetting=GlobalClass.getBackupSettings(this);

		getSupportActionBar().setHomeButtonEnabled(true);

		getSupportActionBar().setTitle("Backup Settings");

		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.header));

		mSenderEmailEditText.setText(backupSetting.getSenderEmail());

		mSenderpasswordEditText.setText(backupSetting.getSenderPassword());

		mRecipientEmailEditText.setText(backupSetting.getRecipientEmail());

		mHoursEditText.setText("Hrs: "+backupSetting.getHours());

		mMinEditText.setText("Min: "+backupSetting.getMinutes());

		mSecEditText.setText("Sec: "+backupSetting.getSeconds());



	}

	@Override
	public void setViewListeners() 
	{
		mHoursEditText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) 
			{

				new AlertDialog.Builder(BackupSettingActivity.this)
				.setTitle("Set Hours")
				.setAdapter(hoursAdapter, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {


						mHoursEditText.setText("Hrs: "+hoursList.get(which));

						hours=hoursList.get(which);
						dialog.dismiss();
					}
				}).create().show();

				mHoursEditText.clearFocus();



			}
		});

		mMinEditText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) 
			{

				new AlertDialog.Builder(BackupSettingActivity.this)
				.setTitle("Set Minutes")
				.setAdapter(minAdapter, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {


						mMinEditText.setText("Min: "+minList.get(which));

						mins=minList.get(which);

						dialog.dismiss();
					}
				}).create().show();

				mMinEditText.clearFocus();



			}
		});


		mSecEditText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) 
			{

				new AlertDialog.Builder(BackupSettingActivity.this)
				.setTitle("Set Seconds")
				.setAdapter(secAdapter, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {


						mSecEditText.setText("Sec: "+secList.get(which));

						secs=secList.get(which);

						dialog.dismiss();
					}
				}).create().show();

				mSecEditText.clearFocus();



			}
		});

		save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) 
			{
				HashMap<String, String> values=new HashMap<>();

				if((mSenderEmailEditText!=null&&mSenderEmailEditText.getText().toString().length()!=0)
						&&(mSenderpasswordEditText!=null&&mSenderpasswordEditText.getText().toString().length()!=0)
						&&(mRecipientEmailEditText!=null&&mRecipientEmailEditText.getText().toString().length()!=0)
						&&(mHoursEditText!=null&&mHoursEditText.getText().toString().length()!=0)
						&&(mMinEditText!=null&&mMinEditText.getText().toString().length()!=0)
						&&(mSecEditText!=null&&mSecEditText.getText().toString().length()!=0))
				{
					values.put("senderemail", mSenderEmailEditText.getText().toString());

					values.put("senderemailpass", mSenderpasswordEditText.getText().toString());

					values.put("recipientemail", mRecipientEmailEditText.getText().toString());

					values.put("hrs", mHoursEditText.getText().toString());

					values.put("min", mMinEditText.getText().toString());

					values.put("sec", mSecEditText.getText().toString());
					
					values.put("duration", String.valueOf(GlobalClass.getBackupDurationInMillis( mHoursEditText.getText().toString(), mMinEditText.getText().toString(), mSecEditText.getText().toString())));

					GlobalClass.setBackupSettings(BackupSettingActivity.this, values);
					
					
					
					Intent informationSenderIntent = new Intent(BackupSettingActivity.this, InformationSenderReceiver.class);

					informationSenderPendingIntent=PendingIntent.getBroadcast(BackupSettingActivity.this, 2, informationSenderIntent,PendingIntent.FLAG_UPDATE_CURRENT);

					am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), GlobalClass.getBackupDurationInMillis( mHoursEditText.getText().toString(), mMinEditText.getText().toString(), mSecEditText.getText().toString()), informationSenderPendingIntent);

				}
				else
				{
					Toast.makeText(BackupSettingActivity.this, "Please enter all the fields", Toast.LENGTH_LONG).show();
				}


				

			}
		});


	}

}
