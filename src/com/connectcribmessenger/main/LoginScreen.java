package com.connectcribmessenger.main;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class LoginScreen extends ActionBarActivity 
{
	Button login;
	
	EditText username,password;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.account_login_scr);
		getSupportActionBar().setLogo(getResources().getDrawable(R.drawable.logo_small));
		//populatedata();

		
		login=(Button)findViewById(R.id.login);
		
		username=(EditText)findViewById(R.id.username);
		
		password=(EditText)findViewById(R.id.password);
		

		login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) 
			{
				if(username.getText().toString().length()>0&&password.getText().toString().length()>0)
				{
					Account account = new Account(username.getText().toString(), getString(R.string.ACCOUNT_TYPE));
					
					AccountManager am = AccountManager.get(LoginScreen.this);
					
					
					boolean accountCreated = am.addAccountExplicitly(account, password.getText().toString(), null);
					 
					Bundle extras = getIntent().getExtras();
					
					if (extras != null) 
					{
					 
						if (accountCreated) 
					 {  //Pass the new account back to the account manager
					  
							AccountAuthenticatorResponse response = extras.getParcelable(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE);
					  Bundle result = new Bundle();
					 
					  result.putString(AccountManager.KEY_ACCOUNT_NAME, username.getText().toString());
					  
					  result.putString(AccountManager.KEY_ACCOUNT_TYPE, getString(R.string.ACCOUNT_TYPE));
					  
					  response.onResult(result);
					
					 }
					 
					 
					 finish();
					
					}
					
				}
				

			}
		});
	}

	
}
