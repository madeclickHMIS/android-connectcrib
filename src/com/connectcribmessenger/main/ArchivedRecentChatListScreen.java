package com.connectcribmessenger.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.connectcribmessenger.adapters.RecentChatListAdapter;
import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.DatabaseAccessor;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.interfaces.CommonMethods;
import com.example.android.displayingbitmaps.util.ImageCache;
import com.example.android.displayingbitmaps.util.ImageFetcher;
import com.example.android.displayingbitmaps.util.Utils;

public class ArchivedRecentChatListScreen extends ActionBarActivity implements Callback,CommonMethods
{

	//Button archivedBtn;

	ListView list;

	Handler handler;

	private AppBuilderClass mAppBuilder;

	private boolean isDataAvailable=false;

	private ImageFetcher mImageFetcher;

	private RecentChatListAdapter archivedChatListAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.archived_recent_chat_list_screen);

		instantiateViews();

		instantiateNonViewClasses();

		setproperties();

		setViewListeners();

		registerForContextMenu(list);


	}
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.context_menu_archived_recent_chat_list_screen, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		switch (item.getItemId()) {
		case R.id.item1:
			//Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
			unArchive(info.targetView, info.position);
			return true;

		default:
			return super.onContextItemSelected(item);
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.dashboard_actions, menu);
		return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent intent=new Intent(this,ProfileActivity.class);
			startActivity(intent);
			return true;
			//case R.id.action_notification:
			// openSearch();
			//	return true;
		case R.id.action_settings:
			// openSettings();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		if(isDataAvailable)
		{
			mImageFetcher.setExitTasksEarly(false);

			archivedChatListAdapter.notifyDataSetChanged();
			//mAppBuilder.activiyCallbacks(Constants.ACTIVITY_ARCHIVED_RECENT_CHAT_LIST_SCREEN, Constants.ONRESUME);
		}
		/**Get profile data from the device database and store in App
		 * The purpose of this method is to that the number of the user
		 * would be matched with the sender of the messages, if the sender is user itself
		 * then the messages would be considered outgoing and will be put right side in the 
		 * chatlist row
		 * This method returns result through handler in handleMessage() method in this activity
		 */
		//mAppBuilder.displayArchivedRecentChatsInList("", handler, list);
		mAppBuilder.getProfileDataFromDatabase(handler);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

		if(isDataAvailable)
		{
			mImageFetcher.setPauseWork(false);
			mImageFetcher.setExitTasksEarly(true);
			mImageFetcher.flushCache();
			//mAppBuilder.activiyCallbacks(Constants.ACTIVITY_ARCHIVED_RECENT_CHAT_LIST_SCREEN, Constants.ONPAUSE);
		}

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();


		if(isDataAvailable)
		{
			mImageFetcher.closeCache();
			//mAppBuilder.activiyCallbacks(Constants.ACTIVITY_ARCHIVED_RECENT_CHAT_LIST_SCREEN, Constants.ONDESTROY);
		}

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		GlobalClass.IS_ARCHIVED_RECENT_CHATS_LIST_CREEN_OPENED=false;
	}
	@Override
	public boolean handleMessage(Message msg) {
		String status=msg.getData().getString("SqlitedataProviderThread");

		if(!isDataAvailable)
		{
			if(status.equals("recent_chats"))
			{
				isDataAvailable=true;
			}
			/**
			 * This method gets the recent chats from the database and displays it in
			 * the ListView on this Activity
			 * */

			//Toast.makeText(this, "Showing data", Toast.LENGTH_SHORT).show();
			//Toast.makeText(this, "data-->"+GlobalClass.recentArchivedMessageList.toString(), Toast.LENGTH_SHORT).show();
			//mAppBuilder.displayArchivedRecentChatsInList(status, handler, list);
			displayArchivedRecentChatsInList(status, handler, list);
		}



		return true;
	}

	public void displayArchivedRecentChatsInList(String msg,Handler handler,ListView list)
	{
		createImageFetcherFromNetwork();

		archivedChatListAdapter=new RecentChatListAdapter(this, GlobalClass.recentArchivedMessageList,mImageFetcher);

		list.setAdapter(archivedChatListAdapter);
	}

	public void createImageFetcherFromNetwork()
	{

		int mImageThumbWidth = getResources().getDimensionPixelSize(R.dimen.list_icon_width);

		int mImageThumbHeight =getResources().getDimensionPixelSize(R.dimen.list_icon_height);

		ImageCache.ImageCacheParams cacheParams =
				new ImageCache.ImageCacheParams(this, Constants.IMAGE_CACHE_DIR);

		cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

		// The ImageFetcher takes care of loading images into our ImageView children asynchronously
		mImageFetcher = new ImageFetcher(this,mImageThumbWidth,mImageThumbHeight);
		mImageFetcher.setLoadingImage(R.drawable.empty_photo);
		mImageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);



	}
	@Override
	public void instantiateNonViewClasses() 
	{
		handler=new Handler(this);

		mAppBuilder=new AppBuilderClass(this);

	}
	@Override
	public void instantiateViews() 
	{
		list=(ListView)findViewById(R.id.listView1);

	}
	@Override
	public void setproperties() 
	{
		getSupportActionBar().setHomeButtonEnabled(true);

		getSupportActionBar().setTitle("Chats");

		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.header));

		GlobalClass.IS_ARCHIVED_RECENT_CHATS_LIST_CREEN_OPENED=true;

	}
	@Override
	public void setViewListeners() {
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) 
			{
				archivedChatListAdapter.onItemClick(position, archivedChatListAdapter.getImageView(position));
				//mAppBuilder.callOnItemClickArchivedRecentChatList(position);

			}
		});
		list.setOnScrollListener(new OnScrollListener() 
		{

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) 
			{
				// Pause fetcher to ensure smoother scrolling when flinging
				if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING)
				{
					// Before Honeycomb pause image loading on scroll to help with performance
					if (!Utils.hasHoneycomb()) 
					{
						if(isDataAvailable)
						{
							mImageFetcher.setPauseWork(true);

						}  
					}
				} 
				else 
				{
					mImageFetcher.setPauseWork(false);

				}


			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub

			}
		});


	}
	
	public void unArchive(View rowView, final int position) {

		final Animation animation = AnimationUtils.loadAnimation(this,android.R.anim.slide_out_right); 
		animation.setDuration(800);
		rowView.startAnimation(animation);

				animation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				Handler handle = new Handler();
				
				handle.post(new Runnable() 
				{

					@Override
					public void run() {
						DatabaseAccessor dbAccessor=new DatabaseAccessor(ArchivedRecentChatListScreen.this);

						if(GlobalClass.recentArchivedMessageList.get(position).getChatType().equals(Constants.CHAT_TYPE_PEER_TO_PEER))
						{
							dbAccessor.updatePeerToPeerArchivedStatus(GlobalClass.recentArchivedMessageList.get(position).getSenderNumber(), Constants.MESSAGE_ARCHIVED_STATUS_NO);

						}
						else
						{
							dbAccessor.updateGroupArchivedStatus(GlobalClass.recentArchivedMessageList.get(position).getGroupInfo(), Constants.MESSAGE_ARCHIVED_STATUS_NO);
						}

						if(GlobalClass.recentArchivedMessageList!=null)
						{
							GlobalClass.recentArchivedMessageList.remove(position);
						}


						Toast.makeText(ArchivedRecentChatListScreen.this, "Conversation has been archived", Toast.LENGTH_LONG).show();


						archivedChatListAdapter.notifyDataSetChanged();
						
					}
				});
			}
		});
		
		



	}
}
