package com.connectcribmessenger.main;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.connectcribmessenger.adapters.ContactListAdapter;
import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.backgroundtasks.GetContactsFromPhone;
import com.connectcribmessenger.entities.Contact;
import com.connectcribmessenger.interfaces.CommonMethods;
import com.example.android.displayingbitmaps.util.ImageCache;
import com.example.android.displayingbitmaps.util.ImageFetcher;
import com.example.android.displayingbitmaps.util.Utils;

public class ContactListAllScreen extends ActionBarActivity implements CommonMethods, Callback
{
	private ListView mListView;

	private ImageFetcher mImageFetcher;

	private ContactListAdapter contactAdapter;

	private Context context;

	private Handler mHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		context=this;
		
		setContentView(R.layout.contact_list_layout);

		instantiateViews();

		instantiateNonViewClasses();
		
		setproperties();

		registerForContextMenu(mListView);

		createImageFetcherFromNetwork();

		setViewListeners();

		if(GlobalClass.isInternetAvailable(this))
		{
			setListAdapter();	
		}
		else
		{
			new AlertDialog.Builder(this).
			setMessage("Internet Connection is not available. Please try again.").
			setPositiveButton("Retry", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) 
				{
					setListAdapter();

				}
			}).
			setNegativeButton("Cancel", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

					dialog.dismiss();

				}
			}).create().show();
		}



	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo)
	{
		super.onCreateContextMenu(menu, v, menuInfo);

		MenuInflater inflater = getMenuInflater();

		inflater.inflate(R.menu.context_menu_recent_chatlist, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) 
	{
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();

		int position = info.position;

		switch (item.getItemId()) 
		{
		case R.id.item1:

			//Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();

			Intent intent=new Intent(this,ViewContactScreen.class);

			startActivity(intent);

			return true;



		default:

			return super.onContextItemSelected(item);
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();

		inflater.inflate(R.menu.contact_list_option_menu, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle presses on the action bar items
		switch (item.getItemId()) 
		{

		case R.id.action_tell_friend:

			Toast.makeText(this.getApplicationContext(), "This is to invite a friend to use ConnectCrib", Toast.LENGTH_SHORT).show();

			return true;
			//case R.id.action_notification:
			// openSearch();
			//	return true;

		case R.id.action_contact:

			Intent intent=new Intent(this,ProfileActivity.class);

			startActivity(intent);

			// openSettings();

			return true;

		default:

			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mImageFetcher.setExitTasksEarly(false);



	}

	@Override
	protected void onPause() 
	{
		// TODO Auto-generated method stub
		super.onPause();

		mImageFetcher.setPauseWork(false);

		mImageFetcher.setExitTasksEarly(true);

		mImageFetcher.flushCache();


	}
	@Override
	protected void onStop() 
	{
		// TODO Auto-generated method stub
		super.onStop();



	}
	@Override
	protected void onDestroy() 
	{
		// TODO Auto-generated method stub
		super.onDestroy();

		mImageFetcher.closeCache();

	}



	private void setListAdapter(ArrayList<Contact> contactList)
	{
		contactAdapter=new ContactListAdapter(this, contactList , mImageFetcher);

		mListView.setAdapter(contactAdapter);
	}

	
	public void createImageFetcherFromNetwork()
	{

		int mImageThumbWidth = getResources().getDimensionPixelSize(R.dimen.list_icon_width);

		int mImageThumbHeight = getResources().getDimensionPixelSize(R.dimen.list_icon_height);

		ImageCache.ImageCacheParams cacheParams =
				new ImageCache.ImageCacheParams(this, Constants.IMAGE_CACHE_DIR);

		cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

		// The ImageFetcher takes care of loading images into our ImageView children asynchronously
		mImageFetcher = new ImageFetcher(this,mImageThumbWidth,mImageThumbHeight);

		mImageFetcher.setLoadingImage(R.drawable.empty_photo);

		mImageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);



	}



	@Override
	public void instantiateNonViewClasses() 
	{
		mHandler=new Handler(this);

	}

	@Override
	public void instantiateViews() 
	{
		mListView=(ListView)findViewById(R.id.listView1);

	}

	@Override
	public void setproperties() 
	{
		getSupportActionBar().setHomeButtonEnabled(true);

		getSupportActionBar().setTitle("Contacts");

		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.header));

	}

	@Override
	public void setViewListeners() 
	{
		// TODO Auto-generated method stub

		mListView.setOnItemClickListener(new OnItemClickListener() 
		{

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) 
			{

				contactAdapter.onItemClick(position, contactAdapter.getImageView(position));
			}
		});

		mListView.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) 
			{
				// Pause fetcher to ensure smoother scrolling when flinging
				if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING)
				{
					// Before Honeycomb pause image loading on scroll to help with performance
					if (!Utils.hasHoneycomb()) 
					{


						mImageFetcher.setPauseWork(true);
					}
				} 
				else 
				{
					mImageFetcher.setPauseWork(false);

				}

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub

			}
		});

	}

	/*This method does following tasks:
	 *1. Get all the contacts from the device.
	 *2. Send the device contacts to server for matching device contacts with the already registered
	 *   contacts.
	 *3. Finally returns the list of all the connectcrib contacts returned from server.*/
	public void setListAdapter()
	{
		/*This background task fetches all the contacts which has number*/
		if(GlobalClass.isInternetAvailable(this))
		{
			new GetContactsFromPhone(this, mHandler).execute(null,null,null);	
		}
		else
		{
			//If no internet connection is available then dialog is displayed
			//showDialog(Constants.DIALOG_GCM_REGISTRATION_NO_INTERNET,"Internet");

			new AlertDialog.Builder(this)
			.setTitle("ConnectCrib")
			.setMessage("No Internet Connection Available.")

			.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) { 
					dialog.dismiss();
				}
			})
			.setIcon(android.R.drawable.ic_dialog_alert)
			.show();
		}

		
	}

	@Override
	public boolean handleMessage(Message msg) {
		ArrayList<Contact> GetConnectCribContactsAsyncResult=msg.getData().getParcelableArrayList("GetConnectCribContactsAsyncResult");
		
		if(GetConnectCribContactsAsyncResult!=null)
		{
			if(GetConnectCribContactsAsyncResult.size()>0)
			{
				setListAdapter(GetConnectCribContactsAsyncResult);
			}
			else
			{
				Toast.makeText(this, "0 ConnectCrib Contacts found", Toast.LENGTH_LONG).show();
				finish();

			}

		}

		return true;
	}

}

