package com.connectcribmessenger.main;

import java.math.BigDecimal;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.widget.ListView;

import com.connectcribmessenger.adapters.VideoListScreenAdapter;
import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.entities.Video;
import com.connectcribmessenger.interfaces.CommonMethods;

public class VideoListScreen extends ActionBarActivity implements CommonMethods
{
	private ListView mListView;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.videolist);

		instantiateViews();

		instantiateNonViewClasses();

		setproperties();

		setViewListeners();
	}

	@Override
	public void instantiateNonViewClasses() 
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void instantiateViews() 
	{
		mListView=(ListView)findViewById(R.id.videolist);

	}

	@Override
	public void setproperties() 
	{
		getSupportActionBar().setHomeButtonEnabled(true);

		getSupportActionBar().setTitle("Select Video");

		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.header));

		//mListView.setAdapter(new VideoListScreenAdapter(this, getVideos()));
		setAdapter();
	}

	@Override
	public void setViewListeners() 
	{
		// TODO Auto-generated method stub

	}
public void setAdapter()
{
	new AsyncTask<Void, Void, ArrayList<Video>>()
	{
		ProgressDialog pd;
		
		protected void onPreExecute() 
		{
			pd=new ProgressDialog(VideoListScreen.this);
			
			pd.setMessage("Getting Video List...");
			
			pd.setCancelable(false);
			
			pd.show();
		};

		@Override
		protected ArrayList<Video> doInBackground(Void... params) {
			// TODO Auto-generated method stub
			return getVideos();
		}
		
		protected void onPostExecute(ArrayList<Video> result) 
		{
			pd.cancel();
			
			mListView.setAdapter(new VideoListScreenAdapter(VideoListScreen.this, result));
		};
		
	}.execute();
}
	public ArrayList<Video> getVideos()
	{
		
		ArrayList<Video> videoList=new ArrayList<>();

		String[] proj = { MediaStore.Video.Media._ID,
				MediaStore.Video.Media.DATA,
				MediaStore.Video.Media.DISPLAY_NAME,
				MediaStore.Video.Media.SIZE,
				MediaStore.Video.Media.DURATION};
		/*Cursor videocursor = managedQuery(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
	                proj, null, null, null);*/
		Cursor videocursor = getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
				proj, null, null, MediaStore.Video.Media.DATE_TAKEN + " DESC");

		int count = videocursor.getCount();

		GlobalClass.printDebugLog("VideoListScreen", "Total Number of videos-->"+count);

		while(videocursor.moveToNext())
		{
			long durationInMillis=Long.parseLong(videocursor.getString(4));

			float durationInSeconds=durationInMillis/1000;

			BigDecimal durationInMinutes=round((durationInSeconds/60),2);

			long sizeInBytes=Long.parseLong(videocursor.getString(3));

			float sizeInKiloBytes=sizeInBytes/1024;

			//float durationInMegaByte=durationInKiloBytes/1024;
			BigDecimal sizeInMegaByte=round(sizeInKiloBytes/1024, 2);

			if(sizeInMegaByte.intValue()<Constants.VIDEO_MAX_SiZE_IN_MB&&durationInMinutes.intValue()<Constants.VIDEO_MAX_DURATION_IN_MIN)
			{
				Video video=new Video();

				video.setTitle(videocursor.getString(2));

				video.setPath(videocursor.getString(1));

				video.setThumbnail(GlobalClass.getVideoThumbnail(videocursor.getString(1)));


				//int remainingSizeInSeconds=(int)(sizeInSeconds%60);

				video.setSize(sizeInMegaByte.floatValue()+" mb");



				video.setDuration(durationInMinutes.floatValue()+" min");

				videoList.add(video);	
			}


		}
		
		return videoList;
	}

	public  BigDecimal round(float d, int decimalPlace) {
		BigDecimal bd = new BigDecimal(Float.toString(d));
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);       
		return bd;
	}
}
