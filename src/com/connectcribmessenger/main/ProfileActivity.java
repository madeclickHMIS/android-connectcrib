package com.connectcribmessenger.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

public class ProfileActivity extends ActionBarActivity
{
	Button back,account_btn,cribmemory_btn,location_btn,notification_btn,updates_btn,import_contacts,about_btn;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.profile_screen);
		back=(Button)findViewById(R.id.back);
		account_btn=(Button)findViewById(R.id.account_btn);
		cribmemory_btn=(Button)findViewById(R.id.crib_memory_btn);
		location_btn=(Button)findViewById(R.id.location_btn);
		notification_btn=(Button)findViewById(R.id.notification_btn);
		updates_btn=(Button)findViewById(R.id.updates_btn);
		import_contacts=(Button)findViewById(R.id.import_contacts_btn);
		about_btn=(Button)findViewById(R.id.about_btn);
		//getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		//getSupportActionBar().setTitle("Profile");

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				finish();

			}
		});
		about_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent=new Intent(ProfileActivity.this,DetailScreen.class);

				startActivity(intent);

			}
		});
		import_contacts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent=new Intent(ProfileActivity.this,DetailScreen.class);

				startActivity(intent);

			}
		});
		updates_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent=new Intent(ProfileActivity.this,DetailScreen.class);

				startActivity(intent);

			}
		});
		notification_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent=new Intent(ProfileActivity.this,DetailScreen.class);

				startActivity(intent);

			}
		});
		location_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent=new Intent(ProfileActivity.this,DetailScreen.class);

				startActivity(intent);

			}
		});
		cribmemory_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent=new Intent(ProfileActivity.this,DetailScreen.class);

				startActivity(intent);

			}
		});
		account_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent=new Intent(ProfileActivity.this,DetailScreen.class);

				startActivity(intent);

			}
		});
	}
}
