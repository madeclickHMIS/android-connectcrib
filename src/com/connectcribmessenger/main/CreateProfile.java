package com.connectcribmessenger.main;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.DatabaseAccessor;
import com.connectcribmessenger.auxilliaryclasses.GetValues;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.entities.Profile;
import com.connectcribmessenger.interfaces.CommonMethods;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;

public class CreateProfile extends ActionBarActivity implements EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener,CommonMethods 
{
	private ImageView profile_image;

	private Button changePic;

	private Button removePic;

	private Button next;

	private Button mSmileyBtn;

	private EditText name;

	private EditText mood;

	private TextView availabilityStatus;

	private FrameLayout mEmojiContainerLayout;

	private boolean isEmojiOpened=false;

	private String number;

	private List<String> availabilityStatusList;

	private ArrayAdapter<String> availabilityStatusAdapter;

	private String profileImagePath;

	private Bitmap profileimageBitmap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.create_profile);

		instantiateViews();

		instantiateNonViewClasses();

		setproperties();

		setViewListeners();
		
		if(savedInstanceState!=null)
		{
			//GlobalClass.profileData=savedInstanceState.getParcelable("profiledata");

			//isfirst=savedInstanceState.getBoolean("isfirst");
			
			name.setText(savedInstanceState.getString("name"));
			
			mood.setText(savedInstanceState.getString("mood"));
			
			availabilityStatus.setText(savedInstanceState.getString("availability_status"));
		}
	}
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		// Save the user's current game state

		//savedInstanceState.putParcelable("profiledata", GlobalClass.profileData);

		//savedInstanceState.putBoolean("isfirst", isfirst);
		
		savedInstanceState.putString("name", name.getText().toString());
		
		savedInstanceState.putString("mood", mood.getText().toString());
		
		savedInstanceState.putString("availability_status", availabilityStatus.getText().toString());

		// Always call the superclass so it can save the view hierarchy state
		super.onSaveInstanceState(savedInstanceState);
	}
	@Override
	public void instantiateNonViewClasses() 
	{
		number=getIntent().getStringExtra("number");

		availabilityStatusList = Arrays.asList(getResources().getStringArray(R.array.availabilty_status_options));

		availabilityStatusAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, availabilityStatusList);

	}

	@Override
	public void instantiateViews() 
	{
		profile_image=(ImageView)findViewById(R.id.profilepic_imageview);

		changePic=(Button)findViewById(R.id.changepic_btn);

		removePic=(Button)findViewById(R.id.removepic_btn);

		next=(Button)findViewById(R.id.next_btn);

		mSmileyBtn=(Button)findViewById(R.id.smiley_btn);

		name=(EditText)findViewById(R.id.name);

		mood=(EditText)findViewById(R.id.emoji_edittext);

		availabilityStatus=(TextView)findViewById(R.id.availability_status);

		mEmojiContainerLayout=(FrameLayout)findViewById(R.id.emoji_container);


	}

	@Override
	public void setproperties() 
	{
		getSupportActionBar().setHomeButtonEnabled(false);

		getSupportActionBar().setTitle("Create Profile");

		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.header));

		mEmojiContainerLayout.setVisibility(View.GONE);

		mSmileyBtn.setBackgroundResource(R.drawable.smile);

		setEmojiconFragment(false);

		availabilityStatus.setText(availabilityStatusList.get(0));

		profileimageBitmap=((BitmapDrawable)getResources().getDrawable(R.drawable.demo_profilepic)).getBitmap();
		//scaledpic=Bitmap.createScaledBitmap(scaledpic, pic_width, pic_height, false);

		profile_image.setImageBitmap(profileimageBitmap);

	}

	@Override
	public void setViewListeners() 
	{
		mood.setOnFocusChangeListener(new OnFocusChangeListener() 
		{

			@Override
			public void onFocusChange(View v, boolean hasFocus) 
			{
				if(hasFocus)
				{
					if(isEmojiOpened)
					{
						mEmojiContainerLayout.setVisibility(View.GONE);

						mSmileyBtn.setBackgroundResource(R.drawable.smile);

						isEmojiOpened=false;
					}


				}

			}
		});



		availabilityStatus.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) 
			{
				new AlertDialog.Builder(CreateProfile.this)
				.setTitle("Set Availabilty Status")
				.setAdapter(availabilityStatusAdapter, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {


						availabilityStatus.setText(availabilityStatusList.get(which));
						dialog.dismiss();
					}
				}).create().show();

				availabilityStatus.clearFocus();
				return false;
			}
		});

		name.setOnFocusChangeListener(new OnFocusChangeListener() 
		{

			@Override
			public void onFocusChange(View v, boolean hasFocus) 
			{
				if(hasFocus)
				{
					if(isEmojiOpened)
					{
						mEmojiContainerLayout.setVisibility(View.GONE);

						mSmileyBtn.setBackgroundResource(R.drawable.smile);

						isEmojiOpened=false;
					}
				}

			}
		});
		mSmileyBtn.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				if(isEmojiOpened)
				{
					mEmojiContainerLayout.setVisibility(View.GONE);

					mSmileyBtn.setBackgroundResource(R.drawable.smile);

					isEmojiOpened=false;
				}
				else
				{
					hideSoftKeyboard(mood);

					mEmojiContainerLayout.setVisibility(View.VISIBLE);

					mSmileyBtn.setBackgroundResource(R.drawable.down_arrow);

					isEmojiOpened=true;
				}
				// TODO Auto-generated method stub

			}
		});

		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				hideSoftKeyboard(mood);

				if(isEmojiOpened)
				{
					mEmojiContainerLayout.setVisibility(View.GONE);

					mSmileyBtn.setBackgroundResource(R.drawable.smile);

					isEmojiOpened=false;
				}

				if(name!=null&&!(name.getText().toString().length()==0)&&mood!=null&&!(mood.getText().toString().length()==0)&&availabilityStatus!=null&&!(availabilityStatus.getText().toString().length()==0))
				{
					if(profileimageBitmap!=null)
					{
						if(GlobalClass.isInternetAvailable(CreateProfile.this))
						{
							updateProfile(number,name.getText().toString(),mood.getText().toString(),availabilityStatus.getText().toString(),createBase64OfImage(profileimageBitmap));	
						}
						else
						{
							new AlertDialog.Builder(CreateProfile.this).
							setMessage("Internet Connection is not available. Please try again.").
							setPositiveButton("Retry", new android.content.DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) 
								{
									updateProfile(number,name.getText().toString(),mood.getText().toString(),availabilityStatus.getText().toString(),createBase64OfImage(profileimageBitmap));

								}
							}).
							setNegativeButton("Cancel", new android.content.DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {

									dialog.dismiss();

								}
							}).create().show();
						}



					}
					else
					{
						Toast.makeText(getApplicationContext(), "Please set profile pic", Toast.LENGTH_LONG).show();
					}

				}
				else
				{

					Toast.makeText(getApplicationContext(), "Please enter all the fields", Toast.LENGTH_LONG).show();

				}

			}
		});

		changePic.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				startActivityForResult(intent, Constants.DIALOG_PICK_FROM_GALLERY);
				//myDialog.dismiss();

			}
		});

		removePic.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v)
			{

				profileimageBitmap=((BitmapDrawable)getResources().getDrawable(R.drawable.demo_profilepic)).getBitmap();
				//scaledpic=Bitmap.createScaledBitmap(scaledpic, pic_width, pic_height, false);

				profile_image.setImageBitmap(profileimageBitmap);
			}
		});


	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == Constants.DIALOG_PICK_FROM_GALLERY && resultCode == RESULT_OK) 
		{  
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getContentResolver().query(selectedImage,
					filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String picturePath = cursor.getString(columnIndex);
			profileImagePath=picturePath;
			cursor.close();


			profileimageBitmap=decodeSampledBitmapFromResource(picturePath, getResources().getDimensionPixelSize(R.dimen.profile_icon_width), getResources().getDimensionPixelSize(R.dimen.profile_icon_height));
			/*scaledpic=Bitmap.createScaledBitmap(scaledpic, pic_width, pic_height, false);*/
			if(profileimageBitmap!=null)
			{
				Log.i("profile_pic", "not null");
				profile_image.setImageDrawable(null);
				profile_image.refreshDrawableState();
				profile_image.setImageBitmap(profileimageBitmap);
			}
			else
			{
				Log.i("profile_pic", "not null");
			}
		}
	}

	public static int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {

		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;
		Log.v("height", ""+height);
		Log.v("width", ""+width);
		if (height > reqHeight || width > reqWidth) {


			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
			Log.v("inSampleSize2", ""+inSampleSize);
		}
		Log.v("inSampleSize1", ""+inSampleSize);
		return inSampleSize;
	}

	public static Bitmap decodeSampledBitmapFromResource(String pathName,
			int reqWidth, int reqHeight) {


		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(pathName, options);

		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(pathName, options);
	}
	@Override
	public void onEmojiconBackspaceClicked(View v) 
	{
		EmojiconsFragment.backspace(mood);

	}

	@Override
	public void onEmojiconClicked(Emojicon emojicon) 
	{

		EmojiconsFragment.input(mood, emojicon);

	}

	private void setEmojiconFragment(boolean useSystemDefault) 
	{
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.emoji_container, EmojiconsFragment.newInstance(useSystemDefault))
		.commit();
	}

	public void hideSoftKeyboard(EditText editText)
	{
		InputMethodManager imm = (InputMethodManager)getSystemService(
				Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
	}

	@Override
	public void onBackPressed() 
	{
		// TODO Auto-generated method stub
		super.onBackPressed();

		if(isEmojiOpened)
		{
			mEmojiContainerLayout.setVisibility(View.GONE);

			mSmileyBtn.setBackgroundResource(R.drawable.smile);

			isEmojiOpened=false;
		}
	}



	public String createBase64OfImage(Bitmap profileimage)
	{
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		profileimage.compress(Bitmap.CompressFormat.PNG, 90, bao);

		byte [] imageinByteArray=bao.toByteArray();

		String imageinstring=Base64.encodeToString(imageinByteArray, Base64.DEFAULT);

		return imageinstring;
	}

	public void updateProfile(String number,String name,String mood,String availabilitystatus,String profileimage)
	{
		/*This background task fetches all the contacts which has number*/
		new AsyncTask<String, Void, String>()
		{
			ProgressDialog pd;
			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
				pd=new ProgressDialog(CreateProfile.this);

				pd.setMessage("Saving Profile..");

				pd.show();
			}
			@Override
			protected String doInBackground(String... params)
			{


				HttpParams myParams = new BasicHttpParams();

				HttpConnectionParams.setConnectionTimeout(myParams, 10000);

				HttpConnectionParams.setSoTimeout(myParams, 10000);

				HttpClient httpclient = new DefaultHttpClient(myParams );


				try {

					HttpPost httpPost = new HttpPost(Constants.APP_SERVER_ROOT_PATH);

					List<NameValuePair> nameValuePair=new ArrayList<NameValuePair>();	

					/*GlobalClass.printDebugLog("SendSmsForOtp", "Api-->"+Constants.APP_SERVER_ROOT_PATH);

					GlobalClass.printDebugLog("SendSmsForOtp", "key-->"+Constants.API_SEND_SMS_PARAMETER_1_VALUE);

					GlobalClass.printDebugLog("SendSmsForOtp", "userId-->"+GlobalClass.REGISTRATION_ID);

					GlobalClass.printDebugLog("SendSmsForOtp", "number-->"+number);*/

					nameValuePair.add(new BasicNameValuePair(Constants.API_UPDATE_PROFILE_PARAMETER_1,Constants.API_UPDATE_PROFILE_PARAMETER_1_VALUE));

					nameValuePair.add(new BasicNameValuePair(Constants.API_UPDATE_PROFILE_PARAMETER_2,params[5]));

					nameValuePair.add(new BasicNameValuePair(Constants.API_UPDATE_PROFILE_PARAMETER_4,params[0]));

					nameValuePair.add(new BasicNameValuePair(Constants.API_UPDATE_PROFILE_PARAMETER_5,params[1]));

					nameValuePair.add(new BasicNameValuePair(Constants.API_UPDATE_PROFILE_PARAMETER_6,params[2]));

					nameValuePair.add(new BasicNameValuePair(Constants.API_UPDATE_PROFILE_PARAMETER_7,params[3]));

					nameValuePair.add(new BasicNameValuePair(Constants.API_UPDATE_PROFILE_PARAMETER_8,params[4]));



					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));  



					//httppost.setHeader("Content-type", "application/json");

					//StringEntity se = new StringEntity(params[0]); 

					//se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

					//httppost.setEntity(se); 

					HttpResponse response = httpclient.execute(httpPost);

					String jsonData = EntityUtils.toString(response.getEntity());

					GlobalClass.printDebugLog("ConnectCrib Serever Data", "Contacts on connectcrib-->\n"+jsonData);


					JSONObject jobj=new JSONObject(jsonData);



					return jobj.toString();


				} 

				catch (ClientProtocolException e)
				{
					GlobalClass.printDebugLog("ConnectCrib Serever Data", "ClientProtocolException while getting connectcrib contacts");

					e.printStackTrace();
				} 
				catch (IOException e) 
				{
					GlobalClass.printDebugLog("ConnectCrib Serever Data", "IOException while getting connectcrib contacts");
					e.printStackTrace();

				} 
				catch (JSONException e) 
				{
					GlobalClass.printDebugLog("ConnectCrib Serever Data", "JSONException while getting connectcrib contacts");

					e.printStackTrace();
				}

				catch (Exception e) {
					e.printStackTrace();
				}
				return null;  


				/*int code=uploadFile(profileImagePath,params[1],params[2],
						params[3], params[4]) ;

				return String.valueOf(code);*/
			}

			@Override
			protected void onPostExecute(String result)
			{
				pd.dismiss();

				try {
					JSONObject jobj=new JSONObject(result);
					
					GlobalClass.printDebugLog("CreateProfile", "Connectcrib success-->"+jobj.getString("success"));
					
					GlobalClass.printDebugLog("CreateProfile","Profile pic-->"+ jobj.getString("success"));
					
					if((jobj.getString("success")).equals("1"))
					{
						Profile profile=new Profile();

						profile.setProfileName(CreateProfile.this.name.getText().toString());

						profile.setProfileNumber(CreateProfile.this.number);

						profile.setProfileStatus(CreateProfile.this.mood.getText().toString());

						profile.setProfilePic(jobj.getString("data"));
						
						profile.setmAvailabilityStatus(CreateProfile.this.availabilityStatus.getText().toString());

						GlobalClass.profileData=profile;
						
						DatabaseAccessor dbAccessor=new DatabaseAccessor(CreateProfile.this);

						dbAccessor.insertProfile(CreateProfile.this.name.getText().toString(),CreateProfile.this.number,CreateProfile.this.mood.getText().toString(), jobj.getString("data"),CreateProfile.this.availabilityStatus.getText().toString());

						Intent i=new Intent(CreateProfile.this,WelcomScreen.class);

						startActivity(i);

						finish();
					}
					else
					{
						Toast.makeText(CreateProfile.this, "Profile could not be saved", Toast.LENGTH_LONG).show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				

				/*if(result.equals(200))
				{
					Intent i=new Intent(CreateProfile.this,WelcomScreen.class);

					startActivity(i);

					finish();
				}
				else
				{
					Toast.makeText(CreateProfile.this, "Profile could not be saved", Toast.LENGTH_LONG).show();
				}*/
				 
			}
		}.execute(number,name,mood,availabilitystatus,profileimage,new GetValues(CreateProfile.this).getRegistrationId());	
	}

	public int uploadFile(String profileimage, String number,String name,
			String mood, String availabilitystatus) 
	{
		int serverResponseCode = 0;
		JSONObject jsonObject = null ;
		try {
			//contact = new JSONArray();

			jsonObject = new JSONObject();

			jsonObject.put("key", "update");
			
			jsonObject.put("number", number);

			jsonObject.put("name", name);

			jsonObject.put("lm", mood);

			jsonObject.put("status", availabilitystatus);

			//contact.put(jsonObject);

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		String fileName = profileimage;

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(profileimage);

		if (!sourceFile.isFile()) {
			// dialog.dismiss();

			Log.e("uploadFile", "Source File not exist :" + profileImagePath);
			return 0;

		} else {
			try {

				// open a URL connection to the Servlet
				FileInputStream fileInputStream = new FileInputStream(
						sourceFile);
				URL url = new URL(Constants.APP_SERVER_ROOT_PATH
						+ URLEncoder.encode(jsonObject.toString(), "UTF-8"));

				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				conn.setRequestProperty("uploaded_file", fileName);

				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
						+ fileName + "\"" + lineEnd);

				dos.writeBytes(lineEnd);

				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0) {

					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();

				String serverResponseMessage = conn.getResponseMessage();

				Log.i("uploadFile", "HTTP Response is : "
						+ serverResponseMessage + ": " + serverResponseCode);

				if (serverResponseCode == 200) 
				{

					runOnUiThread(new Runnable() {
						public void run() 
						{
							Toast.makeText(CreateProfile.this, "File Upload Complete.", 
									Toast.LENGTH_SHORT).show();
						}
					});
				}

				// close the streams //
				fileInputStream.close();
				dos.flush();
				dos.close();

			} 

			catch (Exception ex) {

				// dialog.dismiss();
				ex.printStackTrace();

				runOnUiThread(new Runnable() {
					public void run() 
					{
						Toast.makeText(CreateProfile.this, "Failed to save profile", 
								Toast.LENGTH_SHORT).show();
					}
				});

				Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
			}
			// dialog.dismiss();
			return serverResponseCode;

		} // End else block
	}

}
