
/*
 * Copyright statement here.
 */

package com.connectcribmessenger.main;

import java.util.ArrayList;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.connectcribmessenger.entities.RecentChat;
import com.connectcribmessenger.functionality.AppFunctionalityProvider;


/*
 * This class provides all the top level methods neccessary for the app functionality
 */
public class AppBuilderClass 
{
	private Context mContext;

	private AppFunctionalityProvider mAppFunctionalityProvider;

	//Constructor
	public AppBuilderClass(Context context)
	{
		mContext=context;

		mAppFunctionalityProvider=new AppFunctionalityProvider(mContext);
	}

	//Used in SplashScreen Activity
	public void gcmRegistration(Handler handler)
	{
		mAppFunctionalityProvider.gcmRegisterRegistrationId(handler);
	}

	//Used in AccountActivationScreen
	public void getCurrentCountryCode(Handler mHandler)
	{
		mAppFunctionalityProvider.getCurrentCountryCode(mHandler);
	}

	public void registerIncomingSmsReceiver(Handler mHandler)
	{
		mAppFunctionalityProvider.registerIncomingSmsReceiver(mHandler);
	}

	public void unregisterIncomingSmsReceiver()
	{
		mAppFunctionalityProvider.unregisterIncomingSmsReceiver();
	}

	public String getOTPFromSms(String sms)
	{
		return mAppFunctionalityProvider.getOTPFromSms(sms);
	}

	public void sendSmsForOtp(Handler handler,String number)
	{
		mAppFunctionalityProvider.sendSmsForOtp(handler,number);
	}

	public void confirmOtp(Handler handler,String otp,String number)
	{
		mAppFunctionalityProvider.confirmOtp(handler, otp,number);
	}

	public String getMobileNumber()
	{
		return mAppFunctionalityProvider.getMobileNumber();
	}

	public void setTextToTermsAndConditions(TextView view)
	{

		mAppFunctionalityProvider.setTextToTermsAndConditions(view);

	}

	public void setTextToNumberEditText(EditText view)
	{

		mAppFunctionalityProvider.setTextToNumberEditText(view);

	}

	public void storeAppFirstTimeOpenedStatus(boolean val)
	{
		mAppFunctionalityProvider.storeAppFirstTimeOpenedStatus(val);

	}

	public void navigateFromSplash(String onhandleMessage,long startTime)
	{
		mAppFunctionalityProvider.navigateFromSplash(onhandleMessage,startTime);

	}

	public void startSplashScreenAnimation(ImageView bannerImageView,ImageView dotImageView)
	{
		mAppFunctionalityProvider.startSplashScreenAnimation(bannerImageView, dotImageView);

	}

	public void navigateToVerificationScreen(EditText numberEditText)
	{
		mAppFunctionalityProvider.navigateToVerificationScreen(numberEditText);
	}

	public void setFlagCountryNameCountryCode(String countryCode,ImageView flagImageView,TextView countryNameTextView,TextView countryCodeTextView)
	{
		mAppFunctionalityProvider.setFlagCountryNameCountryCode(countryCode, flagImageView, countryNameTextView, countryCodeTextView);
	}

	public void verifyContactNumber(String number,String sendSmsForOtp,String incomingSmsReceiver,String ConfirmOtp)
	{
		mAppFunctionalityProvider.verifyContactNumber(number,sendSmsForOtp, incomingSmsReceiver, ConfirmOtp);
	}

	public void showDialog(int dialog_case,String tag)
	{
		mAppFunctionalityProvider.showDialog(dialog_case, tag);

	}

	public void getProfileDataFromDatabase(Handler handler)
	{
		mAppFunctionalityProvider.getProfileDataFromDatabase(handler);
	}

	
	public ArrayList<RecentChat> getRecentChatList(ArrayList<RecentChat> totalMessageList)
	{
		return mAppFunctionalityProvider.getRecentChatList(totalMessageList);
	}

	/*public String getTimeToDisplay(String timeinmillis)
	{
		return mAppFunctionalityProvider.getTimeToDisplay(timeinmillis);
	}*/
	public void fetchConversation(Handler handler)
	{
		mAppFunctionalityProvider.fetchConversation(handler);

	}
	public void createImageFetcherFromNetwork()
	{

		mAppFunctionalityProvider.createImageFetcherFromNetwork();



	}
	/*//Method to set conversation in the listview of ChatScreen.java
	//Used in ChatScreen.java handleMessage() method
	public void setConversationToListView(String msg,ListView listview)
	{
		mAppFunctionalityProvider.setConversationToListView(msg, listview);

	}*/

	public void activiyCallbacks(String activityName,String callBackMethodName)
	{
		mAppFunctionalityProvider.activiyCallbacks(activityName, callBackMethodName);
	}
	public void saveAndUpdateMessage(String message,String messageType,String msgId,String seenStatus)
	{
		mAppFunctionalityProvider.saveAndUpdateMessage(message,messageType,msgId,seenStatus);
	}

	//Used in ChatListAdapter
	public void setDrawableInText(TextView view, String fulltext,int resourceId,int start,int end,int size) 
	{
		mAppFunctionalityProvider.setDrawableInText(view, fulltext, resourceId, start, end, size);

	}

	//Used in ChatScreen.java

	public void hideSoftKeyboard(EditText editText)
	{
		mAppFunctionalityProvider.hideSoftKeyboard(editText);
	}

	public void sendCCSMessage(String message,String messageType,String chatType,String pic,String groupInfo,String msgId)
	{
		mAppFunctionalityProvider.sendCCSMessage(message, messageType, chatType, pic, groupInfo,msgId);
	}

	public void getLatestRecentListAndUpdateList(Handler handler)
	{
		mAppFunctionalityProvider.getLatestRecentListAndUpdateList(handler);
	}

	//Used in RecentChatListScreen.java
	public void callOnItemClickRecentChatList(int position)
	{

		mAppFunctionalityProvider.callOnItemClickRecentChatList(position);
	}
	//Used in RecentChatListScreen.java
	public void callOnItemClickArchivedRecentChatList(int position)
	{

		mAppFunctionalityProvider.callOnItemClickArchivedRecentChatList(position);
	}

	//Used in RecentChatList.java
	public void displayArchivedRecentChatsInList(String msg,Handler handler,ListView list)
	{
		mAppFunctionalityProvider.displayArchivedRecentChatsInList(msg, handler, list);
	}

	public void removeListItem(View rowView, final int position,Button archiveBtn) {

		mAppFunctionalityProvider.removeListItem(rowView, position,archiveBtn);

	}

	public void unArchive(View rowView, final int position) 
	{

		mAppFunctionalityProvider.unArchive(rowView, position);

	}
	
	

	
}
