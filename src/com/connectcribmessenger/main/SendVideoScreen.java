package com.connectcribmessenger.main;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.connectcribmessenger.adapters.ContactListForSelectionAdapter;
import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.backgroundtasks.GetContactsFromPhone;
import com.connectcribmessenger.entities.Contact;
import com.connectcribmessenger.entities.SelectedContactForMediaMessage;
import com.connectcribmessenger.interfaces.CommonMethods;
import com.example.android.displayingbitmaps.util.ImageCache;
import com.example.android.displayingbitmaps.util.ImageFetcher;
import com.example.android.displayingbitmaps.util.Utils;

public class SendVideoScreen extends ActionBarActivity implements CommonMethods, Callback
{
	private VideoView mVideoView;

	private Button mAddRecipientsBtn;

	private Button mCancel;

	private Button mSend;

	private String mVideoPath;

	private TextView contactSeletced;

	private RelativeLayout mVideoFrameLayout;

	private ImageView mPlay_btn;

	private ImageFetcher mImageFetcher;

	//private SetAdapterAsyncTask adapterAsync;

	private ArrayList<Contact> contactList;

	private Handler mHandler;

	private ArrayList<SelectedContactForMediaMessage> selectedItems;


	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.send_video_screen);

		instantiateViews();

		instantiateNonViewClasses();

		setproperties();

		setViewListeners();
	}

	@Override
	public void instantiateNonViewClasses()
	{
		//adapterAsync=new SetAdapterAsyncTask();
		mHandler=new Handler(this);
	}

	@Override
	public void instantiateViews()
	{
		// TODO Auto-generated method stub

		mVideoView=(VideoView)findViewById(R.id.videoView);

		mAddRecipientsBtn=(Button)findViewById(R.id.add_recipients);

		mCancel=(Button)findViewById(R.id.cancel);

		mSend=(Button)findViewById(R.id.send);

		mVideoFrameLayout=(RelativeLayout)findViewById(R.id.videoFrameLayout);

		mPlay_btn=(ImageView)findViewById(R.id.play_button);

		contactSeletced=(TextView)findViewById(R.id.contactSelected);

	}

	@Override
	public void setproperties() 
	{
		if(GlobalClass.chatAdapter!=null)
		{
			contactSeletced.setText("1"+" Contacts selected");
			
			mAddRecipientsBtn.setVisibility(View.GONE);
		}
		else
		{
			mAddRecipientsBtn.setVisibility(View.VISIBLE);
			
			contactSeletced.setText("0"+" Contacts selected");
		}


		mVideoPath=getIntent().getStringExtra("videoPath");

		mVideoView.setVideoPath(mVideoPath);

		mVideoView.requestFocus();


		getSupportActionBar().setHomeButtonEnabled(true);

		getSupportActionBar().setTitle("Selected Video");

		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.header));

	}

	@Override
	public void setViewListeners()
	{
		mAddRecipientsBtn.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				instantiateNonViewClasses();

				showConnectCribContactsDialog();
				//adapterAsync.execute(null,null,null);

			}
		});

		mCancel.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				finish();

			}
		});

		mSend.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				if(GlobalClass.chatAdapter!=null)
				{
					Intent intent=new Intent(SendVideoScreen.this,UploadVideoProgressDialog.class);

					intent.putExtra("videoPath",mVideoPath);

					startActivity(intent);
				}
				else
				{
					if(selectedItems!=null&&selectedItems.size()>0)
					{
						Intent intent=new Intent(SendVideoScreen.this,UploadVideoProgressDialog.class);

						intent.putExtra("videoPath",mVideoPath);

						startActivity(intent);

					}
					else
					{
						Toast.makeText(SendVideoScreen.this, "Please select any recipient", Toast.LENGTH_SHORT).show();
					}
				}
				


			}
		});
		mPlay_btn.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v) 
			{
				mVideoView.start();

				mPlay_btn.setVisibility(View.GONE);


			}
		});

		mVideoView.setOnCompletionListener(new OnCompletionListener() 
		{

			@Override
			public void onCompletion(MediaPlayer mp) 
			{

				mPlay_btn.setVisibility(View.VISIBLE);
				//mThumbNailImageView.setVisibility(View.VISIBLE);
			}
		});

		mVideoView.setOnPreparedListener(new OnPreparedListener() 
		{

			@Override
			public void onPrepared(MediaPlayer mp) 
			{

				mVideoView.start();

				Handler handler=new Handler();

				handler.postDelayed(new Runnable() {

					@Override
					public void run() 
					{
						mVideoView.pause();

					}
				}, 500);


			}
		});

		mVideoFrameLayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v) 
			{

				if(mVideoView.isPlaying())
				{
					mVideoView.pause();

					mPlay_btn.setVisibility(View.VISIBLE);

				}


			}
		});
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();


	}

	public void contactListDialog(final ArrayList<Contact> contactList)
	{
		final Dialog myDialog = new Dialog(this);

		myDialog.setCancelable(false);

		//myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		myDialog.setTitle("Select Contacts");

		myDialog.setContentView(R.layout.dialog_contact_selection);

		myDialog.getWindow().setGravity(Gravity.CENTER);

		DisplayMetrics displayMetrics = new DisplayMetrics();

		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

		final int height = displayMetrics.heightPixels;

		final int width = displayMetrics.widthPixels;

		myDialog.getWindow().setLayout((int) (width*.80), (int) (height*.60));
		//myDialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);


		Button cancel=(Button)myDialog.findViewById(R.id.cancel);

		Button ok=(Button)myDialog.findViewById(R.id.ok);

		final ListView listView=(ListView)myDialog.findViewById(R.id.listView1);

		createImageFetcherFromNetwork();

		listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

		final ContactListForSelectionAdapter contactAdapter=new ContactListForSelectionAdapter(this, contactList , mImageFetcher);

		listView.setAdapter(contactAdapter);

		listView.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) 
			{
				// Pause fetcher to ensure smoother scrolling when flinging
				if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING)
				{
					// Before Honeycomb pause image loading on scroll to help with performance
					if (!Utils.hasHoneycomb()) 
					{
						mImageFetcher.setPauseWork(true);
					}
				} else 
				{
					mImageFetcher.setPauseWork(false);

				}

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub

			}
		});

		ok.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{

				selectedItems = new ArrayList<>();

				for (int i = 0; i < contactList.size(); i++) 
				{

					if (GlobalClass.contactList.get(i).isSelected())
					{
						SelectedContactForMediaMessage selectedContact=new SelectedContactForMediaMessage();

						selectedContact.setName(GlobalClass.contactList.get(i).getName());

						selectedContact.setNumber(GlobalClass.contactList.get(i).getNumber());

						selectedContact.setProfilePic(GlobalClass.contactList.get(i).getProfilePicPath());

						selectedContact.setConversationType(Constants.CHAT_TYPE_PEER_TO_PEER);

						selectedContact.setMessageType(Constants.MESSAGE_TYPE_VIDEO);

						selectedItems.add(selectedContact);
					}

				}
				GlobalClass.selectedItems=selectedItems;

				GlobalClass.printDebugLog("pending", "Api to upload the video select to all the contacts seleted");

				Toast.makeText(SendVideoScreen.this, ""+selectedItems.size()+" Contacts selected", Toast.LENGTH_LONG).show();

				contactSeletced.setText(""+selectedItems.size()+" Contacts selected");

				myDialog.dismiss();


			}
		});

		cancel.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				myDialog.dismiss();

			}
		});
		myDialog.setOnDismissListener(new OnDismissListener() 
		{

			@Override
			public void onDismiss(DialogInterface dialog)
			{
				mImageFetcher.closeCache();

			}
		});


		myDialog.show();
	}


	public void createImageFetcherFromNetwork()
	{

		int mImageThumbWidth = getResources().getDimensionPixelSize(R.dimen.list_icon_width);

		int mImageThumbHeight = getResources().getDimensionPixelSize(R.dimen.list_icon_height);

		ImageCache.ImageCacheParams cacheParams =
				new ImageCache.ImageCacheParams(this, Constants.IMAGE_CACHE_DIR);

		cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

		// The ImageFetcher takes care of loading images into our ImageView children asynchronously
		mImageFetcher = new ImageFetcher(this,mImageThumbWidth,mImageThumbHeight);

		mImageFetcher.setLoadingImage(R.drawable.empty_photo);

		mImageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);



	}

	/*This method does following tasks:
	 *1. Get all the contacts from the device.
	 *2. Send the device contacts to server for matching device contacts with the already registered
	 *   contacts.
	 *3. Finally returns the list of all the connectcrib contacts returned from server.*/
	public void showConnectCribContactsDialog()
	{
		/*This background task fetches all the contacts which has number*/
		if(GlobalClass.isInternetAvailable(this))
		{
			new GetContactsFromPhone(this, mHandler).execute(null,null,null);	
		}
		else
		{
			//If no internet connection is available then dialog is displayed
			//showDialog(Constants.DIALOG_GCM_REGISTRATION_NO_INTERNET,"Internet");

			new AlertDialog.Builder(this)
			.setTitle("ConnectCrib")
			.setMessage("No Internet Connection Available.")

			.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) { 
					dialog.dismiss();
				}
			})
			.setIcon(android.R.drawable.ic_dialog_alert)
			.show();
		}


	}
	private void showConnectCribContactsDialog(ArrayList<Contact> contactList)
	{
		/*contactAdapter=new ContactListAdapter(this, contactList , mImageFetcher);

		mListView.setAdapter(contactAdapter);*/

		contactListDialog(contactList);
	}
	@Override
	public boolean handleMessage(Message msg) 
	{
		ArrayList<Contact> GetConnectCribContactsAsyncResult=msg.getData().getParcelableArrayList("GetConnectCribContactsAsyncResult");

		if(GetConnectCribContactsAsyncResult!=null)
		{
			if(GetConnectCribContactsAsyncResult.size()>0)
			{
				showConnectCribContactsDialog(GetConnectCribContactsAsyncResult);
			}
			else
			{
				Toast.makeText(this, "0 ConnectCrib Contacts found", Toast.LENGTH_LONG).show();
				finish();

			}

		}

		return true;}
}
