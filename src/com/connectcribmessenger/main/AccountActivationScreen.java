package com.connectcribmessenger.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;

public class AccountActivationScreen extends FragmentActivity implements Callback 
{
	Button next;

	TextView termsConditions;

	ImageView flag_imageview;

	TextView country_name_label;

	TextView mnc_label;

	EditText numberEditText;

	private static AppBuilderClass mAppBuilderClass;

	private static Handler mHandler;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.account_activation_option_screen);

		//Mint.initAndStartSession(this, "f11fe183");

		mHandler=new Handler(this);

		mAppBuilderClass=new AppBuilderClass(this);

		termsConditions=(TextView)findViewById(R.id.terms);

		country_name_label=(TextView)findViewById(R.id.country_label);

		mnc_label=(TextView)findViewById(R.id.mnc);

		flag_imageview=(ImageView)findViewById(R.id.flag_imageview);

		numberEditText=(EditText)findViewById(R.id.numberEditText);

		next=(Button)findViewById(R.id.next_btn);


		//sets text on the termsConditions TextView
		mAppBuilderClass.setTextToTermsAndConditions(termsConditions);

		//This sets the EditText with the default number of the mobile
		mAppBuilderClass.setTextToNumberEditText(numberEditText);

		//This triggers a background process to fetch current country code
		getCurrentCountryCode();



		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				//mAppBuilderClass.navigateToVerificationScreen(numberEditText);

				String number=numberEditText.getText().toString();

				if(number!=null&&number.length()!=0)
				{
					//Toast.makeText(AccountActivationScreen.this, "Height-->"+next.getHeight()+" and width-->"+next.getWidth(), Toast.LENGTH_LONG).show();

					//Intent intent=new Intent(AccountActivationScreen.this,RecentChatListScreen.class);

					

					/*For now we are directly navigating to CreateProfile.
					 *Actually from here we must navigate to Verification Activity
					 * */
					
					Intent intent=new Intent(AccountActivationScreen.this,VerificationScreen.class);
					//Intent intent=new Intent(AccountActivationScreen.this,CreateProfile.class);
					
					GlobalClass.printDebugLog("number", "AccountActivationScreen-->"+GlobalClass.INTERNATIONAL_MOBILE_COUNTRY_CODE+number);
					
					intent.putExtra("number", GlobalClass.INTERNATIONAL_MOBILE_COUNTRY_CODE+number);

					startActivity(intent);

					finish();	
				}
				else
				{
					numberEditText.setError("Field Can Not Be Empty.");
					numberEditText.requestFocus();
				}



			}
		});

		termsConditions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) 
			{

				mAppBuilderClass.showDialog(Constants.DIALOG_TERMS_AND_CONDITION,  "terms&conditions");

			}
		});
	}



	@Override
	public boolean handleMessage(Message msg) 
	{
		String countryCode=msg.getData().getString("countryCode");
		if(countryCode!=null)
		{
			mAppBuilderClass.setFlagCountryNameCountryCode(countryCode, flag_imageview, country_name_label, mnc_label);
		}		

		return true;
	}

	public static void getCurrentCountryCode()
	{
		mAppBuilderClass.getCurrentCountryCode(mHandler);
	}

}
