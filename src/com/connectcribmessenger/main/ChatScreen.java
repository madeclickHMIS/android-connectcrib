package com.connectcribmessenger.main;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.connectcribmessenger.adapters.ChatAdapter;
import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.TextCrypter;
import com.connectcribmessenger.auxilliaryclasses.DatabaseAccessor;
import com.connectcribmessenger.auxilliaryclasses.GetValues;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.backgroundtasks.GetOnlineOfflineStatus;
import com.connectcribmessenger.backgroundtasks.SqliteDataProviderThread;
import com.connectcribmessenger.interfaces.CommonMethods;
import com.example.android.displayingbitmaps.util.ImageCache;
import com.example.android.displayingbitmaps.util.ImageFetcher;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.rockerhieu.emojicon.EmojiconEditText;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;
//import com.connectcribmessenger.services.GetOnlineOfflineStatus;
//import com.connectcribmessenger.backgroundtasks.GetOnlineOfflineStatus;

public class ChatScreen extends ActionBarActivity implements EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener, Callback,CommonMethods
{

	EmojiconEditText writeEditText;


	Button sendMessage;

	Button openSmiley;

	Button captureImage;

	ListView chatList;

	boolean isEmojiOpened=false;

	FrameLayout emojIconsLayout;

	Handler handler;

	AppBuilderClass mAppBuilderClass;

	String message_type;

	AtomicInteger msgId;

	private ChatAdapter chatAdapter;

	private GetValues mGetValues;

	private static int retry=0;

	private String[]  popUpContents;

	private PopupWindow popupWindowMenu;

	private TextView mStatus;

	private ImageView profilePicImage;

	private ImageFetcher mImageFetcher;

	private Button backImage;

	private Button attachment_btn;

	private Button openContextMenu;

	private Button sendvideo_btn;

	private Button camera_btn;

	private Button audio_btn;
	private boolean isDataAvailable=false;
	private int RECORD_VIDEO=0;
	private Uri mUri;
	private TextView mName;

	private GetOnlineOfflineStatus getOnlineOfflinestatus;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		//requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.chat_screen);

		createImageFetcherFromNetwork();
		//Toast.makeText(this, ""+getIntent().getStringExtra(Constants.INTENT_SHORTCUT_NUMBER), Toast.LENGTH_SHORT).show();

		instantiateViews();

		instantiateNonViewClasses();	

		setproperties();

		/*makeMessagesSeen();*/

		setViewListeners();

		/**
		 * Getting data to display in the listview*/

//		mAppBuilderClass.fetchConversation(handler);

		getOnlineOfflineStatus();


	}

	private void setEmojiconFragment(boolean useSystemDefault) {
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.emojicons, EmojiconsFragment.newInstance(useSystemDefault))
		.commit();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		/*MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.chat_screen_menu, menu);*/
		return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		/*// Handle presses on the action bar items
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent intent=new Intent(this,ProfileActivity.class);
			startActivity(intent);
			return true;
		//case R.id.action_notification:
			// openSearch();
		//	return true;
		case R.id.action_settings:
			// openSettings();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}*/
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onEmojiconClicked(Emojicon emojicon) 
	{
		EmojiconsFragment.input(writeEditText, emojicon);

	}

	@Override
	public void onEmojiconBackspaceClicked(View v) 
	{
		EmojiconsFragment.backspace(writeEditText);

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mImageFetcher.setPauseWork(false);

		mImageFetcher.setExitTasksEarly(true);

		mImageFetcher.flushCache();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mAppBuilderClass.fetchConversation(handler);
		GlobalClass.HANDLER_CHAT_SCREEN=handler;

		GlobalClass.isAppRunning=true;
		mImageFetcher.setExitTasksEarly(false);
	}
	@Override
	public boolean handleMessage(Message msg) 
	{
		String param=msg.getData().getString("SqlitedataProviderThread");

		String ccs_message=msg.getData().getString("ccs_message");

		String gcmIntentservice_Status=msg.getData().getString("GcmIntentservice_Status");

		String getOnlineOfflineServiceResult=msg.getData().getString("GetOnlineOfflineAsyncResult");

		String profile_pic=msg.getData().getString("profile_pic");

		if(profile_pic!=null)
		{
			profilePicImage.setImageDrawable(null);
			profilePicImage.refreshDrawableState();
			mImageFetcher.loadImage(profile_pic, profilePicImage);
		}

		if(getOnlineOfflineServiceResult!=null)
		{
			GlobalClass.printDebugLog("ChatScreen Handler", "getOnlineOfflineStatus-->\n"+getOnlineOfflineServiceResult);

			if(!getOnlineOfflineServiceResult.equals("nodata"))
			{
				mStatus.setText(getOnlineOfflineServiceResult);	
			}

		}

		if(gcmIntentservice_Status!=null)
		{
			GlobalClass.printDebugLog("ChatScreen Handler", "gcmIntentservice_Status-->\n"+gcmIntentservice_Status);

			if(!gcmIntentservice_Status.equals("nodata"))
			{
				mStatus.setText(gcmIntentservice_Status);	
			}

		}

		if(ccs_message!=null)
		{
			if(ccs_message.equals("update_list"))
			{
				chatAdapter.notifyDataSetChanged();
				//chatList.setAdapter(chatAdapter);

				chatList.smoothScrollToPosition(GlobalClass.MessageList.size()-1);
			}	
		}

		if(param!=null)
		{
			makeMessagesSeen();
			setConversationToListView(param);

		}

		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();

		if(isEmojiOpened)
		{
			emojIconsLayout.setVisibility(View.GONE);
			isEmojiOpened=false;
		}
		else
		{
			GlobalClass.HANDLER_CHAT_SCREEN=null;

			GlobalClass.isAppRunning=false;

			finish();
		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		GlobalClass.HANDLER_CHAT_SCREEN=null;

		GlobalClass.isAppRunning=false;
	}



	@Override
	public void instantiateNonViewClasses() 
	{
		// TODO Auto-generated method stub


		mGetValues=new GetValues(this);

		handler=new Handler(this);

		msgId=new AtomicInteger();

		chatAdapter=new ChatAdapter(this,mImageFetcher);

		GlobalClass.HANDLER_CHAT_SCREEN=handler;

		GlobalClass.isAppRunning=true;


		mAppBuilderClass=new AppBuilderClass(this);

		getOnlineOfflinestatus=new GetOnlineOfflineStatus(this, handler);
	}

	@Override
	public void instantiateViews() 
	{
		chatList=(ListView)findViewById(R.id.chatlist);

		writeEditText=(EmojiconEditText)findViewById(R.id.editEmojicon);

		openSmiley=(Button)findViewById(R.id.smiley_btn);

		captureImage=(Button)findViewById(R.id.camera_btn);

		sendMessage=(Button)findViewById(R.id.send_btn);

		sendvideo_btn=(Button)findViewById(R.id.video_btn);

		emojIconsLayout=(FrameLayout)findViewById(R.id.emojicons);

		backImage=(Button)findViewById(R.id.back);

		attachment_btn=(Button)findViewById(R.id.attachment_btn);

		//Button openContextMenu=(Button)findViewById(R.id.openmenu_btn);
		mName = (TextView)findViewById(R.id.name);

		mStatus = (TextView) findViewById(R.id.status);

		profilePicImage=(ImageView)findViewById(R.id.profilepic);

	}
	private void launchUploadActivity(boolean isImage)
	{
		Intent i = new Intent(ChatScreen.this, SendVideoScreen.class);
		
		i.putExtra("videoPath", mUri.getPath());
		
		i.putExtra("isImage", isImage);
		
		startActivity(i);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{


		if (requestCode == RECORD_VIDEO) 
		{
			if (resultCode == RESULT_OK) 
			{

				// video successfully recorded
				// launching upload activity

				launchUploadActivity(false);

			} else if (resultCode == RESULT_CANCELED) {

				// user cancelled recording
				Toast.makeText(getApplicationContext(),
						"User cancelled video recording", Toast.LENGTH_SHORT)
						.show();

			} 
			else 
			{
				// failed to record video
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to record video", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}
	@Override
	public void setproperties() 
	{
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false); 

		List<String> menuList = new ArrayList<String>();

		menuList.add("Send 60 sec Video");

		menuList.add("Status");

		menuList.add("Send Voice Message");

		menuList.add("Backup Message");

		// convert to simple array
		popUpContents = new String[menuList.size()];

		menuList.toArray(popUpContents);

		popupWindowMenu = popupWindowMenu();
		// TODO Auto-generated method stub
		emojIconsLayout.setVisibility(View.GONE);

		setEmojiconFragment(false);


		mName.setText(GlobalClass.conversationInfo.getConversationName());

		mStatus.setText("");

		profilePicImage.setImageDrawable(null);

		profilePicImage.refreshDrawableState();
		
		GlobalClass.printDebugLog("ChatScreen", "profileimage-->"+GlobalClass.conversationInfo.getConversationImagePath());
		//profilePicImage.setBackgroundDrawable(GlobalClass.conversationInfo.getConversationImage());
		mImageFetcher.loadImage(GlobalClass.conversationInfo.getConversationImagePath(), profilePicImage);

		GlobalClass.IS_CHATSCREEN_OPENED=true;

		GlobalClass.HANDLER_CHAT_SCREEN=handler;

		GlobalClass.chatAdapter=chatAdapter;

		GlobalClass.listview=chatList;

		chatList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

		chatList.setMultiChoiceModeListener(new MultiChoiceModeListener() 
		{
			private int nr = 0;
			@Override
			public void onItemCheckedStateChanged(ActionMode mode, int position,
					long id, boolean checked) 
			{
				// Here you can do something when items are selected/de-selected,
				// such as update the title in the CAB

				//Toast.makeText(ChatScreen.this, "onItemCheckedStateChanged-->"+checked, Toast.LENGTH_SHORT).show();

				if (checked)
				{
					nr++;

					chatAdapter.setNewSelection(position, checked);                    

				}
				else 
				{
					nr--;

					chatAdapter.removeSelection(position);                 
				}

				mode.setTitle(nr + " selected");

			}

			@Override
			public boolean onActionItemClicked(ActionMode mode, MenuItem item) 
			{
				// Respond to clicks on the actions in the CAB
				/*AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();

				int position = info.position;
				 */
				//Toast.makeText(ChatScreen.this, "onActionItemClicked", Toast.LENGTH_SHORT).show();

				switch (item.getItemId()) 
				{

				case R.id.item_delete:

					Toast.makeText(ChatScreen.this, item.getTitle(), Toast.LENGTH_SHORT).show();

					deleteConversation();

					nr=0;

					//chatAdapter.clearSelection();

					mode.finish(); // Action picked, so close the CAB

					return true;

				default:

					return false;
				}
			}

			@Override
			public boolean onCreateActionMode(ActionMode mode, Menu menu) 
			{
				// Inflate the menu for the CAB
				//Toast.makeText(ChatScreen.this,"onCreateActionMode", Toast.LENGTH_SHORT).show();

				nr = 0;

				MenuInflater inflater = mode.getMenuInflater();

				inflater.inflate(R.menu.contextual_action_menu, menu);

				return true;
			}

			@Override
			public void onDestroyActionMode(ActionMode mode) 
			{
				// Here you can make any necessary updates to the activity when

				// the CAB is removed. By default, selected items are deselected/unchecked.

				//Toast.makeText(ChatScreen.this,"onDestroyActionMode", Toast.LENGTH_SHORT).show();

				//chatAdapter.clearSelection();
			}

			@Override
			public boolean onPrepareActionMode(ActionMode mode, Menu menu) 
			{
				// Here you can perform updates to the CAB due to
				// an invalidate() request
				//Toast.makeText(ChatScreen.this,"onPrepareActionMode", Toast.LENGTH_SHORT).show();

				return false;
			}

		});

		chatList.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub
				//Toast.makeText(ChatScreen.this,"setOnItemLongClickListener", Toast.LENGTH_SHORT).show();

				chatList.setItemChecked(position, !chatAdapter.isPositionChecked(position));

				return false;
			}
		});


	}

	@Override
	public void setViewListeners() {



		openSmiley.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				if(isEmojiOpened)
				{
					emojIconsLayout.setVisibility(View.GONE);

					isEmojiOpened=false;
				}
				else
				{
					mAppBuilderClass.hideSoftKeyboard(writeEditText);

					emojIconsLayout.setVisibility(View.VISIBLE);

					isEmojiOpened=true;
				}
				// TODO Auto-generated method stub

			}
		});

		sendMessage.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				mAppBuilderClass.hideSoftKeyboard(writeEditText);

				if(writeEditText!=null&&!writeEditText.getText().toString().matches(""))
				{
					message_type=Constants.MESSAGE_TYPE_TEXT;

					//					mAppBuilderClass.saveAndUpdateMessage(writeEditText.getText().toString(),"text");

					String id = Integer.toString(msgId.incrementAndGet());
					
					GlobalClass.printDebugLog("ChatAdapter","before sending profile pic-->"+ GlobalClass.profileData.getProfilePic());


					sendCCSMessage(writeEditText.getText().toString(), message_type, GlobalClass.conversationInfo.getConversationType(), GlobalClass.profilePic, GlobalClass.conversationInfo.getConversationGroupInfo(),id);

					writeEditText.setText("");

				}
				else
				{
					if(isEmojiOpened)
					{
						emojIconsLayout.setVisibility(View.GONE);
						isEmojiOpened=false;
					}
					else
					{
						emojIconsLayout.setVisibility(View.VISIBLE);

						isEmojiOpened=true;
					}
				}


			}
		});

		backImage.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				ChatScreen.this.finish();

			}
		});
		sendvideo_btn.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				videoOptionDialog().show();
				/*Intent intent=new Intent(ChatScreen.this,VideoListScreen.class);

				startActivity(intent);*/

			}
		});

		/*openContextMenu.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				//ChatScreen.this.openContextMenu(v);

				popupWindowMenu.showAsDropDown(v, -5, 0);

			}
		});*/

	}

	private void makeMessagesSeen()
	{
		if(GlobalClass.conversationInfo.getConversationType().equals(Constants.CHAT_TYPE_GROUP))
		{
			HashMap<String, String> insertValues=new HashMap<>();

			insertValues.put("param", "updateGroupMessageSeen");

			SqliteDataProviderThread sqlitedataprovider=new SqliteDataProviderThread(this, insertValues);

			sqlitedataprovider.execute(null,null,null);
		}
		else if(GlobalClass.conversationInfo.getConversationType().equals(Constants.CHAT_TYPE_PEER_TO_PEER))
		{
			HashMap<String, String> insertValues=new HashMap<>();

			insertValues.put("param", "updatePeer2PeerMessageSeen");

			SqliteDataProviderThread sqlitedataprovider=new SqliteDataProviderThread(this, insertValues);

			sqlitedataprovider.execute(null,null,null);
		}
	}






	public void setConversationToListView(String msg)
	{
		if(msg.equals("peer2peer")||msg.equals("group"))
		{

			chatList.setAdapter(chatAdapter);

			scrollMyListViewToBottom();
		}

	}

	private void scrollMyListViewToBottom() {

		chatList.post(new Runnable() {
			@Override
			public void run() {
				// Select the last row so it will scroll into view...
				chatList.setSelection(chatAdapter.getCount() - 1);
			}
		});
	}
	/**
	 * Upstream a GCM message up to the 3rd party server
	 * @param message
	 */
	public void sendCCSMessage(String msg,String messageType,String chatType,String pic,String groupInfo,String msgId)
	{
		TextCrypter crypter=new TextCrypter();
		String message = null;
		try 
		{
			message = TextCrypter.bytesToHex(crypter.encrypt(msg));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		//String message=GlobalClass.EncryptData(msg, Constants.CRYPTOGRAPHY_CLIENTPRIVATEKEY, Constants.CRYPTOGRAPHY_CLIENTMODULUSKEY);
		
		GlobalClass.printDebugLog("ChatScreen OriginalMessage", "Original Message-->"+msg);
		GlobalClass.printDebugLog("ChatScreen EncryptedMessage", "Encrypted Message-->"+message);

		final GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(ChatScreen.this);





		String time=String.valueOf(Calendar.getInstance().getTimeInMillis());

		if(mGetValues.getRegistrationId() == null || mGetValues.getRegistrationId().equals(""))
		{
			Toast.makeText(ChatScreen.this, "You must register first", Toast.LENGTH_LONG).show();
			return;
		}

		new AsyncTask<String, Void, String>()
		{
			@Override
			protected String doInBackground(String... params)
			{
				String msg = "";
				try
				{
					Bundle data = new Bundle();
					data.putString("message", params[0]);
					if(params[1].equals("peer2peer"))
					{

						data.putString("chat_type", "peer2peer");

						data.putString("sender_name", GlobalClass.profileData.getProfileName());

						data.putString("sender_number", GlobalClass.profileData.getProfileNumber());

						data.putString("recipient_number", GlobalClass.conversationInfo.getConversationNumber());

						data.putString("message", params[0]);

						data.putString("message_type", params[2]);

						data.putString("message_time", params[3]);

						data.putString("sender_pic", params[4]);
					}
					else if(params[1].equals("group"))
					{
						data.putString("chat_type", "group");

						data.putString("sender_name", GlobalClass.profileData.getProfileName());

						data.putString("sender_number", GlobalClass.profileData.getProfileNumber());

						data.putString("recipient_number",  params[5]);

						data.putString("message", params[0]);

						data.putString("message_type", params[2]);

						data.putString("message_time", params[3]);

						data.putString("group_info", params[5]);
					}

					saveAndUpdateMessage(params[0],params[2],params[6],Constants.MESSAGE_SEEN);

					GlobalClass.printDebugLog("addrees", Constants.GCM_CCS_SENDER_ID+"@gcm.googleapis.com");

					GlobalClass.printDebugLog("ttl", ""+Constants.GCM_TIME_TO_LIVE);

					GlobalClass.printDebugLog("msgId", ""+params[6]);

					GlobalClass.printDebugLog("data", ""+data);

					gcm.send(Constants.GCM_CCS_SENDER_ID+"@gcm.googleapis.com", params[6], 1000, data);


					//GlobalClass.MESSAGE_DELIVERY_STATUS="sending";

					//mStoreValues.storeMessageDeliveryStatus(params[6], "sending");

					GlobalClass.MESSAGE_TYPE=params[2];

					GlobalClass.MESSAGE=params[0];

					msg=GlobalClass.MESSAGE;
				}
				catch (IOException ex)
				{
					msg = "Error :" + ex.getMessage();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg)
			{
				//chatAdapter=new ChatAdapter(ChatScreen.this);
				chatList.setAdapter(chatAdapter);


				chatList.setSelection(chatAdapter.getCount() - 1);
				//Toast.makeText(ChatScreen.this, msg, Toast.LENGTH_SHORT).show();
			}
		}.execute(message, chatType,messageType,time,pic,groupInfo,msgId);
	}

	public void saveAndUpdateMessage(String message,String messageType,String msgId,String seenStatus)
	{
		DatabaseAccessor dbAccessor=new DatabaseAccessor(this);

		String time=String.valueOf(Calendar.getInstance().getTimeInMillis());

		if(GlobalClass.conversationInfo.getConversationType().equals("peer2peer"))
		{
			//			dbAccessor.insertPeer2PeerMessage(GlobalClass.MessageList.get(0).getMessageSenderNumber(), message, messageType, time, Constants.MESSAGE_DELIVERY_STATUS_NO, Constants.MESSAGE_INOUT_OUTGOING_STATUS, GlobalClass.conversationInfo.getConversationImagePath(), GlobalClass.MessageList.get(0).getMessageSendername(),msgId,seenStatus,Constants.MESSAGE_ARCHIVED_STATUS_NO);
			dbAccessor.insertPeer2PeerMessage(GlobalClass.conversationInfo.getConversationNumber(), message, messageType, time, Constants.MESSAGE_DELIVERY_STATUS_NO, Constants.MESSAGE_INOUT_OUTGOING_STATUS, GlobalClass.conversationInfo.getConversationImagePath(), GlobalClass.conversationInfo.getConversationName(),msgId,seenStatus,Constants.MESSAGE_ARCHIVED_STATUS_NO);

			chatAdapter.updateList(message, Constants.MESSAGE_DELIVERY_STATUS_NO, GlobalClass.conversationInfo.getConversationName(),GlobalClass.conversationInfo.getConversationNumber(), time, messageType,Constants.MESSAGE_INOUT_OUTGOING_STATUS,chatList,seenStatus);

		}
		else
		{
			dbAccessor.insertGroupMessage(GlobalClass.profileData.getProfileNumber(), GlobalClass.profileData.getProfileName(), message, messageType, time, Constants.MESSAGE_DELIVERY_STATUS_NO, "outgoing", GlobalClass.conversationInfo.getConversationGroupInfo(),msgId,seenStatus,Constants.MESSAGE_ARCHIVED_STATUS_NO);

			chatAdapter.updateList(message, Constants.MESSAGE_DELIVERY_STATUS_NO, GlobalClass.profileData.getProfileName(), GlobalClass.profileData.getProfileNumber(), time, messageType,"outgoing",chatList,seenStatus);
		}


	}
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo)
	{
		super.onCreateContextMenu(menu, v, menuInfo);

		MenuInflater inflater = getMenuInflater();

		inflater.inflate(R.menu.chat_screen_menu, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) 
	{
		/*AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();

		int position = info.position;*/

		switch (item.getItemId()) 
		{
		case R.id.voicemessage:

			Toast.makeText(this, "Send Voice Message From Here", Toast.LENGTH_SHORT).show();



			return true;

		case R.id.videomessage:

			Toast.makeText(this, "Send Video Message From Here", Toast.LENGTH_SHORT).show();



			return true;
		case R.id.status:

			Toast.makeText(this, "Send Status From Here", Toast.LENGTH_SHORT).show();



			return true;

		case R.id.backup:

			Toast.makeText(this, "Send backup From Here", Toast.LENGTH_SHORT).show();



			return true;


		default:

			return super.onContextItemSelected(item);
		}
	}

	public PopupWindow popupWindowMenu() 
	{


		// initialize a pop up window type
		PopupWindow popupWindow = new PopupWindow(this);

		// the drop down list is a list view
		ListView listViewMenu = new ListView(this);

		// set our adapter and pass our pop up window contents
		listViewMenu.setAdapter(menuAdapter(popUpContents));

		// set the item click listener
		listViewMenu.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				// get the context and main activity to access variables
				Context mContext = v.getContext();
				// MainActivity mainActivity = ((MainActivity) mContext);

				// add some animation when a list item was clicked
				Animation fadeInAnimation = AnimationUtils.loadAnimation(v.getContext(), android.R.anim.fade_in);
				
				fadeInAnimation.setDuration(10);
				
				v.startAnimation(fadeInAnimation);

				// dismiss the pop up
				popupWindowMenu.dismiss();

				// get the text and set it as the button text

				Toast.makeText(mContext, "Selected Positon is: " + position, 100).show();

			}
		});

		// some other visual settings
		popupWindow.setFocusable(true);
		popupWindow.setWidth(250);
		popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

		// set the list view as pop up window content
		popupWindow.setContentView(listViewMenu);

		return popupWindow;
	}

	/*
	 * adapter where the list values will be set
	 */
	private ArrayAdapter<String> menuAdapter(String menuArray[]) {

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, menuArray) {

			@Override
			public View getView(int position, View convertView, ViewGroup parent) 
			{

				// setting the ID and text for every items in the list

				String text = getItem(position);               

				// visual settings for the list item
				TextView listItem = new TextView(ChatScreen.this);

				listItem.setText(text);
				
				listItem.setTag(position);

				listItem.setTextSize(18);
				
				listItem.setPadding(10, 10, 10, 10);
				
				listItem.setTextColor(Color.WHITE);

				return listItem;
			}
		};

		return adapter;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		GlobalClass.IS_CHATSCREEN_OPENED=false;
		
		GlobalClass.HANDLER_CHAT_SCREEN=null;
		
		GlobalClass.chatAdapter=null;
		
		GlobalClass.listview=null;
		
		mImageFetcher.closeCache();

		/*Intent intentService=new Intent(this,UpdateOfflineStatus.class);

		startService(intentService);*/



		if(getOnlineOfflinestatus!=null)
		{
			GlobalClass.printDebugLog("ChatScreen", "getOnlineOfflinestatus not null");
			getOnlineOfflinestatus.cancel(true);
		}
		else
		{
			GlobalClass.printDebugLog("ChatScreen", "getOnlineOfflinestatus  null");
		}


		//getOnlineOfflinestatus.cancel(true);
	}

	private void getOnlineOfflineStatus()
	{
		//getOnlineOfflinestatus=new GetOnlineOfflineStatus(this, handler);

		getOnlineOfflinestatus.execute(null,null);

		//Intent intentService=new Intent(this,GetOnlineOfflineStatus.class);

		//startService(intentService);

	}



	public void createImageFetcherFromNetwork()
	{

		int mImageThumbWidth = getResources().getDimensionPixelSize(R.dimen.list_icon_width);

		int mImageThumbHeight = getResources().getDimensionPixelSize(R.dimen.list_icon_height);

		ImageCache.ImageCacheParams cacheParams =
				new ImageCache.ImageCacheParams(this, Constants.IMAGE_CACHE_DIR);

		cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

		// The ImageFetcher takes care of loading images into our ImageView children asynchronously
		mImageFetcher = new ImageFetcher(this,mImageThumbWidth,mImageThumbHeight);

		mImageFetcher.setLoadingImage(R.drawable.empty_photo);

		mImageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);



	}

	public void createImageFetcherForThumnailPic()
	{

		int mImageThumbWidth = getResources().getDimensionPixelSize(R.dimen.list_icon_width);

		int mImageThumbHeight = getResources().getDimensionPixelSize(R.dimen.list_icon_height);

		ImageCache.ImageCacheParams cacheParams =
				new ImageCache.ImageCacheParams(this, Constants.IMAGE_CACHE_DIR);

		cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

		// The ImageFetcher takes care of loading images into our ImageView children asynchronously
		mImageFetcher = new ImageFetcher(this,mImageThumbWidth,mImageThumbHeight);

		mImageFetcher.setLoadingImage(R.drawable.empty_photo);

		mImageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);



	}

	public void deleteConversation() 
	{
		new AsyncTask<Void, Void, Void>()
		{
			ProgressDialog pd;
			protected void onPreExecute() 
			{
				pd=new ProgressDialog(ChatScreen.this);
				pd.setCancelable(false);
				pd.setMessage("Deleting Messages...");

				pd.show();

			};
			@Override
			protected Void doInBackground(Void... params) 
			{
						
				//Toast.makeText(ChatScreen.this, "deleteConversation called", Toast.LENGTH_SHORT).show();


				DatabaseAccessor dbAccessor=new DatabaseAccessor(ChatScreen.this);

				Map<Integer, Boolean> map = chatAdapter.getAllSelection();

				//Toast.makeText(ChatScreen.this, "Total selected items-->"+map.size(), Toast.LENGTH_SHORT).show();
				GlobalClass.printDebugLog("ChatScreen deleteConversation", "Total selected items-->"+map.size());

				Set keys = map.keySet();

				ArrayList<Integer> indexToDelete=new ArrayList<>();

				for (Iterator i = keys.iterator(); i.hasNext();)
				{
					int key = (int) i.next();


					boolean value = map.get(key);

					//Toast.makeText(ChatScreen.this, "key-->"+key+" and value-->"+value, Toast.LENGTH_SHORT).show();
					GlobalClass.printDebugLog("ChatScreen deleteConversation", "key-->"+key+" and value-->"+value);



					if(value)
					{
						GlobalClass.printDebugLog("ChatScreen deleteConversation", "index record to delete-->"+key);

						if(GlobalClass.conversationInfo.getConversationType().equals(Constants.CHAT_TYPE_PEER_TO_PEER))
						{

							dbAccessor.deletePeer2peerMessages(GlobalClass.conversationInfo.getConversationNumber(),GlobalClass.MessageList.get(key).getMessageId());

						}

						else
						{

							dbAccessor.deleteGroupMessages(GlobalClass.MessageList.get(key).getMessageSendernumber(),GlobalClass.MessageList.get(key).getMessageId());

						}


						//Toast.makeText(ChatScreen.this, key+" Chat has been deleted", Toast.LENGTH_SHORT).show();

						indexToDelete.add(key);


					}

				}

				GlobalClass.setChatScreenMessages(GlobalClass.conversationInfo.getConversationType(), GlobalClass.conversationInfo.getConversationNumber(), GlobalClass.conversationInfo.getConversationGroupId(), ChatScreen.this);

				return null;
			}

			protected void onPostExecute(Void result) 
			{
				pd.cancel();
				
				//nr=0;

				chatAdapter.clearSelection();

				//mode.finish(); // Action picked, so close the CAB
				chatAdapter.notifyDataSetChanged();
			};

		}.execute();

		


	}
	public Dialog videoOptionDialog()
	{
		DisplayMetrics displayMetrics = new DisplayMetrics();

		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

		final int height = displayMetrics.heightPixels;

		final int width = displayMetrics.widthPixels;

		final Dialog myDialog = new Dialog(this);
		myDialog.setCancelable(false);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		myDialog.setContentView(R.layout.video_option_dialog_layout);

		myDialog.getWindow().setGravity(Gravity.CENTER);

		//		myDialog.getWindow().setLayout((width*75)/100, LayoutParams.WRAP_CONTENT);

		myDialog.getWindow().setLayout((width*75)/100, (width*80)/100);

		myDialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);

		Button selectVideo=(Button)myDialog.findViewById(R.id.selectvideo);

		Button recordvideo=(Button)myDialog.findViewById(R.id.capturevideo);

		Button cancel=(Button)myDialog.findViewById(R.id.button_close);
		//TextView message_text=(TextView)myDialog.findViewById(R.id.textView1);
		//message_text.setText(message);

		selectVideo.setOnClickListener(new OnClickListener() 
		{									
			public void onClick(View v) 
			{
				Intent intent=new Intent(ChatScreen.this,VideoListScreen.class);

				startActivity(intent);

				myDialog.dismiss();

			}
		});

		recordvideo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) 
			{
				// Checking camera availability
				if (!isDeviceSupportCamera()) {
					Toast.makeText(getApplicationContext(),
							"Sorry! Your device doesn't support camera",
							Toast.LENGTH_LONG).show();
					// will close the app if the device does't have camera
					// finish();
				}
				else
				{

					mUri = getOutputMediaFileUri();

					if(mUri!=null)
					{
						recordVideo(mUri);
					}
					else
					{
						Toast.makeText(ChatScreen.this, "Sorry, can not record video from camera", Toast.LENGTH_LONG).show();
					}	
				}

				myDialog.dismiss();

			}
		});
		cancel.setOnClickListener(new OnClickListener() {									
			public void onClick(View v) {

				myDialog.dismiss();
			}
		});				

		return myDialog;
		//myDialog.show();
	}
	/**
	 * Launching camera app to record video
	 */
	private void recordVideo(Uri mUri)
	{
		Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);



		// set video quality
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, mUri); // set the image file
		// name

		// start the video capture Intent
		startActivityForResult(intent, RECORD_VIDEO);
	}
	/**
	 * Checking device has camera hardware or not
	 * */
	private boolean isDeviceSupportCamera() {
		if (getApplicationContext().getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA)) {
			// this device has a camera
			return true;
		} else {
			// no camera on this device
			return false;
		}
	}
	/** Create a file Uri for saving an image or video */
	private  Uri getOutputMediaFileUri(){
		//		return Uri.fromFile(getOutputMediaFile());

		return Uri.fromFile(createImageFile());
	}

	private File createImageFile(){

		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "ConnectCribVideo_" + timeStamp + "_";

		File filedir = null;
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
		{
			//filedir = new File(Environment.getExternalStorageDirectory()+File.separator +"CrazyZooImages"+File.separator +String.valueOf(System.currentTimeMillis())+ "_image.jpg");

			filedir=new File(getExternalFilesDir(null).toString()+"/ConnectCrib/Media/RecordedVideo");

			if (filedir.exists ())
			{
				Log.i("filedir.exists ()",""+filedir.exists ());
				try 
				{
					filedir=new File(filedir,imageFileName+".mp4");

				} catch (Exception e)
				{
					e.printStackTrace();
				}

			}
			else
			{
				Log.i("filedir.exists ()",""+filedir.exists ());
				filedir.mkdirs();
			}


		}
		else
		{
			Toast.makeText(this, "SD card not available to write", Toast.LENGTH_LONG).show();
		}



		//image_path=filedir.getAbsolutePath();
		//image_path=getExternalFilesDir(null).toString()+"/CrazyZoo/"+imageFileName+".jpg";
		//noteimagepath=getExternalFilesDir(null).toString()+"/MakeNotesImages/"+imageFileName+".jpeg";
		//Log.i("absolute_path2",image_path);
		return  filedir;
	}
}


