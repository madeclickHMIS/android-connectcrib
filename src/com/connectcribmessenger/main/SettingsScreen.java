package com.connectcribmessenger.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.connectcribmessenger.interfaces.CommonMethods;

public class SettingsScreen extends ActionBarActivity implements CommonMethods
{
	private Button mHelpButton;

	private Button mAccountButton;

	private Button mProfileButton;

	private Button mNotificationButton;

	private Button mChatSettingsButton;

	private Button mContactsButton;

	private Button mBackupButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.settings_layout);
		
		setproperties();

		instantiateViews();

		setViewListeners();
		
	}

	@Override
	public void instantiateNonViewClasses() 
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void instantiateViews() 
	{
		mHelpButton=(Button)findViewById(R.id.help_btn);

		mAccountButton=(Button)findViewById(R.id.account_btn);

		//mProfileButton=(Button)findViewById(R.id.profile_bt);

		mNotificationButton=(Button)findViewById(R.id.notification_btn);

		mChatSettingsButton=(Button)findViewById(R.id.chatsettings_btn);

		mContactsButton=(Button)findViewById(R.id.contacts_btn);

		mBackupButton=(Button)findViewById(R.id.backup_btn);

	}

	@Override
	public void setproperties() 
	{
		getSupportActionBar().setHomeButtonEnabled(true);

		getSupportActionBar().setTitle("Settings");

		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.header));

	}

	@Override
	public void setViewListeners() 
	{
		
		mBackupButton.setOnClickListener(new OnClickListener() 
		{
			
			@Override
			public void onClick(View v) 
			{
				Intent intent=new Intent(SettingsScreen.this,BackupSettingActivity.class);
				
				startActivity(intent);
				
			}
		});

	}

}
