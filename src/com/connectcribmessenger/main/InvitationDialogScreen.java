package com.connectcribmessenger.main;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.DatabaseAccessor;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.interfaces.CommonMethods;

public class InvitationDialogScreen extends Activity implements CommonMethods 
{
	private TextView message;

	private Button accept;

	private Button cancel;

	private Button block;

	private String name;

	private String number;
	
	private Bundle extras;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.dialog_invitechat);



		instantiateViews();

		setproperties();

		instantiateNonViewClasses();

		setViewListeners();

	}
	@Override
	public void instantiateNonViewClasses() 
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void instantiateViews() 
	{

		message=(TextView)findViewById(R.id.textview);

		accept=(Button)findViewById(R.id.accept);

		cancel=(Button)findViewById(R.id.cancel);

		block=(Button)findViewById(R.id.block);


	}

	@Override
	public void setproperties() 
	{
		//name=getIntent().getStringExtra("name");

		//number=getIntent().getStringExtra("number");
		
		extras=getIntent().getBundleExtra("extras");

		message.setText("Do you want to communicate with "+ extras.getString(Constants.GCM_DATA_KEY_SENDER_NAME)+"("+extras.getString(Constants.GCM_DATA_KEY_SENDER_NUMBER)+")"+" ?");

		DisplayMetrics displayMetrics = new DisplayMetrics();

		//getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

		final int height = displayMetrics.heightPixels;

		final int width = displayMetrics.widthPixels;

		getWindow().setLayout((width*80)/100, LayoutParams.WRAP_CONTENT);

	}

	@Override
	public void setViewListeners() 
	{
		accept.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				DatabaseAccessor dbAccessor=new DatabaseAccessor(InvitationDialogScreen.this);
				
				dbAccessor.insertPeer2PeerMessage(extras.getString(Constants.GCM_DATA_KEY_SENDER_NUMBER), extras.getString(Constants.GCM_DATA_KEY_MESSAGE), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_TYPE), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_TIME), Constants.MESSAGE_INCOMING_DELIVERY_STATUS, Constants.MESSAGE_INOUT_INCOMING_STATUS, extras.getString(Constants.GCM_DATA_KEY_PROFILE_PIC), extras.getString(Constants.GCM_DATA_KEY_SENDER_NAME), extras.getString(Constants.GCM_DATA_KEY_MESSAGE_ID), Constants.MESSAGE_UNSEEN,Constants.MESSAGE_ARCHIVED_STATUS_NO);

				dbAccessor.updatePeerToPeerArchivedStatus(extras.getString(Constants.GCM_DATA_KEY_SENDER_NUMBER), Constants.MESSAGE_ARCHIVED_STATUS_NO);
				
				finish();
				
				GlobalClass.cancelNotification(getApplicationContext(), 1);
				
				if(!GlobalClass.IS_RECENT_CHATS_LIST_CREEN_OPENED&&!GlobalClass.IS_CHATSCREEN_OPENED)
				{
					Intent intent=new Intent(InvitationDialogScreen.this,RecentChatListScreen.class);
					
					startActivity(intent);
				}	
				
			}
		});

		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) 
			{
				GlobalClass.cancelNotification(getApplicationContext(), 1);
				finish();

			}
		});

		block.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				GlobalClass.cancelNotification(getApplicationContext(), 1);

			}
		});

	}

}
