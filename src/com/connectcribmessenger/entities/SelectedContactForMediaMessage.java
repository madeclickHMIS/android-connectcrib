package com.connectcribmessenger.entities;

public class SelectedContactForMediaMessage 
{
	private String name;

	private String number;

	private String messageType;

	private String conversationType;

	private String grroupInfo;
	
	private String profilePic;
	
	

	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getConversationType() {
		return conversationType;
	}

	public void setConversationType(String conversationType) {
		this.conversationType = conversationType;
	}

	public String getGrroupInfo() {
		return grroupInfo;
	}

	public void setGrroupInfo(String grroupInfo) {
		this.grroupInfo = grroupInfo;
	}


}
