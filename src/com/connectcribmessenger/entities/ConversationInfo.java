package com.connectcribmessenger.entities;

import android.graphics.drawable.Drawable;

public class ConversationInfo 
{
	private Drawable conversationImage;
	
	private String conversationImagePath;
	
	private String conversationName;
	
	private String conversationType;
	
	private String conversationNumber;
	
	private String conversationGroupId;
	
	private String conversationGroupInfo;
	
	
	
	public String getConversationGroupInfo() 
	{
		return conversationGroupInfo;
	}
	public void setConversationGroupInfo(String conversationGroupInfo) {
		this.conversationGroupInfo = conversationGroupInfo;
	}
	public String getConversationImagePath() 
	{
		return conversationImagePath;
	}
	public void setConversationImagePath(String conversationImagePath) {
		this.conversationImagePath = conversationImagePath;
	}
	public Drawable getConversationImage() {
		return conversationImage;
	}
	public void setConversationImage(Drawable conversationImage) {
		this.conversationImage = conversationImage;
	}
	public String getConversationName() {
		return conversationName;
	}
	public void setConversationName(String conversationName) {
		this.conversationName = conversationName;
	}
	public String getConversationType() {
		return conversationType;
	}
	public void setConversationType(String conversationType) {
		this.conversationType = conversationType;
	}
	public String getConversationNumber() {
		return conversationNumber;
	}
	public void setConversationNumber(String conversationNumber) {
		this.conversationNumber = conversationNumber;
	}
	public String getConversationGroupId() {
		return conversationGroupId;
	}
	public void setConversationGroupId(String conversationGroupId) {
		this.conversationGroupId = conversationGroupId;
	}
	
	

}
