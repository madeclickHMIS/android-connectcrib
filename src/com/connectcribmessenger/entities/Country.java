package com.connectcribmessenger.entities;

/**
 * POJO
 *
 */
public class Country 
{
	private String mCode;
	
	private String mName;
	
	private int mCountryFlagIcon;
	
	private int mMobileNumberCountryCode;

	public String getCode() {
		return mCode;
	}

	public void setCode(String code) {
		this.mCode = code;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		this.mName = name;
	}
	
	public int getCountryFlagIcon() {
		return mCountryFlagIcon;
	}

	public void setCountryFlagIcon(int mCountryFlagIcon) {
		this.mCountryFlagIcon = mCountryFlagIcon;
	}

	public int getMobileNumberCountryCode() {
		return mMobileNumberCountryCode;
	}

	public void setMobileNumberCountryCode(int mMobileNumberCountryCode) {
		this.mMobileNumberCountryCode = mMobileNumberCountryCode;
	}

}