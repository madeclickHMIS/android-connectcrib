package com.connectcribmessenger.entities;

public class RecentChat 
{
	private String chatType;

	private String senderName;

	private String senderNumber;
	
	private String pic;
	
	private String message;

	private String messageType;
	
	private String newMessageCount;
	
	private String messageInOutStatus;

	private String time;
	
	private String deliverStatus;
	
	private String groupId;
	
	private String groupName;
	
	private String groupInfo;
	
	private String message_id;
	
	private String seenStatus;
	
	private String archivedStatus;
	
	
	
	public String getArchivedStatus() {
		return archivedStatus;
	}

	public void setArchivedStatus(String archivedStatus) {
		this.archivedStatus = archivedStatus;
	}

	public String getNewMessageCount() {
		return newMessageCount;
	}

	public void setNewMessageCount(String newMessageCount) {
		this.newMessageCount = newMessageCount;
	}

	public String getSeenStatus() {
		return seenStatus;
	}

	public void setSeenStatus(String seenStatus) {
		this.seenStatus = seenStatus;
	}

	public String getMessage_id() {
		return message_id;
	}

	public void setMessage_id(String message_id) {
		this.message_id = message_id;
	}

	public String getGroupInfo() {
		return groupInfo;
	}

	public void setGroupInfo(String groupInfo) {
		this.groupInfo = groupInfo;
	}

	public String getMessageInOutStatus() {
		return messageInOutStatus;
	}

	public void setMessageInOutStatus(String messageInOutStatus) {
		this.messageInOutStatus = messageInOutStatus;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getDeliverStatus() {
		return deliverStatus;
	}

	public void setDeliverStatus(String deliverStatus) {
		this.deliverStatus = deliverStatus;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getChatType() {
		return chatType;
	}

	public void setChatType(String chatType) {
		this.chatType = chatType;
	}


	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSenderNumber() {
		return senderNumber;
	}

	public void setSenderNumber(String senderNumber) {
		this.senderNumber = senderNumber;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}


}
