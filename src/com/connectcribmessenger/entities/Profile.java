package com.connectcribmessenger.entities;

import android.os.Parcel;
import android.os.Parcelable;

public class Profile implements Parcelable
{
	private int mProfileId;

	private String mProfileName;

	private String mProfileNumber;

	private String mProfileStatus;

	private String mProfilePic;

	private String mAvailabilityStatus;

	public Profile()
	{

	}

	public String getmAvailabilityStatus() {
		return mAvailabilityStatus;
	}

	public void setmAvailabilityStatus(String mAvailabilityStatus) {
		this.mAvailabilityStatus = mAvailabilityStatus;
	}

	public void setProfileId(int profileId)
	{
		mProfileId=profileId;
	}

	public int getProfileId()
	{
		return mProfileId;
	}

	public void setProfileName(String profileName)
	{
		mProfileName=profileName;
	}

	public String getProfileName()
	{
		return mProfileName;
	}

	public void setProfileNumber(String profileNumber)
	{
		mProfileNumber=profileNumber;
	}

	public String getProfileNumber()
	{
		return mProfileNumber;
	}

	public void setProfileStatus(String profileStatus)
	{
		mProfileStatus=profileStatus;
	}

	public String getProfileStatus()
	{
		return mProfileStatus;
	}

	public void setProfilePic(String profilePic)
	{
		mProfilePic=profilePic;
	}

	public String getProfilePic()
	{
		return mProfilePic;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) 
	{
		dest.writeInt(mProfileId);
		dest.writeString(mProfileName);
		dest.writeString(mProfileNumber);
		dest.writeString(mProfileStatus);
		dest.writeString(mProfilePic);
		dest.writeString(mAvailabilityStatus);


	}

	public Profile(Parcel in)
	{

		mProfileId=in.readInt();

		mProfileName=in.readString();

		mProfileNumber=in.readString();

		mProfileStatus=in.readString();

		mProfilePic=in.readString();

		mAvailabilityStatus=in.readString();

	}

	public static final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
		public Profile createFromParcel(Parcel in) {
			return new Profile(in); 
		}

		public Profile[] newArray(int size) {
			return new Profile[size];
		}
	};



}
