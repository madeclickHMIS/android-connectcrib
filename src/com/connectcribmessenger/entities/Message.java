package com.connectcribmessenger.entities;

import android.graphics.Bitmap;

public class Message 
{
	private String message;
	
	private String messageSendername;
	
	private String messageType;
	
	private String messageInOutStatus;
	
	private String messageTime;
	
	private String messageId;
	
	private String messageDeliveryStatus;
	
	private String messageSendernumber;
	
	private String messageSeenStatus;
	
	private Bitmap thumbNail;
	
	private String videoSize;
	
	private String videoDuration;
	
	private String videoStatus;
	
	private String videoLink;
	
	private String videoThumbNailLink;
	
	
	
	public String getVideoLink() {
		return videoLink;
	}
	public void setVideoLink(String videoLink) {
		this.videoLink = videoLink;
	}
	public String getVideoThumbNailLink() {
		return videoThumbNailLink;
	}
	public void setVideoThumbNailLink(String videoThumbNailLink) {
		this.videoThumbNailLink = videoThumbNailLink;
	}
	public Bitmap getThumbNail() {
		return thumbNail;
	}
	public void setThumbNail(Bitmap thumbNail) {
		this.thumbNail = thumbNail;
	}
	public String getVideoSize() {
		return videoSize;
	}
	public void setVideoSize(String videoSize) {
		this.videoSize = videoSize;
	}
	public String getVideoDuration() {
		return videoDuration;
	}
	public void setVideoDuration(String videoDuration) {
		this.videoDuration = videoDuration;
	}
	public String getVideoStatus() {
		return videoStatus;
	}
	public void setVideoStatus(String videoStatus) {
		this.videoStatus = videoStatus;
	}
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getMessageSeenStatus() {
		return messageSeenStatus;
	}
	public void setMessageSeenStatus(String messageSeenStatus) {
		this.messageSeenStatus = messageSeenStatus;
	}
	public String getMessageInOutStatus() {
		return messageInOutStatus;
	}
	public void setMessageInOutStatus(String messageInOutStatus) {
		this.messageInOutStatus = messageInOutStatus;
	}
	public String getMessageSendernumber() {
		return messageSendernumber;
	}
	public void setMessageSendernumber(String messageSendernumber) {
		this.messageSendernumber = messageSendernumber;
	}
	public String getMessageSendername() {
		return messageSendername;
	}
	public void setMessageSendername(String messageSendername) {
		this.messageSendername = messageSendername;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getMessageTime() {
		return messageTime;
	}
	public void setMessageTime(String messageTime) {
		this.messageTime = messageTime;
	}
	public String getMessageDeliveryStatus() {
		return messageDeliveryStatus;
	}
	public void setMessageDeliveryStatus(String messageDeliveryStatus) {
		this.messageDeliveryStatus = messageDeliveryStatus;
	}
	public String getMessageSenderNumber() {
		return messageSendernumber;
	}
	public void setMessageSenderNumber(String messageSendernumber) {
		this.messageSendernumber = messageSendernumber;
	}
	
	

}
