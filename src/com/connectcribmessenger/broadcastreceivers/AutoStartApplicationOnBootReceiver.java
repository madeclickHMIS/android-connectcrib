package com.connectcribmessenger.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AutoStartApplicationOnBootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent arg1) {
		try 
		{
			/*Intent startApp=new Intent(context,ApplicationStarter.class);
			startApp.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(startApp);
			Log.i("AutoStartApplicationOnBootReceiver", "Application auto started after boot !!");*/
		} 
		catch 
		(Exception e)
		{
			e.printStackTrace();
			Log.i("AutoStartApplicationOnBootReceiver", "Exception occured in application auto start after boot !!");
		}
		
	}

}
