package com.connectcribmessenger.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.connectcribmessenger.backgroundtasks.InformationSenderAsync;

public class InformationSenderReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) 
	{
		//		InformationSenderThread infosenderThread=new InformationSenderThread(context);
		//		
		//		infosenderThread.start();
		try 
		{
			InformationSenderAsync infosenderAsync=new InformationSenderAsync(context);
			
			infosenderAsync.execute("");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}


	}

}
