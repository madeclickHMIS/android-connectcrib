package com.connectcribmessenger.broadcastreceivers;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.Toast;

import com.connectcribmessenger.services.GcmIntentService;

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) 
	{
		//Toast.makeText(context, "GCM message received", Toast.LENGTH_LONG).show();
		Log.i("GcmBroadcastReceiver", "message Received");
		// Explicitly specify that GcmIntentService will handle the intent.
        ComponentName comp = new ComponentName(context.getPackageName(),
                GcmIntentService.class.getName());
		 /*ComponentName comp = new ComponentName("com.connectcribmessenger.services",
	                GcmIntentService.class.getName());*/
        // Start the service, keeping the device awake while it is launching.
        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);

	}

}
