package com.connectcribmessenger.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.main.AppBuilderClass;


public class IncomingSmsReceiver extends BroadcastReceiver {

	// Get the object of SmsManager
	final SmsManager sms = SmsManager.getDefault();

	private Context mActivityContext;
	

	String senderNum ;

	String message ;

	boolean ontextmsg;

	SharedPreferences prefs;

	SharedPreferences.Editor editor;

	boolean msgOpen;

	private Handler mHandler;

	private AppBuilderClass mAppBuilderClass;

	public IncomingSmsReceiver(Context context,Handler mHandler)
	{
		this.mHandler=mHandler;
		mActivityContext=context;

	}

	public void onReceive(Context context, Intent intent) 
	{


			mAppBuilderClass=new AppBuilderClass(mActivityContext);

			final Bundle bundle = intent.getExtras();

			try {

				if (bundle != null) {

					final Object[] pdusObj = (Object[]) bundle.get("pdus");

					for (int i = 0; i < pdusObj.length; i++) 
					{

						SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);

						String phoneNumber = currentMessage.getDisplayOriginatingAddress();

						senderNum = phoneNumber;

						message = currentMessage.getDisplayMessageBody();

					} 

					GlobalClass.printDebugLog("SmsReceived", "senderNum: "+ senderNum + "; message: " + message);
					
					GlobalClass.printDebugLog("SmsReceived", "senderNum: "+ senderNum + "; parsedmessage: " + mAppBuilderClass.getOTPFromSms(message));
					
					Message msg=new Message();

					Bundle b=new Bundle();
					GlobalClass.printDebugLog("SmsReceived", "senderNum: "+ senderNum + "; parsedmessage: " + mAppBuilderClass.getOTPFromSms(message));
					b.putString("IncomingSmsReceiver", mAppBuilderClass.getOTPFromSms(message));
					GlobalClass.printDebugLog("SmsReceived", "senderNum: "+ senderNum + "; parsedmessage: " + mAppBuilderClass.getOTPFromSms(message));
					msg.setData(b);

					//GlobalClass.VERIFICATIONSCREEN_HANDLER.sendMessage(msg);
					mHandler.sendMessage(msg);
					GlobalClass.printDebugLog("SmsReceived", "senderNum: "+ senderNum + "; parsedmessage: " + mAppBuilderClass.getOTPFromSms(message));
				} 

			} 
			catch (Exception e) 
			{
				Log.e("SmsReceiver", "Exception smsReceiver" +e);

			}
		}


}