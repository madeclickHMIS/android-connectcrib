
/**
 * Copyright statement here.
 */

package com.connectcribmessenger.functionality;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import android.app.Activity;
import android.app.Dialog;
import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.Spannable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AbsListView.LayoutParams;

import com.connectcribmessenger.adapters.ChatAdapter;
import com.connectcribmessenger.adapters.ChatListAdapter;
import com.connectcribmessenger.adapters.RecentChatListAdapter;
import com.connectcribmessenger.auxilliaryclasses.Constants;
import com.connectcribmessenger.auxilliaryclasses.DatabaseAccessor;
import com.connectcribmessenger.auxilliaryclasses.GetValues;
import com.connectcribmessenger.auxilliaryclasses.GlobalClass;
import com.connectcribmessenger.auxilliaryclasses.StoreValues;
import com.connectcribmessenger.backgroundtasks.ConfirmOtp;
import com.connectcribmessenger.backgroundtasks.GcmRegistration;
import com.connectcribmessenger.backgroundtasks.GetCountryCode;
import com.connectcribmessenger.backgroundtasks.SendSmsForOtp;
import com.connectcribmessenger.backgroundtasks.SqliteDataProviderThread;
import com.connectcribmessenger.broadcastreceivers.IncomingSmsReceiver;
import com.connectcribmessenger.customclasses.EmojiconSpan;
import com.connectcribmessenger.dialogs.DialogClass;
import com.connectcribmessenger.entities.Country;
import com.connectcribmessenger.entities.RecentChat;
import com.connectcribmessenger.main.AccountActivationScreen;
import com.connectcribmessenger.main.ArchivedRecentChatListScreen;
import com.connectcribmessenger.main.CreateProfile;
import com.connectcribmessenger.main.R;
import com.connectcribmessenger.main.RecentChatListScreen;
import com.connectcribmessenger.main.VerificationScreen;
import com.example.android.displayingbitmaps.util.ImageCache;
import com.example.android.displayingbitmaps.util.ImageFetcher;
import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * Provides the methods for the AppBuilderClass.
 * It provides all the functionalities to the App via AppBuilderClass.
 */
public class AppFunctionalityProvider 
{
	private Context mContext;

	private FragmentActivity mFragmentActivity;

	private Activity mActivity;

	private GetValues mGetValues;

	private IncomingSmsReceiver mIncomingSmsReceiver;

	private StoreValues mStoreValues;

	private ImageFetcher mImageFetcher;

	private ChatListAdapter chatListAdapter;

	RecentChatListAdapter archivedChatListAdapter;

	private ChatAdapter chatAdapter;

	private ListView listview;

	DatabaseAccessor dbAccessor;

	private IntentService mIntentService;

	//Constructor
	public AppFunctionalityProvider(Context context)
	{
		this.mContext=context;

		//mFragmentActivity=(FragmentActivity) mContext;

		mActivity=(Activity) mContext;

		mGetValues=new GetValues(mContext);

		mStoreValues=new StoreValues(mContext);
	}
	/*
	public AppFunctionalityProvider(IntentService context)
	{
		this.mIntentService=context;


	}*/

	/**
	 * This method performs following tasks:
	 * 
	 * 1.Checks for playservices on the devices 
	 * if the same is available then only further processing will be done.
	 * 
	 * 2.If PlayServices are available then it is checked whether RegistrationId is already available
	 * on the device or not. if Registration Id is already available then a "succes" message is send 
	 * to the handler of SplashScreen.class else a request for GcmRegistration is processed.
	 * 
	 * 3.GcmRegistration performs three major tasks:
	 * 
	 * a.Get Registration Id from GCM server
	 * 
	 * b.Send the Registration Id got from the GCM server to App server.
	 * 
	 * c.After sending the Registration Id to App server successfully, 
	 * it is locally saved in sharedpreference file for further use.
	 * 
	 */
	public void gcmRegisterRegistrationId(Handler handler)
	{
		//Checking for PlayServices.
		if (mGetValues.checkPlayServices()) 
		{

			//Checking if the Regsitration Id is already registred on the server and stored on the device.
			if(mGetValues.isRegistrationIdStored())
			{
				/*
				 * If Registration Id is already registered then
				 * this block of code sends a succes message to the Handler of the SplashScreen.class
				 */


				GlobalClass.REGISTRATION_ID=mGetValues.getRegistrationId();

				GlobalClass.printDebugLog("Already Registered", "Already registered on server-->"+GlobalClass.REGISTRATION_ID);

				Message message=new Message();

				Bundle b=new Bundle();	

				b.putString("status", "success");

				message.setData(b);

				handler.sendMessage(message);



			}
			else
			{
				/**
				 * If Registration Id is not registered yet then
				 * this block of code checks for internet connection and if there is internet connection available then
				 * sends a request to process GCM Registration
				 */

				//Checks for internet connection.
				//Returns true for available internet else false
				if(GlobalClass.isInternetAvailable(mContext))
				{
					//This is an AsynTask class for performing the Registration task
					GcmRegistration gcmRegistration=new GcmRegistration(mContext, handler);

					gcmRegistration.execute(null,null,null);
				}
				else
				{
					//If no internet connection is available then dialog is displayed
					showDialog(Constants.DIALOG_GCM_REGISTRATION_NO_INTERNET,"Internet");


				}

			}
		}
		else
		{
			//If playservices are not available the a log will be printed.
			Log.i("tag", "No valid Google Play Services APK found.");
		}

	}

	/**
	 * Gives the current country of the user.
	 */

	public Country getCurrentCountry()
	{
		return mGetValues.getCountry();
	}

	/**
	 * Gets country code by using the API ""
	 */

	public void getCurrentCountryCode(Handler mHandler)
	{
		if(GlobalClass.isInternetAvailable(mContext))
		{
			//This is an AsynTask class for getting Country Code
			GetCountryCode getCountryCode=new GetCountryCode(mHandler);

			getCountryCode.execute(null,null,null);
		}
		else
		{
			//If no internet connection is available then dialog is displayed
			showDialog(Constants.DIALOG_GET_COUNTRY_CODE_NO_INTERNET,"Internet");


		}

	}

	/**
	 * This method registers the IncomingSmsReceiver dynamically 
	 */

	public void registerIncomingSmsReceiver(Handler mHandler)
	{
		mIncomingSmsReceiver=new IncomingSmsReceiver(mContext,mHandler);
		//mIncomingSmsReceiver=new IncomingSmsReceiver();

		IntentFilter intentFilter = new IntentFilter();

		intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");


		mActivity.registerReceiver(mIncomingSmsReceiver, intentFilter);
	}

	/**
	 * This method unregisters the IncomingSmsReceiver dynamically 
	 */

	public void unregisterIncomingSmsReceiver()
	{
		mActivity.unregisterReceiver(mIncomingSmsReceiver);

	}

	/**
	 * This method registers the BroadcastReceiver dynamically 
	 */

	public void registerbroadcastreceiver(BroadcastReceiver receiver,IntentFilter intentFiler)
	{
		LocalBroadcastManager.getInstance(mContext).registerReceiver(receiver, intentFiler);
	}

	/**
	 * Extracts 4 digit OTP from sms
	 */

	public String getOTPFromSms(String sms)
	{
		return mGetValues.getOTPFromSms(sms);
	}

	/**
	 * Runs a background process that sends a request to server for sending an sms to a specific number
	 * send through api.
	 */

	public void sendSmsForOtp(Handler handler,String number)
	{
		if(GlobalClass.isInternetAvailable(mContext))
		{
			//This is an AsynTask class for sending sms to a given number for getting OTP
			SendSmsForOtp sendSmsForOtp=new SendSmsForOtp(handler,number);

			sendSmsForOtp.execute(null,null,null);

			TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);  //gets the current TelephonyManager
			if (tm.getSimState() != TelephonyManager.SIM_STATE_ABSENT)
			{

			} else 
			{
				Toast.makeText(mContext, "No Sim Card", Toast.LENGTH_LONG).show();
				DisplayMetrics displayMetrics = new DisplayMetrics();
				((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
				int dialog_height = displayMetrics.heightPixels;
				int dialog_width = displayMetrics.widthPixels;
				final Dialog myDialog = new Dialog(mContext);
				//myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

				myDialog.setTitle("ConnectCrib");

				myDialog.setContentView(R.layout.dialog_no_sim_card);
				myDialog.getWindow().setGravity(Gravity.CENTER);
				myDialog.getWindow().setLayout((dialog_width*75)/100, LayoutParams.WRAP_CONTENT);
				//myDialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);

				Button verify=(Button)myDialog.findViewById(R.id.verify);

				Button cancel=(Button)myDialog.findViewById(R.id.cancel);

				final EditText otpEditText=(EditText)myDialog.findViewById(R.id.otp_edittext);


				//TextView message_text=(TextView)myDialog.findViewById(R.id.textView1);
				//message_text.setText(message);

				verify.setOnClickListener(new OnClickListener() 
				{									
					public void onClick(View v) 
					{
						if(otpEditText.getText().toString().length()>0)
						{
							GlobalClass.OTP=otpEditText.getText().toString();

							VerificationScreen.confirmOtp();

							myDialog.dismiss();
						}

						else
						{
							Toast.makeText(mContext, "Please enter OTP", Toast.LENGTH_LONG).show();
						}

					}
				});

				cancel.setOnClickListener(new OnClickListener() 
				{									
					public void onClick(View v) 
					{
						myDialog.dismiss();

					}
				});



				myDialog.show();

			}
		}
		else
		{
			//If no internet connection is available then dialog is displayed
			showDialog(Constants.DIALOG_SEND_SMS__NO_INTERNET,"Internet");


		}

	}

	/**
	 * Runs a background process that sends a request to server for confirming the received OTP
	 * send through api.
	 */

	public void confirmOtp(Handler handler,String otp,String number)
	{
		if(GlobalClass.isInternetAvailable(mContext))
		{
			//This is an AsynTask class for sending sms to a given number for getting OTP
			ConfirmOtp confirmOtp=new ConfirmOtp(handler, otp,number);

			confirmOtp.execute(null,null,null);
		}
		else
		{
			//If no internet connection is available then dialog is displayed

			showDialog(Constants.DIALOG_CONFIRM_OTP__NO_INTERNET,"Internet");


		}

	}


	/**
	 * This method gives the mobile number
	 */

	public String getMobileNumber()
	{

		return mGetValues.getMobileNumber();

	}

	/**
	 * set terms condition textview text
	 */

	public void setTextToTermsAndConditions(TextView view)
	{

		CharSequence sequence = Html.fromHtml("<body><b><a href=\"\">Terms and Conditions</a></b></body>");

		view.setText(sequence);

	}

	/**
	 * set text into EditText
	 */

	public void setTextToNumberEditText(EditText view)
	{

		String mobileNumber=mGetValues.getMobileNumber();

		if(mobileNumber!=null)
		{
			view.setText(mobileNumber);
		}

	}

	/**
	 * store true in sharedpreference if App is already registered and number is verified
	 */

	public void storeAppFirstTimeOpenedStatus(boolean val)
	{
		mStoreValues.storeAppFirstTimeOpenedStatus(val);

	}

	/**
	 * when all the conditions are met app navigates from SplashScreen
	 */

	public void navigateFromSplash(String onhandleMessage,long startTime)
	{

		long endTime=System.currentTimeMillis();

		GlobalClass.printDebugLog("Splash_Time_Out", "Splash_Time_Out-->"+Constants.SPLASH_TIME_OUT);

		if(onhandleMessage.equals("success"))
		{
			GlobalClass.printDebugLog("Splash handler", "Regsitration Successful");

			GlobalClass.printDebugLog("Splash handler", "Time elapsed in registration-->"+(endTime-startTime));

			if((endTime-startTime)<Constants.SPLASH_TIME_OUT)
			{
				GlobalClass.printDebugLog("Splash handler", "Regsitration registration execution time is less than SPLASH_TIME_OUT-->"+(endTime-startTime));
				new Handler().postDelayed(new Runnable() 
				{

					@Override
					public void run() 
					{
						navigateFromSplashToEitherScreen();
					}
				}, Constants.SPLASH_TIME_OUT-(endTime-startTime));	
			}
			else
			{
				GlobalClass.printDebugLog("Splash handler", "Regsitration registration execution time is greater than SPLASH_TIME_OUT-->"+(endTime-startTime));

				navigateFromSplashToEitherScreen();
			}

		}
		else if(onhandleMessage.equals("failed"))
		{
			GlobalClass.printDebugLog("Splash handler", "Regsitration Failed");

			showDialog(Constants.DIALOG_GCM_REGISTRATION_FAILED,"Registration");

		}
		else
		{
			GlobalClass.printDebugLog("Splash handler", "Regsitration Failed due to no network");

			showDialog(Constants.DIALOG_GCM_REGISTRATION_NO_INTERNET,"Internet");
			/*DialogFragment dialog=new DialogClass(mFragmentActivity, Constants.DIALOG_GCM_REGISTRATION_NO_INTERNET);

			dialog.show(mFragmentActivity.getSupportFragmentManager(), "Internet");*/
		}

	}
	/**
	 * when all the conditions are met app navigates from SplashScreen to AccountActivationScreen if App 
	 * has opened for the first time
	 * If App has already opened once it should navigate to RecentChatListScreen
	 */

	public void navigateFromSplashToEitherScreen()
	{
		if(mGetValues.getAppFirstTimeOpenedStatus())
		{
			//Intent i = new Intent(mActivity, AccountActivationScreen.class);
			Intent i = new Intent(mActivity, AccountActivationScreen.class);
			mActivity.startActivity(i);
			mActivity.finish();	
		}
		else
		{
			Intent i = new Intent(mActivity, RecentChatListScreen.class);
			mActivity.startActivity(i);
			mActivity.finish();	
		}

	}

	/**
	 * Starts the banner and dot animation in splash screen
	 */

	public void startSplashScreenAnimation(ImageView bannerImageView,ImageView dotImageView)
	{
		startSplashScreenBannnerAnimation(bannerImageView);
		startSplashScreenDotAnimation(dotImageView);

	}

	/**
	 * Starts the banner animation in splash screen
	 */

	public void startSplashScreenBannnerAnimation(ImageView bannerImageView)
	{
		bannerImageView.setBackgroundResource(R.drawable.splash_anim);
		AnimationDrawable frameAnimation = (AnimationDrawable) bannerImageView.getBackground();
		frameAnimation.start();

	}

	/**
	 * Starts the banner and dot animation in splash screen
	 */

	public void startSplashScreenDotAnimation(ImageView dotImageView)
	{

		dotImageView.setBackgroundResource(R.drawable.progress_anim);
		AnimationDrawable frameAnimation = (AnimationDrawable) dotImageView.getBackground();
		frameAnimation.start();
	}

	/**
	 * navigates from AccountActivationScreen To VerificationScreen
	 */
	public void navigateToVerificationScreen(EditText numberEditText)
	{
		String number=numberEditText.getText().toString();

		if(number!=null&&number.length()!=0)
		{
			//Toast.makeText(AccountActivationScreen.this, "Height-->"+next.getHeight()+" and width-->"+next.getWidth(), Toast.LENGTH_LONG).show();
			Intent i=new Intent(mActivity,VerificationScreen.class);

			i.putExtra("number", GlobalClass.INTERNATIONAL_MOBILE_COUNTRY_CODE+number);

			mActivity.startActivity(i);
			mActivity.finish();	
		}
		else
		{
			numberEditText.setError("Field Can Not Be Empty.");
			numberEditText.requestFocus();
		}
	}

	public void setFlagCountryNameCountryCode(String countryCode,ImageView flagImageView,TextView countryNameTextView,TextView countryCodeTextView)
	{

		if(!countryCode.equals("noval"))
		{
			GlobalClass.COUNTRY_CODE=countryCode;

			Country country=getCurrentCountry();

			countryNameTextView.setText(country.getName());

			countryCodeTextView.setText("+"+country.getMobileNumberCountryCode());

			GlobalClass.INTERNATIONAL_MOBILE_COUNTRY_CODE=countryCodeTextView.getText().toString();

			flagImageView.setImageResource(country.getCountryFlagIcon());
		}
		else
		{
			showDialog(Constants.DIALOG_GET_COUNTRY_CODE_FAILED,"failed");

		}
	}

	public void verifyContactNumber(String number,String sendSmsForOtp,String incomingSmsReceiver,String ConfirmOtp)
	{
		if(sendSmsForOtp!=null)
		{
			GlobalClass.printDebugLog("SendSmsForOtp", "SendSmsForOtp Response-->"+sendSmsForOtp);
			if(!sendSmsForOtp.equals("1"))
			{
				showDialog(Constants.DIALOG_SEND_SMS__FAILED,"failed");

			}

		}

		if(incomingSmsReceiver!=null)
		{
			GlobalClass.printDebugLog("VerificationScreen", "OTP Received from receiver-->"+incomingSmsReceiver);
			GlobalClass.OTP=incomingSmsReceiver;

			VerificationScreen.confirmOtp();

		}

		if(ConfirmOtp!=null)
		{
			if(ConfirmOtp.equals("1"))
			{
				Intent intent=new Intent(mActivity,CreateProfile.class);

				GlobalClass.printDebugLog("number", "AppFunctionalityScreen-->"+number);

				intent.putExtra("number",number);

				mActivity.startActivity(intent);

				mActivity.finish();
				/*Intent i = new Intent(mActivity,WelcomScreen.class);
				mActivity.startActivity(i);
				mActivity.finish();*/
			}
			else
			{
				showDialog(Constants.DIALOG_CONFIRM_OTP__FAILED,"failed");

			}
		}

	}

	public void showDialog(int dialog_case,String tag)
	{
		try 
		{

			FragmentTransaction ft = mFragmentActivity.getSupportFragmentManager().beginTransaction();

			Fragment prev = mFragmentActivity.getSupportFragmentManager().findFragmentByTag(tag);

			if (prev != null) 
			{
				ft.remove(prev);
			}

			ft.addToBackStack(null);

			// Create and show the dialog.
			DialogFragment dialog = DialogClass.newInstance(dialog_case);

			dialog.show(ft, tag);



		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

	}

	public void getProfileDataFromDatabase(Handler handler)
	{
		HashMap<String,String> insertValues=new HashMap<>();

		insertValues.put("param", "profiledata");

		SqliteDataProviderThread sqliteThread=new SqliteDataProviderThread(mActivity, handler,insertValues);

		sqliteThread.execute();
	}

	public void getRecentChatScreenListData(Handler handler)
	{
		HashMap<String,String> insertValues=new HashMap<>();

		insertValues.put("param", "recent_chats");

		SqliteDataProviderThread sqliteThread=new SqliteDataProviderThread(mActivity,handler, insertValues);

		sqliteThread.execute();
	}

	//Used in RecentChatList.java
	/*public void displayRecentChatsInList(String msg,Handler handler,ListView list,Button archiveCountBtn)
	{
		if(msg.equals("profileData"))
		{
			HashMap<String,String> insertValues=new HashMap<>();

			insertValues.put("param", "recent_chats");

			SqliteDataProviderThread sqliteThread=new SqliteDataProviderThread(mActivity,handler, insertValues);

			sqliteThread.execute();
		}
		else if(msg.equals("recent_chats")||msg.equals("get_latest_recent_chat_list"))
		{
			createImageFetcherFromNetwork();
			//getArchivedRecentchatMessages();
			showArchiveCountBtn(archiveCountBtn);
			chatListAdapter=new ChatListAdapter(mActivity, GlobalClass.recentMessageList,mImageFetcher);

			list.setAdapter(chatListAdapter);
		}
	}
	 */
	//Used in RecentChatList.java
	public void displayArchivedRecentChatsInList(String msg,Handler handler,ListView list)
	{
		createImageFetcherFromNetwork();

		archivedChatListAdapter=new RecentChatListAdapter(mActivity, GlobalClass.recentArchivedMessageList,mImageFetcher);

		list.setAdapter(archivedChatListAdapter);
	}

	//used in RecentChatListScreen
	public void showArchiveCountBtn(Button archiveCountBtn)
	{
		if(GlobalClass.recentArchivedMessageList!=null)
		{
			if(GlobalClass.recentArchivedMessageList.size()>0)
			{
				if(archiveCountBtn!=null)
				{
					archiveCountBtn.setVisibility(View.VISIBLE);
				}

				archiveCountBtn.setText(""+GlobalClass.recentArchivedMessageList.size()+" Archives");

				archiveCountBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent=new Intent(mActivity,ArchivedRecentChatListScreen.class);

						mActivity.startActivity(intent);

					}
				});
			}
			else
			{
				if(archiveCountBtn!=null)
				{
					archiveCountBtn.setVisibility(View.GONE);
				}

			}

		}

	}

	public void getArchivedRecentchatMessages()
	{
		ArrayList<RecentChat> archivedRecentChatList=new ArrayList<>();

		for(int i=0;i<GlobalClass.recentMessageList.size();i++)
		{
			if(GlobalClass.recentMessageList.get(i).getArchivedStatus().equals(Constants.MESSAGE_ARCHIVED_STATUS_YES))
			{
				archivedRecentChatList.add(GlobalClass.recentMessageList.get(i));
			}
		}

		GlobalClass.recentArchivedMessageList=archivedRecentChatList;
	}
	//Used in RecentChatListScreen.java
	public void callOnItemClickRecentChatList(int position)
	{

		chatListAdapter.onItemClick(position, chatListAdapter.getImageView(position));
	}
	public void callOnItemClickArchivedRecentChatList(int position)
	{
		archivedChatListAdapter.onItemClick(position, chatListAdapter.getImageView(position));
	}

	public void unArchive(View rowView, final int position) {

		final Animation animation = AnimationUtils.loadAnimation(mActivity,android.R.anim.slide_out_right); 
		animation.setDuration(800);
		rowView.startAnimation(animation);

		/*		animation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub

			}
		});*/
		Handler handle = new Handler();
		handle.postDelayed(new Runnable() 
		{

			@Override
			public void run() {
				dbAccessor=new DatabaseAccessor(mActivity);

				if(GlobalClass.recentMessageList.get(position).getChatType().equals(Constants.CHAT_TYPE_PEER_TO_PEER))
				{
					dbAccessor.updatePeerToPeerArchivedStatus(GlobalClass.recentArchivedMessageList.get(position).getSenderNumber(), Constants.MESSAGE_ARCHIVED_STATUS_NO);

				}
				else
				{
					dbAccessor.updateGroupArchivedStatus(GlobalClass.recentArchivedMessageList.get(position).getGroupInfo(), Constants.MESSAGE_ARCHIVED_STATUS_NO);
				}

				if(GlobalClass.recentArchivedMessageList!=null)
				{
					GlobalClass.recentArchivedMessageList.remove(position);
				}




				Toast.makeText(mActivity, "Conversation has been archived", Toast.LENGTH_LONG).show();




				archivedChatListAdapter.notifyDataSetChanged();
				animation.cancel();
			}
		},1000);



	}
	public void removeListItem(View rowView, final int position,final Button archiveCountBtn) {

		final Animation animation = AnimationUtils.loadAnimation(mActivity,android.R.anim.slide_out_right); 
		animation.setDuration(800);
		rowView.startAnimation(animation);
		Handler handle = new Handler();
		handle.postDelayed(new Runnable() {

			@Override
			public void run() {
				dbAccessor=new DatabaseAccessor(mActivity);

				if(GlobalClass.recentMessageList.get(position).getChatType().equals(Constants.CHAT_TYPE_PEER_TO_PEER))
				{
					dbAccessor.updatePeerToPeerArchivedStatus(GlobalClass.recentMessageList.get(position).getSenderNumber(), Constants.MESSAGE_ARCHIVED_STATUS_YES);

				}
				else
				{
					dbAccessor.updateGroupArchivedStatus(GlobalClass.recentMessageList.get(position).getGroupInfo(), Constants.MESSAGE_ARCHIVED_STATUS_YES);
				}

				if(GlobalClass.recentArchivedMessageList!=null)
				{
					GlobalClass.recentArchivedMessageList.add(GlobalClass.recentMessageList.get(position));
				}
				else
				{
					ArrayList<RecentChat> archivedChat=new ArrayList<>();

					archivedChat.add(GlobalClass.recentMessageList.get(position));

					GlobalClass.recentArchivedMessageList=archivedChat;
				}

				showArchiveCountBtn(archiveCountBtn);

				Toast.makeText(mActivity, "Conversation has been archived", Toast.LENGTH_LONG).show();

				GlobalClass.recentMessageList.remove(position);


				chatListAdapter.notifyDataSetChanged();
				animation.cancel();
			}
		},1000);



	}
	//Used in GcmIntentService

	public void getLatestRecentListAndUpdateList(Handler handler)
	{
		HashMap<String,String> insertValues=new HashMap<>();

		insertValues.put("param", "get_latest_recent_chat_list");

		SqliteDataProviderThread sqliteThread=new SqliteDataProviderThread(mActivity,handler, insertValues);

		sqliteThread.execute();
	}


	//Methode to get the list of the recent chats

	public  ArrayList<RecentChat> getRecentChatList(ArrayList<RecentChat> totalMessageList)
	{
		ArrayList<RecentChat> recentChatList=new ArrayList<>();

		int arrayLength=totalMessageList.size();



		Log.i("GlobalClass", "Size of the recent chatlist-->"+arrayLength);

		for(int i=0;i<arrayLength;i++)
		{
			boolean flag = false;
			String number=null;
			String chatType=null;
			String groupId=null;


			number=totalMessageList.get(i).getSenderNumber();
			chatType=totalMessageList.get(i).getChatType();
			groupId=totalMessageList.get(i).getGroupId();

			Log.i("GlobalClass", "number for loop "+i+"-->"+number);

			if(recentChatList.isEmpty())
			{

				Log.i("GlobalClass", "recentchat added when list empty ");
				recentChatList.add(totalMessageList.get(i));

			}
			else
			{
				Log.i("GlobalClass", "List not empty");
				for(int j=0;j<recentChatList.size();j++)
				{
					if(chatType.equals(Constants.CHAT_TYPE_PEER_TO_PEER))
					{
						if(number.equals(recentChatList.get(j).getSenderNumber()))
						{
							flag=true;
						}
					}
					else
					{
						if(recentChatList.get(j).getGroupId().equals(groupId))
						{
							flag=true;
						}
					}


				}

				if(flag==false)
				{
					Log.i("GlobalClass", "recentchat added for loop "+i);
					recentChatList.add(totalMessageList.get(i));
				}
			}
		}

		return getUnArchivedRecentChatList(recentChatList);
	}
	public ArrayList<RecentChat> getUnArchivedRecentChatList(ArrayList<RecentChat> recentChat)
	{
		ArrayList<RecentChat> unArchivedRecentChats=new ArrayList<>();
		ArrayList<RecentChat> archivedRecentChats=new ArrayList<>();

		for(int i=0;i<recentChat.size();i++)
		{
			if(recentChat.get(i).getArchivedStatus().equals(Constants.MESSAGE_ARCHIVED_STATUS_NO))
			{
				unArchivedRecentChats.add(recentChat.get(i));
			}
			else
			{
				archivedRecentChats.add(recentChat.get(i));
			}
		}
		GlobalClass.recentArchivedMessageList=archivedRecentChats;
		return unArchivedRecentChats;
	}
	/*//Method to get presentable time
	public String getTimeToDisplay(String timeinmillis)
	{
		String timeToDisplay=null;

		Calendar c=Calendar.getInstance();

		int currentDay=c.get(Calendar.DATE);

		int currentMonth=c.get(Calendar.MONTH)+1;

		int currentYear=c.get(Calendar.YEAR);

		String currentDate=""+currentDay+"/"+currentMonth+"/"+currentYear;

		c.setTimeInMillis(Long.parseLong(timeinmillis));

		int previousDay=c.get(Calendar.DATE);

		int previousMonth=c.get(Calendar.MONTH)+1;

		int previousYear=c.get(Calendar.YEAR);

		String previousDate=""+previousDay+"/"+previousMonth+"/"+previousYear;

		if(currentDate.equals(previousDate))
		{
			String hour=null;
			String min=null;
			if(String.valueOf(c.get(Calendar.MINUTE)).length()==2)
			{
				timeToDisplay=""+c.get(Calendar.HOUR)+":"+c.get(Calendar.MINUTE);
			}
			else
			{
				timeToDisplay=""+c.get(Calendar.HOUR)+":0"+c.get(Calendar.MINUTE);
			}

			if(String.valueOf(c.get(Calendar.MINUTE)).length()<2)
			{
				min="0"+c.get(Calendar.MINUTE);
				//timeToDisplay=""+c.get(Calendar.HOUR)+":"+c.get(Calendar.MINUTE);
			}

			if(String.valueOf(c.get(Calendar.HOUR)).length()<2)
			{
				hour="0"+c.get(Calendar.HOUR);	
			}
			
			else
			{
				timeToDisplay=""+c.get(Calendar.HOUR)+":0"+c.get(Calendar.MINUTE);
			}
			timeToDisplay=hour+":"+min;

		}
		else if((currentDay-previousDay)==1&&(currentMonth==previousMonth)&&(currentYear==previousYear))
		{
			timeToDisplay="Yesterday";
		}
		else
		{
			timeToDisplay=previousDate;
		}
		return timeToDisplay;
	}*/

	public void fetchConversation(Handler handler)
	{
		String chatType;

		String senderNumber;

		String groupId;

		if(GlobalClass.conversationInfo!=null)
		{
			chatType=GlobalClass.conversationInfo.getConversationType();

			senderNumber=GlobalClass.conversationInfo.getConversationNumber();

			groupId=GlobalClass.conversationInfo.getConversationGroupId();

		}
		else
		{

			chatType=mActivity.getSharedPreferences(Constants.SHAREDPREFERENCE_FILE, Activity.MODE_PRIVATE).getString(Constants.SHAREDPREFERENCE_KEY_CHAT_TYPE, "noval");

			senderNumber=mActivity.getSharedPreferences(Constants.SHAREDPREFERENCE_FILE, Activity.MODE_PRIVATE).getString(Constants.SHAREDPREFERENCE_KEY_SENDER_NUMBER, "noval");

			groupId=mActivity.getSharedPreferences(Constants.SHAREDPREFERENCE_FILE, Activity.MODE_PRIVATE).getString(Constants.SHAREDPREFERENCE_KEY_GROUP_ID, "noval");

		}

		if(!chatType.equals("noval")&&!senderNumber.equals("noval")&&!groupId.equals("noval"))
		{
			HashMap<String,String> insertValues=new HashMap<>();

			insertValues.put(Constants.HASHMAP_KEY_PARAM, Constants.HASHMAP_VALUE_CHAT_SCREEN);

			insertValues.put(Constants.HASHMAP_KEY_CHAT_TYPE, chatType);

			insertValues.put(Constants.HASHMAP_KEY_SENDER_NUMBER, senderNumber);

			insertValues.put(Constants.HASHMAP_KEY_GROUP_ID, groupId);


			SqliteDataProviderThread sqliteThread=new SqliteDataProviderThread(mActivity,handler, insertValues);

			sqliteThread.execute();
		}


	}

	public void createImageFetcherFromNetwork()
	{

		int mImageThumbWidth = mActivity.getResources().getDimensionPixelSize(R.dimen.list_icon_width);

		int mImageThumbHeight = mActivity.getResources().getDimensionPixelSize(R.dimen.list_icon_height);

		ImageCache.ImageCacheParams cacheParams =
				new ImageCache.ImageCacheParams(mActivity, Constants.IMAGE_CACHE_DIR);

		cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

		// The ImageFetcher takes care of loading images into our ImageView children asynchronously
		mImageFetcher = new ImageFetcher(mActivity,mImageThumbWidth,mImageThumbHeight);
		mImageFetcher.setLoadingImage(R.drawable.empty_photo);
		mImageFetcher.addImageCache(((FragmentActivity) mActivity).getSupportFragmentManager(), cacheParams);



	}

	public void activiyCallbacks(String activityName,String callBackMethodName)
	{
		if((activityName.equals(Constants.ACTIVITY_RECENT_CHAT_LIST_SCREEN)||activityName.equals(Constants.ACTIVITY_ARCHIVED_RECENT_CHAT_LIST_SCREEN))&&callBackMethodName.equals(Constants.ONRESUME))
		{
			mImageFetcher.setExitTasksEarly(false);
			chatListAdapter.notifyDataSetChanged();
		}
		else if((activityName.equals(Constants.ACTIVITY_RECENT_CHAT_LIST_SCREEN)||activityName.equals(Constants.ACTIVITY_ARCHIVED_RECENT_CHAT_LIST_SCREEN))&&callBackMethodName.equals(Constants.ONPAUSE))
		{
			mImageFetcher.setPauseWork(false);
			mImageFetcher.setExitTasksEarly(true);
			mImageFetcher.flushCache();
		}
		else if((activityName.equals(Constants.ACTIVITY_RECENT_CHAT_LIST_SCREEN)||activityName.equals(Constants.ACTIVITY_ARCHIVED_RECENT_CHAT_LIST_SCREEN))&&callBackMethodName.equals(Constants.ONDESTROY))
		{
			mImageFetcher.closeCache();
		}
		else if((activityName.equals(Constants.ACTIVITY_RECENT_CHAT_LIST_SCREEN)||activityName.equals(Constants.ACTIVITY_ARCHIVED_RECENT_CHAT_LIST_SCREEN))&&callBackMethodName.equals(Constants.ONSCROLLSTATECHANGED_STATE_TRUE))
		{
			mImageFetcher.setPauseWork(true);
		}
		else if((activityName.equals(Constants.ACTIVITY_RECENT_CHAT_LIST_SCREEN)||activityName.equals(Constants.ACTIVITY_ARCHIVED_RECENT_CHAT_LIST_SCREEN))&&callBackMethodName.equals(Constants.ONSCROLLSTATECHANGED_STATE_FALSE))
		{
			mImageFetcher.setPauseWork(false);
		}
	}

	public void saveAndUpdateMessage(String message,String messageType,String msgId,String seenStatus)
	{
		DatabaseAccessor dbAccessor=new DatabaseAccessor(mActivity);

		String time=String.valueOf(Calendar.getInstance().getTimeInMillis());

		if(GlobalClass.conversationInfo.getConversationType().equals("peer2peer"))
		{
			dbAccessor.insertPeer2PeerMessage(GlobalClass.MessageList.get(0).getMessageSenderNumber(), message, messageType, time, Constants.MESSAGE_DELIVERY_STATUS_NO, "outgoing", GlobalClass.conversationInfo.getConversationImagePath(), GlobalClass.MessageList.get(0).getMessageSendername(),msgId,seenStatus,Constants.MESSAGE_ARCHIVED_STATUS_NO);

			chatAdapter.updateList(message, Constants.MESSAGE_DELIVERY_STATUS_NO, GlobalClass.MessageList.get(0).getMessageSendername(), GlobalClass.MessageList.get(0).getMessageSenderNumber(), time, messageType,"outgoing",listview,seenStatus);

		}
		else
		{
			dbAccessor.insertGroupMessage(GlobalClass.profileData.getProfileNumber(), GlobalClass.profileData.getProfileName(), message, messageType, time, Constants.MESSAGE_DELIVERY_STATUS_NO, "outgoing", GlobalClass.conversationInfo.getConversationGroupInfo(),msgId,seenStatus,Constants.MESSAGE_ARCHIVED_STATUS_NO);
			chatAdapter.updateList(message, Constants.MESSAGE_DELIVERY_STATUS_NO, GlobalClass.profileData.getProfileName(), GlobalClass.profileData.getProfileNumber(), time, messageType,"outgoing",listview,seenStatus);
		}


	}

	//Used in ChatListAdapter
	public void setDrawableInText(TextView view, String fulltext,int resourceId,int start,int end,int size) 
	{
		view.setText(fulltext, TextView.BufferType.SPANNABLE);

		Spannable str = (Spannable) view.getText();

		str.setSpan(new EmojiconSpan(mActivity, resourceId, size), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

	}

	//Used in ChatScreen.java

	public void hideSoftKeyboard(EditText editText)
	{
		InputMethodManager imm = (InputMethodManager)mActivity.getSystemService(
				Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
	}

	/**
	 * Upstream a GCM message up to the 3rd party server
	 * @param message
	 */
	public void sendCCSMessage(String message,String messageType,String chatType,String pic,String groupInfo,String msgId)
	{
		final GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(mActivity);



		String time=String.valueOf(Calendar.getInstance().getTimeInMillis());

		if(mGetValues.getRegistrationId() == null || mGetValues.getRegistrationId().equals(""))
		{
			Toast.makeText(mActivity, "You must register first", Toast.LENGTH_LONG).show();
			return;
		}

		new AsyncTask<String, Void, String>()
		{
			@Override
			protected String doInBackground(String... params)
			{
				String msg = "";
				try
				{
					Bundle data = new Bundle();
					data.putString("message", params[0]);
					if(params[1].equals("peer2peer"))
					{

						data.putString("chat_type", "peer2peer");

						data.putString("sender_name", GlobalClass.profileData.getProfileName());

						data.putString("sender_number", GlobalClass.profileData.getProfileNumber());

						data.putString("message", params[0]);

						data.putString("message_type", params[2]);

						data.putString("message_time", params[3]);

						data.putString("sender_pic", params[4]);
					}
					else if(params[1].equals("group"))
					{
						data.putString("chat_type", "group");

						data.putString("sender_name", GlobalClass.profileData.getProfileName());

						data.putString("sender_number", GlobalClass.profileData.getProfileNumber());

						data.putString("message", params[0]);

						data.putString("message_type", params[2]);

						data.putString("message_time", params[3]);

						data.putString("group_info", params[5]);
					}

					saveAndUpdateMessage(params[0],params[2],params[6],Constants.MESSAGE_UNSEEN);
					gcm.send(Constants.GCM_CCS_SENDER_ID + "@gcm.googleapis.com", params[6], Constants.GCM_TIME_TO_LIVE, data);
					//GlobalClass.MESSAGE_DELIVERY_STATUS="sending";

					//mStoreValues.storeMessageDeliveryStatus(params[6], "sending");

					GlobalClass.MESSAGE_TYPE=params[2];

					GlobalClass.MESSAGE=params[0];
				}
				catch (IOException ex)
				{
					msg = "Error :" + ex.getMessage();
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg)
			{
				Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
			}
		}.execute(message, chatType,messageType,time,pic,groupInfo,msgId);
	}

	//Used in ChatList adapter
	public  void updateRecentChat(String chatType,String messageId,String groupId,String delivery_status)
	{
		for(int i=0;i<GlobalClass.recentMessageList.size();i++)
		{
			if(GlobalClass.recentMessageList.get(i).getChatType().equals(Constants.CHAT_TYPE_PEER_TO_PEER)&&GlobalClass.recentMessageList.get(i).getMessage_id().equals(messageId)||GlobalClass.recentMessageList.get(i).getChatType().equals(Constants.CHAT_TYPE_GROUP)&&GlobalClass.recentMessageList.get(i).getGroupId().equals(groupId)&&GlobalClass.recentMessageList.get(i).getMessage_id().equals(messageId))
			{
				GlobalClass.recentMessageList.get(i).setDeliverStatus(delivery_status);


			}

		}

	}


}
